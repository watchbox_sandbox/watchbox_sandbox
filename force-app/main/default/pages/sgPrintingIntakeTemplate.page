<apex:page showHeader="false" 
           sidebar="false" 
           controller="sgPrintingIntakeController"
           standardStylesheets="false"
           applyBodyTag="false"
           applyHtmlTag="false"
           docType="html-5.0"
           renderAs="PDF">
    <html>
        <head>
            <style>
                @page{
                	margin:0.25in;
                	size:letter;
                }
                body {
                    margin:0px;
                    padding:0px;
                    font-family:Arial, Helvetica, sans-serif;
                    font-size:10pt;
                }
                table {
                	border-spacing: 0px;
                	border-collapse: separate;
                	border: 0px;
                }
                .template-header {
                	margin-bottom: 20px;
                }
                .template-header .content {
                	width:250px;
                }
                .shim {
                	height:2px;
                }
                .shim-lg {
                	height:10px;
                }
                .shim-xlg {
                	height:20px;
                }
                .shim-xxlg {
                	height:40px;
                }
                .body-title {
                    padding: 5px;
                    background-color: #0673a5;
                    color: #ffffff;
                    margin-bottom: 10px;
                }
                .grid-header {
                	background-color: #c9ecfc;
                }
                .grid-header > th {
                	padding: 5px;
                	border: 0px;
                	text-align:center;
                }
                .grid-row > td {
                	padding:5px;
                	text-align:center;
                }
                .inventory-grid-title {
                	font-weight:bold;
                	margin-bottom:10px;
                	margin-top:20px;
                	color: #999999;
                }
                .icon {
                	    background-repeat: no-repeat;
                        position: relative;
                		width:18px;
                        height: 18px;
                		left:50%;
                		margin-left:-9px;
                }
                .checked {
                	background-image: url('https://s3.amazonaws.com/salesforceproduction/sgresources/images/checked_checkbox-16.png');
                }
                .unchecked {
                	background-image: url('https://s3.amazonaws.com/salesforceproduction/sgresources/images/unchecked_checkbox-16.png');
                }
            </style>
        </head>
    <body>
        <!-- Template Header -->
        <table class="template-header" width="100%" border="0">
            <tr>
                <td>
                    <img src="{!Logo}" alt="{!LogoAlt}" />
                </td>
                <td class="content">
                    <table width="100%" border="0">
                        <tr>
                            <td class="heading" colspan="2"><b>INTAKE RECEIPT</b></td>
                        </tr>
                        <tr><td class="shim-lg" colspan="2"></td></tr>
                        <tr>
                            <td class="label">RECEIPT NUMBER:</td>
                            <td class="value">{!Intake.Name}</td>
                        </tr>
                        <tr>
                            <td class="label">CREATED DATE:</td>
                            <td class="value"><apex:outputText value="{0, date, MMMM d','  yyyy}"><apex:param value="{!Intake.CreatedDate}" /></apex:outputText></td>
                        </tr>
                        <tr>
                            <td class="label">ASSOCIATE:</td>
                            <td class="value">{!Intake.Owner.Name}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- Template Body -->
        <div class="template-body">
        	<div class="body-title">LANDED WATCH RECEIPT</div>
            <div class="shim-xlg"></div>
            <!-- Inventory Grid -->
        	<div class="inventory-grid-container">
                <table width="100%" border="0" cellspacing="0">
                    <thead>
                        <tr class="grid-header">
                            <th>Inventory</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Reference</th>
                            <th>Serial</th>
                            <th>Year</th>
                            <th>Box</th>
                            <th>Paper</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="grid-row">
                            <td>{!Intake.Product__r.Inventory_Id__c}</td>
                            <td>{!Intake.Brand__c}</td>
                            <td>{!Intake.Watch_Model__r.Name}</td>
                            <td>{!Intake.Reference__c}</td>
                            <td>{!Intake.Watch_Serial__c}</td>
                            <td>{!Intake.Watch_Year__c}</td>
                            <td><div class="icon {!IF(Intake.Boxes__c, 'checked', 'unchecked')}"></div></td>
                            <td><div class="icon {!IF(Intake.Papers__c, 'checked', 'unchecked')}"></div></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
        	</div>
        </div>
    </body>
    </html>
</apex:page>