({
    init : function(component, event, helper) {
        debugger;
        console.log('>>>',window.location.href);
        //  alert(component.get("v.showSpinner"));
        
        // alert(component.get("v.recordId"));
        // helper.getDealName(component,event);
        helper.getPicklistValuesForSaleBrand(component,event);
        helper.getPicklistValuesForClosingPotential(component,event);
        helper.getPicklistValuesForBoxPaper(component,event);
        helper.getPicklistValuesForSellingBelowMSP(component,event);
        helper.getPicklistValuesForReasonFoeSelling(component,event);
        component.set('v.showSpinner', false);
        //URL 
        var value = helper.getParameterByName(component , event, 'variable');
        // Set the value, assumes you have created attribute "attributename" in your markup
        //alert(value);
        //  component.set("attributename", value);
        
        
        
        var recordIds = component.get("v.recordId");
        var OppoIds = component.get("v.oppid");
        //  alert(OppoIds);
        component.set("v.Watchsale.Opportunity__c", OppoIds);
        component.find("dealLookup").set("v.value", component.get("v.Watchsale.Opportunity__c"));
        /*  var action = component.get("c.getWatchSale");
        //  alert("OppoIds"+OppoIds);
        action.setParams({"recordId": recordIds});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                
                var c = response.getReturnValue();
                component.set("v.Watchsale", c);
                // component.set("v.strbrandfamily", component.get("v.Watchsale.Brand_Family_Id__c"));
                // component.set("v.strbrandfamily", component.get("v.Watchsale.Brand_Family_Id__c"));
                // alert(component.get("v.Watchsale.Opportunity__c"));
                
                if(component.get("v.Watchsale.Brand_Family_Id__c") != null){
                    component.set("v.ReferencePicklistdisabled" , false);
                    console.log('else');
                    var action = component.get("c.getlstReferences");
                    action.setParams({ 
                        "strBrandFamily": component.get("v.Watchsale.Brand_Family_Id__c") ,
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        //alert(state);
                        if(state==="SUCCESS"){
                            var result = response.getReturnValue();
                            console.log(result);
                            
                            var mapReference = new Map();
                            var refNoMap =[];
                            for(var key in result){
                                mapReference.set(key,result[key])
                                refNoMap.push({key: key, value: result[key]});
                            }
                            component.set("v.listDependingValues", mapReference);
                            
                            component.set("v.referencenoList", refNoMap);
                            // console.log('mapBranchFamilies!!!!!!!'+mapBranchFamilies);
                            component.set("v.ReferencePicklistdisabled" , false);
                            //component.set("v.referencenoList", mapBranchFamilies);
                           // component.set('v.showSpinner', false); 
                            
                        }
                        
                        
                    });
                    
                    $A.enqueueAction(action);
                    
                }
                if(component.get("v.Watchsale.Brand_Family_Id__c") == null){
                    component.set("v.ReferencePicklistdisabled" , true);
                    component.set("v.listDependingValues", '--None--');
                }
                
                helper.getBrandfamilies(component,event);
            }
        });*/
        
        //  $A.enqueueAction(action);
    },
    
    handleSaleBrandOnChange : function(component, event, helper) {
        var SaleBrand = component.get("v.Watchsale.Sale_Brand__c");
        component.set('v.showSpinner', true); 
        var salebrand1= component.find("BrandPicklist").get("v.value");
        //component.set("v.referencenoList", '--None--');
        component.set("v.branchFamiliesList", '--None--');
        //   var ref=component.find("ReferencePicklist").get("v.value");
        //var recordToDisply = component.find("brandfamily").get("v.value");
        if(salebrand1=='' ||  salebrand1== null){
            
            component.set("v.Watchsale.Reference_Number_Id__c", '');
            component.set("v.Watchsale.Brand_Family_Id__c", '');
            component.set("v.Watchsale.Reference_Number__c", '');
            component.set("v.Watchsale.Brand_Family__c", '');
            console.log('if');
            component.set("v.ReferencePicklistdisabled" , true);
            component.set("v.BrandFamilyPicklistdisabled" , true);
            component.set("v.referencenoList", '--None--');
            component.set("v.branchFamiliesList", '--None--');
            
            component.set('v.showSpinner', false); 
            
            
        }
        
        else{     
            component.set("v.errorclass",'slds-hide');
            
            component.set("v.validationError",'');
           // component.set("v.ReferencePicklistdisabled" , false);
            component.set("v.BrandFamilyPicklistdisabled" , false);
            component.set('v.showSpinner', true); 
            helper.getBrandfamilies(component,event);
        }
    },
    
    handleSaleBrandFamilyOnChange : function(component, event, helper) {
        debugger;
        component.set('v.showSpinner', true); 
        var brdfamily= component.find("brandfamily").get("v.value");
        //   var ref=component.find("ReferencePicklist").get("v.value");
        var recordToDisply = component.find("brandfamily").get("v.value");
        if(brdfamily=='' ||  brdfamily== null){
            component.set("v.Watchsale.Reference_Number_Id__c", '');
            component.set("v.Watchsale.Brand_Family_Id__c", '');
            component.set("v.Watchsale.Reference_Number__c", '');
            component.set("v.Watchsale.Brand_Family__c", '');
            console.log('if');
            component.set("v.ReferencePicklistdisabled" , true);
            component.set("v.listDependingValues", '--None--');
            component.set('v.showSpinner', false); 
            
            
        }
        else{
            console.log('else');
            var action = component.get("c.getlstReferences");
            action.setParams({ 
                "strBrandFamily": brdfamily ,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if(state==="SUCCESS"){
                    var result = response.getReturnValue();
                    console.log(result);
                    
                    var mapReference = new Map();
                    var refNoMap =[];
                    for(var key in result){
                        mapReference.set(key,result[key])
                        refNoMap.push({key: key, value: result[key]});
                    }
                    component.set("v.listDependingValues", mapReference);
                    
                    component.set("v.referencenoList", refNoMap);
                    
                    
                    
                    component.set("v.ReferencePicklistdisabled" , false);
                    component.set('v.showSpinner', false); 
                    
                }
                
            });
            
            $A.enqueueAction(action);
        }
    },
    handleSellingBelowMSP : function(component, event, helper) {
        var SellingBelowMSP = component.get("v.Watchsale.Selling_below_MSP__c");
    },
    
    handleReasonFoeSelling : function(component, event, helper) {
        var ReasonFoeSelling = component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c");
    },
    handleBoxPaper : function(component, event, helper) {
        var BoxPaper = component.get("v.Watchsale.Box_Paper__c");
    },
    handleClosingPotential : function(component, event, helper) {
        var closingpotential = component.get("v.Watchsale.Closing_Potential__c	");
    },
    onSave : function(component, event, helper) {
        debugger;
        var refNo = component.find("refPicklist").get("v.value");
        var brandfam=component.find("brandfamily").get("v.value");
        var salebrandvalue=component.get("v.Watchsale.Sale_Brand__c");
        //  alert("salebrandvalue!!!!!"+salebrandvalue);
        var idVsBranchFamilyNameMap= new Map();
        var idVsRefidNameMap=new Map();
        idVsBranchFamilyNameMap = component.get("v.mapBranchFamilies");
        idVsRefidNameMap= component.get("v.listDependingValues");
        var brandFamilyName ='';
        var refnoName='';
        if(idVsBranchFamilyNameMap != null && idVsBranchFamilyNameMap != undefined && idVsBranchFamilyNameMap != '' && brandfam!=null && brandfam!='') {
            if(idVsBranchFamilyNameMap.has(brandfam)){
                brandFamilyName = idVsBranchFamilyNameMap.get(brandfam);
            }
        }
        
        if(idVsRefidNameMap != null && idVsRefidNameMap != undefined && idVsRefidNameMap != '' && refNo!=null && refNo!='') {
            if(idVsRefidNameMap.has(refNo)){
                refnoName = idVsRefidNameMap.get(refNo);
            }
        }
        
        component.set("v.Watchsale.Brand_Family__c",brandFamilyName);
        component.set("v.Watchsale.Reference_Number__c",refnoName);
        // alert('@@@@@@@@'+component.find("noofferid").get("v.value"));
        
        if( component.get("v.Watchsale.Sale_Brand__c")== null 
           || component.get("v.Watchsale.Sale_Brand__c")== ''){
            
            console.log("Validation error if");
            // alert("Error");
            component.set("v.errorclass",'slds-show');
            component.set("v.validationError",'Sale Brand must be selected	.	');
            //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
            
            
        }
        
        
        //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
       
             else if( component.get("v.Watchsale.Client_s_Offer__c   ")== null 
                            || component.get("v.Watchsale.Client_s_Offer__c ")== '' ){
                        
                        console.log("Client_s_Offer__c error if");
                        // alert("Error");
                        component.set("v.errorclass",'slds-show');
                        component.set("v.validationError",'Clients offer field is mandatory	');
                    }
        
                else  if( component.find("noofferid").get("v.value")!=true 
                         && (component.get("v.Watchsale.MSP__c ")== null 
                             || component.get("v.Watchsale.MSP__c ")== '' )){
                    
                    console.log("Validation error if");
                    // alert("Error");
                    component.set("v.errorclass",'slds-show');
                    component.set("v.validationError",'WB Offer field is mandatory.	');
                    //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
                    
                    
                }
                   else if( component.get("v.Watchsale.Selling_below_MSP__c  ")=='Yes' 
                && (component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c ")== null 
                    || component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c ")== '' )){
            
            console.log("Validation error if");
            // alert("Error");
            component.set("v.errorclass",'slds-show');
            component.set("v.validationError",'Reason for Selling Below MSP is mandatory .		');
            //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
            
            
        }
        
                        else {
                            console.log("onSave");
                            var obwatchSale = component.get("v.Watchsale");
                            //  alert("obwatchSale!!!"+obwatchSale);
                            var action = component.get("c.insertWatchSale");
                            action.setParams({ 
                                "objwatchSale": obwatchSale
                            });
                            
                            action.setCallback(this, function(resp) {
                                console.log(resp.getState());
                                
                                var strtheme = component.get("v.themeclassic");
                                if(strtheme == 'true' ||strtheme == true){
                                    var urlEvent = $A.get("e.force:navigateToURL");
                                    urlEvent.setParams({
                                        "url": '/'+component.get('v.oppid'),
                                        "isredirect" : "true"
                                    });
                                    urlEvent.fire();
                                }else{
                                    var navigateEvent = $A.get("e.force:navigateToURL");
                                    navigateEvent.setParams({ "url": '/'+component.get('v.oppid') });
                                    navigateEvent.fire();
                                }
                                
                                
                                
                            });
                            $A.enqueueAction(action);
                        }
    },
    onCancel : function(component, event, helper) {
        //alert(component.get('v.oppid')); 
        
        var strtheme = component.get("v.themeclassic");
        //  alert(strtheme);
        if(strtheme == 'true' ||strtheme == true){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": '/'+component.get('v.Watchsale.Opportunity__c'),
                "isredirect" : "true"
            });
            urlEvent.fire();
        }else{
            // Navigate back to the record view
            /*  var navigateEvent = $A.get("e.force:navigateToSObject");
                    navigateEvent.setParams({ "recordId":component.get('v.oppid') });
                    navigateEvent.fire();*/
            var navigateEvent = $A.get("e.force:navigateToURL");
            navigateEvent.setParams({ "url": '/'+component.get('v.oppid') });
            navigateEvent.fire();
            
        }
    },
    handleDealContextChange : function(component, event, helper){
        var selectedDeal = component.find("dealLookup").get("v.value");
        var dealInContext = component.get("v.dealInContext");
        console.log("@@@handleUserContextChange selectedUser:", selectedDeal);
        console.log("@@@handleUserContextChange userInContext:", dealInContext);
        if(selectedDeal && dealInContext != selectedDeal){
            helper.getdealData(component);
        }
    },
    
})