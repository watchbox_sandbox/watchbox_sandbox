({
    getReferenceIds : function(component,event) {
        var action = component.get("c.getReferenceIds");
        action.setParams({
            "watchsale" : component.get('v.watchsale'),
        	"watchbrand" : component.get('v.watchbrand')
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
               // component.set("v.selectedRecord" , responseValue);    // that could be v.SearchKeyWord instead of v.selectedRecord ?
                //component.set("v.selectedLookupRecord" , responseValue);
            } });
        $A.enqueueAction(action);
    },
    
    getPicklistValuesForSaleBrand : function(component, event){
         var action = component.get("c.getsaleBrandValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var SaleBrandMap = [];
                for(var key in result){
                    SaleBrandMap.push({key: key, value: result[key]});
                }
                component.set("v.SaleBrand", SaleBrandMap);
            }
        });
        $A.enqueueAction(action);
    },
    
      getPicklistValuesForClosingPotential : function(component, event){
         var action = component.get("c.getClosingPotential");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var ClosingPotentialMap = [];
                for(var key in result){
                    ClosingPotentialMap.push({key: key, value: result[key]});
                }
                component.set("v.ClosingPotential", ClosingPotentialMap);
            }
        });
        $A.enqueueAction(action);
    },
    
      getPicklistValuesForBoxPaper : function(component, event){
         var action = component.get("c.getBoxPaper");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var BoxPaperMap = [];
                for(var key in result){
                    BoxPaperMap.push({key: key, value: result[key]});
                }
                component.set("v.BoxPaper", BoxPaperMap);
            }
        });
        $A.enqueueAction(action);
    },
    
      getPicklistValuesForSellingBelowMSP : function(component, event){
         var action = component.get("c.getSellingBelowMSP");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var SellingBelowMSPMap = [];
                for(var key in result){
                    SellingBelowMSPMap.push({key: key, value: result[key]});
                }
                component.set("v.SellingBelowMSP", SellingBelowMSPMap);
            }
        });
        $A.enqueueAction(action);
    },
    
      getPicklistValuesForReasonFoeSelling : function(component, event){
         var action = component.get("c.getReasonForSelling");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var ReasonFoeSellingMap = [];
                for(var key in result){
                    ReasonFoeSellingMap.push({key: key, value: result[key]});
                }
                component.set("v.ReasonFoeSelling", ReasonFoeSellingMap);
            }
        });
        $A.enqueueAction(action);
    },
    
     sellingPicklist : function(component, event){
         debugger;
          var sellingValue = component.find("sellingId");
        var sellingValueNew = sellingValue.get("v.value");
         component.set("v.sellValue",sellingValueNew);
         
     },
    getBrandfamilies : function(component, event){
        
        debugger;
        var action = component.get("c.getlstBranchFamilies");
        action.setParams({"salebrand": component.get("v.Watchsale.Sale_Brand__c")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('::::'+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var mapBranchFamilies = new Map();
                var branchFamiliesList =[];
                for(var key in result){
                    mapBranchFamilies.set(key,result[key])
                    branchFamiliesList.push({key: key, value: result[key]});
                }
                component.set("v.mapBranchFamilies", mapBranchFamilies);
                component.set("v.branchFamiliesList", branchFamiliesList);
                component.set("v.strbrandfamily", component.get("v.Watchsale.Brand_Family_Id__c"));
                
                console.log('@@mapBranchFamilies'+mapBranchFamilies);
                component.set('v.showSpinner', false); 
            }
            if(state==="ERROR"){
                component.set('v.showSpinner', false); 
            }
            
        });
       // component.set('v.showSpinner', false);
        $A.enqueueAction(action);
    },
    getDealName : function(component, event){
        debugger;
        var dealID= component.get("v.recordId");
        console.log(dealID);
        var action=component.get("c.getOpportunityName");
        action.setParams({
            watchsaleId : dealID
        });
          action.setCallback(this, function(response) {
            if(response.getState()=="SUCCESS"){
                var result=response.getReturnValue();
                component.set("v.dealName",result.Name);
            }
        });
        $A.enqueueAction(action);
    },
    
    getdealData : function(component) {
      //  this.toggleSpinner(component); //show
        var selecteddeal;
        var recordIds = component.get("v.recordId");
        var dealLookup = component.find("dealLookup");
            if(dealLookup){
                selecteddeal = dealLookup.get("v.value");
            }
        
      /* var getDeal = component.get("c.getWatchSale");
        getDeal.setParams({
            "recordId" : recordIds 
        });
        getDeal.setCallback(this, function(response){
            var state = response.getState();
            console.log("helper.getDeal response state:", state);
            if (state === "SUCCESS") {
                var timeSheetsObj = JSON.parse(response.getReturnValue());
                component.find("dealLookup").set("v.value", component.get("v.Watchsale.Opportunity__c"));
                
                console.log('@@@ user id', timeSheetsObj.dealInContext);
                component.set("v.dealInContext", timeSheetsObj.dealInContext);
                
                	component.find("dealLookup").set("v.value",timeSheetsObj.dealInContext);
                
               
            }
            else{
                console.log("An error occurred in helper.getTimeSheetsByOwnerAndApproverAction:", response.getError());
            }
           // this.toggleSpinner(component); //hide
        });
        $A.enqueueAction(getDeal);*/
    },
    
    //URL
    getParameterByName: function(component, event, name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
      //  alert(url);
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})