({
    doInit  : function(component, event, helper) {
        debugger;
        var oppid=component.get("v.recordId");
        var action = component.get("c.getDealDetails");
        action.setParams({ oppId : oppid});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){ 
                debugger;
                var result =response.getReturnValue(); 
                if(result !== null && result !== undefined && result !== ''){
                    if(result[0].Is_watch_Purchased_if_already_in_Stock__c==true && result[1].Is_ref_currently_in_stock__c==true){
                        var oppid=component.get("v.recordId");
                        var action = component.get("c.submitApprovalProcess");
                        action.setParams({ oppid : oppid});
                        action.setCallback(this, function(response) {
                            var state = response.getState();
                            if(state === "SUCCESS"){ 
                                
                                component.set("v.successmsg",'Your deal has been submitted for approval.');                      
                            }
                            else{
                                component.set("v.successmsg",'Either your previous approval is in process or your manager is not defined.');                      
                            }
                        });
                        $A.enqueueAction(action);
                        
                    }
                    else if(result[0].Is_watch_Purchased_if_already_in_Stock__c==true && result[1].Is_ref_currently_in_stock__c==false){
                        component.set("v.successmsg",'Reference number should be already in stock for approval.');                      
                    }
                        else if(result[0].Is_watch_Purchased_if_already_in_Stock__c==false){
                            component.set("v.successmsg",'Custom Setting is disabled');  
                        }
                }
                
                
                
            }
        });
        $A.enqueueAction(action);
        
    },
    
    
})