({
    init : function(component, event, helper) {
        debugger;
        //  helper.getDealName(component,event);
        component.set("v.Watchsale.Brand_Family_Id__c", '');
        helper.getPicklistValuesForSaleBrand(component,event);
        helper.getPicklistValuesForClosingPotential(component,event);
        helper.getPicklistValuesForBoxPaper(component,event);
        helper.getPicklistValuesForSellingBelowMSP(component,event);
        helper.getPicklistValuesForReasonFoeSelling(component,event);
        var action = component.get("c.getWatchSale");
        var recordIds = component.get("v.recordId");
        action.setParams({"recordId": recordIds});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS"){
                component.set("v.showSpinner", true); 
                var c = response.getReturnValue();
                component.set("v.Watchsale", c);
                // alert(component.get("v.Watchsale.Brand_Family_Id__c"));
                if(component.get("v.Watchsale.Brand_Family_Id__c") == null ||
                   component.get("v.Watchsale.Brand_Family_Id__c") == undefined ){
                    // alert("null Brand_Family_Id__c");
                    component.set("v.ReferencePicklistdisabled" , true);
                    component.set("v.listDependingValues", '--None--');
                }
                // component.set("v.strbrandfamily", component.get("v.Watchsale.Brand_Family_Id__c"));
                // component.set("v.strbrandfamily", component.get("v.Watchsale.Brand_Family_Id__c"));
                if(component.get("v.Watchsale.Brand_Family_Id__c") != null){
                    component.set("v.ReferencePicklistdisabled" , false);
                    console.log('else');
                    var action = component.get("c.getlstReferences");
                    action.setParams({ 
                        "strBrandFamily": component.get("v.Watchsale.Brand_Family_Id__c") ,
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if(state==="SUCCESS"){
                            var result = response.getReturnValue();
                            console.log(result);
                            
                            var mapReference = new Map();
                            var refNoMap =[];
                            for(var key in result){
                                mapReference.set(key,result[key])
                                refNoMap.push({key: key, value: result[key]});
                            }
                            component.set("v.listDependingValues", mapReference);
                            
                            component.set("v.referencenoList", refNoMap);
                            // console.log('mapBranchFamilies!!!!!!!'+mapBranchFamilies);
                            component.set("v.ReferencePicklistdisabled" , false);
                            //component.set("v.referencenoList", mapBranchFamilies);
                            component.set('v.showSpinner', false); 
                            
                        }
                        
                        
                    });
                    
                    $A.enqueueAction(action);
                    
                }
                
                helper.getBrandfamilies(component,event);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.showSpinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.showSpinner", false);
    },
    
    handleSaleBrandFamilyOnChange : function(component, event, helper) {
        debugger;
        component.set('v.showSpinner', true); 
        var brdfamily= component.find("brandfamily").get("v.value");
        //   var ref=component.find("ReferencePicklist").get("v.value");
        var recordToDisply = component.find("brandfamily").get("v.value");
        if(brdfamily=='' ||  brdfamily== null){
            component.set("v.Watchsale.Reference_Number_Id__c", '');
            component.set("v.Watchsale.Brand_Family_Id__c", '');
            component.set("v.Watchsale.Reference_Number__c", '');
            component.set("v.Watchsale.Brand_Family__c", '');
            console.log('if');
            component.set("v.ReferencePicklistdisabled" , true);
            component.set("v.listDependingValues", '--None--');
            component.set('v.showSpinner', false); 
            
            
        }
        else{
            console.log('else');
            var action = component.get("c.getlstReferences");
            action.setParams({ 
                "strBrandFamily": brdfamily ,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if(state==="SUCCESS"){
                    var result = response.getReturnValue();
                    console.log(result);
                    
                    var mapReference = new Map();
                    var refNoMap =[];
                    for(var key in result){
                        mapReference.set(key,result[key])
                        refNoMap.push({key: key, value: result[key]});
                    }
                    component.set("v.listDependingValues", mapReference);
                    
                    component.set("v.referencenoList", refNoMap);
                    
                    
                    
                    component.set("v.ReferencePicklistdisabled" , false);
                    component.set('v.showSpinner', false); 
                    
                }
                
            });
            
            $A.enqueueAction(action);
        }
    },
    
    handleSaleBrandOnChange : function(component, event, helper) {
        var SaleBrand = component.get("v.Watchsale.Sale_Brand__c");
        
        var salebrand1= component.find("BrandPicklist").get("v.value");
        component.set("v.referencenoList", '--None--');
        component.set("v.branchFamiliesList", '--None--');
        //   var ref=component.find("ReferencePicklist").get("v.value");
        //var recordToDisply = component.find("brandfamily").get("v.value");
        if(salebrand1=='' ||  salebrand1== null){
            
            component.set("v.Watchsale.Reference_Number_Id__c", '');
            component.set("v.Watchsale.Brand_Family_Id__c", '');
            component.set("v.Watchsale.Reference_Number__c", '');
            component.set("v.Watchsale.Brand_Family__c", '');
            console.log('if');
            component.set("v.ReferencePicklistdisabled" , true);
            component.set("v.BrandFamilyPicklistdisabled" , true);
            component.set("v.referencenoList", '--None--');
            component.set("v.branchFamiliesList", '--None--');
            
            component.set('v.showSpinner', false); 
            
            
        }
        
        else{
            component.set("v.errorclass",'slds-hide');
            
            component.set("v.validationError",'');
            //alert(component.get("v.errorclass"));
            
            //component.set("v.ReferencePicklistdisabled" , false);
            component.set("v.BrandFamilyPicklistdisabled" , false);
            
            component.set('v.showSpinner', true); 
            helper.getBrandfamilies(component,event);
        }
    },
    
    handleSellingBelowMSP : function(component, event, helper) {
        var indutry = component.get("v.Watchsale.Selling_below_MSP__c");
    },
    
    handleReasonFoeSelling : function(component, event, helper) {
        var indutry = component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c");
    },
    
    handleClosingPotential : function(component, event, helper) {
        var indutry = component.get("v.Watchsale.Closing_Potential__c");
    },
    handleBoxPaper : function(component, event, helper) {
        var indutry = component.get("v.Watchsale.Box_Paper__c");
    },
    onCancel : function(component, event, helper) {
        var strtheme = component.get("v.themeclassic");
        if(strtheme == 'true' ||strtheme == true){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": '/'+component.get('v.Watchsale.Opportunity__c'),
                "isredirect" : "true"
            });
            urlEvent.fire();
        }else{
            // Navigate back to the record view
            var navigateEvent = $A.get("e.force:navigateToSObject");
            navigateEvent.setParams({ "recordId": component.get('v.Watchsale.Opportunity__c') });
            navigateEvent.fire();
        }
    },
    onSave : function(component, event, helper) {
        debugger;
        var refNo = component.find("refPicklist").get("v.value");
        var brandfam=component.find("brandfamily").get("v.value");
        var salebrandvalue=component.get("v.Watchsale.Sale_Brand__c");
        
        var idVsBranchFamilyNameMap= new Map();
        var idVsRefidNameMap=new Map();
        idVsBranchFamilyNameMap = component.get("v.mapBranchFamilies");
        idVsRefidNameMap= component.get("v.listDependingValues");
        var brandFamilyName ='';
        var refnoName='';
        if(idVsBranchFamilyNameMap != null && idVsBranchFamilyNameMap != undefined && idVsBranchFamilyNameMap != '' && brandfam!=null && brandfam!='') {
            if(idVsBranchFamilyNameMap.has(brandfam)){
                brandFamilyName = idVsBranchFamilyNameMap.get(brandfam);
            }
        }
        
        if(idVsRefidNameMap != null && idVsRefidNameMap != undefined && idVsRefidNameMap != '' && refNo!=null && refNo!='') {
            if(idVsRefidNameMap.has(refNo)){
                refnoName = idVsRefidNameMap.get(refNo);
            }
        }
        component.set("v.Watchsale.Brand_Family__c",brandFamilyName);
        component.set("v.Watchsale.Reference_Number__c",refnoName);
        
       if( component.get("v.Watchsale.Sale_Brand__c")== null 
           || component.get("v.Watchsale.Sale_Brand__c")== ''){
            
            console.log("Validation error if");
            // alert("Error");
            component.set("v.errorclass",'slds-show');
            component.set("v.validationError",'Sale Brand must be selected	.	');
            //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
            
            
        }
        
        
        //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
       
             else if( component.get("v.Watchsale.Client_s_Offer__c   ")== null 
                            || component.get("v.Watchsale.Client_s_Offer__c ")== '' ){
                        
                        console.log("Client_s_Offer__c error if");
                        // alert("Error");
                        component.set("v.errorclass",'slds-show');
                        component.set("v.validationError",'Clients offer field is mandatory	');
                    }
        
                else  if( component.find("noofferid").get("v.value")!=true 
                         && (component.get("v.Watchsale.MSP__c ")== null 
                             || component.get("v.Watchsale.MSP__c ")== '' )){
                    
                    console.log("Validation error if");
                    // alert("Error");
                    component.set("v.errorclass",'slds-show');
                    component.set("v.validationError",'WB Offer field is mandatory.	');
                    //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
                    
                    
                }
                   else if( component.get("v.Watchsale.Selling_below_MSP__c  ")=='Yes' 
                && (component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c ")== null 
                    || component.get("v.Watchsale.Reason_for_Selling_Below_MSP__c ")== '' )){
            
            console.log("Validation error if");
            // alert("Error");
            component.set("v.errorclass",'slds-show');
            component.set("v.validationError",'Reason for Selling Below MSP is mandatory .		');
            //component.find("ErrorHandlingDivId").set("v.errors", [{message:"Reason for Selling is mandatory below MSP."}]);
            
            
        }
        
        
                    else{
                        console.log("onSave");
                        var obwatchSale = component.get("v.Watchsale");
                        var action = component.get("c.saveWatchSale");
                        action.setParams({ 
                            "objwatchSale": obwatchSale
                        });
                        
                        action.setCallback(this, function(resp) {
                            console.log(resp.getState());
                            if (resp.getState() === "SUCCESS") {
                                /* var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been updated successfully.",
                    "type":"Success"
                });
                toastEvent.fire();*/
                                    
                                    
                                    var strtheme = component.get("v.themeclassic");
                                    if(strtheme == 'true' ||strtheme == true){
                                        var urlEvent = $A.get("e.force:navigateToURL");
                                        urlEvent.setParams({
                                            "url": '/'+component.get('v.Watchsale.Opportunity__c'),
                                            "isredirect" : "true"
                                        });
                                        urlEvent.fire();
                                    }else{
                                        var navigateEvent = $A.get("e.force:navigateToSObject");
                                        navigateEvent.setParams({ "recordId": component.get('v.Watchsale.Opportunity__c') });
                                        navigateEvent.fire();
                                    }
                                    
                                }
                                
                                
                            });
                            $A.enqueueAction(action);
                        }
    },
    
    
    selling : function(component, event, helper) {
        helper.sellingPicklist(component,event);
        
    },
    
    SearchReferenceNumber :function(component, event, helper) {
        console.log("SearchReferenceNumber");
        debugger;
        var select= component.find("brandfamily").get("v.value");
        console.log("Selected Value:::::::"+select);
        var action = component.get("c.searchData");
        action.setParams({ 
            "salebrand": component.get('v.salebrand') ,
            "saleModel": component.get('v.saleModel') ,
        });
        action.setCallback(this, function(resp) {
            /* if (resp.getState() === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "The record has been searched successfully.",
                            "type":"Success"
                        });
                        toastEvent.fire();
               
                
            }*/
            var size=resp.getReturnValue();
            console.log(size);
            if(size.length>0){
                console.log("in if");
                
                component.set("v.bDisabledDependentFld" , false);
            }
            component.set("v.listDependingValues", resp.getReturnValue());
            
        });
        
        $A.enqueueAction(action);
        
    }
})