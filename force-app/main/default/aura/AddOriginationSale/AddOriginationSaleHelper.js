({
    getPicklistValuesForOriginationBrand : function(component, event){
        debugger;
        var action = component.get("c.originationBrandValues");
        var givingStatus = component.find("givingStatus");
        var opts=[];
        action.setCallback(this, function(response) {
            opts.push({
                label: "Select",
                value: ""
            });
            for(var i=0;i< response.getReturnValue().length;i++){
                opts.push({label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            component.set("v.optionsGivingStatus", opts);
            
        });
        $A.enqueueAction(action);
    },
    getDealName : function(component, event){
        debugger;
        var dealID= component.get("v.recordId");
        var action=component.get("c.getOpportunityName");
        action.setParams({
            oppId : dealID
        });
        action.setCallback(this, function(response) {
            if(response.getState()=="SUCCESS"){
                var result=response.getReturnValue();
                component.set("v.dealName",result.Name);
            }
        });
        $A.enqueueAction(action);
    },
   saveOriginationSale : function(component, event){
	debugger;
       var dealID= component.get("v.recordId");
       var isValidReason = false; 
	 var wbDirectValue = component.find("wbDirectId").get("v.value");
	 var tradeOfferValue = component.find("tradeOfferId").get("v.value");
	var action=component.get("c.getDealStatusLossReason");
	action.setParams({
		oppid :dealID
	});
	action.setCallback(this, function(response) {
		if(response.getState()=="SUCCESS"){
			var result=response.getReturnValue();
			if(result.Deal_Status__c=='Deal Lost' && result.Loss_Reason__c=='Couldn\'t Agree on Price' && (wbDirectValue==null ||wbDirectValue==''|| tradeOfferValue==null||tradeOfferValue=='')){
				component.find("wbDirectId").set("v.errors", [{message: "WB Direct Offer is mandatory when Loss Reason is 'Couldn\'t Agree on Price'"}]);
				component.find("tradeOfferId").set("v.errors", [{message: "Trade offer is mandatory when Loss Reason is 'Couldn\'t Agree on Price'"}]);
                isValidReason = false;
			}else{
                isValidReason = true;
                component.find("wbDirectId").set("v.errors", null);
                component.find("tradeOfferId").set("v.errors", null);
            }if(isValidReason){
				var dealID= component.get("v.recordId");
				var action2=component.get("c.addOriginationSale");
				action2.setParams({
					originationObj : component.get("v.OriginationObj"),
					oppid :dealID
				});
	 			action2.setCallback(this, function(response) {
					if(response.getState()=="SUCCESS"){
			 			var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							"title": "Success!",
							"message": "Record Saved Successfully"
						});
						toastEvent.fire();
						$A.get("e.force:closeQuickAction").fire();
			
					}else{
                     	var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							"title": "Error!",
							"message": "Something went wrong"
						});
						toastEvent.fire();
						$A.get("e.force:closeQuickAction").fire();
					}
		
				});
				$A.enqueueAction(action2);
			}
		}
		
	});
	$A.enqueueAction(action);
   
	}
})