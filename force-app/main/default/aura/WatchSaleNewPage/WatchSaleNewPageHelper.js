({
    getPicklistValuesForSaleBrand : function(component, event){
        debugger;
        var action = component.get("c.saleBrandValues");
        var givingStatus = component.find("givingStatus");
        var opts=[];
        action.setCallback(this, function(response) {
            opts.push({
                label: "Select",
                value: ""
            });
             for(var i=0;i< response.getReturnValue().length;i++){
                opts.push({label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            component.set("v.optionsGivingStatus", opts);
            
        });
       $A.enqueueAction(action);
    },
     sellingPicklist : function(component, event){
         debugger;
          var sellingValue = component.find("sellingId");
        var sellingValueNew = sellingValue.get("v.value");
         component.set("v.sellValue",sellingValueNew);
         
     },
    reasonPicklist : function(component, event){
         
         var reasonValue = component.find("reasonId");
        var reasonValueNew = reasonValue.get("v.value");
         component.set("v.reasonValue",reasonValueNew);
         
     },
    
    getDealName : function(component, event){
        debugger;
        var dealID= component.get("v.recordId");
        var action=component.get("c.getOpportunityName");
        action.setParams({
            oppId : dealID
        });
          action.setCallback(this, function(response) {
            if(response.getState()=="SUCCESS"){
                var result=response.getReturnValue();
                component.set("v.dealName",result.Name);
            }
        });
        $A.enqueueAction(action);
    },
    saveWatchSale : function(component, event){
        debugger;
         var reasonValue = component.find("reasonId");
        var reasonValueNew=component.get("v.reasonValue");
        component.set("v.watchSaleObj.Reason_for_Selling_Below_MSP__c",reasonValueNew);
       var dealID= component.get("v.recordId");
 		var sellingValueNew = component.find("sellingId").get("v.value");
        isValidReason = true; 
        if(sellingValueNew=="Yes" && (reasonValueNew==undefined || reasonValueNew=="--None--")){
             reasonValue.set("v.errors", [{message: "Reason for Selling is mandatory below MSP."}]);
			 isValidReason = false;
        }
        else{
     		reasonValue.set("v.errors", [{message:null}]);
            isValidReason = true;
        }
        
        var wbDirectIdValue = component.find("wbDirectId").get("v.value");
        isValidWBOffer = true;
        if (wbDirectIdValue == '' || wbDirectIdValue == null) {
            component.find("wbDirectId").set("v.errors", [{message:"WB-Direct Purchase Offer is mandatory"}]);
            isValidWBOffer = false;
        } else {
            component.set("v.watchSaleObj.WB_Direct_Purchase_Offer__c",wbDirectIdValue);
            component.find("wbDirectId").set("v.errors", null);
            isValidWBOffer = true;
        } 
        var clientOfferIdValue = component.find("clientOfferId").get("v.value");
        isValidCOffer= true;
        if (clientOfferIdValue == '' || clientOfferIdValue == null) {
            component.find("clientOfferId").set("v.errors", [{message:"Client's Offer is mandatory"}]);
            isValidCOffer= false;
        } else {
            component.set("v.watchSaleObj.Client_s_Offer__c",clientOfferIdValue);
            component.find("clientOfferId").set("v.errors", null);
            isValidCOffer = true;
        } 
        
        
       /* if(WBDirectOfferNew==undefined){
            debugger;
             $A.util.addClass(WBDirectOffer, 'slds-has-error');
             WBDirectOffer.set("v.errors", [{message: "WB-Direct Purchase Offer is mandatory."}]);
			 isValidReason = false;
        }
        else{
             $A.util.removeClass(WBDirectOffer, 'slds-has-error');
     		WBDirectOffer.set("v.errors", [{message:null}]);
            isValidReason = true;
        }
        if(clientOfferNew==undefined){
            debugger;
             $A.util.addClass(clientOffer, 'slds-has-error');
             clientOffer.set("v.errors", [{message: "Client Offer is mandatory."}]);
			 isValidReason = false;
        }
        else{
             $A.util.removeClass(clientOffer, 'slds-has-error');
     		clientOffer.set("v.errors", [{message:null}]);
            isValidReason = true;
        }*/
        if(isValidReason && isValidWBOffer && isValidCOffer){
            var action=component.get("c.addWatchSale");
            action.setParams({
                watchSaleobj : component.get("v.watchSaleObj"),
                oppid :dealID
            });
             action.setCallback(this, function(response) {
                if(response.getState()=="SUCCESS"){
                     var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Record Saved Successfully"
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    
                }else{
                   
                     var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Something went wrong"
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                
            });
            $A.enqueueAction(action);
        }
    }
})