({
    doInit : function(component, event, helper){
       helper.getDealName(component,event);
       helper.getPicklistValuesForSaleBrand(component,event);
      
        
	},
    saveWatchSale : function(component, event, helper) {
            helper.saveWatchSale(component,event);
       
    },
    selling : function(component, event, helper) {
            helper.sellingPicklist(component,event);
       
    },
    reason : function(component, event, helper) {
            helper.reasonPicklist(component,event);
       
    }
    
})