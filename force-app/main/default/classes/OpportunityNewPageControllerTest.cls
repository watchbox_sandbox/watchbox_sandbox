@isTest
public class OpportunityNewPageControllerTest {
    @isTest
    public static void saveTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.save();
        list<Opportunity> acc = [select id from Opportunity];//Retrive the record
        integer i = acc.size();
        //system.assertEquals(1,i);//Test that the record is inserted
        
        
    }
     @isTest
    public static void saveTest1(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.expectedCost = 100;
        //opp.Expected_Total_Cost__c =expectedCost;
         obj.currentCost = 100;
        //opp.Current_Adjusted_Cost__c = currentCost;
        opp.Notes_Development__c = '';
          obj.save();
       
       System.assert(ApexPages.hasMessages(ApexPages.severity.WARNING));
        }
     @isTest
    public static void saveTest2(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        opp.Type_of_Transaction_Initial__c = 'sale';
        //obj.watchSaleWrapperList[0].watchSale  = null;
        obj.watchSaleWrapperList[0].watchSale.Sale_Brand__c  = null;
            obj.name = 'Scratch Pad';
        obj.save();
       System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
         obj.saveandnew();
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        
    }
     @isTest
   public static void saveTest4(){
        
        Opportunity opp = new Opportunity(Name='test');
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        opp.Type_of_Transaction_Initial__c = 'Origination';
         obj.originationWrapperList[0].origination.Origination__c = null;
       obj.name = 'Scratch Pad';
        obj.save();
       System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
         obj.saveandnew();
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        
    }
      @isTest
    public static void saveTest3(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.selectedApprover = 'Shannon Beck';
        obj.approvedAmount = 0.0;
      		obj.save();
       System.assert(ApexPages.hasMessages(ApexPages.severity.WARNING));
        
    }
    @isTest
    public static void saveandNewTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
        opp.Type_of_Transaction_Initial__c = 'Sale';
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        //obj.selectedApprover = 'Shannon Beck';
        obj.saveandnew();
        list<Opportunity> acc = [select id from Opportunity];//Retrive the record
        integer i = acc.size();
        //system.assertEquals(1,i);//Test that the record is inserted
       
        
    }
     @isTest
    public static void saveTest5()
    {
        
        Opportunity opp = new Opportunity();
         OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.save();
         System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        obj.saveandnew();
        
         System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        
        
        
        
    }
     @isTest
    public static void saveandNewTest1(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
        opp.Type_of_Transaction_Initial__c = 'Sale';
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.selectedApprover = 'Shannon Beck';
        obj.saveandnew();
        list<Opportunity> acc = [select id from Opportunity];//Retrive the record
        integer i = acc.size();
      
        
        
    }
    @isTest
    public static void saveandNewTest2(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
         OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        opp.Type_of_Transaction_Initial__c = 'Sale';
         obj.expectedCost = 100;
		obj.currentCost = 50;
        opp.Notes_Development__c = null;
        obj.saveandnew();
        System.assert(ApexPages.hasMessages(ApexPages.severity.WARNING));
        
    }
     
     @isTest
    public static void preApprovalTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id,Expected_Total_Cost__c=100.00, Current_Adjusted_Cost__c=50.00, Notes_Development__c=null,Approver__c='Mike Manjos',Approved_Amount__c=0.0);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.getApproverOptions();
        obj.selectedApprover='Mike Manjos';
        obj.ApproverAmountSection();
        obj.saveandnew();
        list<Opportunity> acc = [select id from Opportunity];//Retrive the record
        integer i = acc.size();
        
    }
    @isTest
    public static void DiffAttributesSectionTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.attributeValue='none';
        obj.DiffAttributesSection();  
        
    }
    @isTest
    public static void RenderWatchPurchasedSectionTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.watchPurchasedValue=true;
        obj.RenderWatchPurchasedSection();  
        obj.watchPurchasedValue=false;
        obj.RenderWatchPurchasedSection();
    }
    @isTest
    public static void RenderSecTest(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.demo=true;
        obj.RenderSec();  
        obj.demo=false;
        obj.RenderSec();
    }
 
   
@isTest
    public static void ApproverAmountSection(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
  OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
        obj.selectedApprover = '-None-';
        obj.ApproverAmountSection();
         system.assertEquals('none',obj.selectedApprover);
    }
@isTest
 public static void AddRowSaleTest()
      {
           List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
  OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
         obj.count = 1;
       obj.AddRowSale();
      }
    @isTest
 public static void AddRowOriginateTest()
      {
           List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='dummy test', RecordTypeId=recordTypeNameList[0].Id);
  OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
         obj.ocount = 1;
       obj.AddRowOriginate();
      }
   @isTest
 public static void insertWatchSaleAndOrgListTest()  
 {
     
       Opportunity opp = new Opportunity(Name='dummy test');
      OpportunityNewPageController obj = new OpportunityNewPageController(new ApexPages.StandardController(opp));
 		//Watch_Sale__c.id = 'null';
     obj.insertWatchSaleAndOrgList(opp.id);
     //system.assertEquals(opp.id,Watch_Sale__c.Opportunity__c );
 }
     @isTest
    public static void selectorTest1(){
        List<RecordType> recordTypeNameList=[SELECT name from RecordType where Name='Sale' limit 1];
        Opportunity opp = new Opportunity(Name='test', RecordTypeId=recordTypeNameList[0].Id);
        OpportunityNewPageSelector ops = new OpportunityNewPageSelector();
    List<Watch_Sale__c> wsl =  OpportunityNewPageSelector.getWatchSaleList(opp.Id);
        List<Watch_Purchase__c> wpl =  OpportunityNewPageSelector.getOriginationList(opp.Id);

             Id idl =  OpportunityNewPageSelector.getOpp(opp.Id);
        
       
    
        }
      @isTest
    public static void opportunityWrapperTest(){
        OpportunityWrapperr obj=new OpportunityWrapperr();
       obj.FirstName='Test';
         obj.FullName='Test';obj.LastName='Test';obj.Phone='Test';obj.Email='Test';obj.GAgclid='Test';obj.TransactionType='Test';
         obj.WhatBringsYouInToday='Test';obj.Opportunity='Test';obj.NumberofWatchInCollection='Test';obj.MatchedAccount='Test';obj.SignupWinAWatch='Test';
         obj.WatchImages='Test';obj.interestedinBrand='Test';obj.interestedinModel='Test';obj.InterestedinProduct='Test';obj.InterestedinProductListprice='Test';obj.InterestedinProductUrl='Test';
         obj.interestedinReferenceNumber='Test';obj.GCLID='Test';obj.appcampaignid='Test';obj.EbayUserName='Test';obj.ircid='Test';obj.irtrackid='Test';
         obj.Boxes='Test';
         obj.Papers='Test';
         obj.irpid='Test';
         obj.irclickid='Test';
         obj.PageID='Test';
         obj.IPCountry='Test';
         obj.IPAddress='Test';
         obj.PageURL='Test'; obj.ReceiveUpdates='Test'; obj.SubmissionDate='Test'; obj.Comments='Test'; obj.LeadSource='Test';
         obj.InitialDealDetails='Test'; obj.AdditionalInfo='Test'; obj.SellBrand='Test'; obj.NameYourPriceASK='Test'; obj.SellModel='Test'; obj.LastServiceDate='Test';
      obj.NewWatchModel='Test';obj.BoxesandPapers='Test';obj.BoxOnly='Test';obj.ModelNumber='Test';obj.TypeofMetal='Test';obj.SellWatchSerial='Test';obj.DialDescription='Test';
     obj.WatchReference='Test'; obj.Images='Test'; obj.WatchBrand='Test'; obj.InventoryID='Test'; obj.WatchModel='Test'; obj.SKU='Test';
         obj.SalePrice='Test'; obj.GACampaign='Test'; obj.GASegment='Test'; obj.GAContent='Test'; obj.GATerm='Test'; obj.GAMedium='Test';
        obj.GAVisits='Test';obj.ReferringURL='Test';obj.GaSource='Test';obj.CloseDate='Test';obj.StageName='Test';obj.Quantity='Test';obj.UnitPrice='Test';
              obj.Accountid='Test';      obj.Contactid='Test';      obj.opportunityid='Test';
        }

    
}