public class LuxAssetLoanLead {
    @future(callout=true)
    public static void createLuxAssetDeal (String leadLoan) {
        Auath__mdt authMdt = [SELECT Id,Client_Secret__c,ConsumerKey__c,Password__c,SecurityToken__c,Username__c,URL__c FROM Auath__mdt];//Here write a sample query or fetch from custom settings the consumer ,client secret and username and password of destination org
        String clientId = authMdt.ConsumerKey__c;
        String clientSecret = authMdt.Client_Secret__c;
        String username= authMdt.Username__c;
        String password= authMdt.Password__c;
        String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setBody(reqbody);
        req.setMethod('POST');
        String endPointLabel = System.Label.setEndpoint;
        req.setEndpoint(endPointLabel);//Note if my domain is set up use the proper domain name else use login.salesforce.com for prod or developer or test.salesforce.com for sandbox instance
        HttpResponse res = h.send(req);
        System.debug('res****'+res);
        System.debug('res.getbody()****'+res.getbody());
        
        OAuth2 objAuthenticationInfo = (OAuth2)JSON.deserialize(res.getbody(), OAuth2.class);
        
        System.debug('objAuthenticationInfo'+objAuthenticationInfo);
        if(objAuthenticationInfo.access_token!=null) {
            Http h1 = new Http();
            HttpRequest req1 = new HttpRequest();
            req1.setHeader('Authorization','Bearer '+objAuthenticationInfo.access_token);
            req1.setHeader('Content-Type','application/json');
            req1.setHeader('accept','application/json');
            System.debug('serialize*****'+leadLoan);    
            req1.setBody(leadLoan);
            // req1.setBody('{"FirstName" : "Jigtggttttttar","FullName" : "Jittggar Dany","LastName" : "Datgtny","Phone" : "9883357585","Email" : "JJ35gtg53@gmail.com","GAgclid" : "GAgclid","TransactionType" : "TransactionType","WhatBringsYouInToday" : "WhatBringsYouInToday","Opportunity" : "Test","NumberofWatchInCollection" : "NumberofWatchInCollection","MatchedAccount" : "MatchedAccount","SignupWinAWatch" : "SignupWinAWatch","WatchImages" : "WatchImages","interestedinBrand" : "interestedinBrand","interestedinModel" : "interestedinModel","InterestedinProduct" : "InterestedinProduct","InterestedinProductListprice" : "InterestedinProductListprice","InterestedinProductUrl" : "InterestedinProductUrl","interestedinReferenceNumber" : "interestedinReferenceNumber","Boxes" : "Boxes","Papers" : "Papers","GCLID" : "GCLID","appcampaignid" : "appcampaignid","EbayUserName" : "EbayUserName","ircid" : "ircid","irtrackid" : "irtrackid","irpid" : "irpid","irclickid" : "irclickid","IPCountry" : "IPCountry","PageID" : "PageID","IPAddress" : "IPAddress","PageURL" : "PageURL","ReceiveUpdates" : "ReceiveUpdates","SubmissionDate" : "SubmissionDate","Comments" : "Comments","LeadSource" : "LeadSource","InitialDealDetails" : "InitialDealDetails","AdditionalInfo" : "AdditionalInfo","SellBrand" : "SellBrand","NameYourPriceASK" : "NameYourPriceASK","SellModel" : "SellModel","LastServiceDate" : "LastServiceDate","NewWatchModel" : "NewWatchModel","BoxesandPapers" : "BoxesandPapers","BoxOnly" : "BoxOnly","ModelNumber" : "ModelNumber","TypeofMetal" : "TypeofMetal","SellWatchSerial" : "SellWatchSerial","DialDescription" : "DialDescription","WatchReference" : "WatchReference","Images" : "Images","WatchBrand" : "WatchBrand","InventoryID" : "InventoryID","WatchModel" : "WatchModel","SKU" : "SKU","SalePrice" : "SalePrice","GACampaign" : "GACampaign","GASegment" : "GASegment","GAContent" : "GAContent","GATerm" : "GATerm","GAMedium" : "GAMedium","GAVisits" : "GAVisits","ReferringURL" : "ReferringURL","GaSource" : "GaSource","CloseDate" : "2018-01-01","StageName" : "Assets Received","Quantity" : "10","TotalPrice" : "10.00"}');//Send JSON body
            req1.setMethod('POST');
            String endPointLabel2 = System.Label.setEndpoint2;
            req1.setEndpoint(endPointLabel2);//URL will be your Salesforce REST API end point where you will do POST,PUT,DELETE orGET
            HttpResponse res1 = h1.send(req1);
            system.debug('RESPONSE_BODY*****'+res1.getbody());
            Type resultType = Type.forName('responseWrapper');
            responseWrapper deserializeResults = (responseWrapper)JSON.deserialize(String.valueOf(res1.getbody()),resultType);
            system.debug('oppName*****'+deserializeResults.status);
            if(deserializeResults.status == 'success') {
                List<SObject> sobjectList = new List<SObject>();
                
                
                
                List<Opportunity> listOpp = [Select id,Name,Originating_LeadGenLog__c,Originating_LeadGenLog__r.LAC_Opportunity_Created__c from Opportunity where Name =: deserializeResults.oppName  limit 1];
                System.debug('listOpp*****'+listOpp);
                if(listOpp != null && listOpp.size() > 0){
                    listOpp[0].LAC_Opportunity_Created__c = True;
                    listOpp[0].Originating_LeadGenLog__r.LAC_Opportunity_Created__c = True;
                    sobjectList.add(listOpp[0].Originating_LeadGenLog__r);
                    sobjectList.add(listOpp[0]);
                }
               
                Update sobjectList;
            }
        }
    }
    
    public static void prepareBody(Lead_Gen_Log__c leadRecord) 
    {
        OpportunityWrapperr LLW = new OpportunityWrapperr();
        LLW.FirstName = leadRecord.First_Name__c;
        LLW.FullName = leadRecord.First_Name__c + leadRecord.Last_Name__c; 
        LLW.LastName = leadRecord.Last_Name__c; 
        LLW.Phone = leadRecord.Phone__c; 
        LLW.Email = leadRecord.Email__c;
        LLW.MarketingBrand = System.label.MarketingBrand;
        LLW.WhatWouldLike = System.label.WhatWouldLike;
        LLW.LandingPage = System.label.LandingPage;
        LLW.TypeofProduct = System.label.TypeofProduct;
        LLW.CloseDate = String.valueOf(System.today() + 100);
        LLW.StageName = System.label.StageName;
        LLW.Quantity = System.label.Quantity;
        LLW.UnitPrice = System.label.UnitPrice;
        LLW.SellBrand = leadRecord.Sell_Brand__c;
        LLW.SellModel = leadRecord.Sell_Model__c;
        LLW.BoxesandPapers = leadRecord.Boxes_and_Papers__c;
        LLW.NameYourPriceASK = String.valueOf(leadRecord.Name_Your_Price_ASK__c);
        LLW.InitialDealDetails = leadRecord.Initial_Deal_Details__c ;
        LLW.opportunityid = leadRecord.Id;
        System.debug('LLW'+LLW);
        if(leadRecord.Images__c != null) {
           /* String imagesString = '';
            Integer index = leadRecord.Images__c.lastindexOf('/');
            String previousString = leadRecord.Images__c.subString(0,index+1);
            String subStringFianl = leadRecord.Images__c.subString(index+1,leadRecord.Images__c.length());
            String encodedURL = EncodingUtil.URLENCODE(subStringFianl,'UTF-8').replaceAll('\\+','%20');
            imagesString = previousString + encodedURL;*/
            String imagesToAdd ='';
            String imagestartString = '<img src="';
            String imageEndString = ' " width="300" height="200"></img>';
            
            Set<String> imagesSet = new Set<String>();
            List<String> imagesList = new List<String>();
            imagesSet.addAll(leadRecord.Images__c.split(','));  
            imagesList.addAll(imagesSet);
            
            for(Integer i=0;i<imagesList.size();i++) {
                if(i == 10)
                    continue;    
                imagesToAdd = imagesToAdd + imagestartString + imagesList[i] + imageEndString;
            }
            
           // imagesToAdd = imagesToAdd + imagestartString + imagesString + imageEndString;
            LLW.Images = imagesToAdd;
        }
        createLuxAssetDeal(JSON.serialize(LLW));
    }
    
    public static void updateCloneLeads(List<OpportunityWrapperr> oppWrap) {
        createLuxAssetDeal(JSON.serialize(oppWrap));
    }
}