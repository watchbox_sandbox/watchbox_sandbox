@RestResource(urlMapping='/Servicing/*')
global class sgServicingController {
    private static string inspectionId = 'Related_Inventory__c';
    
    private static string statusClause {
        get {
            return '(\'' + string.join(sgServicing.Statuses, '\', \'') + '\')';
        }
    }
    
    public static List<string> ProductFields {
        get {
            List<string> fieldsList = new List<string>{'Id',
                'AWS_Web_Images__c',
                'Cost__c',
                'MSP__c',
                'Ask__c',
                'Origination_Type__c',
                'Original_Dealer_Name__c',
                'Inventory_ID__c',
                'Description',
                'Papers__c',
                'Boxes__c',
                'Model__c',
                'Model__r.Name',
                'Model__r.AWS_Image_Array__c',
                'Model__r.Model_Brand__r.Name',
                'Model__r.Reference__c',
                'Name',
                'Product_Type__c',
                'Status__c',
                'LastModifiedDate',
                'QC_Failed__c',
                'Value__c',
                
                'Deal__r.Salesperson__r.Name__c',
                'Deal__r.Salesperson__r.Profile_Picture__c',
                'Deal__r.Account.Name',
                'Deal__r.Type',
                'Deal__r.Contact_Record__c'
            };
                
            List<string> prefixed = new List<string>();    
            
            for (string f : fieldsList) {
            	prefixed.add('Product__r.' + f);
            }
            
            return prefixed;
    	}
    }
    
    public static List<string> Fields { 
        get {
            List<string> suffixes = new List<string>{
            	'StartDate',
                'EndDate',
                'TimeSeconds'
            };
		            
     		List<string> servicingFields = new List<string>();       
            
            for (string status : sgServicing.Statuses) {
                for (string s : suffixes) {
                    string statusField = status.replace(' ', '').replace('/', '') + s + '__c';
                    servicingFields.add(statusField);
                }
            }
            
            return servicingFields;
        }
    }
    
    public static List<string> InspectionFields {
        get {
            return new List<string>{
                'Parts_Needed__c',
                'Overhaul_Needed__c',
                'Outsource_Work__c',
                'Regulation__c',
                'Touchup_No_Decasing__c',
                'No_Work_Needed__c'
                
            };
        }
    }
    
    public static string FieldsQuery {
        get {
            return 'Id, Watchmaker__c, Watchmaker__r.Name__c, Watchmaker__r.Profile_Picture__c, Timer_Status__c, Order__c, ' + 
                string.join(Fields, ', ') + ', ' + string.join(ProductFields, ', ');
        }
    }
    
	@HttpGet
    global static List<Map<string, string>> get() {
        sgAuth.check();
        
        List<Map<string, string>> products = new List<Map<string, string>>();

        string query = 'select ' + FieldsQuery + ' from Servicing__c where Product__r.Status__c in ' + statusClause
            + ' and Product__r.Location__c = \'' + sgAuth.user.Location__c + '\'';
        
        Map<string, string> params = RestContext.request.params.clone();
        params.put('limit', '300');
        
        products = sgSerializer.serializeFromQuery(query, params);
        
        addNotes(products);
        addInspectionFields(products);
        
        return products;
    }
    
    
    private static void addInspectionFields(List<Map<string, string>> products) {
    	List<string> productIds = new List<string>();
        for (Map<string, string> p : products) {
            p.put('Product__rModelImage', sgUtility.getMainImage(p.get('Product__rModel__rAWS_Image_Array__c')));
            
            productIds.add(p.get('Product__rId'));
        }

        List<Inspection__c> inspectionsList = Database.query('select Id, ' + inspectionId + ', ' + string.join(InspectionFields, ', ') + ' from Inspection__c where Related_Inventory__c in :productIds');
        Map<string, sObject> inspections = sgUtility.MapBy(inspectionId, inspectionsList);

        for (Map<string, string> p : products) {
    		if (inspections.containsKey(p.get('Product__rId'))) {
                Inspection__c i = (Inspection__c) inspections.get(p.get('Product__rId'));
                
                p.put('Inspection__c', i.Id);
                
	            for (string f : InspectionFields) {
                    object val = i.get(f);
                    
                    if (i != null) {
                        p.put(f, string.valueOf(val));
                    }
                }
            }
        }
    }
    
    private static void addNotes(List<Map<string, string>> products) {
        List<string> ids = new List<string>();
        for (Map<string, string> p : products) {
            ids.add(p.get('Id'));
        }
        
        List<Note> notes = [select Id, ParentId, Body from Note where ParentId in :ids];
        Map<string, sObject> notesMap = sgUtility.MapBy('ParentId', notes);
        
        for (Map<string, string> p : products) {
            string key = p.get('Id');
            if (notesMap.containsKey(key)) {
                Note n = (Note) notesMap.get(key);
                
                p.put('NoteId', n.Id);
            	p.put('Note', n.Body);
            }
        }
    }
	
	// change wathcmaker
    @HttpPut
    global static string put(string id, string watchmakerId) {
        sgAuth.hasRole('Admin,Watchmaker Lead');
        
        Team_Member__c watchmaker = [select Id, Profile_Picture__c from Team_Member__c where Id = :watchmakerId and Role__r.Name = 'Watchmaker'];
        
        Servicing__c service = [select Id, Watchmaker__c from Servicing__c where Id = :id];

        service.Watchmaker__c = watchmakerId;
        update service;

        return watchmaker.Profile_Picture__c;
    }
    
    //change status
    @HttpPost
    global static string post(string id, string oldStatus, string newStatus) {
        sgAuth.hasRole('Admin,Watchmaker Lead, Watchmaker');
        
        oldStatus = oldStatus.replaceAll(' ', '');
        
        string timeField = oldStatus + 'TimeSeconds__c';
        string startField = oldStatus + 'StartDate__c';
        string endField = oldStatus + 'EndDate__c';
        
        string query = 'select Product__r.Id, Product__r.Status__c, Id, ' + timeField + ', ' + startField + ', ' + endField + ' from Servicing__c where Id = :id';
        
        Servicing__c s = Database.query(query);
        
        DateTime startDate = (DateTime) s.get(startField);
        
        if (startDate != null && oldStatus != newStatus) {
            DateTime endDate = system.now();
            
            s.put(endField, endDate);
            
            s.Timer_Status__c = 'Stopped';
            
            integer seconds = sgUtility.secondsBetween(startDate, endDate);
            
            s.put(timeField, seconds);
            update s;
        }
        
        Product2 p = new Product2( 
            Id = s.Product__r.Id,
            Status__c = newStatus,
            QC_Failed__c = false
        );
        update p;
        
        return 'Lane updated to ' + newStatus;
    }
    
    //start/stop
    @HttpPatch
    global static string patch(string id, string status, string operation) {
        sgAuth.hasRole('Admin,Watchmaker Lead, Watchmaker');
        
        string timeField = status + 'TimeSeconds__c';
        string startField = status + 'StartDate__c';
        string endField = status + 'EndDate__c';
        
        string query = 'select ' + timeField + ', ' + startField + ', ' + endField + ' from Servicing__c where Id = :id';
        
        Servicing__c s = Database.query(query);
        
        s.Timer_Status__c = operation;
        
        if (operation.toLowerCase() == 'started') {
            
            if (s.get(startField) == null) {
	            DateTime startTime = system.now();
            
    	        s.put(startField, startTime);
            }
            
            update s;
            
            return string.valueOf( ((DateTime) s.get(startField) ).getTime() );
        } else if (operation.toLowerCase() == 'stopped') {
            if (s.get(startField) == null) {
                throw new sgException(status + ' has not been started');
            }
            
            DateTime startTime = (DateTime) s.get(startField);
            DateTime endTime = system.now();
            
            integer seconds = sgUtility.secondsBetween(startTime, endTime);
            
            s.put(endField, endTime);
            s.put(timeField, seconds);
        }
        
        update s;
        
        return null;
    }
        
}