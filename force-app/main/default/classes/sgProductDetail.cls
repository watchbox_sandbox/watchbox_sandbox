global class sgProductDetail {
    global class Post {
        public string Name;
        public string ExternalId;
    }
    
    public Map<string, string> Fields {get; set;}
    
    public List<Map<string, string>> Log {get; set;}

    public List<Map<string, string>> Notes {get; set;}
    
    public List<Post> Posts {get; set;}
}