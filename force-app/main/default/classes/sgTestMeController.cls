@isTest
public class sgTestMeController {
    static testMethod void get() {   
        sgAuth.skipForTest = false;
        
	    Team_Member__c tmObj = sgDataSeeding.TeamMembers(1).get(0);
        tmObj.Profile_Picture__c = 'http://test.com';

        tmObj.Profile_Picture__c = 'http://google.com';
        
        string token = 'token';
        tmObj.Token__c = token;

        tmObj.Token_Expiration__c = system.now().addDays(14);
        
        insert tmObj;
        
        List<string> fields = new List<string> {
            'First_Name__c',
			'Last_Name__c',
            'Work_Email__c',
            'Profile_Picture__c'
        };
        
        RestRequest req = new RestRequest();
        req.addHeader('sgtoken', token);
        RestContext.request = req;
        Map<string,string> serialized = sgMeController.get();
		
        Map<string,string> tm = new Map<string,string>();
        
        for (string f : fields) {
            tm.put(f, (string) tmObj.get(f));
        }
        
        for (string f : fields) {
            system.debug(f);
            system.debug(tm.get(f));
            system.debug(serialized.get(f));
            system.assertEquals(tm.get(f), serialized.get(f));
        }
    }
}