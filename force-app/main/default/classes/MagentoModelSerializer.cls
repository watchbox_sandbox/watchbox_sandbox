public with sharing class MagentoModelSerializer {
    public static MagentoModelDTO modelToDTO(Model__c providedModel) {
        // Re-fetch to get all of our required properties.
        Model__c model = providedModel;
        /*
        [
            SELECT
                Id,
                Name,
                URL_Slug__c,
                Verified__c,
                AWS_Image_Array__c,
                Also_Known_As__c,
                Also_Known_As_2__c,
                Bezel__c,
                Bracelet_Material__c,
                Caliber_Details__c,
                Case_Color__c,
                Case_Material__c,
                Case_Size__c,
                Caseback__c,
                Complications__c,
                Description__c,
                Dial_Color__c,
                Dial_Type__c,
                Gender__c,
                Model_Write_Up__c,
                Movement_Type__c,
                Tonneau_Case_Size__c,
                Watch_Case_Shape__c,
                Watch_Display_Type__c,
                Water_Resistance_Rating__c
            FROM Model__c
            WHERE Id = :providedModel.Id
        ];
		*/
        
        // Create an empty DTO instance.
        MagentoModelDTO mdto = new MagentoModelDTO();

        // Fill er up.
        mdto.sfId = model.Id;
        mdto.name = model.Name;
        mdto.slug = model.URL_Slug__c;
        mdto.verified = model.Verified__c;
        mdto.awsImageArray = model.AWS_Image_Array__c;
        mdto.alsoKnownAs = model.Also_Known_As__c;
        mdto.alsoKnownAs2 = model.Also_Known_As_2__c;
        mdto.bezel = model.Bezel__c;
        mdto.braceletMaterial = model.Bracelet_Material__c;
        mdto.caliberDetails = model.Caliber_Details__c;
        mdto.caseColor = model.Case_Color__c;
        mdto.caseMaterial = model.Case_Material__c;
        mdto.caseSize = model.Case_Size__c;
        mdto.caseback = model.Caseback__c;
        mdto.complications = model.Complications__c;
        mdto.description = model.Description__c;
        mdto.dialColor = model.Dial_Color__c;
        mdto.dialType = model.Dial_Type__c;
        mdto.gender = model.Gender__c;
        mdto.modelWriteUp = model.Model_Write_Up__c;
        mdto.movementType = model.Movement_Type__c;
        mdto.tonneauCaseSize = model.Tonneau_Case_Size__c;
        mdto.caseShape = model.Watch_Case_Shape__c;
        mdto.displayType = model.Watch_Display_Type__c;
        mdto.waterResistanceRating = model.Water_Resistance_Rating__c;

        return mdto;
    }

    public static String modelToDTOJson(List<Model__c> model) {
        List<MagentoModelDTO> mdtos = new List<MagentoModelDTO>();

        for (Model__c s: model) {
            MagentoModelDTO mdto = MagentoModelSerializer.modelToDTO(s);
            mdtos.add(mdto);
        }

        Map<String, List<MagentoModelDTO>> batchData = new Map<String, List<MagentoModelDTO>>{
        	'models' => mdtos
        };

        String dataString = JSON.serialize(batchData);

        return dataString;
    }
}