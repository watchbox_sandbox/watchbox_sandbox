global class NoteBatchScheduler  implements Schedulable {  
    
     global void execute(SchedulableContext SC) {
         
     	NoteBatch bc = new NoteBatch();
        Database.executeBatch(bc);
     }

}