@RestResource(urlMapping='/MagentoTranslationExporter/*')
global class MagentoTranslationExporterController {
    @HttpGet
    global static void getTranslations() {
        String lang = RestContext.request.params.get('lang');

        RestContext.response.addHeader('Content-Type', 'text/csv');

        Translation__c[] translations = [
            SELECT
                Vocabulary_Word__c,
                Chinese_Traditional_Word__c,
                Chinese_Simplified_Word__c,
                Spanish_Word__c,
                German_Word__c,
                French_Word__c
            FROM
                Translation__c
        ];

        RestContext.response.responseBody = MagentoCsvHelpers.translationsToCsv(translations, lang);
    }
}