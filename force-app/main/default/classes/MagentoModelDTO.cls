public with sharing class MagentoModelDTO {
    public String sfId;
    public String name;
    public String slug;
    public Boolean verified;
    public String awsImageArray;
    public String alsoKnownAs;
    public String alsoKnownAs2;
    public String bezel;
    public String braceletMaterial;
    public String caliberDetails;
    public String caseColor;
    public String caseMaterial;
    public Decimal caseSize;
    public String caseback;
    public String complications;
    public String description;
    public String dialColor;
    public String dialType;
    public String gender;
    public String modelWriteUp;
    public String movementType;
    public String tonneauCaseSize;
    public String caseShape;
    public String displayType;
    public String waterResistanceRating;
}