@isTest
public class LuxAssetCallOutMockTest{
    public static testmethod void testLoanLeadCallout() {
        Lead_Gen_Log__c leadGenLog = new Lead_Gen_Log__c();
        leadGenLog.First_Name__c = 'Demo Name';
        leadGenLog.Last_Name__c = 'LastName Test'; 
        leadGenLog.Phone__c = '6958452158'; 
        leadGenLog.Email__c = 'ajinkyademo@gmail.com';
        leadGenLog.Images__c = 'https://www.thewatchbox.com/wp-content/uploads/ninja-forms/2/Rolex-Daytona-3-2.jpg';
        
        LuxAssetCallOutMock fakeResponse = new LuxAssetCallOutMock(200,
                                                                   'Complete',
                                                                   '{"FirstName" : "Jigtggttttttar","FullName" : "Jittggar Dany","LastName" : "Datgtny","Phone" : "9883357585","Email" : "JJ35gtg53@gmail.com","GAgclid" : "GAgclid","TransactionType" : "TransactionType","WhatBringsYouInToday" : "WhatBringsYouInToday","Opportunity" : "Test","NumberofWatchInCollection" : "NumberofWatchInCollection","MatchedAccount" : "MatchedAccount","SignupWinAWatch" : "SignupWinAWatch","WatchImages" : "WatchImages","interestedinBrand" : "interestedinBrand","interestedinModel" : "interestedinModel","InterestedinProduct" : "InterestedinProduct","InterestedinProductListprice" : "InterestedinProductListprice","InterestedinProductUrl" : "InterestedinProductUrl","interestedinReferenceNumber" : "interestedinReferenceNumber","Boxes" : "Boxes","Papers" : "Papers","GCLID" : "GCLID","appcampaignid" : "appcampaignid","EbayUserName" : "EbayUserName","ircid" : "ircid","irtrackid" : "irtrackid","irpid" : "irpid","irclickid" : "irclickid","IPCountry" : "IPCountry","PageID" : "PageID","IPAddress" : "IPAddress","PageURL" : "PageURL","ReceiveUpdates" : "ReceiveUpdates","SubmissionDate" : "SubmissionDate","Comments" : "Comments","LeadSource" : "LeadSource","InitialDealDetails" : "InitialDealDetails","AdditionalInfo" : "AdditionalInfo","SellBrand" : "SellBrand","NameYourPriceASK" : "NameYourPriceASK","SellModel" : "SellModel","LastServiceDate" : "LastServiceDate","NewWatchModel" : "NewWatchModel","BoxesandPapers" : "BoxesandPapers","BoxOnly" : "BoxOnly","ModelNumber" : "ModelNumber","TypeofMetal" : "TypeofMetal","SellWatchSerial" : "SellWatchSerial","DialDescription" : "DialDescription","WatchReference" : "WatchReference","Images" : "Images","WatchBrand" : "WatchBrand","InventoryID" : "InventoryID","WatchModel" : "WatchModel","SKU" : "SKU","SalePrice" : "SalePrice","GACampaign" : "GACampaign","GASegment" : "GASegment","GAContent" : "GAContent","GATerm" : "GATerm","GAMedium" : "GAMedium","GAVisits" : "GAVisits","ReferringURL" : "ReferringURL","GaSource" : "GaSource","CloseDate" : "2018-01-01","StageName" : "Assets Received","Quantity" : "10","TotalPrice" : "10.00"}',null);
        LuxAssetLoanLead.prepareBody(leadGenLog);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
    }
    	
    
    public static testmethod void testOuthCallout() {
        
        OpportunityWrapperr OWrapp = new OpportunityWrapperr();
        OWrapp.FirstName = 'TestFirstName';
        OWrapp.LastName = 'TestLastName';
        OWrapp.MarketingBrand = 'WatchBox';
        OWrapp.WhatWouldLike = 'I\'d like to get a loan';    
        OWrapp.Email = 'testdemo@gmail.com';   
        OWrapp.StageName = 'Assets Received';   
        OWrapp.LandingPage   = 'https://www.thewatchbox.com/get-a-loan/';
        OWrapp.Quantity = '1';
        OWrapp.SellBrand = 'Rolex';
        OWrapp.Boxes = 'Test';
        OWrapp.NameYourPriceASK ='10.10';
        OWrapp.Images = 'https://www.thewatchbox.com/wp-content/uploads/ninja-forms/2/Rolex-Daytona-3-2.jpg';
        OWrapp.UnitPrice = '0';
        OWrapp.CloseDate = '2018-01-01';
        OWrapp.GAgclid = '';
        OWrapp.TransactionType = '';
        OWrapp.WhatBringsYouInToday = '';
        OWrapp.Opportunity = '';
        OWrapp.NumberofWatchInCollection = '';
        OWrapp.MatchedAccount = '';
        OWrapp.SignupWinAWatch = '';
        OWrapp.WatchImages = '';
        OWrapp.interestedinBrand = '';
        OWrapp.interestedinModel = '';
        OWrapp.InterestedinProduct = '';
        OWrapp.InterestedinProductListprice = '';
        OWrapp.InterestedinProductUrl = '';
        OWrapp.interestedinReferenceNumber  = '';
        OWrapp.Papers = '';
        OWrapp.GCLID = '';
        OWrapp.appcampaignid = '';
        OWrapp.EbayUserName = '';
        OWrapp.ircid = '';
        OWrapp.irtrackid = '';
        OWrapp.irpid = '';
        OWrapp.irclickid = '';
        OWrapp.IPCountry = '';
        OWrapp.PageID = '';
        OWrapp.IPAddress = '';
        OWrapp.PageURL = '';
        OWrapp.ReceiveUpdates = '';
        OWrapp.SubmissionDate = '';
        OWrapp.Comments = '';
        OWrapp.LeadSource = '';
        OWrapp.AdditionalInfo = '';
        OWrapp.LastServiceDate = '';
        OWrapp.NewWatchModel = '';
        OWrapp.BoxOnly = '';
        OWrapp.ModelNumber = '';
        OWrapp.TypeofMetal = '';
        OWrapp.SellWatchSerial = '';
        OWrapp.DialDescription = '';
        OWrapp.WatchReference = '';
        OWrapp.WatchBrand = '';
        OWrapp.InventoryID = '';
        OWrapp.WatchModel = '';
        OWrapp.SKU = '';
        OWrapp.SalePrice = '';
        OWrapp.GACampaign = '';
        OWrapp.GASegment = '';
        OWrapp.GAContent = '';
        OWrapp.GATerm = '';
        OWrapp.GAMedium = '';
        OWrapp.GAVisits = '';
        OWrapp.ReferringURL = '';
        OWrapp.GaSource = '';
        OWrapp.Accountid = '';
        OWrapp.Contactid = '';
        OWrapp.opportunityid = '';
        responseWrapper responseWrapp = new responseWrapper();
      
        LuxAssetCallOutMock fakeResponse = new LuxAssetCallOutMock(200,
                                                                   'success',
                                                                   '{"access_token" : "12545844"}',null);
        LuxAssetLoanLead.createLuxAssetDeal(JSON.serialize(OWrapp));
        Test.setMock(HttpCalloutMock.class, fakeResponse);
    }
}