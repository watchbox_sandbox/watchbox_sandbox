@isTest
public class sgTestSiteToSiteController {
	static List<Product2> setup() {
        List<Product2> products = sgDataSeeding.ProductsWithBrands();
        string l = sgAuth.user.Location__c;
        
        for (Product2 p : products) {
            p.Location__c = l;
        }
        
        insert products;
        
        return products;
    }
    
    static testMethod void get() {
        List<Product2> products = setup();
        
        sgSiteToSiteController.Transfers transfers = sgSiteToSiteController.get();
        
        system.assertEquals(products.size(), transfers.CurrentTransfer.S2SDetails__r.size());
    }
    
    static testMethod void post() {
        List<Product2> products = setup();
        
        sgSiteToSiteController.Transfers transfers = sgSiteToSiteController.get();
        
        string field = 'Shipping_Status__c';
        string val = 'In Transit';
        string id  = transfers.CurrentTransfer.Id;
        
        sgSiteToSiteController.post(id, field, val);
        
        Site_To_Site__c postTransfer = Database.query('select Id, ' + field + ' from Site_To_Site__c where Id = :id limit 1');
                
        system.assertEquals((string) postTransfer.get(field), val);
    }
    
    
    static testMethod void put() {
        List<Product2> products = setup();
        
        sgSiteToSiteController.Transfers transfers = sgSiteToSiteController.get();

        List<string> ids = new List<string>{
            transfers.CurrentTransfer.S2SDetails__r.get(0).Id
        };
            
        string scanType = 'Manual';
	
        sgSiteToSiteController.put(ids, scanType);
        
        S2SDetails__c detail = [select Id, Scan_Type__c from S2SDetails__c where Id = :ids.get(0) limit 1];

        system.assertEquals(scanType, detail.Scan_Type__c);
    }
    
    static testMethod void delete1() {
        List<Product2> products = setup();
        
        sgSiteToSiteController.Transfers transfers = sgSiteToSiteController.get();

        List<string> ids = new List<string>{
            transfers.CurrentTransfer.S2SDetails__r.get(0).Id
        };
            
        string scanType = 'Manual';
	
        sgSiteToSiteController.put(ids, scanType);
        
        S2SDetails__c detail = [select Id, Scan_Type__c from S2SDetails__c where Id = :ids.get(0) limit 1];

        system.assertEquals(scanType, detail.Scan_Type__c);
        
        RestRequest req = new RestRequest();
        req.addParameter('id', ids.get(0));
        RestContext.request = req;
        
        sgSiteToSiteController.delete1();
        
        detail = [select Id, Scan_Type__c from S2SDetails__c where Id = :ids.get(0) limit 1];

        system.assertEquals(null, detail.Scan_Type__c);
    }
    
    
    static testMethod void patch() {
        List<Product2> products = setup();
        
        sgSiteToSiteController.Transfers transfers = sgSiteToSiteController.get();
        
        sgSiteToSiteController.patch(transfers.CurrentTransfer.Id);
        
        Site_To_Site__c transfer = [select Id, Committed__c from Site_To_Site__c limit 1];
        
        system.assertEquals(True, transfer.Committed__c);
    }
}