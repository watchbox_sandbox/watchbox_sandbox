@isTest
public class sgTestBatchStatusUpdateController {
    static testMethod void get() {
        Product2 product = new Product2(Name = 'test');
         
        insert product;
        
        RestContext.request = new RestRequest();
        
        RestContext.response = new RestResponse();
        
        RestContext.request.addParameter('id', product.Inventory_Id__c);
        
		sgBatchStatusUpdateController.get();
        
        system.assert(RestContext.response.statusCode == 200);
    }
    
    static testMethod void get404() {
        Product2 product = new Product2(Name = 'test');
         
        insert product;
        
        RestContext.request = new RestRequest();
        
        RestContext.response = new RestResponse();
        
        RestContext.request.addParameter('id', '404');
        
		sgBatchStatusUpdateController.get();
        
        system.assert(RestContext.response.statusCode == 404);
    }
    
    static testMethod void patch() {
        Product2 product = new Product2(Name = 'test', Status__c = 'Sold', WUW_Notes__c = 'note');

        insert product;
        
        List<Product2> products = new List<Product2>();
        
        products.add(product);
       
        sgBatchStatusUpdateController.patch(products, 'a note');
        
        List<Note> notes = [SELECT Body FROM Note WHERE ParentId = :product.Id];
        system.assert(product != null);
        System.assertEquals(1, notes.size());
        System.assertEquals('a note', notes[0].Body);
    }
}