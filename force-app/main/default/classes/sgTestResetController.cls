@isTest
public class sgTestResetController {
    static testMethod void forgot() {
        Team_Member__c t = sgDataSeeding.TeamMembers(1).get(0);
        sgAuth.setPasswordHash(t);
        
        t.Reset_Token__c = null;
        insert t;
        
        sgResetController.forgot(t.Work_Email__c);
        
        Team_Member__c dbT = [select Reset_Token__c from Team_Member__c where Id = :t.Id];
        
        system.assert(dbT != null);
    }
    
    static testMethod void reset() {
        Team_Member__c t = sgDataSeeding.TeamMembers(1).get(0);
        sgAuth.setPasswordHash(t);
        string oldPassword = t.Password__c;
        
        system.assert(t.Reset_Token__c == null);

        string token = 'testtoken';
        
        t.Reset_Token__c = token;
        
        insert t;
        
        sgResetController.reset(token, 'newpassword');
        
        Team_Member__c dbT = [select Reset_Token__c, Password__c from Team_Member__c where Id = :t.Id];
        
        system.debug(oldPassword);
        system.debug(dbt.Password__c);
        
		system.assertEquals(null, dbT.Reset_Token__c);
        system.assert(oldPassword != dbT.Password__c);
    }
    
    static testMethod void test() {
        sgResetController.test();
    }
}