global class sgSearchResult {
    public List<Map<string, string>> Accessories {get; set;}
    public List<Map<string, string>> Brands {get; set;}
    public List<Map<string, string>> Customers {get; set;}
    public List<Map<string, string>> Models {get; set;}
    public List<Map<string, string>> Products {get; set;}
    public List<Map<string, string>> Users {get; set;}
    public List<Map<string, string>> Vendors {get; set;}
}