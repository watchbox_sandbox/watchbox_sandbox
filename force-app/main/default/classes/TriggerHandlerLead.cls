public abstract class TriggerHandlerLead {
    
    public virtual void execute() {
        if(Trigger.isBefore) {
            if(Trigger.isInsert)            
                beforeInsert();            
        }
        else if(Trigger.isAfter) {
            if(Trigger.isInsert)            
                afterInsert();            
        }
    }
    
    public virtual void beforeInsert(){}
    public virtual void afterInsert(){}
    
}