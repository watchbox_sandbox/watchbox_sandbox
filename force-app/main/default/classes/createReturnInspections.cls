public class createReturnInspections {
	public ApexPages.StandardController stdController;
    public Id returnId;
    
    public createReturnInspections(ApexPages.StandardController controller){
        stdController = controller;
        returnId = stdController.getId();
    }
    
    public pageReference createInspectionsForReturns(){
        //initialize variables
        String recordTypeName = 'Inspection - Repairs & Returns';
        RecordType returnInspection = [SELECT id, name FROM RecordType WHERE name = :recordTypeName];
        List<Inspection__c> newInspections = new List<Inspection__c>();
        //query all intake and inspection records linked to current deal. Only intake records that do not have an inspection
        //record looking up to them will be used
        List<Inspection__c> currentInspections = [SELECT id, Intake__c FROM Inspection__c WHERE  return__c = :returnId];
        List<Id> intakesWithInspections = New List<Id>();
        for(Integer x=0;x<currentInspections.size();x++){
            intakesWithInspections.add(currentInspections[x].Intake__c);
        }
        List<Intake__c> currentIntakes = [SELECT id, Model__c, deal__c, Product__c FROM Intake__c WHERE return__c = :returnId AND id NOT IN :intakesWithInspections];
        system.debug(currentIntakes.size()+' '+currentIntakes);
        if(currentIntakes.size() == 0){
            PageReference refreshOpp = new PageReference('/'+returnId);
        	refreshOpp.setRedirect(true);
        	return refreshOpp;
        }
        //iterate through them. create an inspection record for each instake beyond the current number of inspection records. Tie it to the current opp and current intake
        for(Integer i=0;i<currentIntakes.size();i++){
            Inspection__c newInspection = new Inspection__c(intake__c = currentIntakes[i].id, opportunity__c = currentIntakes[i].deal__c, RecordType = returnInspection, return__c = returnId, Related_Inventory__c= currentIntakes[i].Product__c);
            newInspections.add(newInspection);
            system.debug('List of new inspections created: '+newInspections);
        }
        insert newInspections;
        //and now we return to the deal record, making the whole transaction look like a page refresh
        PageReference refreshOpp = new PageReference('/'+returnId);
        refreshOpp.setRedirect(true);
        return refreshOpp;
        
    }
}