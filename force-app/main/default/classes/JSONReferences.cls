public class JSONReferences {

    public String id {get;set;} 
    public String creator {get;set;} 
    public Long creationInstant {get;set;} 
    public String modifier {get;set;} 
    public Long modifiedInstant {get;set;} 
    public String baseReferenceNumber {get;set;} 
    public String brandId {get;set;} 
    public String brandFamilyId {get;set;} 

    public JSONReferences(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'id') {
                        id = parser.getText();
                    } else if (text == 'creator') {
                        creator = parser.getText();
                    } else if (text == 'creationInstant') {
                        creationInstant = parser.getLongValue();
                    } else if (text == 'modifier') {
                        modifier = parser.getText();
                    } else if (text == 'modifiedInstant') {
                        modifiedInstant = parser.getLongValue();
                    } else if (text == 'baseReferenceNumber') {
                        baseReferenceNumber = parser.getText();
                    } else if (text == 'brandId') {
                        brandId = parser.getText();
                    } else if (text == 'brandFamilyId') {
                        brandFamilyId = parser.getText();
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSONReferences consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    
    public static List<JSONReferences> parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return arrayOfJSONReferences(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    

    private static List<JSONReferences> arrayOfJSONReferences(System.JSONParser p) {
        List<JSONReferences> res = new List<JSONReferences>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new JSONReferences(p));
        }
        return res;
    }
}