@isTest(seeAllData=true)
public class modelsByBrandTest {
	
    public static testMethod void testSearchBrands() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.Brand='zenith';
        test.startTest();
        	controller.search();
        test.stopTest();
}
    
    public static testMethod void testFilterBy() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.getAllBrands();
        	controller.getVerifiedStatus();
        	controller.Brand='zenith';
        	controller.filterBy='elite';
        test.startTest();
        	controller.search();
        	controller.save();
        test.stopTest();
}
    public static testMethod void testVerifiedFilter() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.getAllBrands();
        	controller.getVerifiedStatus();
        	controller.Brand='zenith';
        	controller.filterBy='Ladies';
        	controller.isVerified='true';
        test.startTest();
        	controller.search();
        test.stopTest();
}
    
    public static testMethod void testVerifiedUnfiltered() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.getAllBrands();
        	controller.Brand='zenith';
        	controller.getVerifiedStatus();
        	controller.isVerified='true';
        test.startTest();
        	controller.search();
        test.stopTest();
}
    
    public static testMethod void testUnverifiedFilter() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.getAllBrands();
        	controller.getVerifiedStatus();
        	controller.Brand='zenith';
        	controller.filterBy='Ladies';
        	controller.isVerified='false';
        test.startTest();
        	controller.search();
        test.stopTest();
}
    public static testMethod void testUnverifiedUnfiltered() {

	        PageReference pageRef = Page.modelsByBrand;

	        Test.setCurrentPage(pageRef);
     

	        modelsByBrand controller = new modelsByBrand();
        	controller = new modelsByBrand();
        	controller.getAllBrands();
        	controller.Brand='zenith';
        	controller.getVerifiedStatus();
        	controller.isVerified='false';
        test.startTest();
        	controller.search();
        test.stopTest();
}
}