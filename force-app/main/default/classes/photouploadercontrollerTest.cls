@isTest
public class photouploadercontrollerTest
{
    static testMethod void testAttachments()
        
    {
        test.startTest();
        
        PageReference pageRef = Page.photouploader;
        Account cse=new Account(name ='test');
        insert cse;
        
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('id',cse.id);
        
        
        photouploadercontroller controller=new photouploadercontroller(new ApexPages.StandardController(cse));
        controller.profilePicFile =Blob.valueOf('Unit Test Attachment Body'); 
        Attachment a = new Attachment(parentId = controller.pageContact.id, name = 'profilePhoto.jpg', body = controller.profilePicFile);
        insert a;
        
        
        
        
        
        controller.saveFile();
        
        test.stopTest();
        
    }
    static testMethod void testAttachments1()
        
    { 
        test.startTest();
        PageReference pageRef = Page.photouploader;
        Account cse2=new Account(name ='test');
        insert cse2;
        
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('id',cse2.id);
        
        
          photouploadercontroller controller=new photouploadercontroller(new ApexPages.StandardController(cse2));
         controller.profilePicFile=Blob.valueOf('Unit Test Attachment Body'); 
        
        
        
      
        
        Attachment a = new Attachment(parentId = controller.pageContact.id, name = 'profilePhoto.jpg', body =  controller.profilePicFile);
        insert a;
        
        controller.DeleteFile();
        String  id1 = controller.currentPicture;
        
        system.assertEquals(null,id1);
        test.stopTest();
        
        
    }
    static testMethod void testAttachments2()
        
    { 
        test.startTest();
        PageReference pageRef = Page.photouploader;
        Account cse1=new Account(name ='test');
        insert cse1;
        
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('id',cse1.id);
        
        
          photouploadercontroller controller=new photouploadercontroller(new ApexPages.StandardController(cse1));
         controller.profilePicFile=Blob.valueOf('Unit Test Attachment Body'); 
        
        
        
      
        
        Attachment a = new Attachment(parentId = controller.pageContact.id, name = 'profilePhoto.jpg', body =  controller.profilePicFile);
        insert a;
        
       
     
     controller.toggle();
     test.stopTest();
    }
    
}