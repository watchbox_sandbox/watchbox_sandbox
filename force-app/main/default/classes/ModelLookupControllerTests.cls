@isTest
public class ModelLookupControllerTests {
    static testmethod void searchModels_byName_test() {
        Model__c model = TestFactory.buildModel();
        model.model_brand__c = null;
        insert model;

        Model__c result = [SELECT Id FROM Model__c WHERE Name=: model.Name];
        
        System.assertNotEquals(null, result);
        
        Id[] fixedSearchResults = new Id[]{result.Id};
        Test.setFixedSearchResults(fixedSearchResults);
        
        ModelLookupController ctrl = new ModelLookupController();
        ctrl.SearchTerm = model.Name;
        ctrl.searchModels();
        
        System.assertEquals(1, ctrl.Models.size());
    }
    
    static testmethod void searchModels_byReference_test() {
        string reference = '1234';
        Model__c model = TestFactory.buildModel();
        model.Reference__c = reference;
        model.model_brand__c = null;
        insert model;

        Model__c result = [SELECT Id FROM Model__c WHERE Reference__c=:reference];

        System.assertNotEquals(null, result);
        
        Id[] fixedSearchResults = new Id[]{result.Id};
        Test.setFixedSearchResults(fixedSearchResults);
        
        ModelLookupController ctrl = new ModelLookupController();
        ctrl.SearchTerm = reference;
        ctrl.searchModels();
        
        System.assertEquals(1, ctrl.Models.size());
    }

    static testmethod void searchProducts_exception_test() {
        PageReference lookupPage = Page.ModelLookup;
		Test.setCurrentPage(lookupPage); 
        
        //Creating exception scenario
        ModelLookupController ctrl = new ModelLookupController();
        ctrl.SearchTerm = null;
        ctrl.searchModels();
        
        //Assert exception was thrown
        System.assert(ApexPages.hasMessages());
    }
}