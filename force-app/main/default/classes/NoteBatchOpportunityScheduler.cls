global class NoteBatchOpportunityScheduler  implements Schedulable {  
    
     global void execute(SchedulableContext SC) {
         
     	NoteBatchForOpportunity bc = new NoteBatchForOpportunity();
        Database.executeBatch(bc);
     }

}