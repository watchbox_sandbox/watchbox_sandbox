@isTest
private class MagentoTestCsvHelpers {
    @testSetup
    static void init() {
        Translation__c tequila = new Translation__c(
            Vocabulary_Word__c = 'Tequila',
            Chinese_Simplified_Word__c = '龙舌兰酒',
            Chinese_Traditional_Word__c = '龍舌蘭酒'
        );

        insert tequila;

        Translation__c pinkQuartzCrystal = new Translation__c(
            Vocabulary_Word__c = 'Pink Quartz Crystal',
            Chinese_Simplified_Word__c = '粉红色石英晶体',
            Chinese_Traditional_Word__c = '粉紅色石英晶體'
        );

        insert pinkQuartzCrystal;

        Translation__c threeYearCalendar = new Translation__c(
            Vocabulary_Word__c = 'Three-Year Calendar',
            Chinese_Simplified_Word__c = '三年历',
            Spanish_Word__c = 'word in spanish',
            German_Word__c = 'word in german',
            French_Word__c = 'word en fraçais'
        );

        insert threeYearCalendar;
    }

    @isTest
    static void testStringToCsv() {
        System.assertEquals('"Foo""bar"', MagentoCsvHelpers.escape('Foo"bar'));
    }

    @isTest
    static void testBasicTranslationToCsvRow() {
        Translation__c nonUnicodeTrans = new Translation__c(
            Vocabulary_Word__c = 'word',
            Chinese_Simplified_Word__c = 'simplified',
            Chinese_Traditional_Word__c = 'traditional'
        );

        String expected = 'word,simplified';

        String actual = MagentoCsvHelpers.translationToCsvRow(nonUnicodeTrans, 'chinese_simplified');

        System.assertEquals(expected, actual);
    }

    @isTest
    static void testSimplifiedTranslationToCsvRow() {
        Translation__c trans = [
            SELECT
                Vocabulary_Word__c,
                Chinese_Traditional_Word__c,
                Chinese_Simplified_Word__c
            FROM
                Translation__c
            WHERE
                Vocabulary_Word__c = 'Tequila'
        ];

        String expected = 'Tequila,龙舌兰酒';

        String actual = MagentoCsvHelpers.translationToCsvRow(trans, 'chinese_simplified');

        System.assertEquals(expected, actual);
    }

    @isTest
    static void testTraditionalTranslationToCsvRow() {
        Translation__c trans = [
            SELECT
                Vocabulary_Word__c,
                Chinese_Traditional_Word__c,
                Chinese_Simplified_Word__c
            FROM
                Translation__c
            WHERE
                Vocabulary_Word__c = 'Tequila'
        ];


        String expected = 'Tequila,龍舌蘭酒';

        String actual = MagentoCsvHelpers.translationToCsvRow(trans, 'chinese_traditional');

        System.assertEquals(expected, actual);
    }

    @isTest
    static void testUnsupportedTranslationToCsvRow() {
        Translation__c trans = new Translation__c(
            Vocabulary_Word__c = 'Tequila',
            Chinese_Simplified_Word__c = '龙舌兰酒',
            Chinese_Traditional_Word__c = '龍舌蘭酒'
        );

        try {
            MagentoCsvHelpers.translationToCsvRow(trans, 'aramaic');
        }
        catch (MagentoCsvHelpers.UnsupportedTranslationException ex) {
            System.assertEquals('translation type not supported; supported types are: chinese_simplified, chinese_traditional, spanish, german, french', ex.getMessage());
        }
    }

    @isTest
    static void testTranslationsToCsv() {
        Translation__c[] translations = [
            SELECT
                Vocabulary_Word__c,
                Chinese_Traditional_Word__c,
                Chinese_Simplified_Word__c
            FROM
                Translation__c
        ];

        // No three year calendar.
        Blob expected = Blob.valueOf('Tequila,龍舌蘭酒\nPink Quartz Crystal,粉紅色石英晶體\n');

        Blob actual = MagentoCsvHelpers.translationsToCsv(translations, 'chinese_traditional');

        System.assertEquals(expected, actual);
    }

    // Chinese dialects are verified above. These verify the other langs.
    // This could be made less repetitive if languages were dealt with dynamically at runtime...

    @isTest
    static void testFrench() {
        Translation__c[] translations = [
            SELECT
                Vocabulary_Word__c,
                French_Word__c
            FROM
                Translation__c
        ];

        Blob expected = Blob.valueOf('Three-Year Calendar,word en fraçais\n');

        Blob actual = MagentoCsvHelpers.translationsToCsv(translations, 'french');

        System.assertEquals(expected, actual);
    }

    @isTest
    static void testGerman() {
        Translation__c[] translations = [
            SELECT
                Vocabulary_Word__c,
                German_Word__c
            FROM
                Translation__c
        ];

        Blob expected = Blob.valueOf('Three-Year Calendar,word in german\n');

        Blob actual = MagentoCsvHelpers.translationsToCsv(translations, 'german');

        System.assertEquals(expected, actual);
    }

    @isTest
    static void testSpanish() {
        Translation__c[] translations = [
            SELECT
                Vocabulary_Word__c,
                Spanish_Word__c
            FROM
                Translation__c
        ];

        Blob expected = Blob.valueOf('Three-Year Calendar,word in spanish\n');

        Blob actual = MagentoCsvHelpers.translationsToCsv(translations, 'spanish');

        System.assertEquals(expected, actual);
    }
}