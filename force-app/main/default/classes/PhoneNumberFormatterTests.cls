@isTest
public class PhoneNumberFormatterTests {
    static final string PHONE_RESULT = '1234567890';
    
    static testmethod void stripCharacters_stripDash_test() {
        string phoneNumber = '123-456-7890';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Dashes were not removed');
    }
    
    static testmethod void stripCharacters_stripParentheses_test() {
        string phoneNumber = '(123)4567890';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Parentheses were not removed');
    }
    
    static testmethod void stripCharacters_stripPeriods_test() {
        string phoneNumber = '123.456.7890';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Periods were not removed');
    }
    
    static testmethod void stripCharacters_stripSpaces_test() {
        string phoneNumber = ' 123 456 7890 ';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Spaces were not removed');
    }
    
    static testmethod void stripCharacters_stripCountryCode_test() {
        string phoneNumber = '+11234567890';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Country Code was not removed');
    }
    
    static testmethod void stripCharacters_nullInput_test() {
        string result = PhoneNumberFormatter.stripCharacters(null);
        
        System.assertEquals(null, result, 'Null was not returned');
    }
    
    
    static testmethod void stripCharacters_stripSpecial_test() {
        string phoneNumber = '+112!@#$%^&*(){}[]::;<>?+-~34567890';
        string result = PhoneNumberFormatter.stripCharacters(phoneNumber);
        
        System.assertEquals(PHONE_RESULT, result, 'Special characters were not removed');
    }
}