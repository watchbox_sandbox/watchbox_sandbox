@isTest
public class PartNoteNotificationTest {
    public static testmethod void  testPartNotification() {
        Part__c part = TestFactory.createPartTest();
        Note note = TestFactory.createNote(part.Id);
        User u = [Select id,name,FirstName,lastname from user where name='Angel Malone'];
        System.debug('u***'+u);
        System.runAs(u) {
            update note;
        }
        User user = [Select id,name,FirstName,lastname from user where name='Ajinkya Kasabe'];
        System.debug('u***'+u);
        System.runAs(user) {
            update note;
        }
        
        System.debug('note***'+note.LastModifiedById);
        System.debug('note***'+note.CreatedById);
    }
}