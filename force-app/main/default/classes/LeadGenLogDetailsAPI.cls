@RestResource(urlMapping = '/leadGenLog/info/*')
global class LeadGenLogDetailsAPI {
   
    global class Error {
        global integer code;
        global string msg;
        global Error(integer code, string msg) {
            this.code = code;
            this.msg = msg;
        }
    }
    private static string serialize(object o) {
        return JSON.serialize(o);
    }
    @HttpGet
    global static void getLeadGenLogInfo() {
    
    integer retCode = 0;
    string retBody = '';
    
    
    RestResponse res = RestContext.response;
    DateTime currTime = System.now();
	DateTime hrBefore = currTime.addHours(-24);
    List<Lead_Gen_Log__c> leadList = [SELECT Id,First_Name__c,Last_Name__c,Email__c,Phone__c,Lead_Source__c,CreatedDate
                                      FROM Lead_Gen_Log__c 
                                      WHERE (CreatedDate < :currTime and CreatedDate> :hrBefore )
                                      and (Lead_Source__c='Thewatchbox.com - Sell Your Watch' 
                                           or Lead_Source__c='WatchLoans' 
                                           or Lead_Source__c='Thewatchbox.com - Trade Your Watch'
                                           or Lead_Source__c='Thewatchbox.com - Contact Us' 
                                           or Lead_Source__c='Thewatchbox.com - Make An Offer')];
    try {
    if (leadList.size()==0) {
    // lead not found
    retCode = 404;
    retBody = serialize(new Error(retCode, 'LeadGenLog record not found'));
    } else {
   	retCode = 200;
    retBody = serialize(leadList);
    
    }
   
    } catch (Exception e) {
    retCode = 500;
    retBody = serialize(new Error(retCode, 'Error retrieving LeadGenLog info [' + e.getMessage() + ']'));
    }
    
    
    res.headers.put('Content-type', 'application/json');
    res.statusCode = retCode;
    res.responseBody = Blob.valueOf(retBody);
    }

}