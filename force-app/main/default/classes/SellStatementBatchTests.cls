@isTest
public class SellStatementBatchTests {    
    @testSetup
    static void setup() {
        TestFactory.createLeadGenLogSettings();
    }
    
    static testmethod void execute_success_test() {
        Lead_Gen_Log__c lgl = TestFactory.createLeadGenLog();
        
        Opportunity opp = [SELECT Id, Sell_Statement__c FROM Opportunity LIMIT 1];
        
        opp.Sell_Statement__c = null;
        update opp;
        
        Test.startTest();
        SellStatementBatch batch = new SellStatementBatch();
        database.executeBatch(batch);               
        Test.stopTest();
        
        Opportunity result = [SELECT ID, Sell_Statement__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertNotEquals(null, result.Sell_Statement__c);
    }
}