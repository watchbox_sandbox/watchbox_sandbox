@isTest
private class EmailNotificationHelperTest {

     static testMethod void sendnmailtest()
     {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales']; 
         
        User u = new User(Alias = 'standt', Email='belurkar@gmail.com', 
                EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
                 LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='belurkar@gmail.com');
        insert u;
         
       
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp1';
        opp.OwnerId = u.id;
         opp.StageName = 'New';
         opp.CloseDate = Date.today();
        
        insert opp;
         
         Contact con = new Contact();
       
         con.LastName = 'zxcv';
         insert con;
        List<task> t = new List<task>{ new task(WhatID = opp.id,ActivityDate = system.today(),whoID = con.id,Subject='Call',OwnerId = u.id,Status='Open',Priority='Normal')};
		
       String cronExp='0 0 15 ? * * *';
      Test.startTest(); 
         insert t;
        EmailNotificationHelper e= new EmailNotificationHelper();
         e.sendMailForDueTasks();
         
         System.schedule('sendmails', cronExp, e);
	  Test.stopTest();
    }

 }