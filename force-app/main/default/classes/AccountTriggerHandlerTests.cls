@isTest
public class AccountTriggerHandlerTests {
    static testmethod void beforeUpdate_insert_test() {
        Account a = TestFactory.buildAccount();
        a.OwnerId = UserInfo.getUserId();
        insert a;
        
        System.assertNotEquals(null, a.OwnerId);
        System.assertEquals(null, a.Previous_Owner__c);
    }
    
    static testmethod void beforeUpdate_changeOwner_test() {
        Account a = TestFactory.buildAccount();
        a.OwnerId = UserInfo.getUserId();
        insert a;
        
        User user = TestFactory.createSalesUser();
        a.OwnerId = user.Id;
        update a;
        
        Account result = [SELECT Id, Previous_Owner__c, OwnerId FROM Account WHERE Id =:a.Id];
        
        System.assertEquals(user.Id, result.OwnerId, 'Account not updated correctly');
        System.assertEquals(UserInfo.getUserId(), result.Previous_Owner__c, 'Account owner updated incorrectly');
    }
    
    static testmethod void beforeUpdate_intialOwnerChange_test() {
        Account a = TestFactory.createAccount();
        
        a.OwnerId = UserInfo.getUserId();
        update a;
        
        Account result = [SELECT Id, Previous_Owner__c, OwnerId FROM Account WHERE Id =:a.Id];
        
        System.assertEquals(UserInfo.getUserId(), result.OwnerId, 'Account not updated correctly');
        System.assertEquals(null, result.Previous_Owner__c, 'Previous owner updated incorrectly');
    }
    
    static testmethod void beforeUpdate_noChange_test() {
        Account a = TestFactory.buildAccount();
        a.OwnerId = UserInfo.getUserId();
        insert a;
        
        a.Description = 'test';
        update a;
        
        Account result = [SELECT Id, Previous_Owner__c, OwnerId FROM Account WHERE Id =:a.Id];
        
        System.assertEquals(UserInfo.getUserId(), result.OwnerId, 'Account not updated correctly');
        System.assertEquals(null, result.Previous_Owner__c, 'Previous owner updated incorrectly');
    }
    
    static testmethod void beforeInsert_success_test() {
    	Account account = TestFactory.buildAccount();    
        account.Email__c = 'test@test.com';
        insert account;
        
        Account result = [SELECT ID, Ranking_Score__c, Ranking__c FROM Account WHERE Id =: account.Id];
        
        System.assert(result.Ranking_Score__c > 0);
//        System.assert(result.Ranking_Score__c < 0.10);
    }    
    
    static testmethod void beforeUpdate_success_test() {
        Account account = TestFactory.createAccount();    
        account.Email__c = 'test@test.com';       
        update account;
        
        Account result = [SELECT ID, Ranking_Score__c, Ranking__c FROM Account WHERE Id =: account.Id];
        
        System.assert(result.Ranking_Score__c > 0);
       // System.assert(result.Ranking_Score__c < 0.10);
    }
    
    static testmethod void calculateRanking_address_test() {
        Account account = TestFactory.buildAccount();  
        account.BillingStreet = 'Test Street';
        account.BillingPostalCode = '12345';
        account.BillingState = 'Ohio';
        account.BillingCountry = 'United States';
        account.BillingCity = 'City';
        insert account;
        
        Account result = [SELECT ID, Ranking_Score__c, Ranking__c FROM Account WHERE Id =: account.Id];
        
        System.assert(result.Ranking_Score__c > 0);
       // System.assert(result.Ranking_Score__c < 0.10);
    }
    
    static testmethod void calculateRanking_all_test() {
        Account a = TestFactory.buildAccount();  
        a.Email__c = 'test@test.com';
        a.Title__c = 'Title';
        a.Industry = 'Industry';
        a.First_Name__c = 'First';
        a.Last_Name__c = 'Last';
        a.Phone = '123-456-7890';
        a.Rating = 'Whale';
        a.Number_of_Watches_in_Collection__c = 2;
        a.Watchbox_App_Signed_Up__c = 'Yes';
        a.Interests_Hobbies__c = 'Fishing';
        a.Birthday__c = Date.today();
        a.Where_else_do_you_buy_watches__c = 'Boutiques';
        a.Receive_Updates__c = true;
        a.BillingStreet = 'Test Street';
        a.BillingPostalCode = '12345';
        a.BillingState = 'Ohio';
        a.BillingCountry = 'United States';
        a.BillingCity = 'City';
        insert a;
        
        Account result = [SELECT ID, Ranking_Score__c, Ranking__c FROM Account WHERE Id =: a.Id];
        
        System.assertEquals(1, result.Ranking_Score__c);
    }
    
    static testmethod void beforeUpdate_lastActivity_test() {
        Account a = TestFactory.buildAccount();
        User user = TestFactory.createSalesUser();
        a.OwnerId = user.Id;
        insert a;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:a.Id];
        
		System.assertEquals(system.today()-1, result.Last_Activity_Date__c);
        
        System.runAs(user)
        {
            a.Company_Name__c = 'Updated';
        	update a;            
        }
        
        result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:a.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c);
    }
    
    static testmethod void beforeUpdate_differentOwnerlastActivity_test() {
        Account a = TestFactory.buildAccount();
        User user = TestFactory.createSalesUser();
        a.OwnerId = user.Id;
        insert a;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:a.Id];
        
		System.assertEquals(system.today()-1, result.Last_Activity_Date__c);
        
        a.Company_Name__c = 'Updated';
        update a;
        
        result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:a.Id];
        
        System.assertEquals(system.today()-1, result.Last_Activity_Date__c);                       
    }
}