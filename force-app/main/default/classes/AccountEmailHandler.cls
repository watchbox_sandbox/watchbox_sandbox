global class AccountEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        System.debug('Incoming email message - ' + email);
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        SavePoint sp = Database.setSavepoint();
        try {
            Account matchedAccount = null;
            string toAddress = email.toAddresses[0];
            string bccAddress = '';
            System.debug('Executing AccountEmailHandler.handleInboundEmail with toAddress - ' + toAddress);
            Account_Email_Service_Settings__c settings = getSettings();
            if(!Test.isRunningTest()){
                for (Messaging.InboundEmail.Header header : email.headers) {
                    if (header.name == 'Received') {
                    bccAddress = header.value;
                    bccAddress = bccAddress.substringBetween('<','>');
                }
            }            
          }
            List<Account> accounts = [SELECT Id, Name, OwnerId FROM Account WHERE Email__c =: toAddress];
            
            if (!accounts.isEmpty())
                matchedAccount = accounts[0];            
                           
            User fromUser = getUserByEmail(email.fromAddress);
            
            if (matchedAccount == null) {                                
                matchedAccount = new Account();                
                matchedAccount.Name = toAddress;
                matchedAccount.Last_Name__c = toAddress;
                matchedAccount.Email__c = toAddress;
                matchedAccount.RecordTypeId = settings.Account_RecordType_Id__c;
                matchedAccount.Status__c = settings.Account_Status__c;
                
                if (fromUser != null)
                    matchedAccount.OwnerId = fromUser.Id;
                
                insert matchedAccount;
            }      
            
            //Build email message 
            EmailMessage message = new EmailMessage();
            message.RelatedToId = matchedAccount.Id;
            message.Subject = email.subject;
            message.ToAddress = toAddress;
            message.bccAddress = bccAddress;
            message.FromAddress = email.fromAddress;
            message.FromName = email.fromName;            
            
            if (!string.isBlank(email.htmlBody) && email.htmlBody.length() > 130000) {            
                message.HtmlBody = email.htmlBody.substring(0, 130000);            
            } else {
                message.HtmlBody = email.htmlBody;
            }              
            
            if (email.ccAddresses != null && email.ccAddresses.size() > 0)
                message.CcAddress = string.join(email.ccAddresses, ',');
                        
            message.Incoming = false;
            message.TextBody = email.plainTextBody;
            message.Status = '3';
            insert message;    
            
            //create attachments
            createAttachments(email, message);
            
            Id OwnerId = null;
            
            if (fromUser != null) {
                OwnerId = fromUser.Id;
            } else if (matchedAccount.OwnerId != null) {
                OwnerId = matchedAccount.OwnerId;                
            } else {
                OwnerId = settings.Unassigned_User_Id__c;
            }

            if (OwnerID != null)
                updateTaskOwner(message.Id, OwnerId);
        
            result.success = true;
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug('Error during AccountEmailHandler.handleInboundEmail - ' + e.getMessage());
            throw new ApplicationException(e.getMessage() + e.getStackTraceString());
        }
        
        return result;
    }
       
    private void createAttachments(Messaging.InboundEmail email, EmailMessage message) {
        List<Attachment> attachments = new List<Attachment>();
        
        //Process Binary attachments
        if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                try {
                    if (email.binaryAttachments[i].filename != null) { 
                        Attachment binaryAttachment = new Attachment(
                            ParentId = message.Id,                        
                            Name = email.binaryAttachments[i].filename,
                            Body = email.binaryAttachments[i].body);
                        attachments.add(binaryAttachment);                        
                    }
                } catch (Exception e) {
                    System.debug('Error processing binary attachment - ' + email + ' - ' + e.getMessage());
                    throw new ApplicationException(e.getMessage());
                }
            }
        }
        
        //Process Text attachments
        if (email.textAttachments!=null && email.textAttachments.size() > 0) {
            for (integer i = 0 ; i < email.textAttachments.size() ; i++) {
                try {
                    if (email.textAttachments[i].filename != null) { 
                        Attachment textAttachment = new Attachment(
                            ParentId = message.Id, 
                            Name = email.textAttachments[i].filename,
                            Body = Blob.valueOf(email.textAttachments[i].body));
                        attachments.add(textAttachment);
                    }               
                } catch (Exception e) {
                    System.debug('Error processing text attachment - ' + email + ' - ' + e.getMessage());
                    throw new ApplicationException(e.getMessage());
                }
            }
        }
        
        if (!attachments.isEmpty())
            insert attachments;
    }
    
    private User getUserByEmail(string emailAddress) {
        User user = null;
        
        try {
            user = [SELECT Id FROM User WHERE Email =: emailAddress LIMIT 1];
        } catch (Exception e) {
            System.debug('No user found in handleInboundEmail.getUserByEmail - ' + emailAddress + ' - ' +e.getMessage());
        }
        
        return user;
    }
    
    private void updateTaskOwner(Id messageId, Id OwnerId) {
        Task task = [SELECT Id, OwnerId FROM Task WHERE Id IN (SELECT ActivityId FROM EmailMessage WHERE Id =:messageId)];
        
        task.OwnerId = OwnerId;
        update task;                    
    }
    
    private Account_Email_Service_Settings__c getSettings() {
        Account_Email_Service_Settings__c settings = Account_Email_Service_Settings__c.getValues('AccountEmailService');  
        
        return settings;
    }
}