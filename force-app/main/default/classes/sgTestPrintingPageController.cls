@isTest
public class sgTestPrintingPageController {

    static testMethod void construct() {
        //arrange
        //
		createOrg('Govberg');
        createTeamMember('Govberg');
		Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('templateName', 'test');
        
        //act
        //
        sgPrintingPageController ctl = new sgPrintingPageController();
        ctl.Back();
        
        //assert
        //
        System.assertNotEquals(null, ctl.Organization);
    }
    
    static testMethod void construct_error() {
        //arrange
        //
		
        //act
        //
        sgPrintingPageController ctl = new sgPrintingPageController();

        //assert
        //
        System.assertEquals(true, ApexPages.hasMessages());
    }
    
    static testMethod void generateDocument_error() {
        //arrange
        //
		createOrg('Govberg');
        createTeamMember('Govberg');
		Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('templateName', 'test');
        sgPrintingPageController ctl = new sgPrintingPageController();
        
        //act
        //
        ctl.GenerateDocument();
        
        //assert
        //
        System.assertEquals(true, ApexPages.hasMessages());
    }
    
    //**********************************************************
    // helper methods
    
    static void createOrg(String name) {
        Organization__c org = new Organization__c(
        	Name = name
        );
        insert org;
    }
    
    static void createTeamMember(String orgName) {
        Team_Member__c teamMember = new Team_Member__c(
        	Organization__c = orgName,
            SF_User_ID__c = UserInfo.getUserId()
        );
        insert teamMember;
    }
}