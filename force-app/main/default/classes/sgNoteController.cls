@RestResource(urlMapping='/Notes/*')
global class sgNoteController {
    @HttpPost
    global static string post(string id, string note) {
        sgAuth.check();
        
        Note n = new Note(
            ParentId = id,
            Title = '-',
            Body = note
        );
            
        insert n;
        
        return 'Note added';
    }
}