public class sgPostingQueueable implements Queueable {

    String productId;
    String channelName;
    
    public sgPostingQueueable(String productId, String channelName) {
        this.productId = productId;
        this.channelName = channelName;
    }
    
    public void execute(QueueableContext context) {
        String channelPostId = [SELECT Id FROM ChannelPost__c WHERE Product__c = :productId AND Channel__c = :channelName LIMIT 1].Id;
    	sgPostingIntegrationController.ExecuteFuture(channelPostId);
    }

}