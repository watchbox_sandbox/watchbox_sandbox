@isTest
public class WatchCollectionUtlityTests {
	static testmethod void removeWatchCollectionByWatchSales_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Product2 product = TestFactory.createProduct();
        Watch_Sale__c watchSale = TestFactory.createWatchSale(product, opp);
        Exception ex = null;
        
        Watch_Collection__c collection = TestFactory.createWatchCollection(account);
        collection.Watch_Sale_ID__c = Id.ValueOf(watchSale.Id);
        update collection;        
		
        Set<Id> ids = new Set<Id>();
        ids.add(watchSale.Id);
        WatchCollectionUtility.removeWatchCollectionByWatchSales(ids);
        
        try {
            Watch_Collection__c result = [SELECT Id FROM Watch_Collection__c WHERE Id =: collection.Id];
        } catch (Exception e) {
            ex = e;
        }
        
        System.assertNotEquals(null, ex, 'Watch Collection record not cascaded properly');
    }
    
    static testmethod void createWatchCollectionByOpportunities_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Product2 product = TestFactory.createProduct();
        Watch_Sale__c watchSale = TestFactory.createWatchSale(product, opp);
        
		Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        opportunityMap.put(opp.Id, opp);
        WatchCollectionUtility.createWatchCollectionByOpportunities(opportunityMap);
        
        Watch_Collection__c result = [SELECT Id FROM Watch_Collection__c];
        
        System.assertNotEquals(null, result);
    }    
}