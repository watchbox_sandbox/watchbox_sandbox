@RestResource(urlMapping='/SiteToSite/*')
global class sgSiteToSiteController {
    global class Transfers {
        public Site_To_Site__c CurrentTransfer {get; set;}
        public List<string> Carriers {get; set;}
        public Map<string, string> Locations {get; set;}
        public List<string> TransferTypes {get; set;}
        
        
    }
    
    private static string userLocation {
        get {
            return sgAuth.user.Location__c;
        }
    }
    
    private static string userId {
        get {
            return sgAuth.user.Id;
        }
    }
	
    private static Site_To_Site__c getTransfer(string id) {
        List<string> productFields = new List<String>{'Id',
            'Inventory_ID__c',
            'Model__r.Name', 
            'Model__r.AWS_Image_Array__c', 
            'Ask__c', 
            'Cost__c',
            'MSP__c',
            'WUW_Owner__c',
            'Watch_Reference__c',
            'Model__r.model_brand__r.Name',
            'Deal__r.Salesperson__r.Name__c',
            'Last_Price_Change__c',
            'Posted_On_WUW__c',
            'Days_Inventoried__c'
        };
            
        string productFieldsString = '';
        for (string s : productFields) {
            productFieldsString += 'Inventory_Item__r.' + s + ', ';
        }
            
        string query = 
            'select Id, Name, Carrier__c, Shipping_Status__c, Transfer_Type__c, Starting_Location__c, Starting_Location__r.Name, Total_Cost__c,' +
            'Destination_Location__c, Destination_Location__r.Name, Destination_Team_Member__r.Name__c, Starting_Team_Member__r.Name__c,' +
            '(select ' + productFieldsString + 'Name, Item_Cost__c, Inventory_Item__c, Scan_Type__c ' +
            'from S2SDetails__r)' +
            'from Site_To_Site__c where';
        
        if (id != null) {
            query += ' Id = :id';
        } else {
            string location = userLocation;
            query += ' Starting_Location__c = :location and Committed__c = false';
        }
        
        query += ' limit 1';
        
        return Database.query(query);
    }
    
	@HttpGet
    global static Transfers get() {
        sgAuth.check();
		
        Transfers t = new Transfers();
        
        try {
        	t.CurrentTransfer = getTransfer(null);
            
            if (t.CurrentTransfer == null) {
                t.CurrentTransfer = newTransfer();
            }
        } catch (Exception e) {
            t.CurrentTransfer = newTransfer();
        }
		
		t.Carriers = sgUtility.getPicklistValues('Site_To_Site__c', 'Carrier__c');
        t.Locations = sgLookupController.get('Location__c');
		t.TransferTypes = sgUtility.getPicklistValues('Site_To_Site__c', 'Transfer_Type__c');
        
        return t;
    }
    
    //update transfer fields
    @HttpPost
    global static void post(string id, string field, string val) {
        Site_To_Site__c transfer = Database.query('select Id, ' + field + ' from Site_To_Site__c where Id = :id limit 1');
        
        transfer.put(field, val);
        update transfer;
    }
        
    private static Site_To_Site__c newTransfer() {
        sgAuth.check();
        
        Site_To_Site__c transfer = new Site_To_Site__c (
        	Starting_Location__c = userLocation,
            Starting_Team_Member__c = userId = userId
        );
        insert transfer;
        
		List<Product2> products = [select Id, Cost__c from Product2 where Location__c = :userLocation and IsActive = true limit 2000];
        List<S2SDetails__c> details = new List<S2SDetails__c>();
        for (Product2 p : products) {
            details.add(new S2SDetails__c(
            	S2S_Session__c = transfer.Id,
                Inventory_Item__c = p.Id,
                Item_Cost__c = p.Cost__c
            ));
        }        
        
        insert details;
        
		return getTransfer(transfer.Id);
    }
    
    //mark scanned
    @HttpPut
    global static void put(List<string> detailIds, string scanType) {
        sgAuth.check();
        
        string query = 'select Id, Scan_Type__c from S2SDetails__c where Id in :detailIds';
        List<S2SDetails__c> details = Database.query(query);
        
        for (S2SDetails__c d : details) {
            d.Scan_Type__c = scanType;
        }
        
        update details;
    }
    
    //finalize transfer
    @HttpPatch
    global static boolean patch(string id) {
        sgAuth.check();
        
        finishTransfer(id);
        
        return true;
    }
    
    @HttpDelete
    global static void delete1() {
        string id = RestContext.request.params.get('id');
        
        S2SDetails__c d = [select Id, Scan_Type__c from S2SDetails__c where Id = :id limit 1];
        d.Scan_Type__c = null;
        
        update d;
    }
    
    private static void finishTransfer(string id) {
        Site_To_Site__c transfer = [select Id, Committed__c, (select Id, Scan_Type__c from S2SDetails__r) from Site_To_Site__c where Id = :id limit 1];

        //set transfer to committed (in transit)
        transfer.Committed__c = true;
        
        update transfer;
        
        //get rid of products from the transfers location that has not been added (no scan type)
        List<S2SDetails__c> deletes = new List<S2SDetails__c>();
        for (S2SDetails__c d : transfer.S2SDetails__r) {
            if (d.Scan_Type__c == null) {
                deletes.add(d);
            }
        }
        
        delete deletes;
    }
}