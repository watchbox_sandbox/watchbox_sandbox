@isTest
public class TestFactory {
    public static final integer TEST_AMOUNT = 10;
    
    public static Account createAccount() {
        Account account = buildAccount();
        
        insert account;
        return account;
    }
    
    public static Account buildAccount() {
      
        Account account = new Account(
            Name = 'Test Account',
            Last_Name__c = 'Test',
            IsNoteAttached__c='N',
            Email__c = 'ajinkya.kasabe76@gmail.com',
            Phone = '9822331184',
            Last_Activity_Date__c=Date.today().addDays(-1)
        );
        
        return account;
    }
    
    public static Contact createContact(Account account) {
        if (account == null)
            account = createAccount();
        
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = account.Id;
        contact.Email = 'test@test.com';
        insert contact;
        
        return contact;        
    }
    
    public static Opportunity createOpportunity(Account account) {
        
        Opportunity opp = buildOpportunity(account);       
        insert opp;
        
        return opp;
    }
    public static Opportunity createOpportunity() {
        
        Opportunity opp = buildOpportunity();       
        insert opp;
        
        return opp;
    }
    public static Opportunity buildOpportunity(Account account) {
        if (account == null)
            account = createAccount();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.IsNoteAttached__c='N';
        opp.AccountId = account.Id;     
        opp.CloseDate=System.today().addMonths(1); 
        opp.Last_Activity_Date__c=Date.today().addDays(-1);
        
        return opp;
    }
    public static Opportunity buildOpportunity() {
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.CloseDate=System.today().addMonths(1); 
        
        return opp;
    }
    
    public static Part__c createPart() {
        Part__c part = buildPart();
        
        insert part;
        return part;
    }
    public static Part__c buildPart() {
        Account account = createAccount();
        User user=createSalesUser();
        
        Brand_object__c brand = createBrand();
        Part__c part = new Part__c(
            Sales_Associate__c = user.id,
            Sales_Associates_Location__c = 'Hong Kong',
            Type_of_Request__c='Inquiry',
            Person__c=account.id,
            Who_Is_Paying__c='Client',
            Has_the_watch_been_delivered_already__c='Yes',
            Quantity__c=2,
            Strap_Link_s_or_buckle__c='Link',
            Watch_Reference__c='100',
            Watch_Brand__c=brand.id
            // OwnerId=user1.id
        );
        
        return part;
    }
    
    
    public static Product2 buildProduct() {
        Brand_object__c brand = createBrand();
        
        Product2 product = new Product2();
        product.Name = 'Test Product';
        product.AWS_Web_Images__c = 'cnn.com';
        product.Watch_Brand__c = brand.Id;
        product.Model__c = createModel(brand).Id;
        product.Cost__c = TEST_AMOUNT;
        product.ASK__c = TEST_AMOUNT;
        product.MSP__c = TEST_AMOUNT;
        
        return product;
    }
    
    public static Product2 createProduct() {
        Product2 product = buildProduct();
        
        insert product;
        return product;
    }
    
    public static Watch_Sale__c buildWatchSale(Product2 product, Opportunity opp) {
        if (product == null) {
            product = createProduct();
        }
        
        if (opp == null) {
            opp = createOpportunity(null);
        }
        
        Watch_Sale__c ws = new Watch_Sale__c();
        ws.Inventory_Item__c = product.Id;
        ws.Opportunity__c = opp.Id;
        ws.Sale_Brand__c ='Chopard';
        ws.Client_s_Offer__c=464;
        ws.MSP__c=445645;
        ws.Sale_Amount__c = 2;        
        
        return ws;
    }
    
    public static Watch_Sale__c createWatchSale(Product2 product, Opportunity opp) {
        Watch_Sale__c ws = buildWatchSale(product, opp);
        ws.Client_s_Offer__c=453;
        ws.MSP__c=564;
        //ws.Sale_Brand__c ='Anonimo';
        insert ws;
         return ws;        
    }
    
    public static Holds__c buildHold(Product2 product, Opportunity opp) {
        if (product == null) {
            product = createProduct();
        }
        
        if (opp == null) {
            opp = createOpportunity(null);
        }
        
        Holds__c hold = new Holds__c();
        hold.MSP__c = TEST_AMOUNT;
        hold.Sale_Amount__c = TEST_AMOUNT;
        hold.Opportunity__c = opp.Id;
        hold.Inventory_Number__c = product.Id;
        hold.Holder_Owner__c = UserInfo.getUserId();
        hold.Name = opp.Name + '-' + product.Inventory_ID__c;
        
        return hold;
    }
    
    public static Holds__c createHold(Product2 product, Opportunity opp) {
        Holds__c hold = buildHold(product, opp);
        
        insert hold;
        return hold;
    }       
    
    public static Watch_Purchase__c createWatchPurchase(Opportunity opp){
        Watch_Purchase__c wp = buildWatchPurchase(opp);
        
        insert wp;
        return wp;
    }
    public static Watch_Purchase__c buildWatchPurchase(Opportunity opp){
        if(opp==null){
            opp=createOpportunity();
        }
        
        Watch_Purchase__c wp = New Watch_Purchase__c();
        wp.Cost__c = TEST_AMOUNT;
        wp.MSP__c = TEST_AMOUNT;
        wp.Asking__c = TEST_AMOUNT;
        wp.Origination__c ='Chopard';
        wp.Model_Name__c = 'Test Model';
        wp.Origination_Opportunity__c = opp.Id;
        return wp;
    }
    
    public static Watch_Collection__c createWatchCollection(Account account) {
        Watch_Collection__c wc = buildWatchCollection(account);
        
        insert wc;
        return wc;
    }
    
    public static Watch_Collection__c buildWatchCollection(Account account) {
        if (account == null) {
            account = createAccount();
        }
        
        Watch_Collection__c wc = new Watch_Collection__c();
        wc.Account__c = account.Id;
        wc.Brand__c = createBrand().Id;
        wc.Model_Name__c = 'Test Model';
        
        return wc;
    }
    
    public static Brand_object__c buildBrand() {
        Brand_object__c brand = new Brand_object__c();
        brand.Name = 'Test Brand';
        
        return brand; 
    }
    
    public static Brand_object__c createBrand() {
        Brand_object__c brand = buildBrand();
        
        insert brand;
        return brand;
    }
    
    public static Model__c buildModel() {
        return buildModel(null);
    }
    
    public static Model__c buildModel(Brand_object__c brand) {
        if (brand == null) {
            brand = createBrand();    
        }
        
        Model__c model = new Model__c();
        model.Name = 'Test Model';
        model.model_brand__c = brand.Id;
        model.AWS_Image_Array__c = 'http://www.test.com';
        return model;
    }
    
    public static Model__c createModel() {
        return createModel(null);
    }
    
    public static Model__c createModel(Brand_object__c brand) {
        Model__c model = buildModel(brand);
        
        insert model;
        return model;
    }
    
    public static Picklist__c buildPicklist(Id accountId, Id opportunityId) {
        Picklist__c picklist = new Picklist__c();
        
        if (accountId != null) 
            picklist.Account__c = accountId;
        
        if (opportunityId != null) 
            picklist.Deal__c = opportunityId;
        
        return picklist;
    }
    
    public static Picklist__c createPicklist(Id accountId, Id opportunityId) {
        Picklist__c picklist = buildPicklist(accountId, opportunityId);
        
        insert picklist;
        return picklist;
    }
    
    public static Intake__c buildIntake(Opportunity opp, Watch_Purchase__c wp){
        if (opp == null) 
            opp = createOpportunity();     
        
        if (wp == null)
            wp = createWatchPurchase(opp);
        
        Intake__c intake = new Intake__c();
        intake.Deal__c = opp.Id;
        intake.Watch_Purchase__c = wp.Id;
        intake.Model__c = 'Test Model';
        intake.Watch_Brand__c = createBrand().Id;
        intake.Watch_Model__c = createModel(null).Id;
        intake.Assign_Inventory__c = true;
        
        return intake;
    }
    
    public static Intake__c createIntake(Opportunity opp, Watch_Purchase__c wp) {
        Intake__c intake = buildIntake(opp, wp);
        
        insert intake;
        return intake;
    }
    
    public static Inspection__c buildInspection(Opportunity opp, Intake__c intake, Watch_Purchase__c wp) {
        if (opp == null) 
            opp = createOpportunity();  
        
        if (wp == null)
            wp = createWatchPurchase(opp);
        
        if (intake == null) 
            intake = createIntake(opp, wp);        
        
        Inspection__c inspection = new Inspection__c();
        inspection.Opportunity__c = opp.Id;
        inspection.Intake__c = intake.Id;
        inspection.TimeInspected__c = Datetime.now();
        
        return inspection;
    }
    
    public static Inspection__c createInspection(Opportunity opp, Intake__c intake, Watch_Purchase__c wp) {
        Inspection__c inspection = buildInspection(opp, intake, wp);
        
        insert inspection;
        return inspection;
    }
    
    public static Lead_Gen_Log__c buildLeadGenLog() {
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Phone__c = '1234567890';
        
        return lgl;      
    }
      
    public static Lead buildLead() {
        Lead lgl = new Lead();
        lgl.Email = 'ajinkya.kasabe76@gmail.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '9822331184';
        lgl.Company = 'WatchBox';
        
        return lgl;      
    }
    
    public static Lead_Gen_Log__c createLeadGenLog() {
        Lead_Gen_Log__c lgl = buildLeadGenLog();
        insert lgl;
        return lgl;
    }
    
    public static Lead createLead() {
        Lead lgl = buildLead();
        insert lgl;
        return lgl;
    }
    
    
    public static User createSalesUser() {
        User user = new User();
        user.ProfileId = '00e50000001FmYj';
        user.FirstName = 'Lauren123';
        user.LastName = 'Bodoff456';
        user.Email = 'laurenbondoff@govbergwatche.com';
        user.Username = 'unittest@govberg.com';
        user.Alias = 'utest';
        user.CommunityNickname = 'utest';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.IsActive = true;
        
        insert user;
        return user;
    }
    
    public static User createSalesUserForOwner() {
        User user = new User();
        user.ProfileId = '00e50000001FmYj';
        user.FirstName = 'Lagfnhhhhjjfdyr';
        user.LastName = 'Boggfhhhtrr';
        user.Email = 'laureniiyr@govbergwatcheshh.com';
        user.Username = 'unitghesthh@govberg.com';
        user.Alias = 'utjjryr';
        user.CommunityNickname = 'utestjjytu';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.IsActive = true;
        
        insert user;
        return user;
    }
    public static LeadGenLog_Settings__c createLeadGenLogSettings() {             
        LeadGenLog_Settings__c settings = new LeadGenLog_Settings__c();
        settings.Name = 'LeadTrigger';
        settings.Email_Template_Id__c = '00X50000002CGjE';
        settings.Opportunity_RecordType_Id__c = '01250000000EFfv';
        settings.Unassigned_User_Id__c = '00550000005vbPX';
        settings.Org_Email_Id__c = '0D250000000PDut';
        settings.Opportunity_StageName__c = 'New';
        settings.Lead_Source_Ardmore_User_Id__c = '005500000079nhZ';   
        settings.Lead_Source_Walnut_User_Id__c = '005500000074TAV';
        settings.Marketplace__c='0051T000008Amh7QAC';
        settings.Justin_Rix__c='00550000007A8NVAA0';
        settings.Zoe_Abelson__c='005500000074Lb4AAE';
        settings.Josh_Srolovitz__c='005500000074LafAAE';
        insert settings;        
        return settings;    
    }
    
    public static LeadGenLog_Settings__c getLeadSettings() {
        LeadGenLog_Settings__c settings = LeadGenLog_Settings__c.getValues('LeadTrigger');  
        
        return settings;
    }
    
    public static Account_Email_Service_Settings__c createAccountEmailServiceSettings() {             
        Account_Email_Service_Settings__c settings = new Account_Email_Service_Settings__c();
        settings.Name = 'AccountEmailService';
        settings.Account_RecordType_Id__c = '01250000000EFRY';
        settings.Unassigned_User_Id__c = '00550000005vbPX';
        settings.Account_Status__c = 'Email Lead';
        
        insert settings;        
        return settings;    
    }
    
    public static Messaging.InboundEmail buildInboundEmail() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.ccAddresses = new List<string> {'cc@test.com', 'cc2@test.com'};
            email.fromAddress = 'from@test.com';
        email.fromName = 'From Name';
        email.htmlBody = 'html';
        email.plainTextBody = 'plain text';
        email.subject = 'Test Inbound';
        email.toAddresses = new List<string> {'test@test.com'};
            
            //Binary attachment
            Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.body = blob.valueOf('Test Attachment');
        binaryAttachment.fileName = 'test.txt';
        binaryAttachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { binaryAttachment };
            
            //Text attachment
            Messaging.InboundEmail.TextAttachment textAttachment = new Messaging.InboundEmail.TextAttachment();
        textAttachment.body = 'Test Text';
        textAttachment.fileName = 'text.txt';
        textAttachment.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { textAttachment };
            
            return email;
    }
    
    public static Messaging.InboundEnvelope buildInboundEnvelope() {
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = 'from@test.com';
        envelope.toAddress = 'test@test.com';
        
        return envelope;
    }
    
    public static Note buildNote(Id parentId) {       
        Note note = new Note();
        note.Body = 'My test note';
        note.Title = 'My note title';
        List<User> user1 = [Select id,Name,Email from User Where name='Lauren Bodoff'];
        note.OwnerId=user1[0].id;
        if (parentId != null)
            note.ParentId = parentId;
        return note;
    }
    
    public static Note createNote(Id parentId) {
        Note note = buildNote(parentId);
        
        insert note;
        return note;
    }
    
    
    public static Deal_Schedule__c buildDealSchedule(Account account, User user) {
        if (account == null)
            account = createAccount();
        
        if (user == null)
            user = createSalesUser();
        
        Deal_Schedule__c ds = new Deal_Schedule__c();
        ds.Related_Person__c = account.Id;
        ds.Related_Associate__c = user.Id;
        ds.Date__c = System.today();
        
        return ds;
    }
    
    public static Deal_Schedule__c createDealSchedule(Account account, User user) {
        Deal_Schedule__c ds = buildDealSchedule(account, user);
        
        insert ds;
        return ds;
    }
    
    public static Attachment buildAttachment(Id parentId) {                
        Attachment a = new Attachment();        
        a.body = blob.valueOf('Test Attachment');        
        a.ContentType = 'text/plain';
        a.ParentId = parentId;
        a.Name = 'Test Attachment';
        
        return a;
    }
    
    public static Attachment createAttachment(Id parentId) {
        Attachment a = buildAttachment(parentId);
        
        insert a;
        return a;
    }
    
    public static Task buildTask(Id whatId) {
        Task t = new Task();
          if (whatId != null)
        t.WhatId = whatId;
        t.Status = 'Completed';
        t.Priority = 'High';
        
        return t;
    }
    
    public static Task createTask(Id whatId) {
        Task t = buildTask(whatId);
        
        insert t;
        return t;
    }        
    
    public static Task buildTaskByWhoId(Id whoId) {
        Task t = new Task();
        t.WhoId = whoId;
        t.Status = 'Completed';
        t.Priority = 'High';
        
        return t;
    }
    
    public static Task createTaskByWhoId(Id whoId) {
        Task t = buildTaskByWhoId(whoId);
        
        insert t;
        return t;
    }        
    
    public static Event buildEvent(Id whatId) {
        Event e = new Event();
        e.WhatId = whatId;
        e.ActivityDate = System.today();
        e.ActivityDateTime = System.now();
        e.DurationInMinutes = 30;
        
        return e;
    }
    
    public static Event createEvent(Id whatId) {
        Event e = buildEvent(whatId);
        
        insert e;
        return e;
    }
    
    public static boolean isSandbox() {
        Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        return org.IsSandbox;
    }
    public static Opportunity newOpportunity(Account account) {
        if (account == null)
            account = createAccount();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Account-Virtual';
        opp.StageName = 'Test';
        opp.AccountId = account.Id;     
        opp.CloseDate=System.today().addMonths(1); 
        
        return opp;
    }
    public static Opportunity newOpportunityWithoutAccount() {
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Account-Virtual';
        opp.StageName = 'Test';
        opp.CloseDate=System.today().addMonths(1); 
        
        return opp;
    }
    public static Part__c createPartTest() {
        Part__c part = buildPartTest();
        
        insert part;
        return part;
    }
    
    public static Part__c buildPartTest() {
        Account account = createAccount();
        User user=createSalesUserTest();
        
        Brand_object__c brand = createBrand();
        Part__c part = new Part__c(
            Sales_Associate__c = user.id,
            Sales_Associates_Location__c = 'Hong Kong',
            Type_of_Request__c='Inquiry',
            Person__c=account.id,
            Who_Is_Paying__c='Client',
            Has_the_watch_been_delivered_already__c='Yes',
            Quantity__c=2,
            Strap_Link_s_or_buckle__c='Link',
            Watch_Reference__c='100',
            Watch_Brand__c=brand.id
            // OwnerId=user1.id
        );
        
        return part;
    }
    
    
    public static User createSalesUserTest() {
        User user = new User();
        user.ProfileId = '00e50000001FmYj';
        user.FirstName = 'Lauren';
        user.LastName = 'Bodoff';
        user.Email = 'laurenbondoff@govbergwatche.com';
        user.Username = 'unittest@govberg.com';
        user.Alias = 'utest';
        user.CommunityNickname = 'utest';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.IsActive = true;
        
        insert user;
        return user;
    }
    
}