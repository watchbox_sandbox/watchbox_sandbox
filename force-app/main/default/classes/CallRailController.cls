public with sharing class CallRailController{
    public List<Opportunity> oppList{get;set;}
    
    public  CallRailController(){
        oppList=[Select id,name,StageName,AccountPhone4Templates__c,owner.name,Deal_Status__c,CloseDate, CreatedDate from opportunity 
                 where(Lead_Source__c='Call In' or Lead_Source__c='Other' or Lead_Source__c='call in' or Lead_Source__c='Call_Rail' or Lead_Source__c='Call_Rail_ImpactRadius' or Lead_Source__c='Call_Rail Impact Direct' ) 
                 and Deal_Status__c='New' order by createdDate DESC limit 1000];
    }
}