public class RankingController {
    public List<RankingResult> getResults() {
        List<RankingResult> rankings = new List<RankingResult>();
        integer rank = 1;
        
        AggregateResult[] results = [SELECT Owner.Name, AVG(Ranking_Score__c) 
                                     FROM Account 
                                     WHERE Id IN (SELECT AccountId FROM Opportunity WHERE StageName != 'Closed Won' AND StageName != 'Closed Lost')
                                     GROUP BY Owner.Name
                                     ORDER BY AVG(Ranking_Score__c) DESC NULLS LAST];
        
        for (AggregateResult ar : results)  {
            RankingResult rr = new RankingResult();
            rr.Rank = rank;
            rr.Name = (string)ar.get('Name');
            rr.Score = (decimal)ar.get('expr0');
            
            if (rr.Score != null) {
                rr.Score = rr.Score.setScale(2);
            }
            
            rankings.add(rr);
            rank++;
        }
        
        return rankings;
    }    
}