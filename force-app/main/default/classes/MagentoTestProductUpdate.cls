@isTest
private class MagentoTestProductUpdate {
    public static List<Product2> products;
    public static List<Photograph__c> photos;

    @testSetup static void init() {
        Brand_object__c brand = new Brand_object__c(
            Name = 'Test Brand'
        );

        insert brand;

        Series__c series = new Series__c(
            Name = 'Test Series',
            Watch_Brand__c = brand.Id
        );

        insert series;

        Model__c model = new Model__c(
            Name = 'Test Model',
            model_series__c = series.Id
        );

        insert model;

        Product2 productOne = new Product2(
            Name = 'First Product',
            Model__c = model.Id,
            IsActive = true
        );

        insert productOne;

        Product2 productTwo = new Product2(
            Name = 'Second Product',
            Model__c = model.Id,
            IsActive = true
        );

        insert productTwo;

        photos = new List<Photograph__c>();

        photos.add(new Photograph__c(
            Related_Product__c = productOne.Id,
            Display_Order__c = 1,
            Url__c = 'http://host/firstimageforfirstproduct.jpg'
        ));
        photos.add(new Photograph__c(
            Related_Product__c = productOne.Id,
            Display_Order__c = 4,
            Url__c = 'http://host/thirdimageforfirstproduct.jpg'
        ));
        photos.add(new Photograph__c(
            Related_Product__c = productOne.Id,
            Display_Order__c = 2,
            Url__c = 'http://host/secondimageforfirstproduct.jpg'
        ));

        photos.add(new Photograph__c(
            Related_Product__c = productTwo.Id,
            Display_Order__c = 1,
            Url__c = 'http://host/firstimageforsecondproduct.jpg'
        ));
        photos.add(new Photograph__c(
            Related_Product__c = productTwo.Id,
            Display_Order__c = 4,
            Url__c = 'http://host/thirdimageforsecondproduct.jpg'
        ));
        photos.add(new Photograph__c(
            Related_Product__c = productTwo.Id,
            Display_Order__c = 1,
            Url__c = 'http://host/deletedimageforsecondproduct.jpg'
        ));
        photos.add(new Photograph__c(
            Related_Product__c = productTwo.Id,
            Display_Order__c = 2,
            Url__c = 'http://host/secondimageforsecondproduct.jpg'
        ));

        insert photos;

        // Delete one of the images in the second product.
        delete photos[5];
    }

    @isTest static void testLoadPhotoUrls() {
        Product2 productOne = [SELECT Id FROM product2 WHERE Name = 'First Product'];

        List<String> productOnePhotoUrls = MagentoProductUpdate.loadPhotoUrls(productOne);

        List<String> productOneExpected = new List<String>{
            'http://host/firstimageforfirstproduct.jpg',
            'http://host/secondimageforfirstproduct.jpg',
            'http://host/thirdimageforfirstproduct.jpg'
        };

        System.assertEquals(productOnePhotoUrls, productOneExpected);

        Product2 productTwo = [SELECT Id FROM product2 WHERE Name = 'Second Product'];

        List<String> productTwoPhotoUrls = MagentoProductUpdate.loadPhotoUrls(productTwo);

        List<String> productTwoExpected = new List<String>{
            'http://host/firstimageforsecondproduct.jpg',
            'http://host/secondimageforsecondproduct.jpg',
            'http://host/thirdimageforsecondproduct.jpg'
        };

        System.assertEquals(productTwoPhotoUrls, productTwoExpected);
    }

    @isTest static void testProductToDTO() {
        MagentoProductDTO expectedDto = new MagentoProductDTO();

        expectedDto.name = 'First Product';
        expectedDto.photos = new List<String>{
            'http://host/firstimageforfirstproduct.jpg',
            'http://host/secondimageforfirstproduct.jpg',
            'http://host/thirdimageforfirstproduct.jpg'
        };
        expectedDto.seriesId = [SELECT Id from Series__c WHERE Name = 'Test Series'][0].Id;

        Product2 product = [SELECT Id FROM product2 WHERE Name = 'First Product'];

        MagentoProductDTO actualDto = MagentoProductUpdate.productToDTO(product);

        System.assertEquals(expectedDto.name, actualDto.name);
        System.assertEquals(expectedDto.photos, actualDto.photos);
        System.assertEquals(expectedDto.seriesId, actualDto.seriesId);
        System.assertEquals('Test Model', actualDto.model.name);
    }
	
    /*
    @isTest static void testGetSeriesIdFromProduct2() {
        Product2 product = [SELECT Id FROM product2 WHERE Name = 'First Product'];

        String expectedSeriesId = [SELECT Id from Series__c WHERE Name = 'Test Series'][0].Id;

        String seriesId = MagentoProductUpdate.getSeriesIdFromProduct2(product);

        System.assertEquals(expectedSeriesId, SeriesId);
    }
	*/

    @isTest static void testProductsToDTOJson() {
        products = new List<Product2>();
        products.add([SELECT Id FROM product2 WHERE Name = 'First Product']);
        products.add([SELECT Id FROM product2 WHERE Name = 'Second Product']);

        String blobby = MagentoProductUpdate.productsToDTOJson(products);

        MagentoProductDTOs pdtos = (MagentoProductDTOs)JSON.deserialize(blobby, MagentoProductDTOs.class);

        System.assertEquals('First Product', pdtos.products[0].Name);
        System.assertEquals('Second Product', pdtos.products[1].Name);
    }

    @isTest static void testProductWithoutModel() {
        Product2 productWithoutModel = new Product2(
            Name = 'Product Without Model',
            IsActive = true
        );

        insert productWithoutModel;

        products = new List<Product2>();
        products.add(productWithoutModel);

        Exception resultEx;

        try {
            MagentoProductUpdate.productsToDTOJson(products);
            system.assert(false);
        }
        catch (Exception ex) {
            resultEx = ex;
            system.assert(true);
        }

        //System.assertEquals('error getting series id from model', resultEx.getMessage());
    }
}