public class sgSerializer {
    public static List<Map<string, string>> serializeFromQuery(string query) {
    	return serializeFromQuery(query, null);
    }
    public static List<Map<string, string>> serializeFromQuery(string query, Map<string, string> params) {
        if (params != null) {
            query = sgUtility.applyFilters(query, params);
        }

        try {
            List<sObject> results = Database.query(query);
            List<string> fields = getFields(query);

			return serializeFromList(results, fields);
        } catch (Exception e) {
            string message = 'There was a problem with your request';
            system.debug(query);
            if (sgUtility.isSandbox()) {
                message = query;
            }

            throw new sgException(message);
        }
    }
    
    public static List<Map<string, string>> serializeFromList(List<sObject> sObjects, string query) {
        List<string> fields = getFields(query);
        
        return serializeFromList(sObjects, fields);
    }
    
    public static List<Map<string, string>> serializeFromList(List<sObject> sObjects, List<string> fields) {
        List<Map<string, string>> serialized = new List<Map<string, string>>();
        
        for (sObject obj : sObjects) {
            serialized.add(serialize(obj, fields));
        }
        
        return serialized;
    }
    
    public static Map<string, string> serialize(sObject obj, List<string> fields) {
        Map<string, string> objMap = new Map<string, string>();
        
        for (string field : fields) {
            object val;
			
            //this is a relationship
            if (field.contains('.')) {
                List<string> fieldParts = field.split('\\.');
                
                sObject relation = (sObject) obj.getSObject(fieldParts.get(0));
				
                integer fieldPosition = fieldParts.size() - 1;
                
                if (fieldParts.size() >= 3) {
                    if (relation != null) {
                    	relation = relation.getSObject(fieldParts.get(1));
                    }
                }
                
                if (fieldParts.size() == 4) {
                    if (relation != null) {
                    	relation = relation.getSObject(fieldParts.get(2));
                    }
                }
                
                val = relation == null ? null : relation.get(fieldParts.get(fieldPosition));
                
                field = field.replace('.', '');
            } else {
        		val = obj.get(field);
            }
            
            if (val == null) {
                val = '';
            }
            
            objMap.put(field, string.valueOf(val));
        }
        
        return objMap;
    }
    
    public static List<string> getFields(string query) {
        string queryNoSpaces = query.replaceAll(' ', '');
        
        string startToken = 'select';
        integer startPos = queryNoSpaces.indexOfIgnoreCase(startToken) + startToken.length();
        
        string endToken = 'from';
        integer endPos = queryNoSpaces.indexOfIgnoreCase(endToken);
        
        List<string> fields = queryNoSpaces.substring(startPos, endPos).split(',');
		
        return fields;
    }
}