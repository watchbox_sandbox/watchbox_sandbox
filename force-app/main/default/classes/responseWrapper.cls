Public class responseWrapper {
        Public String status {get;set;} //status string
        Public Opportunity Opportunity {get;set;} //18 character Opporutnity record Id
        Public String OpportunityProductId {get;set;} //18 character OpportunityProduct record Id
        Public String ContactId{get;set;} // ContactId
        Public String AccountId{get;set;} // AccountId
        Public String message{get;set;} // message strubg
        Public String oppName{get;set;} 
        
        //constructor
        Public responseWrapper() {
            this.status = 'success';
            this.Opportunity = new Opportunity();
            this.OpportunityProductId = '';
            this.ContactId = '';
            this.AccountId = '';
            this.message = '';
            this.oppName = '';
        }
    }