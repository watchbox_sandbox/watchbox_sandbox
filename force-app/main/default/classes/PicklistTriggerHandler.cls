public class PicklistTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        Set<Id> oppIds = new Set<Id>();
        List<Picklist__c> picklists = (List<Picklist__c>)Trigger.new;
        
        //Generate list of opps to lookup
        for (Picklist__c pl : picklists) {
            if (pl.Deal__c != null) {
                oppIds.add(pl.Deal__c);
            }
        }
        
        if (!oppIds.isEmpty())
        	updatePicklistAccounts(picklists, oppIds);
    }
    
    public override void afterInsert() {
        Set<Id> oppIds = new Set<Id>();
        List<Picklist__c> picklists = (List<Picklist__c>)Trigger.new;
        
        //Generate list of opps to lookup
        for (Picklist__c pl : picklists) {
            if (pl.Deal__c != null) {
                oppIds.add(pl.Deal__c);
            }
        }
        
        if (!oppIds.isEmpty())
            generatePicklistDetails(oppIds);
    }
    
    public override void beforeUpdate() {
        Set<Id> oppIds = new Set<Id>();
        List<Picklist__c> picklists = (List<Picklist__c>)Trigger.new;
        
        for (Id plId : Trigger.newMap.keySet()) {            
            Picklist__c newPickList = (Picklist__c)Trigger.newMap.get(plId);
            Picklist__c oldPicklist = (Picklist__c)Trigger.oldMap.get(plId);  
            
            //Account for opportunity being removed
            if (newPickList.Deal__c == null && oldPicklist.Deal__c != null) {
                newPicklist.Account__c = null;
            //Account for opportunity being changed
            } else if (newPicklist.Deal__c != oldPicklist.Deal__c){
                oppIds.add(newPicklist.Deal__c);
            }
        }

        if (!oppIds.isEmpty())
        	updatePicklistAccounts(picklists, oppIds);    
    }
    
    private void updatePicklistAccounts(List<Picklist__c> picklistsToUpdate, Set<Id> oppIds) {   
        System.debug('Entering PicklistTriggerHandler.updatePicklistAccounts');
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Account.Id FROM Opportunity WHERE Id IN :oppIds]);
        
        //Loop again to update where necessary using lookup data
        for (Picklist__c pl : picklistsToUpdate) {
            if (pl.Deal__c != null) {
                Opportunity opp = oppMap.get(pl.Deal__c);
                
                //Update picklist Account field
                if (opp.Account.Id != null) {
                    pl.Account__c = opp.Account.Id;
                }
            }
        }                
    }
    
    private void generatePicklistDetails(Set<Id> oppIds) {
        System.debug('Entering PicklistTriggerHandler.generatePicklistDetails');
        List<Picklist__c> picklists = (List<Picklist__c>)Trigger.new;
        List<Picklist_Detail__c> picklistDetailsToInsert = new List<Picklist_Detail__c>();
        
        Map<Id, Opportunity> opps = new Map<Id, Opportunity>([SELECT Id, Name, 
                                 							(SELECT Id, Inventory_Item__c FROM Opportunity.Watch_Sales__r)
                                 							FROM Opportunity
                                 							WHERE Id IN :oppIds]);
        
        for (Picklist__c pl : picklists) {
            if (pl.Deal__c != null) {
                Opportunity opp = opps.get(pl.Deal__c);
                
                if (opp != null && !opp.Watch_Sales__r.isEmpty()) {
                    for (Watch_Sale__c ws : opp.Watch_Sales__r) {
                        if (ws.Inventory_Item__c != null) {
                            Picklist_Detail__c pd = new Picklist_Detail__c();
                            pd.Related_Picklist__c = pl.Id;
                            pd.Related_Inventory__c = ws.Inventory_Item__c;
                            picklistDetailsToInsert.add(pd);
                        }                        
                    }
                }
            }            
        }
        
        if (!picklistDetailsToInsert.isEmpty())
            insert picklistDetailsToInsert;
    }
}