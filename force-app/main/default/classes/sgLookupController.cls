@RestResource(urlMapping='/Lookup/*')
global with sharing class sgLookupController {
    global static Map<string, string> get(string objectName) {
    	return get(objectName, 'name', 'id');
    }
    
    global static Map<string, string> get(string objectName, string key) {
    	return get(objectName, key, 'id');
    }
    
    global static Map<string, string> get(string objectName, Map<string, string> additionalParams) {
    	return get(objectName, 'name', 'id', additionalParams);
    }
    
    global static Map<string, string> get(string objectName, string key, string value, Map<string, string> additionalParams) {
        set<string> keys = additionalParams.keySet();
	
        Map<string, string> params = new Map<string, string>{
            'objectName' => objectName,
            'key' => key,
            'value' => value
        };
        
        params.putAll(additionalParams);
        
        Map<string, string> results = getResults(params);
        
        return results;
    }
    
    global static Map<string, string> get(string objectName, string key, string value) {
        if (restContext.request == null) {
            restContext.request = new restrequest();
        }
		
		Map<string, string> params = new Map<string, string>();
        
		params.put('objectName', objectName);
        params.put('key', key);
        params.put('value', value);
        
        return getResults(params);
    }
    
    global static Map<string, string> getResults(Map<string, string> params) {
		string objectName = params.get('objectName');
        string key = params.get('key');
        string value = params.get('value');
        
        if (objectName == null || key == null || value == null) {
            throw new sgException('Missing one of required parameters: "objectName", "key", or "value"');
        }
        
        Map<string, string> lookup = new Map<string, string>();
                
        string query = 'select ' + key + ', ' + value + ' from ' + objectName;
        query = sgUtility.applyFilters(query, params);
        query += ' order by ' + key + ' desc';
        
        List<sObject> results = Database.query(query);
        
        for (sObject r : results) {
            lookup.put(
            	string.valueOf(r.get(key)),
                string.valueOf(r.get(value))
            ); 
        }

		return lookup;        
    }
    
    @HttpGet
    global static Map<string, string> get() {
        sgAuth.check();
            
        Map<string, string> lookup = getResults(RestContext.request.params);
        
        RestContext.request.params.remove('objectName');
        RestContext.request.params.remove('key');
        RestContext.request.params.remove('value');
        
        return lookup;
    }
}