global class HoldService {
    webservice static void extendHold(Id holdId, integer days) {
        
        try {                        
            Holds__c hold = [SELECT Id, Inventory_Number__c FROM Holds__c WHERE Id =: holdId];
        	Product2 inventory = [SELECT Id, Hold_Expiration__c, Status__c FROM Product2 WHERE Id =:hold.Inventory_Number__c];
            
            //If object is not sold add 24hrs
            if (inventory.Status__c != 'Sold') {
                inventory.Hold_Expiration__c = (inventory.Hold_Expiration__c == null) ? Datetime.now().addDays(days) : inventory.Hold_Expiration__c.addDays(days);
                update inventory;
            }
            
        } catch (Exception e) {
            System.debug('Error while extending hold on Id - ' + holdId + ' - ' + e.getMessage());
        }        
    }
}