@IsTest public with sharing class BulkUserOptOUtTest {
    @IsTest(SeeAllData=true) static void testSave() {
        List<User> existingPortalUsers = [SELECT id, email, Name,Opt_out__c,Alias,IsActive,username,ProfileId  FROM User where IsActive = true AND usertype != 'GUEST' AND Name != 'Automated Process'];
        BulkUserOptOUt controller = new BulkUserOptOUt();
        System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
        controller.edit();
        System.assert(controller.getIsEdit() == true);
        controller.cancel();
        System.assert(controller.getIsEdit() == false);
        existingPortalUsers[0].Opt_out__c = false;
        controller.save();
        System.assert(controller.getIsEdit() == false);
        controller.getUser();
        System.assert(controller.getUser() != null);
    }
    
}