@RestResource(urlMapping='/ProductExport/*')
global class sgProductExportController {
    @HttpGet
    global static string get() {
        sgAuth.check();
        
        Map<string, string> columns = new Map<string, string> {
            'Inventory_ID__c' => 'Inv #',
            'Model__r.Model_Brand__r.Name' => 'Brand',
            'Model__r.Reference__c' => 'Ref',
            'Name' => 'Description',
            'Product_Type__c' => 'Type',
            'Status__c' => 'Status',
            'Location__r.Name' => 'Location',
            'Days_Inventoried__c' => 'Days in Inventory',
            'Model__r.Name' => 'Model',
            'Cost__c' => 'Cost',
            'MSP__c' => 'MSP',
            'Ask__c' => 'Ask',
            'Retail__c' => 'Retail',
            'WUW_Sold_Price__c' => 'Sale'
       	};
            
       	Set<string> fields = columns.keySet();
        List<string> fieldsList = new List<string>();
        List<string> headings = new List<string>();
        for (string f : fields) {
            fieldsList.add(f);
            headings.add(columns.get(f));
        }

        string query = 'select ' + string.join(fieldsList, ', ') + ' from Product2';
        
        Map<string, string> params =  RestContext.request.params.clone();        
        
        //this isn't ideal, but the filter is checked when on "All Inventory", so put no restrictions on it then
        if (!params.containsKey('IsActive') || params.get('IsActive') == 'false') {
        	params.put('IsActive', 'true');
        } else {
            params.remove('IsActive');
        }
       	
        params.remove('limit');

        if (!params.containsKey('orderBy')) {
            params.put('orderBy', 'name');
        }
        
        if (params.containsKey('Location__r.Name') && params.get('Location__r.Name') == 'GB Inventory') {
            params.remove('Location__r.Name');
            params.put('IsGBInventory__c', 'true');
        }
        
        if (params.containsKey('CurrentYear')) {
            params.remove('CurrentYear');
            
            string startDate = '>=' + (Date.today().year()) + '-01-01';
            params.put('group2WUW_Date_Created__c', startDate);
            params.put('group2GB_Date_Inventoried__c', startDate);
        }

        if (params.containsKey('query')) {
            string queryParam = params.get('query');
            params.remove('query');
            
            params.put('group1Name', 'like:' + queryParam);
            params.put('group1Model__r.Name ', 'like:' + queryParam);
            params.put('group1Location__r.Name ', 'like:' + queryParam);
            params.put('group1WUW_Inventory_ID__c ', 'like:' + queryParam);
            params.put('group1GB_Inventory_ID__c ', 'like:' + queryParam);
        
        }
        
        List<Map<string, string>> products = sgSerializer.serializeFromQuery(query, params);
        //throw new sgexception(products.size() + '');
        string csv = string.join(headings, ',') + '\n';
        for (Map<string, string> p : products) {
            List<string> line = new List<string>();
            for (string f : fieldsList) {
                string key = f.replace('.', '');
                line.add(p.get(key));
            }
            
            csv += string.join(line, ',') + '\n';
        }
        
        return csv;
    }
}