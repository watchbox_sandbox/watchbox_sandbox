public with sharing class MagentoSeriesDTO {
    public String sfId; // SF_ID__c
    public String description; // Series_Description__c = 'Series One description.',
    public String quote; // Series_Quote__c
    public String slug; // URL_Slug__c = 'brand-one',
    public Boolean active; // WUW_Website_Active__c = true,
    public String brandId; // Watch_Brand__c
    public String name; // Name = 'Series One',
    public MagentoBrandDTO brand; // parent
}