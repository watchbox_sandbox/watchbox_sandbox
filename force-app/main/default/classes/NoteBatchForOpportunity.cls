global class NoteBatchForOpportunity implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id FROM Opportunity where IsNoteAttached__c!='Y' limit 50000000]);
       
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> listOpportunity){
         system.debug('---list of Opportunity----'+listOpportunity);
      
        Map<Id, Opportunity> opportunityUpdateMap = new Map<Id, Opportunity>();
        Set<Id> objectIds = new Set<Id>();
        try{
            for (Opportunity opp : listOpportunity ) {            
                    objectIds.add(opp.Id);
                	opportunityUpdateMap.put(opp.id,opp);
            }
            
            if (!objectIds.isEmpty()) {
                Map<Id, Note> noteMap = new Map<Id, Note>();                        
                for(Note noteObject: [SELECT id,parentId FROM Note WHERE parentId IN :objectIds]){
                    noteMap.put(noteObject.ParentId,noteObject);
                }
                
                 system.debug('---noteMap----'+noteMap);
                    for (Opportunity opp : listOpportunity) {
                    		Opportunity opportunity = null;
                            if(noteMap!=null && noteMap.get(opp.id)!=null ){
                                Note note=noteMap.get(opp.id);
                                opportunity=opportunityUpdateMap.get(note.ParentId);
                                   if (opportunity != null){
                                       opportunity.IsNoteAttached__c='Y';
                                        opportunityUpdateMap.put(opportunity.Id, opportunity);
                                   }
                            }
                            else{
								opportunity=opportunityUpdateMap.get(opp.id);
                                opportunity.IsNoteAttached__c='N';
                                opportunityUpdateMap.put(opportunity.Id, opportunity);
                            }
                               
                     }             
          }
                    
                    //Update opportunity
                    if (!opportunityUpdateMap.isEmpty())
                        update opportunityUpdateMap.values();
         
            
        }catch (Exception e) {
            System.debug('Error during NoteBatchForOpportunity - ' + e.getMessage() + ' - ' + e.getStackTraceString()+ ' - '+e.getLineNumber());
            throw e;
        }   
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}