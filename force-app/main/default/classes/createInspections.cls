public class createInspections {
	public ApexPages.StandardController stdController;
    public Id oppId;
    
    public createInspections(){
        //standard no argument constructor for use with overloaded method(s)
    }
    public createInspections(ApexPages.StandardController controller){
        stdController = controller;
        oppId = stdController.getId();
    }
    
    public PageReference createInspectionRecords(){
        //initialize variables
        String recordTypeName = 'Inspection - Originations';
        RecordType purchaseInspection = [SELECT id, name FROM RecordType WHERE name = :recordTypeName];
        List<Inspection__c> newInspections = new List<Inspection__c>();
        //query all intake and inspection records linked to current deal. Only intake records that do not have an inspection
        //record looking up to them will be used
        List<Inspection__c> currentInspections = [SELECT id, Intake__c FROM Inspection__c WHERE  opportunity__c= :oppId];
        List<Id> intakesWithInspections = New List<Id>();
        for(Integer x=0;x<currentInspections.size();x++){
            intakesWithInspections.add(currentInspections[x].Intake__c);
        }
        List<Intake__c> currentIntakes = [SELECT id, return__c, Model__c, deal__c, Product__c FROM Intake__c WHERE deal__c = :oppId AND id NOT IN :intakesWithInspections AND return__c = null];
        system.debug(currentIntakes.size()+' '+ currentIntakes);
        if(currentIntakes.size() == 0){
            PageReference refreshOpp = new PageReference('/'+oppId);
        	refreshOpp.setRedirect(true);
        	return refreshOpp;
        }
        
        //iterate through them. create an inspection record for each instake beyond the current number of inspection records. Tie it to the current opp and current intake
        for(Integer i=0;i<currentIntakes.size();i++){
            Inspection__c newInspection = new Inspection__c(intake__c = currentIntakes[i].id, recordType =purchaseInspection, Related_Inventory__c= currentIntakes[i].Product__c, opportunity__c=oppId);
            newInspections.add(newInspection);
        }
        insert newInspections;
        //and now we return to the deal record, making the whole transaction look like a page refresh
        PageReference refreshOpp = new PageReference('/'+oppId);
        refreshOpp.setRedirect(true);
        return refreshOpp;
    }
    public PageReference createInspectionRecords(Id getId){
        Id oppId=getId;
        //initialize variables
        List<Inspection__c> newInspections = new List<Inspection__c>();
        //query all intake and inspection records linked to current deal. Only intake records that do not have an inspection
        //record looking up to them will be used
        List<Inspection__c> currentInspections = [SELECT id, Intake__c FROM Inspection__c WHERE  opportunity__c= :oppId];
        List<Id> intakesWithInspections = New List<Id>();
        for(Integer x=0;x<currentInspections.size();x++){
            intakesWithInspections.add(currentInspections[x].Intake__c);
        }
        List<Intake__c> currentIntakes = [SELECT id, Model__c, deal__c, Product__c FROM Intake__c WHERE deal__c = :oppId AND id NOT IN :intakesWithInspections];
        system.debug(currentIntakes.size());
        if(currentIntakes.size() == 0){
            PageReference refreshOpp = new PageReference('/'+oppId);
        	refreshOpp.setRedirect(true);
        	return refreshOpp;
        }
        
        //iterate through them. create an inspection record for each instake beyond the current number of inspection records. Tie it to the current opp and current intake
        for(Integer i=0;i<currentIntakes.size();i++){
            Inspection__c newInspection = new Inspection__c(intake__c = currentIntakes[i].id, Related_Inventory__c= currentIntakes[i].Product__c, opportunity__c=oppId);
            newInspections.add(newInspection);
        }
        insert newInspections;
        return null;
        
    }
}