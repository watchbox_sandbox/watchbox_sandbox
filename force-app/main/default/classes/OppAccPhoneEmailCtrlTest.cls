@isTest
public class OppAccPhoneEmailCtrlTest {
    
     public static testmethod void TestAccountPhoneEmailCtrl() {
        Account Account = new Account();
        Account.Name = 'Test';
        Account.Email__c = 'test@test.com';
        Account.Secondary_Email__c = 'test123@test.com';
        Account.Phone = '12365412563';
        Account.Mobile__c = '12365412563';
        Account.Home_Phone__c = '12365412563';
        insert Account;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.IsNoteAttached__c='N';
        opp.AccountId = Account.Id;     
        opp.CloseDate=System.today().addMonths(1); 
        insert opp;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        OppAccPhoneEmailCtrl cds = new OppAccPhoneEmailCtrl(controller);
     }    
}