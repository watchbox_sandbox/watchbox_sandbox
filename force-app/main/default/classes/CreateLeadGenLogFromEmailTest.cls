@isTest
public class CreateLeadGenLogFromEmailTest {
    static testMethod void testInquiryRegarding() 
    {
        TestFactory.createSalesUser();  
        TestFactory.createLeadGenLogSettings();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        // Test with the subject that matches the  statement.
        email.subject = 'Inquiry regarding Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox';
        email.fromAddress = 'test@test.com';
        email.plainTextBody = 'Inquiry regarding your ad Audemars Piguet Royal Oak Offshore Chronograph 26020ST.OO.D001IN.02.A at Chrono24 /\n' + 'A visitor of Chrono24 has sent an inquiry to you:/\n'+ 'Name:	Seth Schochet/\n '+'Email:	*********@hotm***.com (ID: 2962841)/\n' +'Telephone:	Display telephone number /\n' +'Country:	United States of America /\n' +'Message:/\n' +'Do you have the papers and box? Any scratches? Where are you located?/\n' +'REPLY NOW /\n' +'Details /\n' +'Audemars Piguet Royal Oak Offshore Chronograph 26020ST.OO.D001IN.02.A /\n'+'Internal number: 4034542/\n '+'Price: $16,450\n'+'\n'+' I want to buy for 7000\n' +'REPLY NOW /\n' +'REPLY NOW /\n' +'REPLY NOW /\n' +'REPLY NOW /\n' + 'REPLY NOW /\n' + 'REPLY NOW /\n' +'REPLY NOW /\n';     
            system.debug(email.plainTextBody);
        if(String.isNotBlank(email.plainTextBody)){
            String[] emailBodyRows = email.plainTextBody.split('\n');
            System.debug('emailBodyRows+++++++++++++++++'+emailBodyRows);

         for (String bodyRow :emailBodyRows) {
            String description = emailBodyRows[5]+ ' \r\n'+emailBodyRows[6]+ ' \r\n'+emailBodyRows[10]+ ' \r\n'+emailBodyRows[11]+ ' \r\n'+emailBodyRows[12]; 
             System.debug('***************************'+description);
         }
        CreateLeadGenLogFromEmail emailtest1 = new CreateLeadGenLogFromEmail();
        emailtest1.handleInboundEmail(email,env);
        
    }
    }
    /*
    static testMethod void Youvereceivedapricesuggestion() 
    {
       TestFactory.createSalesUser();  
        TestFactory.createLeadGenLogSettings();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        // Test with the subject that matches the  statement.
       	//String[] emailPlainTextBody= 'Inquiry regarding Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox Chono24 Watchbox';
        email.subject = 'You have received a new price suggestion.';
        email.fromAddress = 'TrustedCheckout@chrono24.com';
        
        email.plainTextBody = 'Dear Test Dealer/\n' + 'Youve received a new price suggestion./\n' + 'Please answer this request in your dealer area./\n' + 'REVIEW PRICE SUGGESTION/\n' + 'Offer information/\n' + 'Richard Mille RM033 Extra Flat RM033/\n'  +	'$64,950/\n' + 'Order information/\n' + 'Code:	4017075/\n' + 'Price:	$64,950.00/\n' + 'Buyers suggested price:	$50,000.00/\n' + 'Buyers name:	Mohannad Abanomy/\n' + 'Country:	Saudi Arabia/\n' + ' Phone number:	+966566663366/\n' + 'Status:	Offer requested/\n' +'Next step:	Review price suggestion/\n'+'You can find more information and other options here:/\n'+'PRICE SUGGESTION TC-1104571';
        system.debug(email.plainTextBody);
        if(String.isNotBlank(email.plainTextBody)){
            String[] emailBodyRows = email.plainTextBody.split('\n');
            System.debug('emailBodyRows+++++++++++++++++'+emailBodyRows);

         for (String bodyRow :emailBodyRows) {
            String description = emailBodyRows[2]+ ' \r\n'+emailBodyRows[6]+ ' \r\n'+emailBodyRows[10]+ ' \r\n'+emailBodyRows[11]+ ' \r\n'+emailBodyRows[12]; 
             System.debug('***************************'+description);
         }
        
    }
    }*/
    static testMethod void receivedapricesuggestion() 
    {
        TestFactory.createSalesUser();  
       TestFactory.createLeadGenLogSettings();
       Lead_Gen_Log__c leadobj = TestFactory.createLeadGenLog();
        
        system.debug(leadobj.Email__c);
 
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        // Test with the subject that matches the  statement.
        email.subject = 'You\'ve received a price suggestion (TC-479058)';
        email.fromAddress = 'TrustedCheckout@chrono24.com';
        email.plainTextBody = 'Dear Test Dealer/\n' + 'You\'ve received a price suggestion/\n' + 'Please answer this request in your dealer area./\n' + 'REVIEW PRICE SUGGESTION/\n' + 'Offer information/\n' + 'Richard Mille RM033 Extra Flat RM033/\n'  +	'$64,950/\n' + 'Order information/\n' + 'Code:	4017075/\n' + 'Price:	$64,950.00'+'\n' + 'Buyer\'s suggested price:	$50,000.00'+'\n' + 'Buyer\'s name:	Mohannad Abanomy/\n' + 'Country:	Saudi Arabia'+'\n' + ' Phone number:	+966566663366/\n' + 'Status:	Offer requested/\n' +'Next step:	Review price suggestion/\n'+'You can find more information and other options here:/\n'+'PRICE SUGGESTION TC-1104571' + ' /\n' +' /\n'+ ' /\n'+ ' /\n' + ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n';
		system.debug(email.plainTextBody);
        if(String.isNotBlank(email.plainTextBody)){
            String[] emailBodyRows = email.plainTextBody.split('\n');
            System.debug('emailBodyRows+++++++++++++++++'+emailBodyRows);

         for (String bodyRow :emailBodyRows) {
            String description = emailBodyRows[2]+ ' \r\n'+emailBodyRows[6]+ ' \r\n'+emailBodyRows[10]+ ' \r\n'+emailBodyRows[11]+ ' \r\n'+emailBodyRows[12]; 
             System.debug('***************************'+description);
         }

        CreateLeadGenLogFromEmail emailtest1 = new CreateLeadGenLogFromEmail();
        emailtest1.handleInboundEmail(email,env);
        
    }
    }
static testMethod void receivedapricesuggestionUpdate() 
    {
        TestFactory.createSalesUser();  
       TestFactory.createLeadGenLogSettings();
       Lead_Gen_Log__c leadobj = TestFactory.createLeadGenLog();
        leadobj.Trusted_Checkout_Deal_ID__c = 'TC-479058';
       
        update leadobj;
        
        system.debug(leadobj.Email__c);
 
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        // Test with the subject that matches the  statement.
        email.subject = 'You\'ve received a price suggestion (TC-479058)';
        email.fromAddress = 'TrustedCheckout@chrono24.com';
        email.plainTextBody = 'Dear Test Dealer/\n' + 'You\'ve received a price suggestion/\n' + 'Please answer this request in your dealer area./\n' + 'REVIEW PRICE SUGGESTION/\n' + 'Offer information/\n' + 'Richard Mille RM033 Extra Flat RM033/\n'  +	'$64,950/\n' + 'Order information/\n' + 'Code:	4017075/\n' + 'Price:	$64,950.00'+'\n' + 'Buyer\'s suggested price:	$50,000.00'+'\n' + 'Buyer\'s name:	Mohannad Abanomy/\n' + 'Country:	Saudi Arabia'+'\n' + ' Phone number:	+966566663366/\n' + 'Status:	Offer requested/\n' +'Next step:	Review price suggestion/\n'+'You can find more information and other options here:/\n'+'PRICE SUGGESTION TC-1104571' + ' /\n' +' /\n'+ ' /\n'+ ' /\n' + ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n'+ ' /\n';


        CreateLeadGenLogFromEmail emailtest1 = new CreateLeadGenLogFromEmail();
        emailtest1.handleInboundEmail(email,env);
        
    }
 
    
 
  }