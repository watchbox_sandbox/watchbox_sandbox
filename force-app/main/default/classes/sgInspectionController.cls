@RestResource(urlMapping='/Inspections/*')
global class sgInspectionController {
	@HttpGet
    global static sgInspectionData get() {
        sgInspectionData s = new sgInspectionData();
        
        Map<string, List<string>> options = s.FieldOptions;
        
        return s;
    }
        
    @HttpPost
    global static string post(Inspection__c inspection, string status, List<string> fields) {
        sgAuth.requireRole('Admin,Watchmaker');
        
        sgInspectionData sgid = new sgInspectionData();
        
        string id = inspection.Id;
		Inspection__c p = Database.query('select ' + string.join(fields, ', ') + ' from Inspection__c where Id = :id limit 1');   

        for (string f : fields) {
            if (!f.contains('__r')) {
                p.put(f, inspection.get(f));
            }
        }
        
        if (inspection.Issue_Type__c != null && inspection.Issue_Type__c != '') {
            p.Mark_Issue__c = true;
            
            Inspection__c emailInspection = [
                select Id, Intake__r.Deal__r.Owner.Email, Related_Inventory__r.Model__r.Name, Intake__r.Reference__c 
                from Inspection__c where Id = :p.Id limit 1
            ];
            
            
            sgEmail em = new sgEmail();
            em.IntakeIssue(
                emailInspection.Intake__r.Deal__r.Owner.Email, 
                emailInspection.Related_Inventory__r.Model__r.Name, 
                emailInspection.Intake__r.Reference__c, 
                inspection.Issue_Type__c + ', ' + inspection.Issue_Notes__c
            );

       	}
        
        if (status == 'Pickups') {
            p.Mark_For_Pickup__c = true;
            p.TimeInspected__c = system.now();
        }
            
        update p;
        
        return '';
    }
    
    @HttpPut
    global static string put(string id) {
        sgAuth.requireRole('Admin,Watchmaker');
        
		Inspection__c i = [select id, Inspection_Start_Time__c from Inspection__c where Id = :id limit 1];

        if (i.Inspection_Start_Time__c == null) {
        	i.Inspection_Start_Time__c = System.now();
        }
        
        update i;
        
        return 'updated ' + id;
    }
    
    @HttpPatch
    global static string patch(string id, string status) {
        Inspection__c p = [select Id from Inspection__c where Id = :id limit 1];
        
        if (status == 'Approve') {
            p.Request_Approval__c = true;
            p.Time_Picked_Up__c = system.now();
        } else if (status == 'Approved') {
            p.Sales_Approved__c = true;
            p.Time_Sales_Approved__c = system.now();
        }
            
        update p;
        
        return '';
    }
}