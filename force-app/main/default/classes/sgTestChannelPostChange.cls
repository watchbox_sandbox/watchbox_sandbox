@isTest
public class sgTestChannelPostChange {

    static testmethod void insertRecord() {
        //arrange
        //
        Product2 product = createProduct();
        
        ChannelPost__c channelPost1 = new ChannelPost__c(
            Channel__c = 'Watchbox International',
            Product__c = product.Id,
            Is_Processing__c = false,
            Is_Dirty__c = false
        );
        ChannelPost__c channelPost2 = new ChannelPost__c(
            Channel__c = 'Facebook',
            Product__c = product.Id,
            Is_Processing__c = true,
            Is_Dirty__c = false
        );
        ChannelPost__c channelPost3 = new ChannelPost__c(
            Channel__c = 'Amazon',
            Product__c = product.Id,
            Is_Processing__c = false,
            Is_Dirty__c = false
        );
        
        //act
        //
        Test.startTest();
        insert channelPost1;
        insert channelPost2;
        insert channelPost3;
        Test.stopTest();
        
        //assert
        //
        //repull the channel post to see if the fields were updated
        channelPost1 = [SELECT Is_Processing__c, Is_Dirty__c FROM ChannelPost__c WHERE Id = :channelPost1.Id];
        
        //watchbox no longer being ignored
        //System.assertEquals(false, channelPost1.Is_Processing__c); //Watchbox should be ignored by the trigger so this shouldn't change
        System.assertEquals(false, channelPost1.Is_Dirty__c);
        
        channelPost2 = [SELECT Is_Processing__c, Is_Dirty__c FROM ChannelPost__c WHERE Id = :channelPost2.Id];
        System.assertEquals(true, channelPost2.Is_Processing__c);
        System.assertEquals(true, channelPost2.Is_Dirty__c);
        
        channelPost3 = [SELECT Is_Processing__c, Is_Dirty__c FROM ChannelPost__c WHERE Id = :channelPost3.Id];
        System.assertEquals(true, channelPost3.Is_Processing__c);
        System.assertEquals(false, channelPost3.Is_Dirty__c);
    }
    
    static testmethod void postRecord() {
        //arrange
        //
        Product2 product = createProduct();
        
        ChannelPost__c channelPost1 = new ChannelPost__c(
            Channel__c = 'Watchbox',
            Product__c = product.Id,
            Is_Processing__c = false,
            Is_Dirty__c = false
        );
        insert channelPost1;
        
        //act
        //
        Test.startTest();
        channelPost1.Post__c = true;
        update channelPost1;
        Test.stopTest();
        
        //assert
        //
        product = [SELECT First_Posted_Date__c FROM Product2 WHERE Id = :product.Id];
        System.assertNotEquals(null, product.First_Posted_Date__c);
    }
    
    static testMethod void missingProduct() {
        //arrange
        //
        ChannelPost__c channelPost1 = new ChannelPost__c(
            Channel__c = 'Facebook',
            Is_Processing__c = false,
            Is_Dirty__c = false
        );
        
        //act
        //
        Test.startTest();
        insert channelPost1;
        channelPost1.Post__c = true;
        update channelPost1;
        Test.stopTest();
        
        //assert
        //
        //the assertion comes from the lack of an exception
    }
    
    static Product2 createProduct() {
        Product2 product = new Product2 (
        	Name = 'test'
        );
        insert product;
        return product;
    }
    
}