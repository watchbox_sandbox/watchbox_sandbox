@isTest
public class OpportunityApprovalProcessTest {
    
    @isTest
    public static void approvalTest(){
        String userid = Userinfo.getUserId();
        User user2 = new User();
        user2.ProfileId = '00e50000001OmgM';
        user2.FirstName = 'test';
        user2.LastName = 'test';
        user2.Email = 'testtest@govbergwatcheshh.com';
        user2.Username = 'test@govberg.com';
        user2.Alias = 'test';
        user2.CommunityNickname = 'tset';
        user2.LocaleSidKey = 'en_US';
        user2.TimeZoneSidKey = 'America/New_York';
        user2.LanguageLocaleKey = 'en_US';
        user2.EmailEncodingKey = 'UTF-8';
        user2.IsActive = true;
        insert user2;
        
        
        User user = new User();
        user.ProfileId = '00e500000014Tsy';
        user.FirstName = 'Lagfnhhhhjjfdyr';
        user.LastName = 'Boggfhhhtrr';
        user.Email = 'laureniiyr@govbergwatcheshh.com';
        user.Username = 'unitghesthh@govberg.com';
        user.Alias = 'utjjryr';
        user.CommunityNickname = 'utestjjytu';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.IsActive = true;
        user.ManagerId=user2.id;
        
        insert user;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.IsNoteAttached__c='N';
        opp.CloseDate=System.today().addMonths(1); 
        opp.Last_Activity_Date__c=Date.today().addDays(-1);
        opp.OwnerId=user.id;
        opp.Is_ref_currently_in_stock__c=true;
        opp.Approval_Status__c='approved';
        insert opp;
        
        
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitting request for approval.');
        req.setNextApproverIds(new ID[]{user2.id} );
        req.setObjectId(opp.Id);
        
        Approval.ProcessResult resu = Approval.process(req);
        
        System.assert(resu.isSuccess());
        
        System.assertEquals('Pending', resu.getInstanceStatus(),'Instance Status'+resu.getInstanceStatus());
        system.debug('status for approval::'+resu.getInstanceStatus());
        List<Id> newWorkItemIds = resu.getNewWorkitemIds();
        system.debug('newWorkItemIds ::'+newWorkItemIds );
        
        Approval.ProcessWorkitemRequest req2 =  new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});//UserInfo.getUserId()
        system.debug('req2::'+req2);
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        system.debug('req3::'+req2);
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
        
        System.assertEquals(  'Approved', result2.getInstanceStatus(),'Instance Status'+result2.getInstanceStatus());
        
        ApexPages.currentPage().getParameters().put('id',opp.id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        OpportunityApprovalProcess.submitApprovalProcess(opp.id); 
        
    }
    @isTest
    public static void getDealTest(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.IsNoteAttached__c='N';
        opp.CloseDate=System.today().addMonths(1); 
        opp.Last_Activity_Date__c=Date.today().addDays(-1);
        opp.Is_ref_currently_in_stock__c=true;
        opp.Approval_Status__c='approved';
        insert opp;
        Deal_Approval_Process__c settingobj=new Deal_Approval_Process__c();
        settingobj.Is_watch_Purchased_if_already_in_Stock__c=true;
        insert settingobj;
        OpportunityApprovalProcess.getDealDetails(opp.Id);
        
    }
    
}