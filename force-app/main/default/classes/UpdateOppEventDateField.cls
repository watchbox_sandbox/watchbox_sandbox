public class UpdateOppEventDateField {
    @future
    public static void updateEventDateField(Set<Id> lstOppIds){
         List<Opportunity>  lstOpportunity = new List<Opportunity>();
        for(Opportunity opp: [Select id,CreatedDate from opportunity where id in:lstOppIds ]){
           Opportunity objOpp = new Opportunity(id=opp.id);
           objOpp.Event_Date__c = opp.CreatedDate.format('dd-MMM-yyyy HH:mm:ss Z');
           lstOpportunity.add(objOpp);
        }    
        if(lstOpportunity.size() > 0 || lstOpportunity !=null){
            update lstOpportunity;
        }
    }
}