@RestResource(urlMapping='/MultiPicklist/*')
global class sgMultiPicklistController {
	@HttpGet
    global static Map<string, List<string>> get() {
        sgAuth.check();
        
        List<string> fields = RestContext.request.params.get('fields').split(',');
        
        Map<string, List<string>> picklists = new Map<string, List<string>>();
        
        for (string f : fields) {
            List<string> values = sgUtility.getPicklistValues(
                RestContext.request.params.get('objectName'), 
                f
            );
            
            values.sort();
            
            picklists.put(f, values);
        }
        
        return picklists;
    }
}