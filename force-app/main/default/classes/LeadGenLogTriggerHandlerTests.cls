@isTest
public class LeadGenLogTriggerHandlerTests {
    
    @testSetup
    static void setup() {
        TestFactory.createLeadGenLogSettings();
    }
    
    static testmethod void processIncomingLeads_foundEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundSecondaryEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'wrong@unittest.com';
        account.Secondary_Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundNoPrimarySecondaryEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Secondary_Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void pricewaiterOffer_test15kabove() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test123@unittest.com';
        insert account;
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Watch_Brand__c='Pre-Owned Panerai Luminor 1950 3-Days GMT Acciaio PAM 535';
        lgl.Phone__c = '123-456-7890';
        lgl.SKU__c='98765';
        lgl.Client_s_Offer__c=17000;
        lgl.Pricewaiter_Id__c='789';
        lgl.Web_price__c=653.2;
        lgl.Lead_Source__c='Govbergwatches.com - PriceWaiter - New Offer';
        insert lgl;
        
    }
    
    
    static testmethod void pricewaiterOffer_test5kbelow() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test1423@unittest.com';
        insert account;
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test1423@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Watch_Brand__c='Pre-Owned Panerai Luminor 1950 3-Days GMT Acciaio PAM 535';
        lgl.Phone__c = '123-456-7890';
        lgl.SKU__c='98765';
        lgl.Client_s_Offer__c=2000;
        lgl.Pricewaiter_Id__c='946464';
        lgl.Web_price__c=653.2;
        lgl.Lead_Source__c='Govbergwatches.com - PriceWaiter - New Offer';
        insert lgl;
        
    }
    
     static testmethod void leadSourceChrono24Test1() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test1423@unittest.com';
        insert account;
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test1423@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Watch_Brand__c='Pre-Owned Panerai Luminor 1950 3-Days GMT Acciaio PAM 535';
        lgl.Phone__c = '123-456-7890';
        lgl.SKU__c='98765';
        lgl.Client_s_Offer__c=2000;
        lgl.Pricewaiter_Id__c='946464';
        lgl.Web_price__c=653.2;
        lgl.Lead_Source__c='Chrono24';
         lgl.IP_Country__c='United States of America';
        insert lgl;
        
    }
    
    static testmethod void leadSourceChrono24Test2() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test1423@unittest.com';
        insert account;
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test1423@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Watch_Brand__c='Pre-Owned Panerai Luminor 1950 3-Days GMT Acciaio PAM 535';
        lgl.Phone__c = '123-456-7890';
        lgl.SKU__c='98765';
        lgl.Client_s_Offer__c=2000;
        lgl.Pricewaiter_Id__c='946464';
        lgl.Web_price__c=653.2;
        lgl.Lead_Source__c='Chrono24';
         lgl.IP_Country__c='Hong Kong';
        insert lgl;
        
    }
    static testmethod void leadSourceChrono24Test3() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test1423@unittest.com';
        insert account;
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test1423@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Watch_Brand__c='Pre-Owned Panerai Luminor 1950 3-Days GMT Acciaio PAM 535';
        lgl.Phone__c = '123-456-7890';
        lgl.SKU__c='98765';
        lgl.Client_s_Offer__c=2000;
        lgl.Pricewaiter_Id__c='946464';
        lgl.Web_price__c=653.2;
        lgl.Lead_Source__c='Chrono24';
         lgl.IP_Country__c='Switzerland';
        insert lgl;
        
    }
    
    static testmethod void processIncomingLeads_foundPhoneMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Phone = '1234567890'; 
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundMobileMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Mobile__c = '1234567890';         
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundNoPhoneMobileMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Phone = '5555555555';
        account.Mobile__c = '1234567890';  
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_noMatches_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';        
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(null, lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_createAccount_test() {       
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(null, lgl.Matched_Account__c);
        
        Account account = [SELECT Id, Lead_Gen_Log_External_Id__c FROM Account LIMIT 1];
        System.assertNotEquals(null, account.Lead_Gen_Log_External_Id__c);
    }
    
    static testmethod void processIncomingLeads_matchedEmailAccountOpportunity_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
        
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        System.assertEquals(account.Id, opp.AccountId);
    }
    
    static testmethod void processIncomingLeads_matchedPhoneAccountOpportunity_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Phone = '1234567890'; 
        insert account;
        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT ID, Matched_Account__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
        
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        System.assertEquals(account.Id, opp.AccountId);
    }
    
    static testmethod void processingIncomingLeads_multiple_test() {
        List<Lead_Gen_Log__c> leads = new List<Lead_Gen_Log__c>();
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        
        Account a = TestFactory.buildAccount();
        a.First_Name__c = 'Test';
        a.Last_Name__c = 'User';
        a.Phone = '1234567890'; 
        insert a;
        
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();
        lgl.Email__c = '';
        leads.add(lgl);
        
        Lead_Gen_Log__c lead = TestFactory.buildLeadGenLog();
        lead.Phone__c = '';
        lead.Images__c = 'https://www.govbergwatches.com/wp-content/uploads/wws_uploads/11e3ff37378fa6904dafd6bfef8733be.jpg,https://www.govbergwatches.com/wp-content/uploads/wws_uploads/11e3ff37378fa6904dafd6bfef8733be.jpg';
        leads.add(lead);
        
        insert leads;
        
        List<Opportunity> opps = [SELECT Id FROM Opportunity];
        List<Account> accounts = [SELECT Id FROM Account];
        
        System.assertEquals(2, opps.size());
        System.assertEquals(2, accounts.size(), 'No additional accounts should be created');
    }
    
    static testmethod void getDealScheduleOwnerId_noResults_test() {
        Account account = TestFactory.createAccount();
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c ds = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(null, ds);
    }
    
    static testmethod void getDealScheduleOwnerId_future_test() {
        Account account = TestFactory.createAccount();
        Deal_Schedule__c ds = TestFactory.buildDealSchedule(account, null);
        ds.Date__c = DateTime.now().addDays(1);
        insert ds;
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(null, result, 'Future date should not be returned');
    }
    
    static testmethod void getDealScheduleOwnerId_multiple_test() {
        Account account = TestFactory.createAccount();
        User user = TestFactory.createSalesUser();
        Deal_Schedule__c ds = TestFactory.buildDealSchedule(account, user);
        ds.Date__c = DateTime.now().addDays(1);
        insert ds;
        
        Deal_Schedule__c ds2 = TestFactory.buildDealSchedule(account, user);
        ds2.Date__c = DateTime.now();
        insert ds2;
        
        Deal_Schedule__c ds3 = TestFactory.buildDealSchedule(account, user);
        ds3.Date__c = DateTime.now().addDays(-1);
        insert ds3;
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(ds2.Id, result.Id, 'Incorrect schedule returned');
    }
    
    static testmethod void getDealScheduleOwnerId_success_test() {
        Account account = TestFactory.createAccount();       
        Deal_Schedule__c ds = TestFactory.createDealSchedule(account, null);
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(ds.Id, result.Id, 'Incorrect schedule returned');
    }
    
    
    
    static testmethod void processingIncomingLeads_walnutLeadSource_test() {
        LeadGenLog_Settings__c settings = TestFactory.getLeadSettings();
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Walnut Retail Checkin';
        insert lgl;
        Opportunity opp = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        System.assertEquals((Id)settings.Lead_Source_Walnut_User_Id__c, opp.OwnerId);
    }
    
    
    static testmethod void processingIncomingLeads_watchBoxApp_testBoxPaper() {
        
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='11';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        System.assertEquals(opp.Boxes_and_Papers__c,'Box & Papers');  
        
    }
    
    static testmethod void processingIncomingLeads_Watchbox_eBay() {
        
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Watchbox eBay';
        lgl.Boxes_and_Papers__c='11';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
      //  System.assertEquals(opp.Boxes_and_Papers__c,'Box & Papers');  
        
    }
    static testmethod void processingIncomingLeads_watchBoxApp_testBox() {
        
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='10';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        System.assertEquals(opp.Boxes_and_Papers__c,'Box only');  
        
    }
    static testmethod void processingIncomingLeads_watchBoxApp_testPaper() {
        
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='01';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        System.assertEquals(opp.Boxes_and_Papers__c,'Papers Only');  
        
    }
    
    static testmethod void processingIncomingLeads_watchBoxApp_Neither() {
        Lead_Gen_Log__c lgl = TestFactory.buildLeadGenLog();        
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='00';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        System.assertEquals(opp.Boxes_and_Papers__c,'Neither');  
    }
    
    
    
    static testmethod void afterInsert_phoneFormatted_test() {                        
        TestFactory.createLeadGenLog();
        Lead_Gen_Log__c lgl = [SELECT Id, Phone__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals('1234567890', lgl.Phone__c);    
    }
    
    static testmethod void testUserSellBrand() {    
        Brand_object__c brandRecord = new Brand_object__c();
        brandRecord.Name = 'Kobold';
        brandRecord.Undesirable_Watch__c = false;
        Insert brandRecord;
        User userRecord = TestFactory.createSalesUserForOwner();
        User_Brand__c userBrand = new User_Brand__c();
        userBrand.Brand__c = brandRecord.Id;
        userBrand.User__c = userRecord.Id;
        insert userBrand;
        Lead_Gen_Log__c  leadGenLogRecord = TestFactory.buildLeadGenLog();
        leadGenLogRecord.Sell_Brand__c = 'Kobold';
        Insert leadGenLogRecord;
        Lead_Gen_Log__c lgl = [SELECT Id, Phone__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals('1234567890', lgl.Phone__c);    
    }
    
    
    static testmethod void testUserWatchBrand() {    
        Brand_object__c brandRecord = new Brand_object__c();
        brandRecord.Name = 'Kobold';
        brandRecord.Undesirable_Watch__c = false;
        Insert brandRecord;
        User userRecord = TestFactory.createSalesUserForOwner();
        User_Brand__c userBrand = new User_Brand__c();
        userBrand.Brand__c = brandRecord.Id;
        userBrand.User__c = userRecord.Id;
        insert userBrand;
        Lead_Gen_Log__c  leadGenLogRecord = TestFactory.buildLeadGenLog();
        leadGenLogRecord.Watch_Brand__c = 'Kobold';
        Insert leadGenLogRecord;
        Lead_Gen_Log__c lgl = [SELECT Id, Phone__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals('1234567890', lgl.Phone__c);    
    }    
    
    static testmethod void testWithNoBrand() {    
        Brand_object__c brandRecord = new Brand_object__c();
        brandRecord.Name = 'Kobold';
        brandRecord.Undesirable_Watch__c = false;
        Insert brandRecord;
        User userRecord = TestFactory.createSalesUserForOwner();
        User_Brand__c userBrand = new User_Brand__c();
        userBrand.Brand__c = brandRecord.Id;
        userBrand.User__c = userRecord.Id;
        insert userBrand;
        Lead_Gen_Log__c  leadGenLogRecord = TestFactory.buildLeadGenLog();
        leadGenLogRecord.Watch_Brand__c = '';
        leadGenLogRecord.Sell_Brand__c = '';
        Insert leadGenLogRecord;
        Lead_Gen_Log__c lgl = [SELECT Id, Phone__c FROM Lead_Gen_Log__c LIMIT 1];
        System.assertEquals('1234567890', lgl.Phone__c);    
    }
    
    
    Static testmethod void TestException() {
        try{
            Lead_Gen_Log__c leadTest = new Lead_Gen_Log__c();
            leadTest.First_Name__c = 'TestFirstName';
            leadTest.Last_Name__c =  'TestLastName';
            insert leadTest;
        }catch(Exception e){
        }
    }
    
    
    
    
}