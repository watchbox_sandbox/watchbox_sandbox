@isTest
public class sgTestPostingBatchScheduled {

    static testmethod void runSchedule() {
        //arrange
        //
        sgPostingBatchScheduled schedule = new sgPostingBatchScheduled();
        
        //act
        //
        Test.startTest();
        String jobId = System.schedule('testBasicScheduledApex', '0 0 0 3 9 ? 2022', schedule);
        Test.stopTest();
        
        //assert
        //
        //there is so little to the schedule class there really isn't anything to test
    }
}