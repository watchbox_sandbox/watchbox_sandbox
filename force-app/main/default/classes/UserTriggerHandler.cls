public class UserTriggerHandler extends TriggerHandler{
    
    public override void beforeUpdate() {
        try{
            User  currentuser=[Select Id,Name,Email,UserRoleId,ProfileId from User where Id=:userinfo.getuserId()];
            system.debug('Current user profile id : '+currentuser.ProfileId);
            for (User user : (List<User>)Trigger.new) {  
                User oldUser=(User)Trigger.oldMap.get(user.Id);
                    if(!currentuser.ProfileId.equals(System.Label.SalesManagerId)  && !currentuser.ProfileId.equals(System.Label.Shannon_Beck_Id)&& !currentuser.ProfileId.equals(System.Label.Shannon_Beck_Mimic_Id) && !currentuser.ProfileId.equals(System.Label.Administrator_Id) && !currentuser.ProfileId.equals(System.Label.System_Administrator_Id)){
                        system.debug('inside if');
                        if(oldUser.Opt_out__c==true && user.Opt_out__c==false){
                            system.debug('you are not allowed');
                            user.addError('You are not authorized person to opt-in');
                        }
                    }
                    if(currentuser.ProfileId.equals(user.ProfileId) && !currentuser.id.equals(user.id) ){
                        if(oldUser.Opt_out__c!=user.Opt_out__c){
                            user.addError('You are not authorized to edit opt-out');
                        }
                    }
                
                
            }
            
        }Catch(Exception e){
            System.debug('Error during user update - ' + e.getMessage());
        }
    }
}