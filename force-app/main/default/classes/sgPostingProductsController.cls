@RestResource(urlMapping='/PostingProducts/*')
global class sgPostingProductsController {
    @HttpGet
    global static List<sgPostingProductsDTO> get () {
        //run the custom auth check
        sgAuth.check();
        //create a list to hold the results
        List<sgPostingProductsDTO> postingProducts = new List<sgPostingProductsDTO>();
        //get the list of products
        List<Product2> products = getProducts();
        //loop through the products and create a sgPostingProductsDTO for each
		
        Map<string, List<ChannelPost__c>> channelPosts = getChannelPosts(sgUtility.extract('Id', products));

        for (Product2 product : products) {
            postingProducts.add(translateToPostingProducts(product, channelPosts.get(product.Id)));
        }
        //return the results
        return postingProducts;
    }
    
    @HttpPost
    global static Boolean post(List<sgPostingProductsDTO> products, List<sgChannelInfoDTO> channelInfo) {
        //run the custom auth check
        sgAuth.check();
        //validate there is a products list
        if (products == null || products.size() == 0) {
            return false;
        }
        //validate the channel info
        if (channelInfo == null || channelInfo.size() == 0) {
            return false;
        }
        //update each product
        for(Integer i = 0; i < products.size(); i++) {
            updateProduct(products[i], channelInfo);
        }
        //if we made it here, it was a success
        return true;
    }
    
    //****************************************************************************
    // support methods
    // 
    public class ProductNotFoundException extends Exception {}
    public class ProductPostException extends Exception {}
    
    ///
    // Returns a list of products based on the querystring parameters
    // 
    static List<Product2> getProducts() {
        //see if there is any filters
        RestRequest req = RestContext.request;
        Map<String,String> params = req.params;
        String InventoryId = params.get('InventoryId');
        String LocationId = params.get('LocationId');
        String IsPublished = params.get('IsPublished');
        
        string query = 'SELECT ID, Inventory_ID__c, Location__r.Name FROM Product2 WHERE IsActive = true and Name != null and ASK__c !=  null and Model__c != null and Model__r.Model_Brand__c != null and Model__r.Model_Series__c != null AND ';
        integer limitAmount = 100;
        
        if (LocationId != null && IsPublished != null) {
            if (IsPublished == 'true'){ 
                query += 'Location__c = :LocationId AND Product2.Id IN (SELECT Product__c FROM ChannelPost__c WHERE Post_Date__c != null) ';                
            }
            else {
                query += 'Location__c = :LocationId AND Product2.Id NOT IN (SELECT Product__c FROM ChannelPost__c WHERE Post_Date__c != null) ';
           }
        }
        else if (IsPublished != null) {
            if (IsPublished == 'true'){ 
                query += 'Product2.Id IN (SELECT Product__c FROM ChannelPost__c WHERE Post_Date__c != null) ';
            }
            else {
                query += 'Product2.Id NOT IN (SELECT Product__c FROM ChannelPost__c WHERE Post_Date__c != null) ';
           }
        }
        else if (InventoryId != null) {
            query += 'Inventory_ID__c = :InventoryId';
            limitAmount = 1;
        }
        else if (LocationId != null) {
            query += 'Location__c = :LocationId ';
        }
        else {
            query += 'Photographed__c != null ';
        }
        
        query += ' LIMIT ' + limitAmount;   
        
        return Database.query(query);
    }
    
    ///
    // Updates the product for each channel info
    // 
    static void updateProduct(sgPostingProductsDTO postingProduct, List<sgChannelInfoDTO> channelInfo) {
        Product2 product;
        //attempt to pull the product
        try {
            product = [SELECT Id, Name FROM Product2 WHERE Id = :postingProduct.Id];
        }
        catch (Exception ex) {
            throw new ProductNotFoundException(postingProduct.Id);
        }
        
        //get the products current channel post recordset
        Map<String, ChannelPost__c> channelPostMap = createChannelPostMap(postingProduct.Id);

        //loop through each channel
        for (Integer c = 0; c < channelInfo.size(); c++) {
            sgChannelInfoDTO channel = channelInfo[c];
            postChannel(postingProduct.Id, channelInfo[c], channelPostMap.get(channel.Name));
        }
    }
    
    ///
    // Looks up the product's channel post records and creates a map with the Channel__c name as the key
    // 
    static Map<String, ChannelPost__c> createChannelPostMap(String productId) {
        Map<String, ChannelPost__c> channelPostMap = new Map<String, ChannelPost__c>();
        
        //get the channels for the product id
        List<ChannelPost__c> productChannels = [SELECT Id, Name, Channel__c, Post__c, Visible__c, Post_Date__c, Posted_Date__c, Visible_Date__c FROM ChannelPost__c WHERE Product__c = :productId];
        
        //loop through each product channel
        for(Integer pc = 0; pc < productChannels.size(); pc++) {
            channelPostMap.put(productChannels[pc].Channel__c, productChannels[pc]);
        }
        
        return channelPostMap;
    }
    
    ///
    // Runs the post logic for the channel
    // 
    static void postChannel(String productId, sgChannelInfoDTO channel, ChannelPost__c channelPost) {
        Boolean isInsert = false; //tracks if the DB transaction is going to be an insert or update
        //#1 new post
        if (channelPost == null) {
            //only add a new post if there are non-default values
            if (channel.Post != false || channel.Visible != false || channel.PostDate != null) {
                channelPost = translateToChannelPost(channel);
                channelPost.Product__c = productId;
                isInsert = true;
            }
            else {
                return; //nothing to do, just exit
            }
        }
        //#2 change
        else {
            //TODO: if there is rejection logic put it here
            
            //only update the post date if the post has changed
            if (channel.Post == true && channelPost.Post__c == false) {
                channelPost.Post_Date__c = channel.PostDate;
            }
            
            channelPost.Post__c = channel.Post;
            channelPost.Visible__c = channel.Visible;
        }
        
        //if post is set and no date is present then default it
        if (channelPost.Post__c && channelPost.Post_Date__c == null) {
            channelPost.Post_Date__c = Date.today();
        }
        //if post is not set the remove the post date
        if (channelPost.Post__c == false && channelPost.Post_Date__c != null) {
            channelPost.Post_Date__c = null;
        }
        
        //if there isn't an organization, default it
        if (channelPost.Organization__c == null) {
            channelPost.Organization__c = [SELECT Id FROM Organization__c WHERE Name = 'Watchbox' LIMIT 1].Id;
        }
        
        //update/insert with modified info
        if (isInsert) {
            insert channelPost;
        }
        else {
            update channelPost;
        }
        //the actual posting happens with a trigger
    }
    
    ///
    // Creates a sgPostingProductsDTO object for the product
    //
    static sgPostingProductsDTO translateToPostingProducts(Product2 product, List<ChannelPost__c> channelPosts) {
        sgPostingProductsDTO posting = new sgPostingProductsDTO();
        posting.Id = product.ID;
        posting.Location = product.Location__r.Name;
        posting.InventoryId = product.Inventory_ID__c;
        //get the Channel Posts
        posting.Channels = new List<sgChannelInfoDTO>();
        
        if (channelPosts != null)  {
            for(ChannelPost__c channel : channelPosts) {
                posting.Channels.add(translateToChannelInfo(channel));
            }
        }
        
        //return the posting entry
        return posting;
    }
    
    ///
    // Creates a ChannelInfo object for a ChannelPost
    // 
    static sgChannelInfoDTO translateToChannelInfo(ChannelPost__c channel) {
        sgChannelInfoDTO chanInfo = new sgChannelInfoDTO();
        chanInfo.Name = channel.Channel__c;
        chanInfo.Post = channel.Post__c;
        chanInfo.Visible = channel.Visible__c;
        chanInfo.PostDate = channel.Post_Date__c;
        chanInfo.PostedDate = channel.Posted_Date__c;
        chanInfo.VisibleDate = channel.Visible_Date__c;
        
        return chanInfo;
    }
    
    static Map<string, List<ChannelPost__c>> getChannelPosts(List<string> productIds) {
        List<ChannelPost__c> channelPosts = 
            [SELECT Name, Channel__c, Post__c, Visible__c, Post_Date__c, Posted_Date__c, Visible_Date__c, Product__c
             FROM ChannelPost__c WHERE Product__c in :productIds ORDER BY Channel__c];
        
        Map<string, List<ChannelPost__c>> channelMap = new Map<string, List<ChannelPost__c>>();
        for (ChannelPost__c c : channelPosts) {
            if (!channelMap.containsKey(c.Product__c)) {
                channelMap.put(c.Product__c, new List<ChannelPost__c>());
            }
            
            channelMap.get(c.Product__c).add(c);
        }
        
        return channelMap;
    }
    
    ///
    // Creates a Channel Post object from a ChannelInfoDTO
    // 
    static ChannelPost__c translateToChannelPost(sgChannelInfoDTO channel) {
        ChannelPost__c channelPost = new ChannelPost__c (
            Name = channel.Name,
            Channel__c = channel.Name,
            Post__c = channel.Post,
            Visible__c = channel.Visible,
            Post_Date__c = channel.PostDate
        );
        return channelPost;
    }
    
}