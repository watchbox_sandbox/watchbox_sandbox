@isTest
public class sgTestMisc {
    static testMethod void sgUtilityQueryOnlyMine() {
        sgUtility.query('select id from User', new Map<string, string>(), true);
    }
    
    static testMethod void sgUtilityIfNull() {
        system.assertEquals('test', sgUtility.ifNull(null, 'test'));
    }
    
    static testMethod void sgUtilityGetBoolean() {
        sgUtility.getBoolean();
    }
    
    static testMethod void sgUtilitySelectStar() {
        sgUtility.selectStar('User');
    }
    
    static testMethod void sgUtilityParseDate() {
        sgUtility.parseDate('6/1/16');
    }
    
    static testMethod void sgUtilityMapBy() {
        List<Product2> products = sgDataSeeding.ProductsWithBrands();
        insert products;
        
        Map<string, sobject> mp = sgUtility.MapBy('id', products);
    }
    
    static testMethod void searchResult() {
        sgSearchResult sr = new sgSearchResult();
        
        sr.Accessories = new List<Map<string, string>>();
        sr.Brands = new List<Map<string, string>>();
        sr.Customers = new List<Map<string, string>>();
        sr.Models = new List<Map<string, string>>();
        sr.Products = new List<Map<string, string>>();
        sr.Users = new List<Map<string, string>>();
        sr.Vendors = new List<Map<string, string>>();
    }
    
    static testMethod void picklist() {
        RestRequest req = new RestRequest();
        req.params.put('objectName', 'Product2');
        req.params.put('field', 'Product_Type__c');
        RestContext.request = req;
        
        List<string> types = sgPicklistLookupController.get();
        
        system.assert(types.size() > 0);
    }
    
    static testMethod void email() {
        sgEmail.test();
    }
    
    static testMethod void accountCreation() {    
    	Team_Member__c tm = sgDataSeeding.TeamMembers().get(0);
        
        insert tm;
        
        sgAccountCreationTrigger.finalizeAccount(tm.Id);
    }
}