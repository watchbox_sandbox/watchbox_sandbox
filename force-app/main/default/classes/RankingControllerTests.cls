@isTest
public class RankingControllerTests {
    static testmethod void getResults_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opportunity = TestFactory.createOpportunity(account);
        
        RankingController controller = new RankingController();
        List<RankingResult> rankings = controller.getResults();
        
        System.assertEquals(1, rankings.size());
    }
}