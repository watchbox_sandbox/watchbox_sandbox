public class sgDataSeeding {
    //seed the org with any data we need
    public static void Setup() {
        insert Groups();
        insert Roles();
        
        list<team_member__c> tms = [select id, isactive__c from team_member__c];

        for (team_member__c tm : tms) {
            tm.isactive__c = true;
        }
        
        update tms;
    }
    
    public static List<Team_Member__c> TeamMembers() {
        return TeamMembers(10);
    }
    
    public static List<Team_Member__c> TeamMembers(integer num) {
        List<Team_Member__c> tm = new List<Team_Member__c>();
        for (integer i = 1; i <= num; i++) {
            Team_Member__c t = new Team_Member__c(
            	First_Name__c = 'Test',
                Last_Name__c = 'User ' + i,
                Work_Email__c = 'ssickman+' + i + '@surgeforward.com',
                IsActive__c = true,
                Password__c = 'a',
                Suppress_Account_Creation_Email__c = true
            );
            sgAuth.setPasswordHash(t);
            
            tm.add(t);
        }
        
        return tm;
    }
    
    public static List<Group__c> Groups(){
        return new List<Group__c> {
            new Group__c(
                Name = 'Admin'
            ),
            new Group__c(
                Name = 'Sales'
            ),
            new Group__c(
                Name = 'Support'
            )
        };
    }
    
    public static List<Role__c> Roles() {
        return new List<Role__c> {
            new Role__c(
                Name = 'Admin'
            ), new Role__c(
                Name = 'Operations'
            ),
            new Role__c(
                Name = 'User'
            ),
            new Role__c(
                Name = 'Watchmaker Lead'
            ),
            new Role__c(
                Name = 'Watchmaker'
            ),
            new Role__c(
                Name = 'Sales Manager'
            ),
            new Role__c(
                Name = 'Sales'
            )   
        };
    }
    
    public static List<Brand_object__c> Brands() {
        List<Brand_object__c> lst = new List<Brand_object__c>();
        
        for (integer i = 0; i < 10; i++) {
            lst.add(new Brand_object__c (
            	Name = 'Brand ' + i
            ));
        }
        
        return lst;
    }
    
    public static List<Series__c> Series() {
        List<Series__c> lst = new List<Series__c>();
        
        List<Brand_object__c> brands = [select id from Brand_object__c];
        
        for (integer i = 0; i < 10; i++) {
            lst.add(new Series__c (
            	Name = 'Series ' + i,
                Watch_Brand__c = brands.get(sgUtility.getRandomNumber(brands.size())).Id
            ));
        }
        
        return lst;
    }
    
    public static List<Model__c> Models() {
        List<Model__c> lst = new List<Model__c>();
        
        List<Brand_object__c> brands = [select id from Brand_object__c];
        List<Series__c> series = [select id from Series__c];
        
        for (integer i = 0; i < 10; i++) {
            lst.add(new Model__c (
            	Name = 'Model ' + i,
                Reference__c = 'Reference ' + i,
                SH_Watch_ID__c = 'Watch ID ' + i,
                Model_brand__c = brands.get(sgUtility.getRandomNumber(brands.size())).Id,
                model_series__c = series.size() != 0 ? series.get(sgUtility.getRandomNumber(series.size())).Id : null,
                Verified__c = true
            ));
        }
        
        return lst;
    }
    
    public static List<Product2> ProductsWithBrands() {
        return ProductsWithBrands(10);
    }
    public static List<Product2> ProductsWithBrands(integer size) {
    	insert Brands();
        insert Series();
        insert Models();
        
        return Products(size);
    }
    
    public static List<Product2> Products() {
        return Products(10);
    }
    
    public static List<Product2> Products(integer size) {
        List<Product2> lst = new List<Product2>();
        
        List<Brand_object__c> brands = [select id from Brand_object__c limit 1000];        
        List<Model__c> models = [select id from Model__c limit 1000];
        
        for (integer i = 0; i < size; i++) {
            lst.add(new Product2 (
            	Name = 'Product ' + i,
                Watch_Brand__c  = brands.get(sgUtility.getRandomNumber(brands.size())).Id,
                Model__c = models.get(sgUtility.getRandomNumber(models.size())).Id,
                IsActive = true,
                WUW_Date_Created__c = Date.today(),
                Product_Type__c = 'Regular Goods'
                
            ));
        }
        
        return lst;
    }
    
    public static List<GBWUW_Sales__c> GBWUWSales() {
        return GBWUWSales(10);
    }
    public static List<GBWUW_Sales__c> GBWUWSales(integer size) {
        List<GBWUW_Sales__c> lst = new List<GBWUW_Sales__c>();
        
        List<Brand_object__c> brands = [select id from Brand_object__c limit 1000];        
        List<Model__c> models = [select id from Model__c limit 1000];
        
        for (integer i = 0; i < size; i++) {
            lst.add(new GBWUW_Sales__c (
                Brand__c  = brands.get(sgUtility.getRandomNumber(brands.size())).Id,
                Trans_Date__c = Date.today()
                
            ));
        }
        
        return lst;
    }
}