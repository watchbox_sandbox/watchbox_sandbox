public class PhoneNumberFormatter {
    public static string stripCharacters(string phoneNumber) {
        string formatted = null;
        
        if (!string.isBlank(phoneNumber)) {            
            formatted = phoneNumber.replace(' ', '');
            formatted = formatted.replace('+1', '');
            
            Pattern nonAlphanumeric = Pattern.compile('[^0-9]');
			Matcher matcher = nonAlphanumeric.matcher(formatted);
			formatted = matcher.replaceAll('');                        
        }        
        
        return formatted;
    }
}