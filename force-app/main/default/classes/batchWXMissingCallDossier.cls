global class batchWXMissingCallDossier implements Database.Batchable<Sobject>,Database.AllowsCallouts,Database.stateful{
    public Integer startIndex = 0;
    public Integer endIndex = 0;
    public Integer indexrange = 0;
    global batchWXMissingCallDossier(Integer startIndex,Integer endIndex){
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String strQuery = 'Select Id,Id__c From WX_Dummy__c where Id__c >=: startIndex and Id__c <=: endIndex';
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<WX_Dummy__c> scope) {        
        indexrange = Integer.valueOf(scope[0].Id__c);
        system.debug('@@indexrange '+indexrange );
        WealthXCallDossier.getAllDossier(indexrange,indexrange); 
    }
    
    global void finish(Database.BatchableContext BC) {
       if(indexrange < 2862733 & !Test.isRunningTest()){
           WXMissingCallDossierScheduled objWXMissingScheduled = new WXMissingCallDossierScheduled();
           objWXMissingScheduled.execute(null);
       }       
    }
}