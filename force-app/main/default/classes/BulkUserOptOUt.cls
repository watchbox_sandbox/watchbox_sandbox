/**
* An apex class that updates portal user details.
Guest users are never able to access this page.
*/
public class BulkUserOptOUt {
    
    private List<User> user;
    private boolean isEdit = false;
    
    public List<User> getUser() {
        return user;
    }
    
    public BulkUserOptOUt() {
        // user = [SELECT id, email, Name,Opt_out__c,Alias,IsActive,username,ProfileId  FROM User where IsActive = true AND usertype != 'GUEST' AND Name != 'Automated Process' AND Department ='Sale'];

        
        user = [SELECT id,usertype,email,LastName,FirstName,Alias,Username,IsActive,ProfileId,Opt_out__c,
                LastLoginDate,ManagerId,Division,Department,User_Location__c,UserRoleId,LeadSourcesHandled__c   
                FROM User 
                where IsActive = true 
                AND usertype != 'GUEST'
                AND Name != 'Automated Process' 
                AND ID != '00550000007ocNg' 
                AND ID != '005500000074Oad' 
                AND Department LIKE '%Sales%' order by LastName ASC];
    }
    
    
    public Boolean getIsEdit() {
        return isEdit;
    }
    
    public void edit() {
        isEdit=true;
    }
    
    public void save() {
        try {
            update user;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string str = System.Label.Email_Id;
            List<String> lstemals = new List<String>();
            lstemals = str.split(',');
            system.debug(lstemals );
            mail.setToAddresses(lstemals);
            mail.setSubject('BulkUserOptOUt - Opt Out of User has changed');
            mail.setSaveAsActivity(false);
            mail.setPlainTextBody( 'BulkUserOptOUt save done by '+UserInfo.getName());
            system.debug(':::'+mail.getPlainTextBody());
             
 			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            isEdit=false;
        } catch(DmlException e) {
            System.debug('Exception e'+e);
            ApexPages.addMessages(e);
        }
    }
    
   
    
    public void cancel() {
        isEdit=false;
        // user = [SELECT id, email, Name,Opt_out__c,Alias,IsActive,username,ProfileId  FROM User where IsActive = true AND usertype != 'GUEST' AND Name != 'Automated Process' AND Department ='Sale'];
         user = [SELECT id,usertype,email,LastName,FirstName,Alias,Username,IsActive,ProfileId,Opt_out__c,LastLoginDate,ManagerId,Division,Department,User_Location__c,UserRoleId,LeadSourcesHandled__c  FROM User where IsActive = true AND usertype != 'GUEST' AND Name != 'Automated Process' AND ID != '00550000007ocNg' AND ID != '005500000074Oad' AND Department LIKE '%Sales%'];
    }    
}