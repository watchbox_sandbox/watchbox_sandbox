/**
 * This class provides test coverage for both the abstract
 * class sgPostingIntegrationMagento as well as all of its
 * extending classes.
 */
@isTest
public class sgTestPostingIntegrationMagento {
    static testMethod void SetPost_Fail() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"error": "missing many important things"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        Exception resultEx = null;
        try {
            poster.SetPost();
        }
        catch (Exception ex) {
            resultEx = ex;
        }
        Test.stopTest();

        // Assert
        System.assertEquals('no products returned', resultEx.getMessage());
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
    }

    static testMethod void SetPost_AnotherFail() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        Exception resultEx = null;
        try {
            poster.SetPost();
        }
        catch (Exception ex) {
            resultEx = ex;
        }
        Test.stopTest();

        // Assert
        System.assertEquals('no products returned', resultEx.getMessage());
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
    }

    static testMethod void SetPost_Success() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"entity_id": "223412345678"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        poster.SetPost();
        Test.stopTest();

        // Assert
        System.assertEquals('223412345678', poster.ChannelPost.External_Id__c);
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"isActive":false'));
    }

    static testMethod void SetPostHk_Success() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"entity_id": "223412345678"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxHk poster = createWatchboxHk(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        poster.SetPost();
        Test.stopTest();

        // Assert
        System.assertEquals('223412345678', poster.ChannelPost.External_Id__c);
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":2'));
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"isActive":false'));
    }


    static testMethod void SetVisible_Success() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"entity_id": "223412345678"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        poster.SetVisible();
        Test.stopTest();

        // Assert
        System.assertEquals('223412345678', poster.ChannelPost.External_Id__c);
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"isActive":true'));
    }

    static testMethod void RemoveVisible_Success() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"entity_id": "223412345678"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        poster.RemoveVisible();
        Test.stopTest();

        // Assert
        System.assertEquals('223412345678', poster.ChannelPost.External_Id__c);
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"isActive":false'));
    }

    static testMethod void DoUpdate_Success() {
        // Arrange.
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/rest/V1/integration/admin/token' => new List<Object> {
                Blob.valueOf('"atoken"')
            },
            '/rest/V1/salesforce/product/sync' => new List<Object> {
                Blob.valueOf('[{"entity_id": "223412345678"}]')
            }
        };

        MockEndpoint testEndpoint = new MockEndpoint(responses);
        sgPostingIntegrationWatchboxGlobal poster = createWatchboxGlobal(testEndpoint, responses, true, true, Date.today(), null, null);

        // Act.
        Test.startTest();
        poster.DoUpdate();
        Test.stopTest();

        // Assert
        System.assertEquals('223412345678', poster.ChannelPost.External_Id__c);
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"websiteId":1'));
        System.assertEquals(true, testEndpoint.getLastPayload().contains('"isActive":false'));
    }


    public class MockEndpoint implements System.StubProvider {
        Map<String,List<Object>> responses;
        Map<String,Integer> responseCallCount = new Map<String,Integer>();
        Blob lastPayload;

        public MockEndpoint(Map<String,List<Object>> resp) {
            responses = resp;
        }

        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (stubbedMethodName != 'Authenticate' && stubbedMethodName != 'UpdateContext' && responses != null) {
                String path = (String)listOfArgs[0];

                lastPayload = (Blob)listOfArgs[2];

                List<Object> responseList = responses.get(path);
                if (responseList != null) {
                    Integer callCount = getCallCount(path);
                    Object response = responseList[callCount - 1];
                    return response;
                }
            }
            return null;
        }

        Integer getCallCount(String path) {
            Integer callCount = 0;
            if (responseCallCount.containsKey(path)) {
                callCount = responseCallCount.get(path);
            }
            callCount++;
            responseCallCount.put(path, callCount);
            return callCount;
        }

        public String getLastPayload() {
            return lastPayload.toString();
        }
    }

    static sgPostingIntegrationWatchboxGlobal createWatchboxGlobal(MockEndpoint testEndpoint, Map<String,List<Object>> responses, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate) {
        String name = 'Watchbox International';
        sgPostingIntegrationWatchboxGlobal poster = new sgPostingIntegrationWatchboxGlobal();
        poster.Name = name;
        poster.Endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, testEndpoint);
        poster.ChannelPost = createChannelPost(name, post, visible, postDate, postedDate, visibleDate);
        Photograph__c photo = createPhotograph(poster.ChannelPost.Product__c, poster.ChannelPost.Organization__c);

        return poster;
    }

    static sgPostingIntegrationWatchboxHk createWatchboxHk(MockEndpoint testEndpoint, Map<String,List<Object>> responses, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate) {
        String name = 'Watchbox Hong Kong';
        sgPostingIntegrationWatchboxHk poster = new sgPostingIntegrationWatchboxHk();
        poster.Name = name;
        poster.Endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, testEndpoint);
        poster.ChannelPost = createChannelPost(name, post, visible, postDate, postedDate, visibleDate);
        Photograph__c photo = createPhotograph(poster.ChannelPost.Product__c, poster.ChannelPost.Organization__c);

        return poster;
    }

    static ChannelPost__c createChannelPost(String name, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate) {
        ChannelPost__c cp = new ChannelPost__c(
            Name = name,
            Channel__c = name,
            Organization__c = createOrganization().Id,
            Product__c = createProduct().Id,
            Post__c = post,
            Post_Date__c = postDate,
            Posted_Date__c = postedDate,
            Visible__c = visible,
            Visible_Date__c = visibleDate,
            Is_Processing__c = true
        );
        insert cp;
        return cp;
    }

    static Product2 createProduct() {
        Brand_object__c brand = new Brand_object__c(
            Name = 'Test Brand'
        );

        insert brand;

        Series__c series = new Series__c(
            Name = 'Test Series',
            Watch_Brand__c = brand.Id
        );

        insert series;

        Model__c model = new Model__c(
            Name = 'Test Model',
            model_series__c = series.Id
        );

        insert model;

        Product2 product = new Product2(
            Name = 'Test',
            Model__c = model.Id
        );
        insert product;
        return product;
    }

    static Photograph__c createPhotograph(String productId, String organizationId) {
        Photograph__c photo = new Photograph__c(
            Organization__c = organizationId,
            Url__c = '/url',
            Related_Product__c = productId
        );
        insert photo;
        return photo;
    }

    static Organization__c createOrganization() {
        Organization__c org = new Organization__c(
            Name = 'Watchbox',
            Organization_Code__c = 'Watchbox'
        );
        insert org;
        return org;
    }
}