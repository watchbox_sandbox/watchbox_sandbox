@isTest
public class sgTestLog {
    static testMethod void test() {
        Product2 p = sgDataSeeding.ProductsWithBrands(1).get(0);
        p.ASK__c = 0;
        
        insert p;
        
        p.ASK__c = 20;
        update p;
        system.debug([select Id from Log__c]);
        system.assert([select Id from Log__c].size() > 0);
    }
}