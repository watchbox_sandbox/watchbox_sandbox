@isTest
public class sgTestLookupController {
    static testMethod void get1() {
        RestRequest req = new RestRequest();
        
        req.addParameter('objectName', 'Role__c');
		req.addParameter('key', 'name');
		req.addParameter('value', 'id');
        
        RestContext.request = req;
        
 		List<Role__c> dbRoles = sgDataSeeding.Roles();
        insert dbRoles;
        
        Map<string, string> dbRoleMap = new Map<string, string>();
        
        for (Role__c r : dbRoles) {
            dbRoleMap.put(
            	r.Name,
                r.Id
            );
        }
        
        Map<string, string> roleMap = sgLookupController.get();
        
        
        for (string key : roleMap.keySet()) {
            system.assert(dbRoleMap.containsKey(key));
            system.assertEquals(dbRoleMap.get(key), roleMap.get(key));
        }
    }
    
    static testMethod void get2() {
        Map<string, string> req = new Map<string, string>();

        //req.put('objectName', 'Role__c');
		req.put('key', 'name');
		req.put('value', 'id');
        
        RestContext.request = new RestRequest();
        
 		List<Role__c> dbRoles = sgDataSeeding.Roles();
        insert dbRoles;
        
        Map<string, string> dbRoleMap = new Map<string, string>();
        
        for (Role__c r : dbRoles) {
            dbRoleMap.put(
            	r.Name,
                r.Id
            );
        }
        
        Map<string, string> roleMap = sgLookupController.get('Role__c', req);
        
        
        for (string key : roleMap.keySet()) {
            system.assert(dbRoleMap.containsKey(key));
            system.assertEquals(dbRoleMap.get(key), roleMap.get(key));
        }
    }
    
    static testMethod void get3() {
        Map<string, string> req = new Map<string, string>();

        //req.put('objectName', 'Role__c');
		req.put('key', 'name');
		req.put('value', 'id');
        
        RestContext.request = new RestRequest();
        
 		List<Role__c> dbRoles = sgDataSeeding.Roles();
        insert dbRoles;
        
        Map<string, string> dbRoleMap = new Map<string, string>();
        
        for (Role__c r : dbRoles) {
            dbRoleMap.put(
            	r.Name,
                r.Id
            );
        }
        
        Map<string, string> roleMap = sgLookupController.get('Role__c');
        
        
        for (string key : roleMap.keySet()) {
            system.assert(dbRoleMap.containsKey(key));
            system.assertEquals(dbRoleMap.get(key), roleMap.get(key));
        }
    }
    
    static testMethod void get4() {
        Map<string, string> req = new Map<string, string>();

        //req.put('objectName', 'Role__c');
		req.put('key', 'name');
		req.put('value', 'id');
        
        //RestContext.request = new RestRequest();
        
 		List<Role__c> dbRoles = sgDataSeeding.Roles();
        insert dbRoles;
        
        Map<string, string> dbRoleMap = new Map<string, string>();
        
        for (Role__c r : dbRoles) {
            dbRoleMap.put(
            	r.Name,
                r.Id
            );
        }
        
        Map<string, string> roleMap = sgLookupController.get('Role__c', 'name');
        
        
        for (string key : roleMap.keySet()) {
            system.assert(dbRoleMap.containsKey(key));
            system.assertEquals(dbRoleMap.get(key), roleMap.get(key));
        }
    }
}