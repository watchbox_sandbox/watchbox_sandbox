public class LACGov_WBSyncService {

    public static void updateOpportunity(List<OpportunityWrapperr> oppWrapperList,UpdateWatchBoxLoanLead.responseWrapper responseJSON){
       
        Set<Id> oppIdSet = new Set<Id>();
        System.debug('oppWrapperList'+oppWrapperList);
        
        for(OpportunityWrapperr oppWrapper:oppWrapperList){
            responseJSON.oppName = oppWrapper.FullName;
            //responseJSON.oppId = oppWrapper.opportunityid;
            oppIdSet.add(oppWrapper.opportunityid);
        }
        System.debug('oppIdSet'+oppIdSet);
        System.debug('responseJSON'+responseJSON);
        if(oppIdSet != null && oppIdSet.size() > 0 ){
            List<Opportunity> oppList = [Select Id,Name,Originating_Lead__c,Originating_LeadGenLog__c,CloseDate from Opportunity where Originating_LeadGenLog__c IN:oppIdSet];  
            List<Opportunity> oppListToUpdate = new List<Opportunity>();
            System.debug('oppList'+oppList);
                if(oppList != null && oppList.size() >0){
                    for(opportunity oppObj:oppList){
                        for(OpportunityWrapperr oppWrapperObj:oppWrapperList){
                            /*if(oppWrapperObj.FullName != null && oppWrapperObj.FullName != ''){
                                oppObj.Name = oppWrapperObj.FullName;
                            }
                            if(oppWrapperObj.CloseDate != null && oppWrapperObj.CloseDate != ''){
                                oppObj.CloseDate = Date.Valueof(oppWrapperObj.CloseDate);
                            }
                            if(oppWrapperObj.StageName != null && oppWrapperObj.StageName != ''){
                            oppObj.StageName = oppWrapperObj.StageName;
                            }*/
                            if(oppWrapperObj.StageName != null && oppWrapperObj.StageName != ''){
                                if(oppWrapperObj.StageName == 'Application Complete'){
                                    oppObj.Deal_Status__c = 'New';	
                                }else if(oppWrapperObj.StageName == 'Asset not shipped'){
                                    oppObj.Type_of_Transaction_Initial__c = 'Loan';
                                    oppObj.Deal_Status__c = 'Deal Lost';
                                    oppObj.Loss_Reason__c = 'Undesirable Watch';
                                    oppObj.Initial_Deal_Details__c = 'LAC: Asset not shipped';
                                }else if(oppWrapperObj.StageName == 'Assets Received'){
                                    oppObj.Type_of_Transaction_Initial__c = 'Loan';
                                    oppObj.Deal_Status__c = 'DIP';
                                    oppObj.DIP_Reason__c = 'Negotiating';
                                    oppObj.Initial_Deal_Details__c = 'LAC: Assets Received';
                                }else if(oppWrapperObj.StageName == 'Asset Shipped Back'){
                                    oppObj.Type_of_Transaction_Initial__c = 'Loan';
                                    oppObj.Deal_Status__c = 'Deal Lost';
                                    oppObj.Loss_Reason__c = 'Undesirable Watch';
                                    oppObj.Initial_Deal_Details__c = 'LAC: Asset Shipped Back';
                                }else if(oppWrapperObj.StageName == 'Final Offer Sent'){
                                    oppObj.Type_of_Transaction_Initial__c = 'Loan';
                                    oppObj.Deal_Status__c = 'DIP';
                                    oppObj.DIP_Reason__c = 'Negotiating';
                                    oppObj.Initial_Deal_Details__c = 'LAC: Final Offer Sent';
                                    oppObj.Final_Deal_Details__c = 'LAC: Final Offer: '+'$'+oppWrapperObj.amount;
                                }else if(oppWrapperObj.StageName == 'Transaction Complete'){
                                    oppObj.Type_of_Transaction_Initial__c = 'Loan';
                                    oppObj.Deal_Status__c = 'Deal Won';
                                    oppObj.Initial_Deal_Details__c = '"LAC: Transaction Complete"';
                                    oppObj.Final_Deal_Details__c = 'LAC: Final Offer: '+'$'+oppWrapperObj.amount;
                                }
                            }
                        }
                        oppListToUpdate.add(oppObj);
                    }  
                }
            System.debug('oppListToUpdate'+oppListToUpdate);
            update oppListToUpdate;
        } 
        
    }
}