public class UserTriggerToSendEmailHandler extends TriggerHandler{
    public override void afterUpdate() {
        EmailTemplate templateId = [Select id,body from EmailTemplate where DeveloperName = 'Opt_out_email'];
        string userName;
        string body='';
        for (User user : (List<User>)Trigger.new) {
            
            User oldUser=(User)Trigger.oldMap.get(user.Id);
            if(user.Opt_out__c!=oldUser.Opt_out__c){
                if(userName==Null){
                    userName='';
                }
                system.debug(user.Name);
                string name='';
                name+=user.FirstName+' '+user.LastName;
                userName+=name+' - '+user.Opt_out__c+'\n';
            }
        }   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string str = System.Label.Email_Id;
        List<String> lstemals = new List<String>();
        lstemals = str.split(',');
        system.debug(lstemals );
        // Strings to hold the email addresses to which you are sending the email.
        if(  username !=null){
            system.debug('userName'+userName);
            body=templateId.body;
            body= body.replace('listofusername', userName );
            mail.setToAddresses(lstemals);
            mail.setSubject('Opt Out of User has changed ');
            mail.setSaveAsActivity(false);
            mail.setTemplateId(templateId.id);
            mail.setPlainTextBody(body);
            system.debug(':::'+mail.getPlainTextBody());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            
            
            
        }  
        
        
    }
}