@isTest
public class sgTestProductDetailController {
    static testMethod void get() {
        Product2 p = new Product2(
        	Name = 'Test Product'
        );
        
        insert p;
        
        RestRequest r = new RestRequest();
        r.params.put('id', p.Id);
        RestContext.request = r;
        
        sgProductDetail product = sgProductDetailController.get();
        
        system.assertEquals(p.Id, product.Fields.get('Id'));
    }
}