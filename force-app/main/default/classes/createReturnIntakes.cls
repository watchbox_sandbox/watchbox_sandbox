public class createReturnIntakes {
	public ApexPages.StandardController stdController;
    public id returnId;
    public createReturnIntakes(ApexPages.StandardController controller){
        //constructor - get opportunity incase createIntakeRecords() fails
        stdController = controller;
        //get opportunityID
        returnId = stdController.getId();
        
        //system.debug(oppId);
    }
    
    public PageReference createIntakesForReturns(){
        //get correct record type
        String recordType = 'Intake Repairs & Returns';
        
        RecordType returnIntake = [SELECT id, name FROM RecordType WHERE name = :recordType];
        system.debug('Record type selected: '+returnIntake.Name);
        system.debug('entered createIntakeRecords method');
        //declare and initialize variables
        Integer recordsToCreate=0;
        List<Intake__c> newIntakes = new List<Intake__c>();
        Set<Id> preexistingReturnIds = new Set<Id>();
        Return__c getReturn = [SELECT id, deal__c FROM Return__c WHERE id = :returnId];
        Integer existingIntakes = [SELECT count() FROM intake__c WHERE return__c = :returnId ];
        Opportunity opp = [SELECT id FROM Opportunity WHERE id = :getReturn.deal__c];
        /*for(intake__c intk : existingIntakes){
            preexistingReturnIds.add(intk.return__c);
            system.debug('Record: '+ intk);
            
        }*/
        //show visual force page in the event no ID is present
        if(returnId == null){
            system.debug('oppId is null :(');
            return null;
        }
        if(existingIntakes < 1){
        	Intake__c newIntake = new Intake__c(return__c = returnid, deal__c = opp.id, RecordType = returnIntake);
        	newIntakes.add(newIntake);
            //insert all new intakes in one push
            insert newIntakes;
        }
        //and now we return to the deal record, making the whole transaction look like a page refresh
        
        PageReference refresh = new PageReference('/'+returnId);
        refresh.setRedirect(true);
        return refresh;
        }
              
        
        
    }