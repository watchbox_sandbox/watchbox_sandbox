//@RestResource(urlMapping='/generateLabel/*')
public class sgGenerateLabelController {
    public sgGenerateLabelController(){
    
    }
/*
    global class sgGenerateLabelDto{
        global Boolean inProgress;
        global String labelUrl;
        global string message;
    }
    @HttpGet
    global static sgGenerateLabelDto get() {
        string batchId = RestContext.request.params.get('batchId');
        string opportunityId = RestContext.request.params.get('opportunityId');
        List<string> picklistDetailIds = RestContext.request.params.get('picklistDetailIds').split(',');
        string ErrorPickListText = 'ERROR';
        string CompletePickListText = 'Completed';
        sgGenerateLabelDto generateLabelDto = new sgGenerateLabelDto();

        //waiting for zenkraft to respond whether this is needed or not
        // AsyncApexJob job = [SELECT id, CreatedDate, Status FROM AsyncApexJob ORDER BY CreatedDate DESC LIMIT 1];
        // if(job.status != CompletePickListText )
        // {
        //  generateLabelDto.inProgress = true;
        //  return generateLabelDto;    
        // }

        zkfedex__BulkShipmentStatus__c[] bulkShipment = [SELECT zkfedex__Status__c, 
                                                                zkfedex__StatusMessage__c 
                                                            FROM 
                                                                zkfedex__BulkShipmentStatus__c 
                                                            WHERE zkfedex__BatchId__c = :batchId];
        if (bulkShipment.size() == 0)
        {
            generateLabelDto.inProgress = true;
            return generateLabelDto;            
        }
        
        if(bulkShipment[0].zkfedex__Status__c == ErrorPickListText)
        {
            generateLabelDto.inProgress = false;
            generateLabelDto.message = bulkShipment[0].zkfedex__StatusMessage__c;
            return generateLabelDto;
        }

        zkfedex__QueuedShipmentStatus__c queuedShipmentStatus = [SELECT zkfedex__Status__c,
                                                                    zkfedex__Shipment__r.id,
                                                                    zkfedex__StatusMessage__c
                                                                FROM 
                                                                    zkfedex__QueuedShipmentStatus__c 
                                                                WHERE zkfedex__BatchId__c = :batchId];

        if(queuedShipmentStatus.zkfedex__Status__c == ErrorPickListText)
        {
            generateLabelDto.inProgress = false;
            generateLabelDto.message = queuedShipmentStatus.zkfedex__StatusMessage__c;
            return generateLabelDto;
        }

        List<Attachment> attachments = [SELECT id, body, name, contentType FROM Attachment Where parentid = :queuedShipmentStatus.zkfedex__Shipment__c]; 

        if(attachments.size() > 0)
        { 
            sgs3 s3 = new sgs3();

            for(Attachment attachment : attachments)
            {
                s3.upload(attachment, attachment.name, attachment.body, attachment.contentType);
                string s3Url = sgs3.s3Url(attachment, attachment.name);
                attachment.body = null;
                attachment.description = s3Url;

                if (attachment.name.contains('ShippingLabel'))
                {
                    generateLabelDto.labelUrl = s3Url;
                    UpdatePicklist(picklistDetailIds, generateLabelDto.labelUrl, queuedShipmentStatus.zkfedex__Shipment__c);
                }
            }
            UPDATE attachments;
        }
         else 
        {
            generateLabelDto.inProgress = true;
        }
        return generateLabelDto;
    }

    global class sgGenerateLabelPostResponseDto{
        string batchId;
    }
    @HttpPost
    global static sgGenerateLabelPostResponseDto post(string picklistId) {
        //refer to https://docs.zenkraft.com/fedex/apexcode
        
        zkfedex__ShipmatePreference__c shipmatePref = [SELECT zkfedex__CompanyName__c, zkfedex__ShippingIsResidential__c, zkfedex__ShippingCity__c,
                zkfedex__ShippingCountry__c, zkfedex__SenderEMailDefault__c, zkfedex__SenderPhoneDefault__c,
                zkfedex__ShippingState__c, zkfedex__ShippingStreet__c, zkfedex__ShippingPostalCode__c, 
                zkfedex__FedExAccountNumber__c, zkfedex__BillingCountry__c, 
                zkfedex__DropoffTypeDefault__c, zkfedex__LabelImageTypeDefault__c
        FROM zkfedex__ShipmatePreference__c
        Limit 1];

        zkfedex__BulkShipment__c bulkShipment = new zkfedex__BulkShipment__c (
        zkfedex__ShipmatePreference__c = shipmatePref.Id
        );
        insert bulkShipment;

        Picklist__c picklist = [select 
                            Account__r.Id,
                            Account__r.ShippingCity, 
                            Account__r.Name, 
                            Account__r.ShippingCountryCode,
                            Account__r.Email__c, 
                            Account__r.Phone, 
                            Account__r.ShippingStateCode, 
                            Account__r.ShippingStreet,
                            Account__r.ShippingPostalCode,
                            Account__r.Company_Name__c,
                            Deal__c,
                            Shipping_Speed2__c
                        from 
                            Picklist__c 
                        where 
                            id = :picklistId 
                        limit 1];

        List<zkfedex__QueuedShipment__c> queuedShipmentsToInsert = new List<zkfedex__QueuedShipment__c>();
        zkfedex__QueuedShipment__c queuedShipment = new zkfedex__QueuedShipment__c (
            zkfedex__BulkShipment__c = bulkShipment.Id,
            zkfedex__ShipDate__c = Date.today(),
          //  zkfedex__DropoffType__c = 'Regular Pickup',
            zkfedex__LabelImageType__c = shipmatePref.zkfedex__LabelImageTypeDefault__c,
            zkfedex__ServiceType__c = picklist.Shipping_Speed2__c,
            zkfedex__PackagingType__c = 'Your Packaging',
            zkfedex__WeightDimensionUnits__c = 'LB / IN',           
            zkfedex__SenderName__c = shipmatePref.zkfedex__CompanyName__c,
            zkfedex__SenderIsResidential__c = shipmatePref.zkfedex__ShippingIsResidential__c,
            zkfedex__SenderCity__c = shipmatePref.zkfedex__ShippingCity__c,
            zkfedex__SenderCompany__c = shipmatePref.zkfedex__CompanyName__c,
            zkfedex__SenderCountry__c = shipmatePref.zkfedex__ShippingCountry__c,
            zkfedex__SenderEmail__c = shipmatePref.zkfedex__SenderEMailDefault__c,
            zkfedex__SenderPhone__c = shipmatePref.zkfedex__SenderPhoneDefault__c,
            zkfedex__SenderState__c = shipmatePref.zkfedex__ShippingState__c,
            zkfedex__SenderStreet__c = shipmatePref.zkfedex__ShippingStreet__c,
            zkfedex__SenderPostalCode__c = shipmatePref.zkfedex__ShippingPostalCode__c,
            

            // recipient info
            zkfedex__RecipientIsResidential__c = false,
            zkfedex__RecipientCity__c =  picklist.Account__r.ShippingCity,
            zkfedex__RecipientCompany__c = picklist.Account__r.Company_Name__c,
            zkfedex__RecipientCountry__c = picklist.Account__r.ShippingCountryCode ,
            zkfedex__RecipientEmail__c = picklist.Account__r.Email__c,
            zkfedex__RecipientName__c = picklist.Account__r.Name,
            zkfedex__RecipientPhone__c = picklist.Account__r.Phone,
            zkfedex__RecipientState__c = picklist.Account__r.ShippingStateCode,
            zkfedex__RecipientStreet__c = picklist.Account__r.ShippingStreet,
            zkfedex__RecipientPostalCode__c = picklist.Account__r.ShippingPostalCode,
            zkfedex__PaymentType__c = 'SENDER',
            zkfedex__Account__c = picklist.Account__r.Id,
            zkfedex__Opportunity__c  = picklist.Deal__c
        );

        insert queuedShipment;

        queuedShipmentsToInsert.add(queuedShipment);

        List<zkfedex__QueuedPackage__c> queuedPackagesToInsert = new List<zkfedex__QueuedPackage__c>();

        for (zkfedex__QueuedShipment__c qs : queuedShipmentsToInsert) {
            zkfedex__QueuedPackage__c queuedPackage = new zkfedex__QueuedPackage__c (
                zkfedex__DeclaredValue__c = 20,
             //   zkfedex__Weight__c = 4,
                zkfedex__QueuedShipment__c = qs.Id          
            );
            queuedPackagesToInsert.add(queuedPackage);
        }

        insert queuedPackagesToInsert;
        sgGenerateLabelPostResponseDto dto = new sgGenerateLabelPostResponseDto();
        dto.batchId = zkfedex.BulkShipmentInterface.processBulkShipment(bulkShipment.Id);
        return dto;
    }

    private static void UpdatePicklist(List<string> picklistDetailIds, string url, string shippingId)
    {
        List<Picklist_Detail__c> picklistDetails = [SELECT 
                                                        Shipped__c,
                                                        PDF_URL__c,
                                                        Tracking_Number__c
                                                    FROM 
                                                        Picklist_Detail__c
                                                    WHERE
                                                        id in :picklistDetailIds];

        for(Picklist_Detail__c picklistDetail : picklistDetails)
        {
            picklistDetail.PDF_URL__c = url;
            picklistDetail.Shipped__c = True;
            picklistDetail.Tracking_Number__c = [SELECT zkfedex__TrackingId__c 
                                                    FROM zkfedex__ShipmentPackage__c 
                                                    WHERE zkfedex__Shipment__c = :shippingId LIMIT 1].zkfedex__TrackingId__c;
        }
        UPDATE picklistDetails;
    }*/
}