@isTest
public class sgTestProductLocationController {

    static testMethod void getProductLocations() {
        //arrange
        //
        Integer locCount = 5;
        createLocations(locCount);
        
        //act
        //
        List<sgProductLocationDTO> results = sgProductLocationController.get();
        
        //assert
        //
        System.assertEquals(locCount, results.size());
    }
    
    static List<Location__c> createLocations(Integer count) {
        List<Location__c> locs = new List<Location__c>();
        
        for (Integer i = 0; i < count; i++) {
            locs.add(createLocation(i));
        }
        System.debug(locs);
        return locs;
    }
    
    static Location__c createLocation(Integer indx) {
        Location__c loc = new Location__c(
        	Name = 'Govberg-' + indx
        );
        insert loc;
        return loc;
    }
    
}