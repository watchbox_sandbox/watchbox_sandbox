@isTest
public class sgTestPrintingIntakeController {

    static testMethod void construct() {
        //arrange
        //
        createOrg('Govberg');
        Intake__c intake = createIntake();
        Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('id', intake.Id);
        parameters.put('organization', 'Govberg');
        
        //act
        //
        sgPrintingIntakeController ctl = new sgPrintingIntakeController();
        
        //assert
        //
        System.assertEquals(intake.Id, ctl.Intake.Id);
    }
        
    //**********************************************************
    // helper methods
    
    static Intake__c createIntake() {
        Intake__c intake = new Intake__c (
        	
        );
        insert intake;
        return intake;
    }
    
    static void createOrg(String name) {
        Organization__c org = new Organization__c(
        	Name = name
        );
        insert org;
    }
    
}