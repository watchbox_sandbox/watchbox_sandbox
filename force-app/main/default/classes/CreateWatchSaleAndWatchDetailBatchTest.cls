@isTest
public class CreateWatchSaleAndWatchDetailBatchTest {
    
    static testmethod void afterinsert_GovbergeBay() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Brand__c='Rolex';
            opp.Sell_Brand__c='Apple';
            opp.Lead_Source__c='Govberg eBay';
            opp.Sale_Price_Discussed__c=9895656;
            opp.Inventory_ID_Scratchpad__c='101';
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_ThewatchboxMakeAnOffer() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Brand__c='Rolex';
            opp.Model__c='rolex 1.0';
            opp.Initial_Deal_Details__c='Shipping to : london'+
                'Payment Method: Paypal'+
                'Offering: $3,000.00';
            opp.Lead_Source__c='Thewatchbox.com - Trade Your Watch';
            opp.Sell_Brand__c='Apple';
            opp.Lead_Source__c='Thewatchbox.com - Make An Offer';
            opp.Sale_Price_Discussed__c=9895656;
            opp.Inventory_ID_Scratchpad__c='101';
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_ThewatchboxSellYourWatch() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Sell_Model__c='Appe 1.2';
            opp.Sell_Brand__c='Apple';
            opp.Lead_Source__c='Thewatchbox.com - Sell Your Watch';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            opp.Inventory_ID_Scratchpad__c='101';
            opp.Name_Your_Price_ASK__c=123.06;
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_GovbergwatchescomTradeForm() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Sell_Model__c='Appe 1.2';
            opp.Sell_Brand__c='Apple';
            opp.Brand__c='Rolex';
            opp.Name_Your_Price_ASK__c=253.55;
            opp.Model__c='3.2';
            opp.Lead_Source__c='Govbergwatches.com - TradeForm';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            opp.Inventory_ID_Scratchpad__c='101';
            opp.Name_Your_Price_ASK__c=123.06;
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_GovbergwatchescomSellyourwatch() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1);
            opp.Sell_Brand__c='apple';
            opp.Sell_Model__c='rolex';
            opp.Initial_Deal_Details__c='tests';
            opp.Lead_Source__c='Govbergwatches.com - Sell your watch';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            opp.Name_Your_Price_ASK__c=123.06;
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_eBayluxwatchbuyercom() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.Sell_Brand__c='apple';
            opp.Sell_Model__c='rolex';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            
            opp.Initial_Deal_Details__c='tests';
            opp.Lead_Source__c='eBay - luxwatchbuyer.com';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_TheWatchBoxcomClickBuy() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Brand__c='Rolex';
            opp.Model__c='1.2';
            opp.Sale_Price_Discussed__c=546946;
            opp.SKU__c='fg';
            opp.Initial_Deal_Details__c='tests';
            opp.Lead_Source__c='TheWatchBox.com - Click&Buy';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            opp.Inventory_ID_Scratchpad__c='101';
            opp.Name_Your_Price_ASK__c=123.06;
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_Thewatchboxcom() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.SKU__c='fg';
            
            opp.Lead_Source__c='Thewatchbox.com';
            
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_ThewatchboxcomTradeYourWatch() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Brand__c='Rolex';
            opp.Model__c='1.2';
            opp.Sell_Brand__c='Apple';
            opp.Sell_Model__c='4654.3';
            
            
           opp.Initial_Deal_Details__c='Shipping to : london'+
                'Payment Method: Paypal'+
                'Offering: $3,000.00';
            opp.Lead_Source__c='Thewatchbox.com - Trade Your Watch';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            
            
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
    static testmethod void afterinsert_WatchboxAppSellYourWatch() {
        try{
            Account account = TestFactory.createAccount();
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Account-Virtual';
            opp.StageName = 'Test';
            opp.AccountId = account.Id;     
            opp.CloseDate=System.today().addMonths(1); 
            opp.Sell_Brand__c='Rolex';
            opp.Sell_Model__c='Apple';
            opp.SKU__c='fg';
            opp.Initial_Deal_Details__c='tests';
            opp.Lead_Source__c='Watchbox App - Sell Your Watch';
            opp.Boxes_and_Papers__c='Box only';
            opp.Initial_Deal_Details__c='test';
            opp.Name_Your_Price_ASK__c=123.06;
            insert opp;
            Test.startTest();
            CreateWatchSaleAndWatchDetailBatch batch = new CreateWatchSaleAndWatchDetailBatch();
            database.executeBatch(batch);
            Test.stopTest();
            
            
        }
        catch (Exception e) {
            System.debug('Error during OpportunityTrigger -  ' + e.getMessage());
        }
    }
}