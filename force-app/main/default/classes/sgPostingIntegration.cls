///
// This abstract class provides a mechanism to post and unpost 
// inventory to various 3rd party sites
// 
public abstract class sgPostingIntegration {
    public class NotImplementedException extends Exception {}
    public class ServiceRequestException extends Exception {}
    
    static String DEFAULT_ORGANIZATION = 'Watchbox';
    
    public String Name {get;set;}
    public String Organization {get;set;}
    public sgEndpoint Endpoint {get;set;}
    public ChannelPost__c ChannelPost {get;set;}
    public List<Task> ActivityLog {get;set;}
    
    public Map<String, Surge_Integration_Settings__c> Services {get; set;}
    
    public sgPostingIntegration() {
        ActivityLog = new List<Task>();
    }
    
    public void Initialize() {
        if (Organization == null) {
            Organization = DEFAULT_ORGANIZATION;
        }
        //get the services
        List<Surge_Integration_Settings__c> serviceRecords = 
        [
            SELECT Service__c,
            Method__c, 
            Path__c, 
            Parameters__c,
            Headers__c
            FROM Surge_Integration_Settings__c
            WHERE Integration__c = :Name
              AND Organization__c = :Organization
        ];
        
        //transform the service records into a map to make it easier to consume
        this.Services = new Map<String, Surge_Integration_Settings__c>();
        for(Surge_Integration_Settings__c record : serviceRecords) {
            this.Services.put(record.Service__c, record);
        }
        
        //try to authenticate the endpoint
        this.Endpoint.Context = ChannelPost;
        this.Endpoint.Authenticate();
    }
    
    public void AddActivity(String subject, String description, String status) {
        ActivityLog.add(new Task (
        	ActivityDate = Date.today(),
            Subject = subject,
            Status = status,
            Description = description,
            WhatId = ChannelPost.Id
        ));
    }
    
    public void SaveActivityLog() {
        if (ActivityLog != null) {
        	insert ActivityLog;
        }
    }
    
    public Blob ExecuteRequest(String serviceName, Blob payload) {
        //get the get product service
        Surge_Integration_Settings__c service = this.Services.get(serviceName);
        Blob result = null;
        try {
        	result = this.Endpoint.Send(service.Path__c, service.Method__c, payload, service.Parameters__c, service.Headers__c);
        }
        catch(Exception ex) {
            throw new ServiceRequestException('Service Call Exception for ' + serviceName + '\n\nException Type: ' + ex.getTypeName() + '\n\n' + ex.getMessage(), ex);
        }
        return result;
    }
    
    public virtual void SetVisible() {
        throw new NotImplementedException('A concrete implementation is required');
    }
    
    public virtual void SetPost() {
        throw new NotImplementedException('A concrete implementation is required');
    }
    
    public virtual void DoUpdate() {
        throw new NotImplementedException('A concrete implementation is required');
    }
    
    public virtual void RemoveVisible() {
        throw new NotImplementedException('A concrete implementation is required');
    }
    
    public virtual void RemovePost() {
        throw new NotImplementedException('A concrete implementation is required');
    }
    
}