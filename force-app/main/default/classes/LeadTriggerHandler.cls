public class LeadTriggerHandler extends TriggerHandlerLead {
    private LeadGenLog_Settings__c Settings {get;set;}
    
    public override void beforeInsert() {
       /*Following code is commented as per the requirement by Demandware on 11/10/2019
         for (Lead lead : (List<Lead>)Trigger.new) {
            if (!string.isBlank(lead.Phone)) {
                lead.Phone = PhoneNumberFormatter.stripCharacters(lead.Phone);
            }                        
        }*/
    }
    
    public override void afterInsert() {
        processIncomingLeads();          
    }
    
    private void processIncomingLeads() {
        System.debug('Entering LeadGenLogTriggerHandler.processIncomingLeads');
        
        //Sets for SOQL query
        Set<string> emailSet = new Set<string>();
        Set<string> phoneSet = new Set<string>();
        
        //Maps for lookups
        Map<string, Account> emailAccountMap = new Map<string, Account>();
        Map<string, Account> phoneAccountMap = new Map<string, Account>();
        
        //Decorator for main operations
        List<LeadGenLogDecorator> decoratedLeads = new List<LeadGenLogDecorator>();
        List<SObject> objectsToInsert = new List<SObject>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        List<Lead> leadsToUpdate = new List<Lead>();
        
        List<Lead> leads = [SELECT Id, Email, Phone, Matched_Account__c, LastName, FirstName, Country__c, Existing_Lead_Client_Owner__c, 
                            Full_Name__c, GA_Campaign__c, GA_Content__c, GA_Medium__c, GA_Segment__c, Ga_Source__c, GA_Term__c, GA_Visits__c, Transaction_Type__c, GA_gclid__c,
                            IP_Address__c, Lead_Source__c, LeadSource,Sell_Brand__c, Sell_Model__c, Page_URL__c, Receive_Updates__c, Submission_Date__c, Additional_Info__c, 
                            Comments__c, Page_ID__c,Images__c,Watch_Images__c,Watch_Model__c, Sell_Watch_Serial__c, Watch_Reference__c, Name_Your_Price_ASK__c, Boxes_and_Papers__c, SKU__c,
                            Last_Service_Date__c, Watch_Brand__c, Sale_Price__c, InventoryID__c, Type_of_Metal__c, Dial_Description__c, Initial_Deal_Details__c,
                            What_Brings_You_In_Today__c, interested_in_Brand__c, interested_in_Model__c, Interested_in_Product__c, Interested_in_Product_List_price__c
                            , interested_in_Reference_Number__c , Boxes__c, Papers__c,Interested_in_Product_Url__c,Referring_URL__c,Source__c,ircid__c,irtrackid__c,irpid__c,irclickid__c,Landing_page_url__c 
                            FROM Lead
                            WHERE Id IN :Trigger.newMap.keySet()]; 
        
        Settings = getLeadSettings();
        
        for (Lead lead : leads) {
            //Build decorator to iterate later
            LeadGenLogDecorator decorator = new LeadGenLogDecorator();
            decorator.LeadRecord = lead; 
            decoratedLeads.add(decorator);
            
            //Build sets for SOQL query
            if (!string.isBlank(decorator.LeadRecord.Email))
                emailSet.add(decorator.LeadRecord.Email);
            
            if (!string.isBlank(decorator.LeadRecord.Phone))
                phoneSet.add(decorator.LeadRecord.Phone);                        
        }        
        
        System.debug('Searching for accounts with email index of - ' + emailSet);
        System.debug('Searching for accounts with phone index of - ' + phoneSet);
        
        String soql = 'SELECT Id, Email__c, Secondary_Email__c, Phone, Mobile__c, OwnerId FROM Account WHERE ';
        String spacer = '';
        
        if (!emailSet.isEmpty()) {
            soql += 'Email__c IN :emailSet OR Secondary_Email__c IN :emailSet';
            spacer = ' OR ';
        }
        
        if (!phoneSet.isEmpty()) 
            soql += spacer + 'Phone IN :phoneSet OR Mobile__c IN :phoneSet';
        
        System.debug('Executing dynamic SOQL - ' + soql);
        
        List<Account> matchedAccounts = Database.query(soql);
        
        System.debug('Found the following matched accounts - ' + matchedAccounts);
        
        
        //Build lookups maps
        for (Account ma : matchedAccounts) {
            emailAccountMap.put(ma.Email__c, ma);
            phoneAccountMap.put(ma.Phone, ma);
            
            //Add secondary lookup
            if (!string.isBlank(ma.Secondary_Email__c)) {
                emailAccountMap.put(ma.Secondary_Email__c, ma);
            }
            
            //Add mobile lookup
            if (!string.isBlank(ma.Mobile__c)) {
                phoneAccountMap.put(ma.Mobile__c, ma);
            }
        }
        
        
        
        for (LeadGenLogDecorator lgld : decoratedLeads) {
            Account matchedAccount = null;
            string externalId = null;
            
            //If no match search by phone index
            if (matchedAccount == null && !string.isBlank(lgld.LeadRecord.Phone)) {
                matchedAccount = phoneAccountMap.get(lgld.LeadRecord.Phone);
                
                if (matchedAccount != null) 
                    System.debug('Found match via Phone Index - ' + matchedAccount.Id);                                                    	
            }            
            
            
            //Search by email index
            if (!string.isBlank(lgld.LeadRecord.Email)) {
                matchedAccount = emailAccountMap.get(lgld.LeadRecord.Email);
                
                if (matchedAccount != null)          
                    System.debug('Found match via Email Index - ' + matchedAccount.Id);                                                               	               
            }
            
            
            
            //If no match create Account
            if (matchedAccount == null) {
                externalId = string.valueOf(lgld.LeadRecord.Id);
                
                System.debug('Building new account with external id - ' + externalId);
                lgld.Account = buildAccount(lgld, externalId);                
                objectsToInsert.add(lgld.Account);                
            } else {
                System.debug('Marking lead with matched account');
                lgld.Account = matchedAccount;
                lgld.LeadRecord.Matched_Account__c = string.valueOf(matchedAccount.Id); 
                leadsToUpdate.add(lgld.LeadRecord);
            }
            
            //Create opportunity
            lgld.Opportunity = buildOpportunity(lgld, externalId);
            objectsToInsert.add(lgld.Opportunity);            
        }
        
        SavePoint sp = Database.setSavepoint();
        
        try {
            if (!objectsToInsert.isEmpty()) {
                insert objectsToInsert;
                update leadsToUpdate;
                sendEmailNotifications(decoratedLeads);            
            }        
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug('Error during LeadGenLogTriggerHandler.processIncomingLeads - ' + e.getMessage());
            throw new ApplicationException(e.getMessage());
        }        
    }
    
    private Account buildAccount(LeadGenLogDecorator lgld, string externalId) {
        Account account = new Account();
        account.BillingCountry = lgld.LeadRecord.Country__c;
        account.Email__c = lgld.LeadRecord.Email;
        
        account.OwnerId = Settings.Unassigned_User_Id__c; 
        account.First_Name__c = lgld.LeadRecord.FirstName;
        account.Name = lgld.LeadRecord.Full_Name__c;
        account.IP_Address__c = lgld.LeadRecord.IP_Address__c;
        account.Last_Name__c = (!string.isBlank(lgld.LeadRecord.LastName)) ? lgld.LeadRecord.LastName : lgld.LeadRecord.Email;
        if(lgld.LeadRecord.Lead_Source__c!=null && lgld.LeadRecord.Lead_Source__c!=''){
            account.Lead_Source__c = lgld.LeadRecord.Lead_Source__c;
        }else {
            account.Lead_Source__c = lgld.LeadRecord.LeadSource;
        }
        
        account.Page_URL__c = lgld.LeadRecord.Page_URL__c;
        account.Phone = lgld.LeadRecord.Phone;
        account.Receive_Updates__c = lgld.LeadRecord.Receive_Updates__c;
        account.Submission_Date__c = lgld.LeadRecord.Submission_Date__c;
        
        if (!string.isBlank(externalId))
            account.Lead_Gen_Log_External_Id__c = externalId;
        
        return account;        
    }
    
    private Opportunity buildOpportunity(LeadGenLogDecorator lgld, string externalId) {
        Opportunity opp = new Opportunity();
        
        if (string.isBlank(externalId)) {
            opp.AccountId = lgld.Account.Id;
            
            //If matched account check for placeholder account/owner assignment
            if (opp.AccountId != null) {
                Deal_Schedule__c ds = getDealScheduleOwnerId(opp.AccountId);
                
                if (ds != null) {
                    opp.OwnerId = ds.Related_Associate__c; 
                    opp.Deal_Schedule__c = ds.Id;
                }                                   
            }                        
        } else {
            Account accountReference = new Account(Lead_Gen_Log_External_Id__c=externalId);
            opp.Account = accountReference;                        
        }
        
        if (opp.OwnerId == null || (lgld.LeadRecord.Lead_Source__c == 'Call_Rail' ||lgld.LeadRecord.LeadSource == 'Call_Rail' ) )
            opp.OwnerId = lgld.Account.OwnerId;
        
        //Owner Lead Source override
        if (lgld.LeadRecord.Lead_Source__c == 'Ardmore Retail Checkin'|| lgld.LeadRecord.LeadSource == 'Ardmore Retail Checkin') {
            if (!string.isBlank(Settings.Lead_Source_Ardmore_User_Id__c)) {
                opp.OwnerId = Settings.Lead_Source_Ardmore_User_Id__c;
            }
        } else if (lgld.LeadRecord.Lead_Source__c == 'Walnut Retail Checkin'|| lgld.LeadRecord.LeadSource == 'Walnut Retail Checkin') {
            if (!string.isBlank(Settings.Lead_Source_Walnut_User_Id__c)) {
                opp.OwnerId = Settings.Lead_Source_Walnut_User_Id__c;
            }
        }
        else if ((lgld.LeadRecord.Lead_Source__c!=null && lgld.LeadRecord.Lead_Source__c.containsIgnoreCase('Intercom')) ||(lgld.LeadRecord.LeadSource!=null && lgld.LeadRecord.LeadSource.containsIgnoreCase('Intercom'))) {
            system.debug('::::inside intercom chat bot');
            if (!string.isBlank(Settings.Intercom_Intercom_Id__c)) {
                 system.debug('@@@@@inside intercom lead ');
                opp.OwnerId = Settings.Intercom_Intercom_Id__c;
            }
        }
        
        opp.StageName = Settings.Opportunity_StageName__c; 
        opp.CloseDate = System.today().addDays(7); 
        opp.RecordTypeId = Settings.Opportunity_RecordType_Id__c; 
        opp.Sell_Brand__c = lgld.LeadRecord.Sell_Brand__c; 
        String LandingPage = lgld.LeadRecord.Landing_page_url__c;
 		System.debug('LandingPage*****'+LandingPage);

        if(LandingPage != null && LandingPage != '' && LandingPage.contains('clickid')) {
            String landingUrl = EncodingUtil.urlDecode(LandingPage,'UTF-8');
            List<String> params = landingUrl.split('&');
		    System.debug('params*****'+params);

            Map<String, String> mapData = new Map<String, String>();
            for (String param : params)
            {
                String name = param.split('=')[0];
                String value = param.split('=')[1];
                System.debug('name***'+name);
                System.debug('value***'+value);
                mapData.put(name, value); 
            }
            System.debug('mapData***'+mapData);
            opp.irclickid__c = mapData.values()[0];
            opp.ircid__c = mapData.get('ircid');
            opp.irtrackid__c = mapData.get('irtrackid');
            opp.irpid__c = mapData.get('irpid');
            opp.Lead_Source__c = 'Call_Rail_ImpactRadius';
            System.debug('opp******data******'+opp);
        } else  if(lgld.LeadRecord.Lead_Source__c == 'Call_Rail'  && lgld.LeadRecord.GA_Campaign__c!=null && lgld.LeadRecord.GA_Campaign__c.containsIgnoreCase('Impact Radius Affiliate - Broad')){
            opp.Lead_Source__c = 'Call_Rail Impact Direct';
        }else  if(lgld.LeadRecord.Lead_Source__c!=null && lgld.LeadRecord.Lead_Source__c!=''){
            opp.Lead_Source__c = lgld.LeadRecord.Lead_Source__c;
        } 
        else{
            opp.Lead_Source__c = lgld.LeadRecord.LeadSource;
        }
        opp.Inventory_ID_Scratchpad__c = lgld.LeadRecord.InventoryID__c;
        opp.Brand__c = lgld.LeadRecord.Watch_Brand__c;
        opp.Sale_Price_Discussed__c = lgld.LeadRecord.Sale_Price__c;             
        opp.GA_Campaign__c = lgld.LeadRecord.GA_Campaign__c;
        opp.GA_Content__c = lgld.LeadRecord.GA_Content__c;
        opp.GA_Medium__c = lgld.LeadRecord.GA_Medium__c;
        opp.GA_Segment__c = lgld.LeadRecord.GA_Segment__c;
        opp.GA_Source__c = lgld.LeadRecord.Ga_Source__c;
        opp.GA_Term__c = lgld.LeadRecord.GA_Term__c;
        opp.GA_nVisits__c = lgld.LeadRecord.GA_Visits__c;
        opp.Name_Your_Price_ASK__c = lgld.LeadRecord.Name_Your_Price_ASK__c;
        opp.Name = lgld.LeadRecord.Full_Name__c; 
        opp.Page_Id__c = lgld.LeadRecord.Page_ID__c;
        opp.Originating_Lead__c = lgld.LeadRecord.Id;
        opp.Source__c = lgld.LeadRecord.Source__c;
       
        if(lgld.LeadRecord.Lead_Source__c!=null && lgld.LeadRecord.Lead_Source__c!='' && lgld.LeadRecord.LeadSource!=null && lgld.LeadRecord.LeadSource!='' ){
            if (lgld.LeadRecord.Lead_Source__c.equalsIgnoreCase(System.Label.LeadSource_WatchboxApp) || lgld.LeadRecord.LeadSource.equalsIgnoreCase(System.Label.LeadSource_WatchboxApp)){
                if(lgld.LeadRecord.Boxes_and_Papers__c!=null && lgld.LeadRecord.Boxes_and_Papers__c!=''){
                    if(lgld.LeadRecord.Boxes_and_Papers__c.equalsIgnoreCase('10')){
                        opp.Boxes_and_Papers__c=System.Label.Boxvalue;
                    }
                    else if(lgld.LeadRecord.Boxes_and_Papers__c.equalsIgnoreCase('01')){
                        opp.Boxes_and_Papers__c=System.Label.PaperValue;
                    }
                    else if(lgld.LeadRecord.Boxes_and_Papers__c.equalsIgnoreCase('11')){
                        opp.Boxes_and_Papers__c=System.Label.BothBoxPaper;
                    }else if(lgld.LeadRecord.Boxes_and_Papers__c.equalsIgnoreCase('00')){
                        opp.Boxes_and_Papers__c=System.Label.Neither;
                    }
                }
            }else{
                opp.Boxes_and_Papers__c = lgld.LeadRecord.Boxes_and_Papers__c;
            }
        }
        opp.GA_gclid__c = lgld.LeadRecord.GA_gclid__c;
        // opp.GCLID__c = lgld.LeadRecord.GA_gclid__c;
        opp.SKU__c = lgld.LeadRecord.SKU__c;
        opp.interested_in_Brand__c = lgld.LeadRecord.interested_in_Brand__c;
        opp.interested_in_Model__c = lgld.LeadRecord.interested_in_Model__c;
        opp.Interested_in_Product__c = lgld.LeadRecord.Interested_in_Product__c;
        opp.Interested_in_Product_List_price__c = lgld.LeadRecord.Interested_in_Product_List_price__c;
        opp.Interested_in_Product_Url__c = lgld.LeadRecord.Interested_in_Product_Url__c;
        opp.interested_in_Reference_Number__c = lgld.LeadRecord.interested_in_Reference_Number__c;   
        opp.Boxes__c = lgld.LeadRecord.Boxes__c;
        opp.Papers__c =   lgld.LeadRecord.Papers__c;
        opp.Referring_URL__c = lgld.LeadRecord.Referring_URL__c;
        
        //Set Transaction type 
        if (lgld.LeadRecord.What_Brings_You_In_Today__c == 'I want to buy a new watch') {
            opp.Type_of_Transaction_Initial__c = 'Sale';
        } else if (lgld.LeadRecord.What_Brings_You_In_Today__c == 'I want to sell a watch') {
            opp.Type_of_Transaction_Initial__c = 'Origination';
        } else if (lgld.LeadRecord.What_Brings_You_In_Today__c == 'I need my watch repaired') {
            opp.Type_of_Transaction_Initial__c = 'Repair';
        } else if (lgld.LeadRecord.What_Brings_You_In_Today__c == 'I need jewelry repaired') {
            opp.Type_of_Transaction_Initial__c = 'Repair';
        } else {
            opp.Type_of_Transaction_Initial__c = lgld.LeadRecord.Transaction_Type__c;
        }
        
        //Set Sell Model
        if (!string.isBlank(lgld.LeadRecord.Sell_Model__c) && lgld.LeadRecord.Sell_Model__c.length() > 255) {            
            opp.Sell_Model__c = lgld.LeadRecord.Sell_Model__c.substring(0, 254);            
        } else {
            opp.Sell_Model__c = lgld.LeadRecord.Sell_Model__c;
        }        
        
        //Set Model
        if (!string.isBlank(lgld.LeadRecord.Watch_Model__c) && lgld.LeadRecord.Watch_Model__c.length() > 255) {            
            opp.Model__c = lgld.LeadRecord.Watch_Model__c.substring(0, 254);            
        } else {
            opp.Model__c = lgld.LeadRecord.Watch_Model__c;
        } 
        
        //Set Initial details
        if (!string.isBlank(lgld.LeadRecord.Additional_Info__c)) 
            opp.Initial_Deal_Details__c = lgld.LeadRecord.Additional_Info__c;                    
        
        if (!string.isBlank(lgld.LeadRecord.Comments__c)) {
            if (!string.isBlank(opp.Initial_Deal_Details__c)) 
                opp.Initial_Deal_Details__c += '\n\n' + lgld.LeadRecord.Comments__c;                      
            else
                opp.Initial_Deal_Details__c = lgld.LeadRecord.Comments__c;
        }
        
        if (!string.isBlank(lgld.LeadRecord.Initial_Deal_Details__c)) {
            if (!string.isBlank(opp.Initial_Deal_Details__c)) 
                opp.Initial_Deal_Details__c += '\n\n' + lgld.LeadRecord.Initial_Deal_Details__c;                      
            else
                opp.Initial_Deal_Details__c = lgld.LeadRecord.Initial_Deal_Details__c;
        }
        
        //Set Images
        if (!string.isBlank(lgld.LeadRecord.Images__c)) {
            try{ 
                Set<String> imagesSetURL = new Set<String>();
                List<String> imagesListURL = new List<String>();
                imagesSetURL.addAll(lgld.LeadRecord.Images__c.split(','));  
                String imagesString = '';
                for(String image : imagesSetURL) {
                    Integer index = image.lastindexOf('/');
                    String previousString = image.subString(0,index+1);
                    String subStringFianl = image.subString(index+1,image.length());
                    String encodedURL = EncodingUtil.URLENCODE(subStringFianl,'UTF-8').replaceAll('\\+','%20');
                    image = previousString + encodedURL;
                    imagesString = imagesString +'\n' + image;
                }
                opp.Images__c = imagesString;
                String imagesToAdd ='';
                String imagestartString = '<img src="';
                String imageEndString = ' " width="300" height="200"></img>';
                Set<String> imagesSet = new Set<String>();
                List<String> imagesList = new List<String>();
                imagesSet.addAll(lgld.LeadRecord.Images__c.split(','));  
                imagesList.addAll(imagesSet);
                
                for(Integer i=0;i<imagesList.size();i++) {
                    if(i == 10)
                        continue;    
                    imagesToAdd = imagesToAdd + imagestartString + imagesList[i] + imageEndString;
                }
                opp.Watch_Images__c = imagesToAdd;
            }catch(Exception e) {
                System.debug('Exception ****'+e.getMessage());
                System.debug('ExceptionStackStrace ****'+e.getStackTraceString());
            }
        }      
        return opp;
    }     
    
    @testVisible
    private Deal_Schedule__c getDealScheduleOwnerId(Id accountId) {
        List<Deal_Schedule__c> schedules = [SELECT Id, Date__c, Related_Associate__c, Related_Person__c 
                                            FROM Deal_Schedule__c 
                                            WHERE Related_Person__c =:accountId AND Date__c <= TODAY
                                            ORDER BY Date__c DESC LIMIT 1];
        
        return (!schedules.isEmpty()) ? schedules[0] : null;
    }
    
    private void sendEmailNotifications(List<LeadGenLogDecorator> decoratedLeads) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        for (LeadGenLogDecorator lgld : decoratedLeads) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateID(Settings.Email_Template_Id__c); 
            mail.setTargetObjectId(lgld.Opportunity.OwnerId);
            mail.treattargetobjectasrecipient = true;
            mail.setSaveAsActivity(false);            
            mail.setWhatId(lgld.Opportunity.Id);
            mail.setOrgWideEmailAddressId(Settings.Org_Email_Id__c); 
            mails.add(mail);
        }
        
        Messaging.sendEmail(mails);        
    }
    
    @TestVisible
    private LeadGenLog_Settings__c getLeadSettings() {
        LeadGenLog_Settings__c settings = LeadGenLog_Settings__c.getValues('LeadTrigger');  
        
        return settings;
    }
     
}