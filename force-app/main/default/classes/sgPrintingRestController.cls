@RestResource(urlMapping='/Printing/*')
global class sgPrintingRestController {

    @HttpGet
    global static String get() {
        //run the custom auth check
        sgAuth.check();
        
        RestRequest req = RestContext.request;
        Map<String,String> params = req.params;
        String templateName = params.get('templateName');
        String templateType = params.get('templateType');
        String organization = params.get('organization');
        String masterId = params.get('id');
		//generate the attachment
        String attachmentId = sgPrintingController.GenerateDocument(templateName, templateType, masterId, organization);
        //get the attachment body
        Blob body = [SELECT Body FROM Attachment WHERE Id = :attachmentId].Body;
        return EncodingUtil.base64Encode(body);
    }
    
}