public class MagentoProductUpdate {
    public class MissingModelException extends Exception {}

    public static String productsToDTOJson(List<Product2> products) {
        List<MagentoProductDTO> pdtos = new List<MagentoProductDTO>();

        for (Product2 p: products) {
            MagentoProductDTO pdto = MagentoProductUpdate.productToDTO(p);
            pdtos.add(pdto);
        }

        String dataString = productDtosToDTOJson(pdtos);

        return dataString;
    }

    public static String productDTOsToDTOJson(List<MagentoProductDTO> pdtos) {
        Map<String, List<MagentoProductDTO>> batchData = new Map<String, List<MagentoProductDTO>>{
        	'products' => pdtos
        };

        String dataString = JSON.serialize(batchData);

        return dataString;
    }

    /**
     * Given a Product2 object, also load some ancillary info (series & photos)
     * and return a MagentoProductDTO ready to serialize.
     */
    public static MagentoProductDTO productToDTO(Product2 providedProduct) {
        return productsToDTO(new List<string>{providedProduct.Id}).get(0);
    }
    
    public static List<MagentoProductDTO> productsToDTO(List<string> productIds) {
    	return productsToDTO(productIds, false);    
    }
    
    public static List<MagentoProductDTO> productsToDTO(List<string> productIds, boolean recentOnly) {
        List<Product2> products = [
            SELECT
                Id,
                Name,
                IsActive,
                Description,
                Inventory_ID__c,
                WUW_URL_Alias__c,
                DisplayUrl,
                ASK__c,
                Cost__c,
                MSP__c,
                Retail__c,
                Watch_Gender__c,
                Product_Type__c,
                Warranty_Type__c,
                Status__c,
                Papers__c,
                Origination_Type__c,
                Condition__c,
                Watch_Case_Color__c,
                Watch_Bracelet__c,
                Boxes__c,
                Watch_Case_Material__c,
                Watch_Dial_Color__c,
                Watch_Package__c,
                Serial__c,
                Model__c,
            
            	Model__r.Id,
                Model__r.Name,
                Model__r.URL_Slug__c,
                Model__r.Verified__c,
                Model__r.AWS_Image_Array__c,
                Model__r.Also_Known_As__c,
                Model__r.Also_Known_As_2__c,
                Model__r.Bezel__c,
                Model__r.Bracelet_Material__c,
                Model__r.Caliber_Details__c,
                Model__r.Case_Color__c,
                Model__r.Case_Material__c,
                Model__r.Case_Size__c,
                Model__r.Caseback__c,
                Model__r.Complications__c,
                Model__r.Description__c,
                Model__r.Dial_Color__c,
                Model__r.Dial_Type__c,
                Model__r.Gender__c,
                Model__r.Model_Write_Up__c,
                Model__r.Movement_Type__c,
                Model__r.Tonneau_Case_Size__c,
                Model__r.Watch_Case_Shape__c,
                Model__r.Watch_Display_Type__c,
                Model__r.Water_Resistance_Rating__c,
            
            	Model__r.Model_Series__c,
	            Model__r.Model_Series__r.Id,
                Model__r.Model_Series__r.Series_Description__c,
                Model__r.Model_Series__r.Series_Quote__c,
                Model__r.Model_Series__r.URL_Slug__c,
                Model__r.Model_Series__r.WUW_Website_Active__c,
                Model__r.Model_Series__r.Watch_Brand__c,
                Model__r.Model_Series__r.Name,
            
            	Model__r.Model_Series__r.Watch_Brand__r.Id,
                Model__r.Model_Series__r.Watch_Brand__r.Name,
                Model__r.Model_Series__r.Watch_Brand__r.Brand_Description__c,
                Model__r.Model_Series__r.Watch_Brand__r.URL_Slug__c,
                Model__r.Model_Series__r.Watch_Brand__r.WUW_Website_Active__c,
                Model__r.Model_Series__r.Watch_Brand__r.Brand_Hex__c
            FROM Product2
            WHERE Id = :productIds
        ];
        
        Map<string, List<string>> photoUrls = loadPhotoUrls(productIds, recentOnly);
		
        List<MagentoProductDTO> dtoProducts = new List<MagentoProductDTO>();
        for (Product2 product : products) {
            if (product.Model__c == null) {
                throw new sgException('Product ' + product.Id + ' lacks a model');
            }
            
            MagentoProductDTO dtoProduct = new MagentoProductDTO();

            dtoProduct.photos = photoUrls.containsKey(product.Id) ? photoUrls.get(product.id) : null;
    
            dtoProduct.seriesId = product.Model__r.Model_Series__c;
    
            dtoProduct.sfId = product.Id;
            dtoProduct.name = product.Name;
            dtoProduct.description = product.Description;
            dtoProduct.inventoryId = product.Inventory_ID__c;
            dtoProduct.urlAlias = product.WUW_URL_Alias__c;
            dtoProduct.displayUrl = product.DisplayUrl;
            dtoProduct.ask = String.valueOf(product.ASK__c);
            dtoProduct.retail = String.valueOf(product.Retail__c);
            dtoProduct.watchGender = product.Watch_Gender__c;
            dtoProduct.productType = product.Product_Type__c;
            dtoProduct.warrantyType = product.Warranty_Type__c;
            dtoProduct.isActive = product.IsActive;
            dtoProduct.status = product.Status__c;
            dtoProduct.papers = product.Papers__c;
            dtoProduct.originationType = product.Origination_Type__c;
            dtoProduct.condition = product.Condition__c;
            dtoProduct.watchCaseColor = product.Watch_Case_Color__c;
            dtoProduct.watchBracelet = product.Watch_Bracelet__c;
            dtoProduct.boxes = product.Boxes__c;
            dtoProduct.watchCaseMaterial = product.Watch_Case_Material__c;
            dtoProduct.watchDialColor = product.Watch_Dial_Color__c;
            dtoProduct.watchPackage = product.Watch_Package__c;
            dtoProduct.serial = product.Serial__c;
    
            dtoProduct.series = MagentoSeriesSerializer.seriesToDTO(product.Model__r.Model_Series__r);
    
            dtoProduct.model = MagentoModelSerializer.modelToDTO(product.Model__r);
            system.debug(product.Model__r);
            system.debug(dtoProduct.series);
            
            dtoProducts.add(dtoProduct);

        }
        
        return dtoProducts;
    }


    /**
     * Given a Product2 object, return a list of its related photos in the proper order.
     */
    public static List<String> loadPhotoUrls(Product2 product) {
        return loadPhotoUrls(new List<string>{product.Id}).get(product.Id);
    }
    public static Map<string, List<String>> loadPhotoUrls(List<string> productIds) {
        return loadPhotoUrls(productIds, false);
    }
    public static Map<string, List<String>> loadPhotoUrls(List<string> productIds, boolean recentOnly) {
        DateTime since = system.now();
        
        if (recentOnly) {
            system.debug('recent');
            since = since.addDays(-700);
        } else {
            since = since.addDays(-365 * 10);
        }
        
        List<Photograph__c> photos = [
            SELECT
            Url__c,
            Related_Product__c
            FROM Photograph__c
            WHERE Related_Product__c in :productIds
            AND Organization__c = null
            AND IsDeleted = FALSE
            AND CreatedDate >= :since
            ORDER BY Display_Order__c ASC
        ];

        Map<string, List<String>> photoUrls = new Map<string, List<String>>();

        for (Photograph__c photo : photos) {
            if (!photoUrls.containsKey(photo.Related_Product__c)) {
                photoUrls.put(photo.Related_Product__c, new List<string>());
            }
            
            photoUrls.get(photo.Related_Product__c).add(photo.Url__c);
        }

        return photoUrls;
    }
}