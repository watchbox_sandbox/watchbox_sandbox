@isTest
public class WatchSaleTriggerHandlerTests {
    public static testmethod void afterUpdate_removeHolds_test() {
        Product2 product = TestFactory.createProduct();
        Product2 newProduct = TestFactory.createProduct();
        Opportunity opp = TestFactory.createOpportunity();        
        Watch_Sale__c ws = TestFactory.createWatchSale(product, opp);
        
        product.Hold_Expiration__c = Datetime.now().addDays(1);
        update product;
        
        ws.Inventory_Item__c = newProduct.Id;
        update ws;
        
        Product2 result = [SELECT Id, Hold_Expiration__c FROM Product2 WHERE ID =:product.Id];
        
        System.assertEquals(null, result.Hold_Expiration__c);
    }
    
    public static testmethod void afterDelete_removeHolds_test() {
        Product2 product = TestFactory.createProduct();
        Opportunity opp = TestFactory.createOpportunity();        
        Watch_Sale__c ws = TestFactory.createWatchSale(product, opp);
        
        product.Hold_Expiration__c = Datetime.now().addDays(1);
        update product;
             
        delete ws;
        
        Product2 result = [SELECT Id, Hold_Expiration__c FROM Product2 WHERE ID =:product.Id];
        
        System.assertEquals(null, result.Hold_Expiration__c);
    }
    
    static testmethod void afterDelete_removeWatchCollection_test() {
        Exception ex = null;
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Product2 product = TestFactory.createProduct();
        
        Watch_Sale__c watchSale = TestFactory.createWatchSale(product, opp);
        watchSale.Added_to_Collection__c = true;
        update watchSale;        
        
        Watch_Collection__c collection = TestFactory.createWatchCollection(account);
        collection.Watch_Sale_ID__c = Id.ValueOf(watchSale.Id);
        update collection;        
        delete watchSale;
        
        try {
            Watch_Collection__c result = [SELECT Id FROM Watch_Collection__c WHERE Id =: collection.Id];
        } catch (Exception e) {
            ex = e;
        }
        
        System.assertNotEquals(null, ex, 'Watch Collection record not cascaded properly');
    }
   
    
    @testSetup static void test_SetUp() {

       
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp1';
        opp.StageName = 'New';
         opp.CloseDate = Date.today();
        
        insert opp;

        List<Watch_Sale__c> list_WatchSale = new List<Watch_Sale__c>();
        Watch_Sale__c sale = new Watch_Sale__c();
        sale.Opportunity__c = opp.id;
        sale.MSP__c = 10.0;
        //sale.WB_Direct_Purchase_Offer__c = 10.0;
        sale.Client_s_Offer__c = 10.0;
        sale.Sale_Brand__c='Chopard';
        list_WatchSale.add(sale);
                
        Watch_Sale__c sale1 = new Watch_Sale__c();
        sale1.Opportunity__c = opp.id;
        sale1.MSP__c = 20.0;
        sale1.Sale_Brand__c='Chopard';
        //sale1.WB_Direct_Purchase_Offer__c = 20.0;
        sale1.Client_s_Offer__c = 20.0;
        list_WatchSale.add(sale1);
               insert list_WatchSale;
       
   }
    
    
    
    static testMethod void countRLonOpportunityUpdatetest(){
      
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp2';
        opp.StageName = 'New';
         opp.CloseDate = Date.today();
        
        insert opp;
        
        Watch_Sale__c sale = new Watch_Sale__c();
        sale.Opportunity__c = opp.id;
        sale.MSP__c = 10.0;
        //sale.WB_Direct_Purchase_Offer__c = 10.0;
        sale.Client_s_Offer__c = 10.0;
        sale.Sale_Brand__c='Chopard';
         insert sale;
     
        
     }
    static testMethod void countRLonOpportunityUpdatetesFalse(){
               
     Watch_Sale__c sale = [select id from  Watch_Sale__c limit 1 ];
    delete sale;
    }

        

    
    //TODO: Find out who wrote this and refactor...assertless
    public static testMethod void updateProductFields(){
        Opportunity testOpp = dataFactory.buildOpportunity();
        Brand_Object__c testBrand = dataFactory.buildBrand();
        insert testOpp;
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries;
        Model__c testModel = dataFactory.buildModel(testBrand.Id, testSeries.id);
        insert testModel;
        Product2 testProduct = dataFactory.buildProduct(testModel.id);
        insert testProduct;
        Watch_Sale__c testWatchSale = dataFactory.buildWatchSale(testOpp, testProduct.id);
        testWatchSale.Client_s_Offer__c=500;
        testWatchSale.MSP__c=1000;
        testWatchSale.Sale_Brand__c='Chopard';
        
        insert testWatchSale;
    }
    
    //TODO: Find out who wrote this a refactor...assertless
    public static testMethod void reassignProductToWatchSaleDeal(){
        //create necessary test records and variables
        Set<Id> prodIds = new Set<Id>();
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Opportunity testOpp2 = dataFactory.buildOpportunity();
        
        insert testOpp2;
        system.debug('Testopp 1: ' +testOpp.id+' - Testopp2: '+testOpp2.id);
        Brand_Object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries;
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Product2 inventory = dataFactory.buildProduct(testModel.id);
        //inventory.deal__c = testOpp.id;
        inventory.Hold_Expiration__c = system.now();
        inventory.Status__c = 'Available';
        insert inventory;
        prodIds.add(inventory.id);
        //watchSaleTriggerHelper.removeHolds(prodids);
        system.debug('Inventory related deal: ' + inventory.Deal__c);
        //now we create a watchSale and link it to our inventory along with a different deal
        //from the one inventory is currently related to in order to check trigger and see if inventory points
        //to the new deal and run our test
        Watch_Sale__c testWS = dataFactory.buildWatchSale(testOpp2, inventory.id);
        testWS.Client_s_Offer__c=500;
        testWS.Sale_Brand__c='Chopard';
        testWS.MSP__c=1000;
        	insert testWS;
        test.startTest();
        	
        	system.debug('WS deal: '+ testWS.Opportunity__c);
        	system.debug('WS inventory: '+ testWS.Inventory_Item__c);
        	system.debug('Inventory deal: '+inventory.Deal__c);
        	//tesst to make sure the inventory is tied to a different opp than when it was created
        	system.assertEquals(1, [SELECT count() from Watch_Sale__c WHERE Inventory_Item__c != null]);
        	system.assertEquals(3, [SELECT count() from Watch_Sale__c]);
        	system.assertEquals(1, [SELECT count() from Product2]);
        	system.assertEquals(3, [SELECT count() from Opportunity]);
        	system.assertNotEquals(inventory.Deal__c, testOpp.id);
        test.stopTest();
    }
}