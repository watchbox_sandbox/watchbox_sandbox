@isTest
public class UserTriggerHandlerTest {
    
    @isTest
    public static void successTest(){
        Profile p = [select id from profile where name='Sales'];
        User user = new User();
        user.ProfileId = p.id;
        user.FirstName = 'Lauren123';
        user.LastName = 'Bodoff456';
        user.Email = 'laurenbondoff@govbergwatche.com';
        user.Username = 'unittest@govberg.com';
        user.Alias = 'utest';
        user.CommunityNickname = 'utest';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LanguageLocaleKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.IsActive = true;
        user.Opt_out__c = true;
        insert user;
        system.assertNotEquals(user.id,null);
        system.runAs(user){        
            User userobj=[Select id,name ,Opt_out__c,ProfileId from User where name='Lauren123 Bodoff456' limit 1];
            try{
                userobj.Opt_out__c=false;
                update userobj;
            }catch(Exception e){
                system.debug('exception occured!!');
            }
        }
        
    }
    
    
}