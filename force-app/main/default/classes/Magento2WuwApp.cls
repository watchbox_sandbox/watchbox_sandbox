public with sharing class Magento2WuwApp {    
  @Future(callout=true) 
  public static void run(String endAction, Map<String, String> PostData, String httpMethod) {
      
      String dataString = '';
      for (String key : PostData.keySet()){
          if(dataString == '') {
              dataString = '{';
          } else {
              dataString += ',';
          }
          
          dataString += '"' + key + '": "' + PostData.get(key) +'"';
      }
      
      dataString += '}';
      
      System.debug('Post Data:' + dataString);
        
     MagentoCallOut(endAction,dataString,httpMethod);
  
  }
  
  public static void MagentoCallOut(String endAction,String dataString, String httpMethod){ 
      
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if(endAction!=''){ 
            String endPoint = 'http://watchuwant-magento.dev.surgeforward.com/'+endAction;
            req.setEndpoint(endPoint);
            req.setMethod(httpMethod); 
            req.setBody(dataString);
            req.setHeader('Content-Type', 'application/json');
            try {
                System.debug('MagentoCallOut in');
                System.debug(endPoint);                
                res = http.send(req);                 
                System.debug('MagentoCallOut out');  
            } catch(System.CalloutException e) {        
                System.debug(res.toString());
            }
        }
       
   } 
  
    
}