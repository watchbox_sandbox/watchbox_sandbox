public virtual class sgPrintingControllerBase {
    public class MissingOrganizationException extends Exception {}

    final String DEFAULT_ORG_NAME = 'Govberg';
    
    protected Map<String, String> parameters;
    protected String templateType;
    
    public Organization__c Org {get;set;}
    
    public sgPrintingControllerBase() {
        PageReference page = ApexPages.currentPage();
        parameters = page.getParameters();
        
        if (parameters.containsKey('templateType')) {
        	templateType = parameters.get('templateType'); 
        }
        
        String orgName = DEFAULT_ORG_NAME;
        if (parameters.containsKey('organization')) {
        	orgName = parameters.get('organization'); 
        }
        try {
            Org = [
                SELECT Name,
                       Logo_Url__c,
                       Address_Line_1__c,
                       Address_Line_2__c,
                       City__c,
                       State_or_Province__c,
                       Postal_Code__c,
                       Phone_Num__c
                FROM Organization__c
                WHERE Name = :orgName
            ];
        }
        catch(Exception ex) {
            throw new MissingOrganizationException('The Organization named "' + orgName + '" does not exist');
        }
    }
    
    public String getLogo() {
        return Org.Logo_Url__c;
    }
    
    public String getLogoAlt() {
        return Org.Name + ' Logo';
    }
    
}