@RestResource(urlMapping='/WatchesInOut/*')
global class sgWatchesInOutController {
    global class WatchesInOut {
        public Map<string, Detail> Ins  = new Map<string, Detail>();
        public Map<string, Detail> Outs = new Map<string, Detail>();
    }
    
    global class Detail {
        public decimal Cost = 0;
        public integer Units = 0;
        public string Color = '';
    }
    
    private static Set<string> Brands = new Set<string> {
        'patek',
        'rolex',
        'ap',
        'a. lange & sohne',
        'panerai',
        'hublot',
        'iwc',
        'vacheron',
        'fp journe',
        'breitling'
    };
    
    @HttpGet
    global static WatchesInOut get() {
        sgAuth.check();
        
        string location  = restContext.request.params.get('location');
        boolean gbInventory = false;
        if (location == 'GB Inventory') {
            location = null;
            gbInventory = true;
        }
        
        string startDate  = restContext.request.params.get('startDate');
        if (startDate == null) {
            startDate = (Date.today().year()) + '-01-01';
        }
        
        string endDate  = restContext.request.params.get('endDate');
        string currentYear  = restContext.request.params.get('currentYear');
         if (currentYear != null){
            endDate = (Date.today().addYears(1).year())  + '-01-01'; 
        }
        if (endDate == null) {
            Date d = Date.today().addDays(1);
            integer day = d.day();
            integer month = d.month();
            
            endDate = 
                string.valueOf(d.year()) + 
                '-' +
                (month < 10 ? '0' : '') + string.valueOf(month) +              
                '-' +
                (day < 10 ? '0' : '') + string.valueOf(day);
        }
        
        WatchesInOut w = new WatchesInOut();
        
        w.Ins  = Ins(location, startDate, endDate, gbInventory);
        w.Outs = Outs(location, startDate, endDate, gbInventory);
        
        return w;
    }
    
    private static Map<string, Detail> Ins(string location, string startDate, string endDate, boolean gbInventory) {
        string query = 'select Cost__c, Model__r.Model_Brand__r.Name, Watch_Brand__r.Brand_Hex__c from Product2 where IsActive = true';
        
        query += ' and (WUW_Date_Created__c >= ' + startDate + ' or GB_Date_Inventoried__c >= ' + startDate + ')';
        query += ' and (WUW_Date_Created__c <= ' + endDate  + ' or GB_Date_Inventoried__c <= ' + endDate + ')';
        
        if (location != null) {
            query += ' and Location__r.Name = \'' + location + '\'';
        }
        
        if (gbInventory) {
            query += ' and ISGBInventory__c = true';
        }
    
        List<Product2> products = Database.query(query);

        Map<string, Detail> Ins = new Map<string, Detail>();
        
        for (Product2 p : products) {
            string brand = p.Model__r.Model_Brand__r.Name;
            string color = p.Watch_Brand__r.Brand_Hex__c;
            if (brand == null) {
                brand = 'Others';
                color = '666';
            }
            
            if (!Ins.containsKey(brand)) {
                Detail d = new Detail();
                d.Color = color;
                Ins.put(brand, d);
            }
            
            detail brandTotal = Ins.get(brand);
            brandTotal.Cost += p.Cost__c != null ? p.Cost__c : 0;
            brandTotal.Units += 1;
            
            
            Ins.put(brand, brandTotal);
        }
        
        return Ins;
    }
    
    private static Map<string, Detail> Outs(string location, string startDate, string endDate, boolean gbInventory) {
        string query = 'select Inventory_Item__r.Model__r.Model_Brand__r.Name, Inventory_Item__r.Model__r.Model_Brand__r.Brand_Hex__c, Inventory_Item__r.Location__r.Name, 	Sale_Amount__c from Watch_Sale__c';
        
        query += ' where Opportunity__r.CloseDate >= ' + startDate;
        query += ' and Opportunity__r.CloseDate <= ' + endDate;
        
        if (location != null) {
            query += ' and Inventory_Item__r.Location__r.Name = \'' + location + '\'';
        }
        
        if (gbInventory != null) {
            query += ' and Inventory_Item__r.ISGBInventory__c = true';
        }

        List<Watch_Sale__c> products = Database.query(query + ' limit 10000');

        Map<string, Detail> Outs = new Map<string, Detail>();
        
        for (Watch_Sale__c p : products) {
            string brand = p.Inventory_Item__r.Model__r.Model_Brand__r.Name;
            string color = p.Inventory_Item__r.Model__r.Model_Brand__r.Brand_Hex__c;
            if (brand == null) {
                brand = 'Others';
                color = '666';
            }
            
            if (!Outs.containsKey(brand)) {
                Detail d = new Detail();
                d.Color = color;
                Outs.put(brand, d);
            }
            
            detail brandTotal = Outs.get(brand);
            brandTotal.Cost += p.Sale_Amount__c != null ? p.Sale_Amount__c : 0;
            brandTotal.Units += 1;
            
            
            Outs.put(brand, brandTotal);
        }
        
        return Outs;
    }
}