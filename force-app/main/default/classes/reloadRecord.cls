public class reloadRecord {
	public static ApexPages.standardController stdController;
    Public static Id objId{get;set;}
    public reloadRecord(){
        
        objId = apexPages.currentPage().getParameters().get('id');
        system.debug(objid);
    }
    
    public PageReference returnToRecord(){
        system.debug('In returnToRecord method');
        PageReference refreshObj = new PageReference('/'+objId);
        refreshObj.setRedirect(true);
        return refreshObj;
    }
}