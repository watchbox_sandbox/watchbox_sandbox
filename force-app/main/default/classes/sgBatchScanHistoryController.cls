@RestResource(urlMapping='/BatchScanHistory/*')
global class sgBatchScanHistoryController {
    @HttpGet
    global static Batch_Scan__c get() {
        sgAuth.check();
        
        Batch_SCan__c batch = [
            select Id, Name, Scan_Start__c, Scan_Completed__c, Team_Member__r.Name__c, Location_Scan__r.Name,
            (select Id, Scanned__c, Scanned_By__r.Name__c, Scan_Type__c, InventoryItem__r.Inventory_ID__c, InventoryItem__r.Model__r.Name,
            InventoryItem__r.Cost__c, InventoryItem__r.ASK__c, InventoryItem__r.MSP__c, 
            InventoryItem__r.Origination_Type__c,  InventoryItem__r.WUW_Owner__c, InventoryItem__r.Watch_Reference__c, 
            InventoryItem__r.Model__r.model_brand__r.Name, InventoryItem__r.Deal__r.Salesperson__r.Name__c, 
            InventoryItem__r.Last_Price_Change__c, InventoryItem__r.Posted_On_WUW__c 
            from Batch_Details__r)
            
            from Batch_Scan__c where Id = :RestContext.request.params.get('id')
            limit 1
        ];
        
        return batch;
    }
}