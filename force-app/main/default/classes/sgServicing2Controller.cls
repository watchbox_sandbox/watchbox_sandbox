//not enough verbs
@RestResource(urlMapping='/Servicing2/*')
global class sgServicing2Controller {
    //change ordering
    @HttpPost
    global static void post(Map<string, integer> ordering) {
        sgAuth.hasRole('Admin,Watchmaker Lead, Watchmaker');
        
        List<Servicing__c> servicings = [select Id, Order__c from Servicing__c where Id in :ordering.keySet()];
        
        for (Servicing__c s : servicings) {
            s.Order__c = ordering.get(s.Id);
        }
        
        update servicings;
    }
    
    //add notes
    @HttpPut
    global static void put(string watchId, string note, string noteId) {
        sgAuth.hasRole('Admin,Watchmaker Lead, Watchmaker');
        
        Servicing__c s = [select Id, Product__r.Inventory_ID__c from Servicing__c where Id = :watchId];
        
        Note n = new Note();
        
        if (noteId != null) {
            n = [select Id, ParentId, Body from Note where Id = :noteId and ParentId = :watchId];
        } else {
            n.ParentId = watchId;
        }
        
        n.Title = 'Servicing Note, Inventory ID: ' + s.Product__r.Inventory_ID__c;
        n.Body = note;
        
        upsert n;
    }
    
    //fail qc
    @HttpPatch
    global static void patch(string productId) {
        Product2 p = [select Id, QC_Failed__c from Product2 where Id = :productId];
        
        p.QC_Failed__c = true;
        update p;
    }
    
}