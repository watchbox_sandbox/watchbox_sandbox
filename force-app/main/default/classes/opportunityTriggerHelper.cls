public class opportunityTriggerHelper {
    
    public static void  repeatCustomer(List<opportunity> triggerNew){
        Set<Id> conIds = New Set<Id>();
        List<Opportunity> oppsToCheck = New List<Opportunity>();
        Integer oppCount;
        List<Opportunity> updatedOpps = New List<Opportunity>();
        for(Opportunity opp : triggerNew){
         	conIds.add(opp.contact_Record__c);   
        } 
        oppsToCheck = [SELECT Id, contact_Record__c FROM Opportunity WHERE contact_Record__c IN :conIds];
        
        for(Opportunity opp : triggerNew){
            oppCount = 1;
            for(Opportunity opp2 : oppstoCheck){
                if(opp.Contact_Record__c == opp2.contact_Record__c && opp.Id != opp2.Id)
                {
                    oppCount++;
                }
            }
            if(oppCount > 1){
                opp.Customer_Type__c = 'Repeat Business';
            }else{
                opp.Customer_Type__c = 'New Business';
            }
        }
        
    }//end repeat customer method
    
    public static void addIdToName(List<Opportunity> triggerNew){
        List<Opportunity> oppsToUpdate = New List<Opportunity>();
        for(opportunity opp : triggerNew){
            opp.name = opp.name + opp.Id;
            oppsToUpdate.add(opp);
        }
        update oppsToUpdate;
    }

}