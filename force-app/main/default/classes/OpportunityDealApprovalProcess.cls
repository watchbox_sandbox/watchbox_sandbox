global class OpportunityDealApprovalProcess {
  
    @AuraEnabled
     webservice static void submitApprovalProcess(Id oppid){
       
        system.debug('----inside approval process---');
      
        Opportunity opp=[Select id,ownerid from Opportunity where id=:oppid];
        List<User> u=[select id,ManagerId from user where id=: opp.ownerid limit 1];
        String userid = Userinfo.getUserId();
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval automatically from opportunity');
        req1.setObjectId(oppid);
        req1.setSubmitterId(opp.OwnerId);
        system.debug('---***----'+oppid+'-------'+u[0].ManagerId);

        // Submit the approval request for the Opportunity
        Approval.ProcessResult result = Approval.process(req1);
         
    }
    
     @AuraEnabled
    public static  List<sobject> getDealDetails(String oppId){
        List<sobject> sObj=new List<sobject>();
      Deal_Approval_Process__c settingObj=[select id,Is_watch_Purchased_if_already_in_Stock__c from Deal_Approval_Process__c limit 1];
        Opportunity opp=[Select id,name,Is_ref_currently_in_stock__c from Opportunity where id=:oppId];
        sObj.add(settingObj);
        sObj.add(opp);
        
        return sObj;
        
        
    }
    

}