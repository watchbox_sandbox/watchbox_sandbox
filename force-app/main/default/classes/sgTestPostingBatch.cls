@isTest
public class sgTestPostingBatch {

    static testmethod void runBatch() {
        //arrange
        //
        createServiceSettings('Watchbox');
        createFakeSetting('Watchbox', 'Sandbox', 'token');
        ChannelPost__c cp1 = addChannelInfo('Test1', 'Watchbox');
        ChannelPost__c cp2 = addChannelInfo('Test2', 'Watchbox');
        ChannelPost__c cp3 = addChannelInfo('Test3', 'Watchbox');
        cp3.Is_Processing__c = false; //update this because the trigger will set it to true
        update cp3;
        
        //act
        //
        Test.StartTest();
        sgPostingBatch batch = new sgPostingBatch();
        Database.executeBatch(batch);
        Test.stopTest();
        
        //assert
        //
        System.assertEquals(1, sgPostingBatch.RunCount);
    }
    
    //***********************************************
    // support
    
    static ChannelPost__c addChannelInfo(String name, String channel) {
        ChannelPost__c chpost = new ChannelPost__c(
            Name = name,
            Channel__c = channel,
            Post__c = true,
            Visible__c = false,
            Is_Dirty__c = true,
            Is_Processing__c = true //set this so the trigger doesn't try to process
        );
        insert chpost;
        return chpost;
    }
    
    static void createServiceSettings(String name) {
        Surge_Integration_Settings__c setting = new Surge_Integration_Settings__c(
            Name = name,
            Integration__c = name,
            Service__c = 'Watchbox',
            Path__c = '/test',
            Parameters__c = 'test=test'
        );
        
        insert setting;
    }
    
    static Surge_Endpoint_Settings__c createFakeSetting(String name, String env, String authMode) {
        Surge_Endpoint_Settings__c setting = new Surge_Endpoint_Settings__c();
        setting.Environment__c = env;
        setting.Name = setting.Endpoint_Name__c = name;
        setting.Auth_Mode__c = authMode;
        setting.Auth_Url__c = 'https://test.authendpoint/?clientId={clientId}/';
        setting.Client_ID__c = '12345';
        setting.Client_Secret__c = '67891';
        setting.Password__c = 'password';
        setting.User_Name__c = 'username';
        setting.Auth_Response_Key__c = 'access_token';
        setting.Endpoint_Url__c = 'https://test.endpoint/';
        insert setting;
        return setting;
    }
}