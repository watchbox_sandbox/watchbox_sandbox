///
// This class gets integration meta data from settings
// and provides methods for authenticating and executing
// calls to the integration endpoint
// *note* the class is marked as virtual so once private methods can be marked as protected whic his required for mocking
// 
public virtual class sgEndpoint {
    public class SettingNotFoundException extends Exception {}
    public class InvalidAuthModeException extends Exception {}
    public class FailedConnectionException extends Exception {}
    public class InvalidResponseException extends Exception {}
    public class InvalidAuthenticationResponseException extends Exception {}
    public class NotImplementedException extends Exception {}
    public class AuthenticationInProgressException extends Exception {}

    String endpointName;
    Long tokenTtl; // In milliseconds. If token time-to-live is less than 0, it should never expire.
    Long tokenExpiresAt; // Unix milliseconds timestamp
    static final Long MAX_TIMESTAMP = 2147483647000L; // 2038
    Boolean isAuthenticating;
    String endpointUrl; //the base url for running all api requests
    String environment; //sandbox or production
    String authMode; //defines the type of authentication
    String authUrl;  //the url to run authentication against
    String authMethod; //the http rest verb used in the auth request
    String authPayload; //the http payload to send in the auth request
    String authHeader; //the http headers for the auth request
    String authResponseKey; //the key that represents the authentication value in the response, blank will use the entire response
    String username; //used for standard auth
    String password; //used for standard auth
    String token; //the refresh token
    String clientId; //the oauth client id
    String clientSecret; //the oauth secret
    integer timeout; //request timeout
    
    public sObject Context {get;set;} //a map representing the context of the call, will be used to replace {!key} values
    public String AccessToken {get;set;} //the resulting token to use for subsequent api calls
    
    public sgEndpoint(String name, String organization) {
        Surge_Endpoint_Settings__c setting;
        
        //determine the environment
        String env;
        if(sgUtility.isSandbox()) {
            env = 'sandbox';
        }
        else {
            env = 'production';
        }
        //get the custom setting
        try {
            setting = [
                SELECT Name,
                Auth_Mode__c,
                Auth_Url__c,
                Auth_Response_Key__c,
                Auth_Method__c,
                Auth_Payload__c,
                Auth_Headers__c,
                Endpoint_Url__c, 
                Environment__c, 
                Password__c, 
                Token_Part1__c, 
                Token_Part2__c, 
                Token_ttl__c,
                Token_expires_at__c,
                Is_authenticating__c,
                User_Name__c,
                Client_Secret__c,
                Client_ID__c,
                Timeout__c
                FROM Surge_Endpoint_Settings__c 
                WHERE Endpoint_Name__c = :name 
                AND Organization__c = :organization
                AND Environment__c = :env 
                LIMIT 1
            ];
        }
        catch(Exception ex) {
            throw new SettingNotFoundException('Missing Endpoint setting for Name: ' + name + ', Environment: ' + env + ', Organization: ' + organization);
        }
        //set the class values
        endpointName = setting.Name;
        tokenTtl = (setting.Token_ttl__c != null) ? (Long)setting.Token_ttl__c : -1L;
        tokenExpiresAt = (setting.Token_expires_at__c != null) ? (Long)setting.Token_expires_at__c : -1L;
        isAuthenticating = setting.Is_Authenticating__c || false;

        authMode = setting.Auth_Mode__c;
        authUrl = setting.Auth_Url__c;
        if (authUrl != null && authUrl.endsWith('/')) {
            authUrl = authUrl.substring(0, authUrl.length() - 1);
        }
        authMethod = (setting.Auth_Method__c != null) ? setting.Auth_Method__c : 'GET';
        authPayload = setting.Auth_Payload__c;
        authResponseKey = setting.Auth_Response_Key__c;
        authHeader = setting.Auth_Headers__c;

        endpointUrl = setting.Endpoint_Url__c;
        if (endpointUrl != null && endpointUrl.endsWith('/')) {
            endpointUrl = endpointUrl.substring(0, endpointUrl.length() - 1);
        }
        environment = setting.Environment__c;
        username = (setting.User_Name__c != null) ? setting.User_Name__c  : '';
        password = (setting.Password__c != null) ? setting.Password__c : '';
        token = '';
        if (setting.Token_Part1__c != null) {
            token = setting.Token_Part1__c;
        }
        if (setting.Token_Part2__c != null) {
            token += setting.Token_Part2__c;
        }
        clientId = (setting.Client_ID__c != null) ? setting.Client_ID__c : '';
        clientSecret = (setting.Client_Secret__c != null) ? setting.Client_Secret__c : '';

        timeout = timeout != null ? integer.valueOf(setting.Timeout__c) : 10000;
        
        this.AccessToken = '';
    }
    
    public void UpdateContext(String key, String value) {
        if (this.Context != null) {
            this.Context.put(key, value);
        }
    }
    
    ///
    // Run the authentication and stores the result internally to be used with subsequent 
    //  endpoint calls
    //
    public void Authenticate() {
        if (authMode == 'oauth') {
            authOAuth();
        }
        else if (authMode == 'token') {
            //we are expecting a token at this point
            if (token == null) {
                throw new InvalidAuthenticationResponseException('Missing token from settings');
            }
            AccessToken = token;
        }
        else if (authMode == 'refresh') {
            //we are expecting a token at this point to be used as the refresh token
            if (token == null) {
                throw new InvalidAuthenticationResponseException('Missing token from settings');
            }
            authOAuth();
        }
        else if (authMode == 'magentotoken') {
            authMagentoToken();
        }
        else if (authMode != 'none') {
            //unknown auth mode
            throw new InvalidAuthModeException('Invalid authentication mode "' + authMode + '"');
        }
    }
    
    public Blob Get(String path, String params, String headers) {
        //Execute web service call
        return Send(path, 'GET', null, params, headers);
    }
    
    public Blob Post(String path, Blob payload, String params, String headers) {
        //Execute web service call
        return Send(path, 'POST', payload, params, headers);
    }
    
    public Blob Put(String path, Blob payload, String params, String headers) {
        //Execute web service call
        return Send(path, 'PUT', payload, params, headers);
    }
    
    public Blob Del(String path, String params, String headers) {
        //Execute web service call
        return Send(path, 'DELETE', null, params, headers);
    }
    
    public Blob Send(String path, String method, Blob payload, String params, String headers) {
        //Execute web service call
        return Request(endpointUrl, path, method, payload, params, headers).getBodyAsBlob();
    }
    
    public HttpResponse Request(String url, String path, String method, Blob payload, String params, String headers) {
        String urlVal = ApplyValues(url);
        String pathVal = ApplyValues(path);
        Map<String, String> paramMap = sgUtility.QueryStringtoMap(ApplyValues(params));
        Map<String, String> headersMap = sgUtility.QueryStringtoMap(ApplyValues(headers));
        if (payload != null) {
            payload = Blob.valueOf(ApplyValues(payload.toString()));
        }

        // If headers aren't defined, make them an emtpy map.
        if (headersMap == null) {
            headersMap = new Map<String, String>{};
        }

        // Add Authorization header for magento
        if (authMode == 'magentotoken' && !String.isBlank(AccessToken)) {
            String bearerToken = 'Bearer ';
            bearerToken += AccessToken;
            headersMap.put('Authorization', bearerToken);
            headersMap.put('Content-Type', 'application/json');
        }

        //Execute web service call 
        HttpResponse res = sendHttpRequest(urlVal, pathVal, method, payload, paramMap, headersMap);
        
        System.debug(res.getBody());
        //check for successful response
        //if we don't have a 200 status then throw an error
        Integer statusCode = res.getStatusCode();
        if (statusCode > 299) {
            //if there is a response in the body then let's pass the on
            String body = res.getBody();
            if (body == null || body.length() == 0) {
                String msg = 'Status Code: ' + String.valueOf(statusCode);
                String payloadStr = '';
                if (payload != null) {
                    payloadStr = payload.toString();
                }
                msg = 'Response:\n' + msg + '\n\nPath:\n' + pathVal + '\n\nMethod:\n' + method + '\n\nHeaders:\n' + headersMap + '\n\nParams:\n' + paramMap + '\n\nPayload:\n' + payloadStr;
                throw new InvalidResponseException(msg);
            }
        }
        
        return res;
    }
    
    //*********************************************************
    //private methods
    
    ///
    // Executes an oauth request and attempts to exctract the resulting token from the response
    //  using the authResponseKey as the token name in the response
    // 
    protected void authOAuth() {
        //get the request values
        Blob payloadBlob = null;
        if (authPayload != null) {
            payloadBlob = Blob.valueOf(ApplyValues(authPayload));
        }

        //Execute web service call
        HttpResponse res = Request(authUrl, null, authMethod, payloadBlob, null, authHeader);
        
        //extract and set the access token
        if (res.getHeader('Content-Type').contains('application/json')) {
            AccessToken = ExtractTokenValueJson(res.getBody());
        }
        else {
            AccessToken = ExtractTokenValue(res.getBody());
        }
    }

    protected void authMagentoToken() {
        // We'll be updating the setting, so let's load it fresh.
        Surge_Endpoint_Settings__c setting = Surge_Endpoint_Settings__c.getInstance(endpointName);

        // If authenticating is already taking place somewhere else, throw.
        if (setting.Is_Authenticating__c) {
            throw new AuthenticationInProgressException('authrorization is in progress');
        }

        // Start the auth process.
        setting.Is_Authenticating__c = true;
        isAuthenticating = true;

        JSONGenerator gen = JSON.createGenerator(false);

        gen.writeStartObject();
        gen.writeStringField('username', username);
        gen.writeStringField('password', password);
        gen.writeEndObject();

        String payload = gen.getAsString();

        HttpResponse res = Request(authUrl, null, authMethod, Blob.valueOf(payload), null, authHeader);

        if (res.getHeader('Content-Type').contains('application/json')) {
            try {
                AccessToken = (String)JSON.deserializeStrict(res.getBody(), String.class);
            }
            catch (Exception ex) {
                throw new InvalidAuthenticationResponseException('cannot get token');
            }

            if (AccessToken.length() < 2) {
                throw new InvalidAuthenticationResponseException('cannot get token');
            }

            // Now we have a new token :)
            setting.Token_Part1__c = AccessToken;
            if (tokenTtl >= 0) {
                setting.Token_expires_at__c = DateTime.now().getTime() + tokenTtl;
            } else {
                setting.Token_expires_at__c = MAX_TIMESTAMP;
            }
        }

        setting.Is_Authenticating__c = false;
        //update setting; TODO: transaction here?
        isAuthenticating = false;
    }

    ///
    // Contstructs the http request, sends it, and handles any exceptions
    // returns the HttpResponse or throws FailedConnectionException exception
    // 
    protected HTTPResponse sendHttpRequest(String base, String path, String method, Blob payload,  Map<String, String> params,  Map<String, String> headers) {
        //create an http request
        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        
        //create the url        
        String url = contructUrl(base, path, params);
        
        //set the payload
        if (payload != null) {
            req.setBodyAsBlob(payload);
        }
        req.setTimeout(timeout);
        //set the headers
        if (headers != null) {
            for(String key : headers.keySet()) {
                req.setHeader(key, headers.get(key));
            }
        }
        
        System.debug(('Base: ' + base + '~Path: ' + path + '~Method: ' + method + '~Params: ' + params + '~Headers: ' + headers + ((payload != null) ? '~Payload:~' + payload.toString() : '')).replaceAll('\n|\r', ''));
        
        //set the url
        req.setEndpoint(url); //apply any vairables using the {variable} notation
        req.setTimeout(120000);
        
        //Execute web service call
        HTTPResponse res;
        try {
            Http http = new Http();
            res = http.send(req);
        }
        catch (Exception ex) {
            String msg = 'Connection Issue-\n\n' + 
                'Message: ' + ex.getMessage() + '\n\n' +
                'Url: ' + base + '\n' + 
                'Path: ' + path + '\n' +
                'Method: ' + method + '\n' +
                'Params: ' + params + '\n' +
                'Headers: ' + headers;
            if (payload != null) {
                msg += '\nPayload:\n' + payload.toString();
            }
            throw new FailedConnectionException(msg, ex);
        }
        
        return res;
    }
    
    ///
    // Constructs a url from the base, path, and parameters
    // 
    protected String contructUrl(String base, String path, Map<String, String> params) {
        //create the url
        String url = base;
        
        //add the path
        if (path != null) {
            //remove the leading / on the path
            if (path.indexOf('/') == 0) {
                path = path.substring(1);
            }
            url = url + '/' + path;
        }
        
        //update the url with the params
        if(params != null) {
            String qs = '';
            for(String key : params.keySet()) {
                qs += '&' + key + '=' + params.get(key);
            }
            //add the params to the url
            if (url.indexOf('?') == -1) {
                url = url + '?' + qs.substring(1);
            }
            else {
                url = url + qs;
            }
        }
        
        //update the url with the data values
        url = ApplyValues(url);
        
        return url;
    }
    
    ///
    // Extracts the authentication token value from the unformated string result
    //  expecting a name=value pair either by itself or delimited by a comma or ampersand
    //  returns the value name represented by the authResponseKey value
    //  
    protected String ExtractTokenValue(String result) {
        if (result.indexOf(authResponseKey) == -1) {
            throw new InvalidAuthenticationResponseException(result);
        }
        //get the string starting from the authResponseKey
        String value = result
            .substring(result.indexOf(authResponseKey))
            .trim();
        //find the = 
        value = value
            .substring(value.indexOf('=') + 1)
            .trim();
        //find the end
        if (value.contains(',')) {
            value = value.substring(value.indexOf('=') - 1);
        }
        else if (value.contains('&')) {
            value = value.substring(value.indexOf('&') - 1);
        }
        
        return value;
    }
    
    ///
    // Converts the string to JSON and then finds the authentication token name
    // represented by the authResponseKey value
    // 
    protected String ExtractTokenValueJson(String resultJson) {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(resultJson);
        if (result != null && result.containsKey(authResponseKey)) {
            return (String)result.get(authResponseKey);
        }
        return null;
    }
    
    ///
    // Applies various values to the content, represented by named values
    // with the {name} format
    // 
    protected String ApplyValues(String content) {
        Map<String, String> data = new Map<String, String>
        {
          'clientId' => clientId,
          'clientSecret' => clientSecret,
          'username' => username,
          'password' => password,
          'token' => token,
          'accessToken' => this.AccessToken
        };

        content = sgUtility.ApplyData(content, data);
        
        if (Context != null) {
            content = sgUtility.ApplyData(content, Context);
        }
        //remove any keys that still remain
        content = sgUtility.RemoveKeys(content);
        
        return content;
    }
    
}