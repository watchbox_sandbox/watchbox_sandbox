@RestResource(urlMapping='/ProductSummary/*')
global class sgProductSummaryController {
    global class Summary {
        public Map<string, Map<string, Detail>> ProductTypes = new Map<string, Map<string, detail>> {
            'Pre-Owned' => new Map<string, detail>(),
            'Closeout' => new Map<string, detail>(),
            'Regular Goods' => new Map<string, detail>()
        };
        
        public integer TotalProductTypeUnits = 0;
        public integer TotalUnits = 0;
        
        public decimal TotalCost = 0;
            
        public Map<string, Detail> Totals = new Map<string, detail> {
            'Pre-Owned' => new Detail(),
            'Closeout' => new Detail(),
            'Regular Goods' => new Detail()
        };
            
        public Map<string, Aging> Aging = new Map<string, Aging>();
        public Aging GlobalAging = new Aging();
        
        public Preowned_Sales__c Scorecard;
        public string query;
    }
    
    global class Aging {
        public decimal x0Cost = 0;
        public decimal x0Units = 0;
        
        public decimal x90Cost = 0;
        public decimal x90Units = 0;
        
        public decimal x180Cost = 0;
        public decimal x180Units = 0;

        public decimal x365Cost = 0;
        public decimal x365Units = 0;        
    }
    
    global class Detail {
        public decimal Cost = 0;
        public integer Units = 0;
        public string Color = '';
    }

    
    private static Set<string> Brands = new Set<string> {
        'patek',
        'rolex',
        'ap',
        'a. lange & sohne',
        'panerai',
        'hublot',
        'iwc',
        'vacheron',
        'fp journe',
        'breitling'
    };
    
    private static Set<string> ProductTypes = new Set<string> {
        'Pre-Owned',
        'Closeout',
        'Regular Goods'
    };
            
    @HttpGet
    global static Summary get() {
        sgAuth.check();
        
        string location  = restContext.request.params.get('location');
        boolean gbInventory = false;
        if (location == 'GB Inventory') {
            location = null;
            gbInventory = true;
        }
        
        
        string startDate  = restContext.request.params.get('startDate');
        if (startDate == null) {
            startDate = (Date.today().year() - 1) + '-01-01';
        }
        
        string endDate  = restContext.request.params.get('endDate');
        if (endDate == null) {
            Date d = Date.today().addDays(1);
            integer day = d.day();
            integer month = d.month();
            
            endDate = 
                string.valueOf(d.year()) + 
                '-' +
                (month < 10 ? '0' : '') + string.valueOf(month) +              
                '-' +
                (day < 10 ? '0' : '') + string.valueOf(day);
        }
        
        string query = 'select Cost__c, Model__r.Model_Brand__r.Name, Watch_Brand__r.Brand_Hex__c, Days_Inventoried__c, Product_Type__c from Product2 where IsActive = true';
        
        //query += ' and (WUW_Date_Created__c >= ' + startDate + ' or GB_Date_Inventoried__c >= ' + startDate + ')';
        //query += ' and (WUW_Date_Created__c <= ' + endDate  + ' or GB_Date_Inventoried__c <= ' + endDate + ')';
        
        if (location != null) {
            query += ' and Location__r.Name = \'' + string.escapeSingleQuotes(location) + '\'';
        }
        
        if (gbInventory == true) {
            query += ' and ISGBInventory__c = true';
        }
    
        List<Product2> products = Database.query(query);

        system.debug('sql:' + query);
        
        Summary s = new Summary();
        s.query = query;
        s.TotalUnits = products.size();
        
        for (Product2 p : products) {
            aging(s, p);
            
            if (ProductTypes.contains(p.Product_Type__c)) {
                s.TotalProductTypeUnits++;
                
                decimal cost = p.Cost__c != null ? p.Cost__c : 0;
                s.TotalCost += cost;
                
                string brand = p.Model__r.Model_Brand__r.Name;
                string color = p.Watch_Brand__r.Brand_Hex__c;
                if (brand == null /*|| !Brands.contains(brand.toLowerCase())*/) {
                    brand = 'Others';
                    color = '666';
                }
                
                if (!s.ProductTypes.get(p.Product_Type__c).containsKey(brand)) {
                    Detail d = new Detail();
                    d.Color = color;
                    s.ProductTypes.get(p.Product_Type__c).put(brand, d);
                }
                
                detail brandTotal = s.ProductTypes.get(p.Product_Type__c).get(brand);
                brandTotal.Cost += cost;
                brandTotal.Units += 1;
                
                detail productTypeTotal = s.Totals.get(p.Product_Type__c);
                productTypeTotal.Cost += cost;
                productTypeTotal.Units += 1;                
                
                s.ProductTypes.get(p.Product_Type__c).put(brand, brandTotal);
            }
        }
        
        try {
            Preowned_Sales__c p = new Preowned_Sales__c();
            //p.O
            
            s.Scorecard = [
                select id, originations__c, preowned_units__c, closeouts_amount__c, clouseouts_units__c, new_amount__c, 
                new_units__c, virtuals_amount__c, virtuals_units__c, total_sales_day_amount__c, total_sales_day_units__c, 
                cashin__c, traded_value__c, total_sales_mtd_amount__c, total_sales_mtd_units__c, preowned_amount__c 
                from Preowned_Sales__c order by CreatedDate desc limit 1
            ];
        } catch (Exception e) {}

        return s;
    }
    
    public static void aging(Summary s, Product2 p) {
        integer age = p.Days_Inventoried__c != null ? p.Days_Inventoried__c.intValue() : 0;
        decimal cost = p.Cost__c != null ? p.Cost__c : 0;


        string productType = p.Product_Type__c;
        
        if (productType == null) {
            productType = 'No Product Type';
        }
        
        if (!s.Aging.containsKey(productType)) {
            s.Aging.put(productType, new Aging());
        }
        
        if (age < 90) {
            s.Aging.get(productType).x0Cost += cost;
            s.Aging.get(productType).x0Units += 1;
            
            s.GlobalAging.x0Cost += cost;
            s.GlobalAging.x0Units += 1;
        } else if (age < 180) {
            s.Aging.get(productType).x90Cost += cost;
            s.Aging.get(productType).x90Units += 1;
            
            s.GlobalAging.x90Cost += cost;
            s.GlobalAging.x90Units += 1;
        } else if (age < 365) {
            s.Aging.get(productType).x180Cost += cost;
            s.Aging.get(productType).x180Units += 1;
            
            s.GlobalAging.x180Cost += cost;
            s.GlobalAging.x180Units += 1;
        } else {
            s.Aging.get(productType).x365Cost += cost;
            s.Aging.get(productType).x365Units += 1;
            
            s.GlobalAging.x365Cost += cost;
            s.GlobalAging.x365Units += 1;
        }
    }
}