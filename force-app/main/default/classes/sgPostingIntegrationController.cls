///
// Get a concrete instance of 
// 
public class sgPostingIntegrationController {
    public class InvalidActionException extends Exception {}
    public class InvalidChannelPostIdException extends Exception{}
    
    static String BASE_NAME = 'sgPostingIntegration';
    static String GENERIC_NAME = BASE_NAME + 'Generic';
    
    static Map<String, String> ACTION_MAP = new Map<String, String> {
        '00000' => 'none',
        '10000' => 'post',
        '01000' => 'invalid',
        '11000' => 'post,visible',
        '00100' => 'unpost',
        '10100' => 'none',
        '01100' => 'invalid',
        '11100' => 'visible',
        '00010' => 'invalid',
        '10010' => 'invalid',
        '01010' => 'invalid',
        '11010' => 'invalid',
        '00110' => 'unpost,unvisible',
        '10110' => 'unvisible',
        '01110' => 'invalid',
        '11110' => 'none',
        '00001' => 'none',
        '10001' => 'post',
        '01001' => 'invalid',
        '11001' => 'post,visible',
        '00101' => 'unpost',
        '10101' => 'update',
        '01101' => 'invalid',
        '11101' => 'post,visible',
        '00011' => 'invalid',
        '10011' => 'invalid',
        '01011' => 'invalid',
        '11011' => 'invalid',
        '00111' => 'unpost,unvisible',
        '10111' => 'unvisible',
        '01111' => 'invalid',
        '11111' => 'update'
    };
    
    public sgPostingIntegration Worker {get;set;}
    public ChannelPost__c ChannelPost {get;set;}
    
    ///
    // The ChannelPost__c object should be marked as processing 
    //  before using this controller to keep from processing it 
    //  multiple times, e.g. when a trigger fires for the same 
    //  ChannelPost__c record that was picked up by the daily
    //  scheduled run
    //  
    public sgPostingIntegrationController(ChannelPost__c channelPost, sgClassResolver resolver, sgEndpoint endpoint) {
        this.ChannelPost = channelPost;
        // get the concrete implementation of the sgPostingIntegration abstract class
        String className = BASE_NAME + channelPost.Channel__c;
        if (resolver.classExists(className)) {
        	Worker = (sgPostingIntegration)resolver.executeClass(className);
        } else if (channelPost.Channel__c == 'Watchbox International') {
            Worker = (sgPostingIntegration)resolver.executeClass('sgPostingIntegrationWatchboxGlobal');
        } else if (channelPost.Channel__c == 'Watchbox Hong Kong') {
            Worker = (sgPostingIntegration)resolver.executeClass('sgPostingIntegrationWatchboxHk');
        } else {
        	//if the class doesn't exist then we'll use the generic
            Worker = (sgPostingIntegration)resolver.executeClass(GENERIC_NAME);
        }
        Worker.Name = channelPost.Channel__c;
        Worker.Endpoint = endpoint;
        Worker.ChannelPost = channelPost;
        Worker.Initialize(); //loads the services list
    }
    
    ///
    // This method determines what needs to be done, i.e
    //  post, unpost, visible, not visible, and then performs it
    //  and finally updates the ChannelPost__c object
    //  
    public void Execute() {
        String action;
        try {
            //get the list of actions to perform
            action = determineAction();
            //perform the action
            if (action == 'post') {
                setPost();
            }
            else if (action == 'visible') {
                setVisible();
            }
            else if (action == 'post,visible') {
                setPostVisible();
            }
            else if (action == 'update') {
                doUpdate();
            }
            else if (action == 'unpost') {
                removePost();
            }
            else if (action == 'unvisible') {
                removeVisible();
            }
            else if (action == 'unpost,unvisible') {
                removeVisiblePost();
            }
            else if (action == 'invalid') {
                throw new InvalidActionException('Invalid State ' + getActionLogic());
            }
            
            //update the channel post object
            ChannelPost.Is_Processing__c = false;
            ChannelPost.Job_Id__c = null;
            update ChannelPost;
            
            if (action != 'none') {
                //create an activity 
                Worker.AddActivity('Channel Post Update Success', 'Performed the following action: ' + action, 'Completed');
            }
        }
        catch(Exception ex) {
            //create an activity
            Worker.AddActivity('Channel Post Update Failure', 'Failed to perform the action: ' + action + ' \nException Type: ' + ex.getTypeName() + ' \nMessage: ' + ex.getMessage() + ' \n\nStack Trace: ' + ex.getStackTraceString(), 'Active');
        }
        finally {
            Worker.SaveActivityLog();
        }
    }
    
    ///
    // Proxy methods for the worker
    void setPost() {
        //check the post date for today
        // otherwise this is deferred
        if (ChannelPost.Post_Date__c <= Date.today()) {
            Worker.SetPost();
            ChannelPost.Posted_Date__c = Date.today();
        }
    }
    void setVisible() {
        Worker.SetVisible();
        ChannelPost.Visible_Date__c = Date.today();
    }
    void setPostVisible() {
        setPost();
        if (ChannelPost.Posted_Date__c == Date.today()) {
        	setVisible();
        }
    }
    void doUpdate() {
        Worker.DoUpdate();
        ChannelPost.Update__c = false;
        ChannelPost.Updated_Date__c = Date.today();
    }
    void removePost() {
        Worker.RemovePost();
        ChannelPost.Posted_Date__c = null;
    }
    void removeVisible() {
        Worker.RemoveVisible();
        ChannelPost.Visible_Date__c = null;
    }
    void removeVisiblePost() {
        removeVisible();
        removePost();
    }
    
    ///
    // Returns a list of actions that need to be performed:
    //  post, unpost, visible, unvisible
    //  
    String determineAction() {
        //lookup the logic in the map
        return ACTION_MAP.get(getActionLogic());
    }
    
    ///
    // Compiles the Post, Visible, Posted_Date, and Visible_Date values
    //  into a binary word, e.g. 
    //  10000: Post = true, Visible = False, Posted_Date = null, Visible_Date = null
    //
    String getActionLogic() {
        //construct logic value
        String logic = ((ChannelPost.Post__c == false) ? '0' : '1') +
        	((ChannelPost.Visible__c == false) ? '0' : '1') +
            ((ChannelPost.Posted_Date__c == null) ? '0' : '1') +
            ((ChannelPost.Visible_Date__c == null) ? '0' : '1') +
        	((ChannelPost.Update__c == false) ? '0' : '1');
        System.debug(logic);
        return logic;
    }
    
    // Since the controller uses callouts, any DML operations before
    //  the controller is ran will not work without a @future method
    //  and future methods can only be static methods with only primitive
    //  values as parameters, ug...
    @Future(callout=true)
    public static void ExecuteFuture(String channelPostId) {
        Execute(channelPostId);
    }
    
    ///
    // This static method creates dependencies and an instance of the 
    //  controller and then executes it
    // 
    // Well it turns out that batches can't have future methods
    //  but this is a nice convenience method that ensures the 
    //  ChannelPost object has all the required fields so we'll 
    //  keep it
    // 
    public static void Execute(String channelPostId) {
        try {
            //create the dependencies for the controller constructor
            ChannelPost__c channelPost = [
                SELECT Id, 
                Channel__c,
                Post__c,
                Post_Date__c,
                Visible__c,
                Product__c,
                Posted_Date__c,
                Visible_Date__c,
                Update__c,
                Is_Processing__c,
                External_Id__c,
                External_Id_2__c,
                Product__r.Inventory_Id__c,
                Product__r.Condition__c,
                Product__r.MSP__c,
                Product__r.ASK__c,
                Product__r.Retail__c,
                Product__r.Model__r.Name,
                Product__r.Model__r.Case_Size__c,
                Product__r.Model__r.Model_brand__c,
                Product__r.Model__r.Model_Write_Up__c,
                Product__r.Model__r.Reference__c,
                Product__r.Watch_Reference__c,
                Product__r.Location__r.Id,
                Organization__r.Id,
                Organization__r.Organization_Code__c,
                Organization__r.Address_Line_1__c,
                Organization__r.Address_Line_2__c,
                Organization__r.City__c,
                Organization__r.State_or_Province__c,
                Organization__r.Postal_Code__c,
                Organization__r.Country_Code__c,
                Organization__r.Ebay_Payment_Policy_Id__c,
                Organization__r.Ebay_Return_Policy_Id__c,
                Organization__r.Ebay_Fulfillment_Policy_Id__c,
                Organization__r.Ebay_Category_Id__c
                FROM ChannelPost__c 
                WHERE Id = :channelPostId
            ];
            sgEndpoint endpoint = new sgEndpoint(channelPost.Channel__c, channelPost.Organization__r.Organization_Code__c);
            sgClassResolver resolver = new sgClassResolver();
            //create the controller and then execute it
            sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
            controller.Execute();
        }
        catch(Exception ex) {
            if (channelPostId != null) {
                createActivity(channelPostId, 'Failed to start the Posting process', 'Open', ex.getMessage() + '\n\n' + ex.getStackTraceString());
            }
            else {
                throw new InvalidChannelPostIdException('Null Id', ex);
            }
        }
    }
    
    ///
    // Helper method to create a Task record
    // 
    static void createActivity(String whatId, String subject, String status, String description) {
        Task task = new Task(
            ActivityDate = Date.today(),
            Subject = subject,
            Status = status,
            Description = description,
            WhatId = whatId
        );
        insert task;
    }
     
    ///
    // Tests 2 ChannelPost__c objects to see if there was an actual change that makes
    //  it dirty
    public static Boolean IsDirty(ChannelPost__c oldVal, ChannelPost__c newVal) {
        //no old value means this is a new record 
        if (oldVal == null) {
            return true;
        }
        //check the few values that count
        if (newVal.Post__c != oldVal.Post__c) {
            return true;
        }
        if (newVal.Visible__c != oldVal.Visible__c) {
            return true;
        }
        if (newVal.Post_Date__c != oldVal.Post_Date__c) {
            return true;
        }
        if (newVal.Update__c != oldVal.Update__c && newVal.Update__c == true) {
            return true;
        }
        //default no change
        return false;
    }
 
}