public class sgPostingIntegrationFacebook extends sgPostingIntegration {
    public class ServiceNotFoundException extends Exception {}
    public class InvalidServiceResponseException extends Exception {}
    public class MissingPhotographException extends Exception{}
    
    static final String CATEGORY_VAL = 'Apparel & Accessories > Jewelry > Watches';
    static final String CURRENCY_TYPE = 'USD';

    String facebookId;
    
    public override void SetPost() {
        //determine if the product already exists
        //if so done
        //else then add the product
        if (!hasPosting()) {
            
        }
        else {
            AddActivity('SetPost: success', 'Product has already been posted', 'Closed');
        }
     }
    
    public override void SetVisible() {
        //determine if the porduct already exists
        //if not then add the product with the visible marker
        //else update the product with visble marker
        if (!hasPosting()) {
            //add product and make visible
            
        }
        else {
            //update the product visibility
        }
    }
    
    public override void RemovePost() {
        //determine if the product already exists
        //if so remove post
        //if not done
        if (hasPosting()) {
            
        }
        else {
            AddActivity('RemovePost: success', 'Product was never posted', 'Closed');
        }
    }
    
    public override void RemoveVisible() {
        //determine if the product already exists
        //if so remove visible
        //if not done
        if (hasPosting()) {
            
        }
        else {
            AddActivity('RemoveVisible: success', 'Product was never posted', 'Closed');
        }
    }
    
    void lookupProduct() {
        Product2 product = getProduct();
        Map<String, Object> fbProduct = getFacebookProduct(product.Inventory_ID__c);
        facebookId = '';
        if (fbProduct != null) {
            facebookId = (String)fbProduct.get('Id');
        }
    }
    
    Boolean hasPosting() {
        //see if we've already done a lookup
        if (facebookId == null) {
        	lookupProduct();
        }
        //if the facebook id is set to an empty string then 
        // the product doesn't exist in facebook yet
        if (facebookId == '') {
            return false;
        }
        else {
            return true;
        }
    }
    
    Map<String, Object> getFacebookProduct(String retailer_id) { 
        //send the get
        Blob resultBlob = this.ExecuteRequest('Get Product', null);
        if (resultBlob == null) {
            throw new InvalidServiceResponseException('Empty response for service GetProduct');
        }
        //extract the data
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(resultBlob.toString());
        List<Object> data = (List<Object>)result.get('data');
        if (data.size() > 0) {
            Map<String, Object> fbProduct = (Map<String, Object>)data[0];
            return fbProduct;
        }
        return null;
    }
    
    String createFacebookProduct() {
        //lookup the CreateProduct service settings
        Surge_Integration_Settings__c createProduct = Services.get('CreateProduct');
        if (createProduct == null) {
            throw new ServiceNotFoundException('Missing service CreateProduct for ' + this.Name);
        }
        //get the request payload
        Blob payload = Blob.valueOf(createFacebookProductRequest());
        
        //run the POST      
        Blob resultBlob = this.Endpoint.Post(createProduct.Parameters__c, payload, null, null);
        if (resultBlob == null) {
            throw new InvalidServiceResponseException('Empty response for service CreateProduct');
        }
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(resultBlob.toString());
        //{
		//  "id": "1490891240929630"
		//}

        return (String)result.get('id');
    }
    
    String createFacebookProductRequest() {
        Product2 product = getProduct();
        
        Map<String, Object> fbProduct = new Map<String, Object> {
            'availability' => product.IsActive,
            'brand' => product.Model__r.model_brand__c,
            'category' => CATEGORY_VAL,
            'color' => product.Model__r.Case_Color__c,
            'condition' => product.Condition__c,
            'currency' => CURRENCY_TYPE,
            'description' => product.Model__r.Description__c,
            'gender' => product.Model__r.Gender__c,
            'image_url' => product.AWS_Web_Images__c,
            'manufacturer_part_number' => product.Model__r.Reference__c,
            'material' => product.Model__r.Case_Material__c,
            'name' => product.name,
            'price' => product.ASK__c,
            'retailer_id' => product.Inventory_ID__c,
            'sale_price' => product.MSP__c,
            'size' => product.Model__r.Case_Size__c,
            'start_date' => product.WUW_Date_Created__c,
            'url' => getPhotographUrl(product)
        };
            
        return JSON.serialize(fbProduct);
    }
    
    Product2 getProduct() {
        return [
            SELECT Name,
            Condition__c,
            AWS_Web_Images__c,
            ASK__c,
            Inventory_ID__c,
            MSP__c,
            WUW_Date_Created__c,
            DisplayUrl,
            Model__r.model_brand__c,
            Model__r.Case_Color__c,
            Model__r.Description__c,
            Model__r.Gender__c,
            Model__r.Reference__c,
            Model__r.Case_Material__c,
            Model__r.Case_Size__c
            FROM Product2
            WHERE Id = :ChannelPost.Product__c
        ];
    }
    
    String getPhotographUrl(Product2 product) {
        Organization__c org = [SELECT Organization_Code__c FROM Organization__c WHERE Id =:this.Organization LIMIT 1];
        
        List<Photograph__c> photo = [
            SELECT Url__c
            FROM Photograph__c
            WHERE Related_Product__c = :product.Id
              AND Organization__c = :org.Id
        ];
        if (photo.size() == 0) {
            throw new MissingPhotographException('No image for the "' + org.Organization_Code__c + '" Organization');
        }
        return photo[0].Url__c;
    }
}