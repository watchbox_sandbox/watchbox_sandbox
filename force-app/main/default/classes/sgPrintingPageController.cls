public class sgPrintingPageController {
    public class InvalidRequestException extends Exception{}
    
    Map<String,String> parameters;
    
    public String Title{get;set;}
    public String Id{get;set;}
    public String TemplateType{get;set;}
    public String TemplateName{get;set;}
    public String Organization{get;set;}
    
    public sgPrintingPageController() {
        PageReference page = ApexPages.currentPage();
        parameters = page.getParameters();
        try {
            Title = parameters.get('title');
            if (Title == null) {
                Title = 'Generate Document';
            }
            Id = parameters.get('id');
            TemplateName = parameters.get('templateName');
            TemplateType = parameters.get('templateType');
            Organization = [SELECT Organization__c FROM Team_Member__c WHERE SF_User_ID__c = :UserInfo.getUserId() LIMIT 1].Organization__c;
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The page failed to load:\n' + ex.getMessage()));
        }
    }
    
    public PageReference GenerateDocument() {
        try {
            String attachmentId = sgPrintingController.GenerateDocument(TemplateName, TemplateType, Id, Organization);
            return new PageReference('/servlet/servlet.FileDownload?file=' + attachmentId);
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an exception during document generation:\n' + ex.getMessage()));
        }
        return ApexPages.currentPage();
    }
    
    public PageReference Back() {
        return new PageReference('/' + Id);
    }
    
}