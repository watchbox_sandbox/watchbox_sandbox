@IsTest
public class JSONReferences_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '['+
        '    {'+
        '        \"id\": \"2dedfae1-c83f-456f-8070-317c2e9b1735\",'+
        '        \"creator\": \"MigrationApplication\",'+
        '        \"creationInstant\": 1559971436869,'+
        '        \"modifier\": \"MigrationApplication\",'+
        '        \"modifiedInstant\": 1559971436869,'+
        '        \"baseReferenceNumber\": \"2612\",'+
        '        \"brandId\": \"0420176a-1650-4f2e-ac1a-af8df100717f\",'+
        '        \"brandFamilyId\": \"fe11ea68-ce3c-4cda-a05b-359ccbf73f47\"'+
        '    },'+
        '    {'+
        '        \"id\": \"764ba3b2-71c3-4916-9a43-2f9641426ac6\",'+
        '        \"creator\": \"MigrationApplication\",'+
        '        \"creationInstant\": 1559971415305,'+
        '        \"modifier\": \"MigrationApplication\",'+
        '        \"modifiedInstant\": 1559971415305,'+
        '        \"baseReferenceNumber\": \"2996\",'+
        '        \"brandId\": \"0420176a-1650-4f2e-ac1a-af8df100717f\",'+
        '        \"brandFamilyId\": \"fe11ea68-ce3c-4cda-a05b-359ccbf73f47\"'+
        '    },'+
        '    {'+
        '        \"id\": \"bacbf68f-69b4-4aa0-babc-a552e054eb0c\",'+
        '        \"creator\": \"MigrationApplication\",'+
        '        \"creationInstant\": 1559971388304,'+
        '        \"modifier\": \"MigrationApplication\",'+
        '        \"modifiedInstant\": 1559971388304,'+
        '        \"baseReferenceNumber\": \"3389\",'+
        '        \"brandId\": \"0420176a-1650-4f2e-ac1a-af8df100717f\",'+
        '        \"brandFamilyId\": \"fe11ea68-ce3c-4cda-a05b-359ccbf73f47\"'+
        '    },'+
        '    {'+
        '        \"id\": \"af1079ec-8b5c-467d-9c00-13280ae2e343\",'+
        '        \"creator\": \"MigrationApplication\",'+
        '        \"creationInstant\": 1559971376636,'+
        '        \"modifier\": \"MigrationApplication\",'+
        '        \"modifiedInstant\": 1559971376636,'+
        '        \"baseReferenceNumber\": \"2427\",'+
        '        \"brandId\": \"0420176a-1650-4f2e-ac1a-af8df100717f\",'+
        '        \"brandFamilyId\": \"fe11ea68-ce3c-4cda-a05b-359ccbf73f47\"'+
        '    }'+
        ']';
        List<JSONReferences> r = JSONReferences.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSONReferences objJSONReferences = new JSONReferences(System.JSON.createParser(json));
        System.assert(objJSONReferences != null);
        System.assert(objJSONReferences.id == null);
        System.assert(objJSONReferences.creator == null);
        System.assert(objJSONReferences.creationInstant == null);
        System.assert(objJSONReferences.modifier == null);
        System.assert(objJSONReferences.modifiedInstant == null);
        System.assert(objJSONReferences.baseReferenceNumber == null);
        System.assert(objJSONReferences.brandId == null);
        System.assert(objJSONReferences.brandFamilyId == null);
    }
}