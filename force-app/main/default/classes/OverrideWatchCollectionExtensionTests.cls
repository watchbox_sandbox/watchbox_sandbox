@istest
public class OverrideWatchCollectionExtensionTests {
	static testmethod void toggleModelDisplay_success_test() {
        Watch_Collection__c wc = TestFactory.createWatchCollection(null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wc);
        OverrideWatchCollectionExtension extension = new OverrideWatchCollectionExtension(sController); 
        extension.toggleModelDisplay();
        
        System.assert(extension.ShowModelOverride); 
    }
    
    static testmethod void save_success_test() {
        Watch_Collection__c wc = TestFactory.buildWatchCollection(null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wc);
        OverrideWatchCollectionExtension extension = new OverrideWatchCollectionExtension(sController); 
        PageReference pr = extension.save();
        
        System.assertNotEquals(null, pr, 'Reference should not be null'); 
    }
    
    static testmethod void save_exception_test() {
        Watch_Collection__c wc = TestFactory.buildWatchCollection(null);
        wc.Model_Name__c = null;
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wc);
        OverrideWatchCollectionExtension extension = new OverrideWatchCollectionExtension(sController); 
        PageReference pr = extension.save();
        
        System.assertEquals(null, pr, 'Reference should be null'); 
        System.assert(ApexPages.hasMessages(), 'Validation error message should exist');
    }
    
    static testmethod void saveNew_success_test() {
        Watch_Collection__c wc = TestFactory.buildWatchCollection(null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wc);
        OverrideWatchCollectionExtension extension = new OverrideWatchCollectionExtension(sController); 
        PageReference pr = extension.saveNew();
        
        System.assertNotEquals(null, pr, 'Reference should not be null'); 
    }
}