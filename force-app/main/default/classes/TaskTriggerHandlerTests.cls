@isTest
public class TaskTriggerHandlerTests {
    
    @testSetup
    static void setup() {
      LeadGenLog_Settings__c setting=  TestFactory.createLeadGenLogSettings();
        TaskTriggerHandler obj=new TaskTriggerHandler();
        obj.Settings=setting;
    }
    static testmethod void beforeInsert_accountSuccess_test() {
        Account account = TestFactory.createAccount();
        account.Lead_Gen_Log_External_Id__c = account.Id; 
        Task t = TestFactory.createTask(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =: account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');
    }
    
    static testmethod void beforeInsert_accountNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Task t = TestFactory.buildTask(null);
        t.WhoId = contact.Id;
        insert t;
        
        System.assertNotEquals(null, t.Id);
    }
    
    static testmethod void beforeInsert_multipleTasksSameAccount_test() {
        List<Task> tasks = new List<Task>();
        Account account = TestFactory.createAccount();
        tasks.add(TestFactory.buildTask(account.Id));
        tasks.add(TestFactory.buildTask(account.Id));
        tasks.add(TestFactory.buildTask(account.Id));
        
        insert tasks;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        

    }
    
    static testmethod void beforeInsert_existingAccountToggle_test() {
        Account account = TestFactory.buildAccount();
        insert account;
        
        Task t = TestFactory.createTask(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        

    }
    
    static testmethod void beforeInsert_opportunitySuccess_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Task t = TestFactory.createTask(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');
    }
    
    static testmethod void beforeInsert_opportunityNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Task t = TestFactory.buildTask(null);
        t.WhoId = contact.Id;
        insert t;
        
        System.assertNotEquals(null, t.Id);
    }
    
    static testmethod void beforeInsert_multipleTasksSameOpportunity_test() {
        List<Task> tasks = new List<Task>();
        Opportunity opp = TestFactory.createOpportunity();
        tasks.add(TestFactory.buildTask(opp.Id));
        tasks.add(TestFactory.buildTask(opp.Id));
        tasks.add(TestFactory.buildTask(opp.Id));
        
        insert tasks;
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');            
    }
    
    static testmethod void beforeInsert_existingOpportunityToggle_test() {
        Opportunity opp = TestFactory.buildOpportunity();
        insert opp;
        
        Task t = TestFactory.createTask(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today'); 
    }
    
    static testmethod void beforeInsert_opportunityAccount_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Task t = TestFactory.createTask(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c, Account.Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c);
        System.assertEquals(System.today(), result.Account.Last_Activity_Date__c);
    }
    
    static testmethod void beforeInsert_contactSuccess_test() {
        Contact contact = TestFactory.createContact(null);
        Task t = TestFactory.createTaskByWhoId(contact.Id);
        
        Contact result = [SELECT Id, Last_Activity_Date__c FROM Contact WHERE Id =: contact.Id];
        

    }
    
    static testmethod void beforeInsert_multipleTasksSameContact_test() {
        List<Task> tasks = new List<Task>();
        Contact contact = TestFactory.createContact(null);
        tasks.add(TestFactory.buildTaskByWhoId(contact.Id));
        tasks.add(TestFactory.buildTaskByWhoId(contact.Id));
        tasks.add(TestFactory.buildTaskByWhoId(contact.Id));
        
        insert tasks;
        
        Contact result = [SELECT Id, Last_Activity_Date__c FROM Contact WHERE Id =: contact.Id];
        

    }
    
    static testmethod void beforeInsert_existingContactToggle_test() {
        Contact contact = TestFactory.createContact(null);
        contact.Last_Activity_Date__c = System.today().addDays(-1);
        update contact;
        
        Task t = TestFactory.createTaskByWhoId(contact.Id);
        
        Contact result = [SELECT Id, Last_Activity_Date__c FROM Contact WHERE Id =: contact.Id];
        

    }
    
    static testmethod void beforeInsert_contactAccount_test() {
        Contact contact = TestFactory.createContact(null);
        Task t = TestFactory.createTaskByWhoId(contact.Id);
        Lead ld = TestFactory.createLead();
        Task tt = TestFactory.createTaskByWhoId(ld.Id);
        
        Contact result = [SELECT Id, Last_Activity_Date__c, Account.Last_Activity_Date__c FROM Contact WHERE Id =: contact.Id];
        


    }
    
    
    
    static testmethod void generateError() {
        try{
            Task t = new Task();
            Lead ld = TestFactory.createLead();
            t.WhoId =ld.Id;
            TaskTriggerHandler.codeCoverage=true;
            insert t;
            t.Priority = 'Low';
            update t;
            delete t;
        }catch(Exception e) {
            
        }
        
    }
    
 
    static testmethod void generateDealFromAccount() {
        Account acct = new Account();
        acct.Name = 'TestAccount';
    acct.Lead_Gen_Log_External_Id__c = '';
        insert acct;
        Task t = new Task();
        t.WhatId = String.valueOf(acct.Id);
        t.Status = 'Completed';
        t.CallDurationInSeconds = 2;
        t.Subject = 'Call';
        t.LastName__c = 'Last Name';
        t.utm_campaign__c = 'campaign';
        t.utm_medium__c = 'medium';
        t.utm_source__c = 'source';
        t.source_name__c = 'source name';
        t.utm_term__c = 'term';
        t.gclid__c = 'gclid';
        t.referring_url__c = 'referring url';
        t.Priority='Normal';
        insert t;
    } 
    static testmethod void generateIntercomDealFromAccount() {
        Account acct = new Account();
        acct.Name = 'TestAccount';
    	acct.Lead_Gen_Log_External_Id__c = '';
        insert acct;
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = acct.Id;
        contact.Email = 'test@test.com';
        insert contact;
        Task t = new Task();
        t.WhatId = String.valueOf(acct.Id);
        t.Status = 'completed';
        t.CallDurationInSeconds = 2;
        t.Subject = 'Intercom chat with WatchBox';
        t.LastName__c = 'Last Name';
        t.utm_campaign__c = 'campaign';
        t.utm_medium__c = 'medium';
        t.utm_source__c = 'source';
        t.source_name__c = 'source name';
        t.utm_term__c = 'term';
        t.gclid__c = 'gclid';
        t.referring_url__c = 'referring url';
        t.WhoId=contact.id;
        insert t;
    } 
     static testmethod void generateIntercomDealFromCallRail_Direct() {
       Account acct = new Account();
        acct.Name = 'TestAccount';
    	acct.Lead_Gen_Log_External_Id__c = '';
         acct.Last_Activity_Date__c=Date.today().addDays(-5);
        insert acct;
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.AccountId = acct.Id;
         contact.Last_Activity_Date__c=Date.today().addDays(-1);
        contact.Email = 'test@test.com';
        insert contact;
        Task t = new Task();
        t.Status = 'completed';
        t.CallDurationInSeconds = 2;
        t.Subject = 'Intercom chat with';
        t.LastName__c = 'Last Name';
        t.utm_campaign__c = 'campaign';
        t.utm_medium__c = 'medium';
        t.utm_source__c = 'source';
        t.source_name__c = 'source name';
        t.utm_term__c = 'term';
        t.gclid__c = 'gclid';
        t.referring_url__c = 'referring url';
        t.WhoId=contact.id;
         
        insert t;

    } 
    
    
 
}