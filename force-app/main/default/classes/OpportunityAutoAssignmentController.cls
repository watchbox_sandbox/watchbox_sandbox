public with sharing class OpportunityAutoAssignmentController {
    private static final string USER_ID = 'userid';
    private static final string OPPORTUNITY_ID = 'opportunityid';
        
    public PageReference autoAssign() {
        PageReference pageRef = null;
        
        try {
            string userId = ApexPages.currentPage().getParameters().get(USER_ID);
            string opportunityId = ApexPages.currentPage().getParameters().get(OPPORTUNITY_ID);
            
            if (!string.isBlank(userId) && !string.isBlank(opportunityId)) {
                User user = [SELECT Id FROM User WHERE Id =: userid];
                System.debug('Found user for assignment - ' + user);
                
                Opportunity opp = [SELECT Id, OwnerId FROM Opportunity WHERE Id =: opportunityid];
                System.debug('Found opportunity for assignment - ' + opp);
                
                opp.OwnerId = user.Id;                
                update opp;
                
                pageRef = new PageReference('/' + opp.Id);
                pageRef.setRedirect(true);  
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Incomplete request for auto assignment data, please contact your administrator');
             	ApexPages.addMessage(myMsg); 
            }
        } catch (Exception e) {
             System.debug('Error getting data for auto assignment OpportunityAutoAssignmentController.autoAssign - ' + e.getMessage());
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error retrieving auto assignment data, please contact your administrator');
             ApexPages.addMessage(myMsg); 
        }        
        
        return pageRef;
    }
}