@isTest
public class WXMissingCallDossierScheduledTest {
    @testSetup
    static void setup() {
        WealthXIndexSettings__c wxIndex = new WealthXIndexSettings__c();
        wxIndex.Name = 'WXIndexMissingIdxRange';
        wxIndex.Start_Index__c = '1';
        wxIndex.End_Index__c = '1';
        wxIndex.No_of_Records__c = '1';
        insert wxIndex;
        
        WX_Dummy__c objWX_Dummy = new WX_Dummy__c();
        objWX_Dummy.Id__c = 1;
        insert objWX_Dummy;
    }
    public static testMethod void callBatchWealthXCallDossier(){
        
        system.assertequals(1,[SELECT count() FROM WealthXIndexSettings__c]);
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            WXMissingCallDossierScheduled objWealthXCallDossierScheduled = new WXMissingCallDossierScheduled(); 
            objWealthXCallDossierScheduled.execute(null);
        Test.stopTest();
    }
}