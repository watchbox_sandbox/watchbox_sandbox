@isTest
public class sgTestBatchScanHistoryController {
    static List<Product2> setup() {
        List<Product2> products = sgDataSeeding.ProductsWithBrands();
        string l = sgAuth.user.Location__c;
        
        for (Product2 p : products) {
            p.Location__c = l;
        }
        
        insert products;
        
        return products;
    }
    
    static testMethod void get() {
        setup();
        
        Batch_Scan__c postBatch = sgBatchScanController.post();
        sgBatchScanController.patch(postBatch.Id);
		
        RestRequest req = new RestRequest();
        req.addParameter('id', postBatch.id);
        RestContext.request = req;
        
        postBatch.Scan_Completed__c = system.now();
        update postBatch;
        
        Batch_Scan__c batch = sgBatchScanHistoryController.get();
        
        system.assert(batch.Scan_Completed__c != null);
    }
}