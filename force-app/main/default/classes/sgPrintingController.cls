public class sgPrintingController {
    public class InvalidTemplateException extends Exception{}
    public class FailedToConvertException extends Exception{}
    
    public static Map<string, PageReference> TemplatePages = new Map<string, PageReference> {
        'invoice' => Page.sgPrintingInvoiceTemplate,
        'intake' => Page.sgPrintingIntakeTemplate,
        'inspection' => Page.sgPrintingInspectionTemplate
    };

    ///
    // Uses the templateName to determine the VF page to use, and creates a PDF passing
    //   the masterId as the id for the VF page. Then the PDF output is saved to 
    //   attachments in the masterId object and finally a url for downloading the 
    //   attachment is returned
    //   
    public static String GenerateDocument(String templateName, String templateType, String masterId, String organization) {
		//create the template
        Blob pdfOutput = createPdfOutput(templateName, templateType, masterId, organization);
        //create a file name
        String fileName = createFileName(templateName, templateType, masterId);
        //save as attachment
        return saveAttachment(fileName, masterId, pdfOutput);
    }
    
    ///
    // Creates a PDF Blob for the template
    // 
    static Blob createPdfOutput(String templateName, String templateType, String masterId, String organization) {
        PageReference pRef = TemplatePages.get(templateName);
        if (pRef == null) {
            throw new InvalidTemplateException('The template name "' + templateName + '" is not valid');
        }
        
        Map<String,String> parameters = pRef.getParameters();
        parameters.put('id', masterId);
        parameters.put('templateType', templateType);
        parameters.put('organization', organization);
        
        Blob pdfOutput;
        try {
            // can't run getContent in a test method
            if (Test.isRunningTest()) {
                return Blob.valueOf('test');
            }
            pdfOutput = pRef.getContentAsPDF();
        }
        catch (Exception ex) {
            throw new FailedToConvertException('Failed to convert template "' + templateName + '" with Id "' + masterId + '"', ex);
        }
        return pdfOutput;
    }
    
    ///
    // Creates a file name using the template name, type and current date
    //
    static String createFileName(String templateName, String templateType, String masterId) {
        //TODO: consider getting the name of the object referenced by master id
        //  and use that for the name
        String name = templateName;
        if (templateType != null && templateType.length() > 0) {
            name += '-' + templateType;
        }
        name += '-' + DateTime.now().format('YYYYMMDD') + '.pdf';
        return name;
    }
    
    ///
    // Saves the PDF Blob to the object's attachments
    // 
    static String saveAttachment(String fileName, String masterId, Blob pdfOutput) {
        Attachment attachment = new Attachment();
        attachment.Body = pdfOutput;
        attachment.Name = fileName;
        attachment.ContentType = 'application/pdf';
        attachment.ParentId = masterId;
        insert attachment;
        return attachment.Id;
    }
    
}