@isTest
public class EventTriggerHandlerTests {
	static testmethod void beforeInsert_accountSuccess_test() {
        Account account = TestFactory.createAccount();
        Event e = TestFactory.createEvent(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =: account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');
    }
    
    static testmethod void beforeInsert_accountNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Event e = TestFactory.buildEvent(null);
        e.WhoId = contact.Id;
        insert e;
        
        System.assertNotEquals(null, e.Id);
    }
    
    static testmethod void beforeInsert_multipleEventsSameAccount_test() {
        List<Event> events = new List<Event>();
        Account account = TestFactory.createAccount();
        events.add(TestFactory.buildEvent(account.Id));
        events.add(TestFactory.buildEvent(account.Id));
        events.add(TestFactory.buildEvent(account.Id));
        
        insert events;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');               
    }
    
    static testmethod void beforeInsert_existingAccountToggle_test() {
        Account account = TestFactory.buildAccount();
        insert account;
        
        Event e = TestFactory.createEvent(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');   
    }
    
    static testmethod void beforeInsert_opportunitySuccess_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Event e = TestFactory.createEvent(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');
    }
    
    static testmethod void beforeInsert_opportunityNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Event e = TestFactory.buildEvent(null);
        e.WhoId = contact.Id;
        insert e;
        
        System.assertNotEquals(null, e.Id);
    }
    
    static testmethod void afterInsert_multipleEventsSameOpportunity_test() {
        List<Event> events = new List<Event>();
        Opportunity opp = TestFactory.createOpportunity();
        events.add(TestFactory.buildEvent(opp.Id));
        events.add(TestFactory.buildEvent(opp.Id));
        events.add(TestFactory.buildEvent(opp.Id));
        
        insert events;
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');               
    }
    
    static testmethod void beforeInsert_existingOpportunityToggle_test() {
        Opportunity opp = TestFactory.buildOpportunity();
        insert opp;
        
        Event e = TestFactory.createEvent(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');   
    }
    
    static testmethod void beforeInsert_opportunityAccount_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Event e = TestFactory.createEvent(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c, Account.Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
                
        System.assertEquals(System.today(), result.Last_Activity_Date__c);
        System.assertEquals(System.today(), result.Account.Last_Activity_Date__c);
    }
}