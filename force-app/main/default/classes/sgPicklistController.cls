@RestResource(urlMapping='/picklist/*')
global class sgPicklistController {
	@HttpGet
    global static sgPicklistDTO get() {
        sgPicklistDTO dto = new sgPicklistDTO();
        dto.selectOptions = new sgPicklistDTO.SelectOptions();
        dto.picklists = new List<sgPicklistDTO.Picklist>();

        dto.selectOptions.speed = sgUtility.getPicklistValues('Picklist__c', 'Shipping_Speed2__c');
        dto.selectOptions.type = sgUtility.getPicklistValues('Picklist__c', 'Picklist_Type__c');

        string type = RestContext.request.params.get('type');
        string priority = RestContext.request.params.get('priority');
        string speed = RestContext.request.params.get('speed');

        List<string> whereClauses = new List<string>();

        if (type != null){
            whereClauses.Add('Picklist_Type__c = :type');
        }

        if (priority != null){
            whereClauses.Add('Priority__c = :priority');
        }

        if (speed != null){
            whereClauses.Add('Shipping_Speed2__c = :speed');
        }

        string baseQuery = 'select ' +
                                'Id,' +
                                'Picklist_Type__c,' +
                                'Shipping_Speed2__c,' +
                                'Destination_Type__c,' +
                                'Priority__c,' +
                                'Deal__c,' +
                                'Watchmaking__c,' +
                                'Site_To_Site__c,' +
                                'Return__c ' +
                            'from Picklist__c';

        boolean isFirst = true;
        string whereClause = '';

        for (string w : whereClauses) {
            string clause = isFirst ? ' where ' : ' and ';
            whereClause = whereClause + clause + w;
            isFirst = false;
        }

        string query = baseQuery + whereClause + ' order by Priority__c Limit 100';
        
        List<Picklist__c> picklists = Database.query(query);

        for(Picklist__c p : picklists) {
            sgPicklistDTO.Picklist picklist = new sgPicklistDTO.Picklist();
            picklist.id = p.Id;
            picklist.type = p.Picklist_Type__c;
            picklist.priority = p.Priority__c;
            picklist.destination = p.Destination_Type__c;
            picklist.shipping  = p.Shipping_Speed2__c;
            picklist.deal = p.Deal__c;
            picklist.watchMaking = p.Watchmaking__c;
            picklist.siteToSite = p.Site_To_Site__c;
            picklist.retrn = p.Return__c;
            picklist.details = new List<sgPicklistDTO.PicklistDetail>();

            List<Picklist_Detail__c> picklistDetails = [
                        select
                            id,
                            Related_Inventory__r.Id,
                            Related_Inventory__r.Cost__c,
                            Related_Inventory__r.Watch_Brand__r.Name,
                            Related_Inventory__r.AWS_Web_Images__c,
                            Related_Inventory__r.Inventory_ID__c,
                            Related_Inventory__r.Watch_Reference__c,
                            Related_Inventory__r.Location__c
                        from Picklist_Detail__c
                        where Related_Picklist__c = :p.Id 
                        and Shipped__c != True];
            if(picklistDetails.size() > 0)
            {
                for(Picklist_Detail__c pd : picklistDetails) {
                    sgPicklistDTO.PicklistDetail detail = new sgPicklistDTO.PicklistDetail();
                    detail.productId = pd.Related_Inventory__r.Id;
                    detail.cost = pd.Related_Inventory__r.Cost__c;
                    detail.imageUrl = sgUtility.getMainImage(pd.Related_Inventory__r.AWS_Web_Images__c);
                    detail.inventoryId = pd.Related_Inventory__r.Inventory_ID__c;
                    detail.reference = pd.Related_Inventory__r.Watch_Reference__c;
                    detail.location = pd.Related_Inventory__r.Location__c;
                    detail.id = pd.id;
                    if (pd.Related_Inventory__r != null && pd.Related_Inventory__r.Watch_Brand__r != null) {
                        detail.brandName = pd.Related_Inventory__r.Watch_Brand__r.Name;
                    }
                    picklist.details.Add(detail);
                }
                dto.pickLists.Add(picklist);
            }
        }
        return dto;
    }
}