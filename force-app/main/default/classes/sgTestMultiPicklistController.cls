@isTest
public class sgTestMultiPicklistController {
    static testMethod void get() {
        RestRequest req = new RestRequest();
        req.addParameter('objectName', 'Landed_Cost__c');
        req.addParameter('fields', 'Expense_Type__c,Cost_Type__c');
        RestContext.request = req;
        
        Map<string, List<string>> picklists = sgMultiPicklistController.get();
        
        List<string> expenseTypes = sgUtility.getPicklistValues('Landed_Cost__c', 'Expense_Type__c');
        expenseTypes.sort();
        
        List<string> costTypes = sgUtility.getPicklistValues('Landed_Cost__c', 'Cost_Type__c');        
        costTypes.sort();
        
        system.assertEquals(expenseTypes, picklists.get('Expense_Type__c'));
        system.assertEquals(costTypes, picklists.get('Cost_Type__c'));
    }
}