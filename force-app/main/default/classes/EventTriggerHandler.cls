public class EventTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        Map<Id, Account> accountUpdateMap = new Map<Id, Account>();
        Map<Id, Opportunity> opportunityUpdateMap = new Map<Id, Opportunity>();        
        Set<Id> objectIds = new Set<Id>();
        
        for (Event e : (List<Event>)Trigger.new) {            
            if (e.WhatId != null)
                objectIds.add(e.WhatId);
        }
        
        if (!objectIds.isEmpty()) {
            Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Last_Activity_Date__c FROM Account WHERE Id IN :objectIds]);                        
            
            if (!accountMap.isEmpty()) {
                for (Event e : (List<Event>)Trigger.new) {
                    if (e.WhatId != null) {
                        Account account = null;
                        //First check in existing list
                        account = accountUpdateMap.get(e.WhatId);
                        
                        //If already in updates
                        if (account == null) {
                            account = accountMap.get(e.WhatId);
                            
                            if (account != null && account.Last_Activity_Date__c != System.today()) {
                                account.Last_Activity_Date__c = System.today(); 
                                accountUpdateMap.put(account.Id, account);
                            }                            	  
                        }
                    }                    
                }
                
                //Update accounts
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
            
            Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Last_Activity_Date__c, Account.Id, Account.Last_Activity_Date__c 
                                                                            FROM Opportunity WHERE Id IN :objectIds]);
            accountUpdateMap = new Map<Id, Account>();
            
            if (!opportunityMap.isEmpty()) {
                for (Event e : (List<Event>)Trigger.new) {
                    if (e.WhatId != null) {
                        Opportunity opp = null;
                        //First check in existing list
                        opp = opportunityUpdateMap.get(e.WhatId);
                        
                        //If already in updates
                        if (opp == null) {
                            opp = opportunityMap.get(e.WhatId);
                            
                            if (opp != null && opp.Last_Activity_Date__c != System.today()) {
                                opp.Last_Activity_Date__c = System.today();                                 
                                opportunityUpdateMap.put(opp.Id, opp);
                                
                                if (opp.Account != null && opp.Account.Last_Activity_Date__c != System.today()) {
                                    Account account = new Account
                                        (
                                            Id=opp.Account.Id, 
                                            Last_Activity_Date__c = System.today()
                                        );
                                    
                                    accountUpdateMap.put(account.Id, account);
                                }
                            }                            	  
                        }
                    }                    
                }
                
                //Update opportunities
                if (!opportunityUpdateMap.isEmpty())
                    update opportunityUpdateMap.values();
                
                //Update accounts for opportunity touches
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
        }  
    }
}