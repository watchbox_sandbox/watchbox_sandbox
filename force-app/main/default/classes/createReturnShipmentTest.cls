@isTest
public class createReturnShipmentTest {
    
    static testMethod void testReturnShipment(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference intakePage = page.vfIntakeconnector;
            
            //create necessary test records
          //  zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
           // insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id, shipping_Paid_By__c = 'WUW', Why_did_we_pay_for_shipping__c = 'Testing');
            insert testReturn;
          //  zkfedex__CustomAddressSource__c cas = new zkfedex__CustomAddressSource__c(zkfedex__ShipmentObjectLookupField__c = 'Return__c');
           // insert cas;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(intakePage);
            intakePage.getParameters().put('id',testReturn.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testReturn);
            createReturnShipmentLabel crs = new createReturnShipmentLabel(controller);
            PageReference result = crs.shipFromReturnRecord();
            system.assertNotEquals(null, result);
            //system.assertEquals(1, [SELECT count() FROM zkfedex__Shipment__c ]);
            
            
        test.stopTest();
    }
    static testMethod void testReturnShipmentNeg(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference intakePage = page.vfIntakeconnector;
            
            //create necessary test records
          //  zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
          //  insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id);
            insert testReturn;
          //  zkfedex__CustomAddressSource__c cas = new zkfedex__CustomAddressSource__c(zkfedex__ShipmentObjectLookupField__c = 'Return__c');
          //  insert cas;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(intakePage);
            intakePage.getParameters().put('id',testReturn.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testReturn);
            createReturnShipmentLabel crs = new createReturnShipmentLabel(controller);
            PageReference result = crs.shipFromReturnRecord();
            system.assertNotEquals(null, result);
         //   system.assertEquals(0, [SELECT count() FROM zkfedex__Shipment__c ]);
        test.stopTest();
    }
    public static testMethod void testReload(){
        test.startTest();
        //create necessary test records
        //    zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
       //     insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id);
            insert testReturn;
        //    zkfedex__CustomAddressSource__c cas = new zkfedex__CustomAddressSource__c(zkfedex__ShipmentObjectLookupField__c = 'Return__c');
        //    insert cas;
        PageReference reloadPage = page.shippingLabelLoadingScreen;
       
        test.setCurrentPage(reloadPage);
            reloadPage.getParameters().put('id', testReturn.id);
            reloadRecord testReloadDelay = new reloadRecord();
            PageReference result = testReloadDelay.returnToRecord();
        test.stopTest();
    }
}