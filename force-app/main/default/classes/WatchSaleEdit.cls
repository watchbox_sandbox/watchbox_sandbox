public class WatchSaleEdit{
    
    /********** Get Watch Sale details based on sale brand or sale model search***********/
    @AuraEnabled
    public static List<sObject> getWatchDetails(String salebrand, String salemodel){
        
        List<sobject> lstWatchSale = new List<sobject>();
        List<Watch_Sale__c> wsList = [select id,Sale_Brand__c,Sale_Model__c,CreatedDate,MSP__c,Inventory_ID__c,
                                      Client_s_Offer__c,Web_price__c,Closing_Potential_Percent__c,
                                      Opportunity__r.Lead_Source__c,Opportunity__r.OwnerId,Opportunity__r.name,
                                      Opportunity__r.Owner.Name ,Opportunity__r.Deal_Status__c,
                                      Opportunity__r.Ip_Country__c
                                      From Watch_Sale__c 
                                      Where Opportunity__r.Lead_Source__c like '%Make an Offer%'
                                      and Sale_Brand__c like '%salebrand%' and Sale_Model__c like '%salemodel%'];
               
        return lstWatchSale;
    }
    @AuraEnabled
    public static Opportunity getOpportunityName(Id watchsaleId){
        system.debug('oppId'+watchsaleId);
        watch_sale__c watchsale= [select id,Opportunity__c from watch_sale__c where id=:watchsaleId];
        string id=watchsale.Opportunity__c;
        Opportunity opp=[select id,name from opportunity where id=:id];
        system.debug('opp'+opp);
        return opp;
    }    
    
    
    @AuraEnabled
    public static Watch_Sale__c getWatchSale(String recordId){
        Watch_Sale__c watchSale=new Watch_Sale__c();
        watchSale = [select id,Sale_Brand__c,Sale_Model__c,CreatedDate,MSP__c,Reference_Number__c,Inventory_ID__c,Opportunity__c,
                     Client_s_Offer__c,Web_price__c,Closing_Potential_Percent__c,Notes__c,Reason_for_Selling_Below_MSP__c, Condition__c,                                               Opportunity__r.Lead_Source__c,Opportunity__r.OwnerId,
                     Opportunity__r.Owner.Name ,Opportunity__r.Deal_Status__c,Closing_Potential__c,Box_Paper__c,Selling_below_MSP__c,
                     Opportunity__r.Ip_Country__c,Reference_Number_Id__c,Brand_Family_Id__c,Brand_Family__c,Opportunity__r.name,No_Offer__c
                     From Watch_Sale__c 
                     WHERE id=:recordId];
        system.debug('watchSale'+watchSale.Brand_Family__c);
        return watchSale;
    }
    
    @AuraEnabled
    public static Map<String, String> getsaleBrandValues(){
        Map<String, String> options = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Sale_Brand__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
   
    
    @AuraEnabled
    public static Map<String, String> getSellingBelowMSP(){
        Map<String, String> options = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Selling_below_MSP__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    @AuraEnabled
    public static Map<String, String> getReasonForSelling(){
        Map<String, String> options = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Reason_for_Selling_Below_MSP__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    @AuraEnabled
    public static Map<String, String> getClosingPotential(){
        Map<String, String> options = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Closing_Potential__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
    @AuraEnabled
    public static Map<String, String> getBoxPaper(){
        Map<String, String> options = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Box_Paper__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    /********************* Updates The watchsale record **********************/
    
    @AuraEnabled
    public static Watch_Sale__c saveWatchSale(Watch_Sale__c objwatchSale){
        system.debug('Watchsale::::'+objwatchSale);
        update  objwatchSale;
        return objwatchSale;
    }
    
      /********************* Insert the watchsale record **********************/
    
    @AuraEnabled
    public static Watch_Sale__c insertWatchSale(Watch_Sale__c objwatchSale){
        system.debug('Watchsale::::'+objwatchSale);
       try
       {
           insert  objwatchSale;
       }
        catch(Exception e){
            system.debug(':::::'+e.getMessage());
            throw new AuraHandledException(e.getMessage());
            
        }
        return objwatchSale;
    }

    
    /********************* Bind Branch Families to Map **********************/
    @AuraEnabled
    public static Map<String, String> getlstBranchFamilies(String salebrand){
        Map<String,String> mapBranchFamilies = new Map<String, String>();
        String strBranchId = getBranchIds(salebrand);
        if(strBranchId != null){            
            mapBranchFamilies = getBranchFamilies(strBranchId);
        }
        system.debug(':::::::::'+mapBranchFamilies );
        return mapBranchFamilies;    
    }
    
    /********************* Bind Referencees to Map **********************/
    @AuraEnabled
    public static Map<String,String> getlstReferences(String strBrandFamily){        
        Map<String,String> mapBranchFamilies = new Map<String, String>();
        mapBranchFamilies = getReferencesIds(strBrandFamily);
        system.debug(':::::::::'+mapBranchFamilies);
        return mapBranchFamilies;
    }
    
    /********************* Get Branch Id Response **********************/
    @AuraEnabled
    public static string getBranchIds(String salebrand) {
        String url = 'http://modelsdb-api.watchbox.io/brands/search';
        String response = '';
        response = getBranchRes(url,salebrand);
        System.debug('@@response '+response );
        if(response !=null || response != ''){
            JSONBranchResult objJSONBranchResult = new JSONBranchResult(System.JSON.createParser(response));
            return objJSONBranchResult.Id;
        }
        return response;
    }
    
    /********************* Get Branch Families Response **********************/
    @AuraEnabled
    public static Map<String, String> getBranchFamilies(String BranchId) {
        Map<String, String> mapBranchFamilies = new Map<String, String>();
        String response = ''; 
        response = getBranchFamiliesRes(BranchId);
        System.debug('@@response '+response );
        if(response != null || response != ''){
            List<JSONBranchfamily> lstBranchfamilies = JSONBranchfamily.parse(response);
            for(JSONBranchfamily branchfamily :lstBranchfamilies){
                mapBranchFamilies.put(branchfamily.Id,branchfamily.name);
            }
        }
        
        return mapBranchFamilies;
    }
    
    /********************* Get References Response **********************/
    
    public static Map<String, String> getReferencesIds(String strBranchfamilyId) {
        Map<String, String> mapReferences = new Map<String, String>();
        String response = '';
        response = getReferencesRes(strBranchfamilyId);
        System.debug('@@response '+response );
        if(response != null || response != ''){
            List<JSONReferences > lstReferences = JSONReferences.parse(response);
            for(JSONReferences ref :lstReferences){
                mapReferences.put(ref.Id,ref.baseReferenceNumber);
            }
        }
        return mapReferences;
    }
    
    /********************* Call References Response **********************/
    public static string getBranchRes(string strURL,String strBranchName){
        String responseBody = '';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse(); 
        req = getRequest('SearchBranchId');               
        req.setBody('{ "searchParams": { "name": ["'+strBranchName+'"] }, "page": { "pageNum": 1, "pageSize": 10 }, "sort": ["-name"] }');
        res = h.send(req); 
        if(res.getStatusCode() == 200){
            responseBody = res.getBody();
        }
        system.debug('@@responseBody '+responseBody );
        return responseBody;
    }
    
    /********************* Call Branch Families Response **********************/
    public static string getBranchFamiliesRes(String strBranchId){        
        String responseBody = '';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse(); 
        req = getRequest('SearchBranchFamily');               
        req.setBody('{ "searchParams": { "brandId": ["'+strBranchId+'"] } }');
        res = h.send(req); 
        if(res.getStatusCode() == 200){
            responseBody = res.getBody();
        }
        system.debug('@@responseBody '+responseBody );
        return responseBody;
    } 
    
    /********************* Call References Response **********************/
    public static string getReferencesRes(string strBranchfamilyId){
        String responseBody = '';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse(); 
        req = getRequest('SearchReferenceId');               
        req.setBody('{ "searchParams": { "brandFamilyId": ["'+strBranchfamilyId+'"] } }');
        res = h.send(req); 
        if(res.getStatusCode() == 200){
            responseBody = res.getBody();
        }
        system.debug('@@responseBody '+responseBody );
        return responseBody;
    }
    
    /********************* Set Rquest Body **********************/
    public static System.HttpRequest getRequest(string strCallApi){
        Bid_Offer_Api_setting__c bidoffer = Bid_Offer_Api_setting__c.getValues(strCallApi);
        
        Blob headerValue = Blob.valueOf(bidoffer.Username__c+':'+bidoffer.Password__c);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        HttpRequest req = new HttpRequest(); 
        req.setMethod('POST');
        req.setEndpoint(bidoffer.End_URL__c);
        req.setTimeout(120000);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Accept',bidoffer.Accept__c);
        req.setHeader('Content-Type',bidoffer.Content_Type__c);        
        return req;
    }   
    public class ResponseWrapper {
        public String message {get; set;}
        public Boolean exc {get; set;}
        public watch_sale__c ws{get;set;}
        public ResponseWrapper(boolean flag,string msg,watch_sale__c watch_sale) {
            exc = flag;
            message = msg;
            ws=watch_sale;
        }
    }
    
}