@RestResource(urlMapping='/PicklistLookup/*')
global class sgPicklistLookupController {
	@HttpGet
    global static List<string> get() {
        sgAuth.check();
        
        List<string> values = sgUtility.getPicklistValues(
            RestContext.request.params.get('objectName'), 
            RestContext.request.params.get('field')
        );
        
        values.sort();
        
        return values;
    }
}