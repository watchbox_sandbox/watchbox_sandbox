@isTest
private class MagentoTestBrandSerializer {
    @testSetup
    static void init() {
        // Brand One is active.
        Brand_object__c brand1 = new Brand_object__c(
            Name = 'Brand One',
            Brand_Description__c = 'Brand One description.',
            URL_Slug__c = 'brand-one',
            WUW_Website_Active__c = true,
            Brand_Hex__c = '#c0ffee'
        );

        insert brand1;

        // Brand Two is inactive.
        Brand_object__c brand2 = new Brand_object__c(
            Name = 'Brand Two',
            Brand_Description__c = 'Brand Two description.',
            URL_Slug__c = 'brand-two',
            WUW_Website_Active__c = false,
            Brand_Hex__c = '#c0ffee'
        );

        insert brand2;
    }

    @isTest
    public static void testBrandToDTO() {
        Brand_object__c brandOne = [
            SELECT
    	        Id,
                Name,
                Brand_Description__c,
                URL_Slug__c,
                WUW_Website_Active__c,
                Brand_Hex__c
            FROM Brand_object__c WHERE Name = 'Brand One'
        ];

        MagentoBrandDTO bdto = MagentoBrandSerializer.brandToDTO(brandOne);

        System.assertEquals(brandOne.Id, bdto.sfId);
        System.assertEquals('Brand One', bdto.name);
        System.assertEquals('Brand One description.', bdto.description);
        System.assertEquals('brand-one', bdto.slug);
        System.assertEquals(true, bdto.active);
        System.assertEquals('#c0ffee', bdto.hex);
    }

    @isTest
    public static void testBrandToDTOWithMissingFields() {
        // TODO: what fields are required?
    }

    @isTest
    public static void testBrandsToDTOJson() {
        List<Brand_object__c> brands = [
            SELECT 
                Id,
                Name,
                Brand_Description__c,
                URL_Slug__c,
                WUW_Website_Active__c,
                Brand_Hex__c 
            FROM Brand_object__c
        ];

        String jsonblob = MagentoBrandSerializer.brandsToDTOJson(brands);

        MagentoBrandDTOs bdtos = (MagentoBrandDTOs)JSON.deserialize(jsonblob, MagentoBrandDTOs.class);

        System.assertEquals('Brand One', bdtos.brands[0].name);
        System.assertEquals('Brand Two', bdtos.brands[1].name);
    }
}