public class sgPostingIntegrationEbayLayoutController {

    Map<String, String> parameters;
    
    public ChannelPost__c ChannelPost{get;set;}
    
    public sgPostingIntegrationEbayLayoutController() {
        PageReference page = ApexPages.currentPage();
        parameters = page.getParameters();
        ChannelPost = [
            SELECT Id,
            Organization__c,
            Product__c,
            Product__r.MSP__c,
            Product__r.ASK__c,
            Product__r.Retail__c,
            Product__r.Savings__c,
            Product__r.Model__r.model_brand__r.Name,
            Product__r.Model__r.Name,
            Product__r.Model__r.Reference__c,
            Product__r.Model__r.Also_Known_As__c,
            Product__r.Model__r.Complications__c,
            Product__r.Model__r.Movement_Type__c,
            Product__r.Model__r.Case_Size__c,
            Product__r.Model__r.Case_Material__c,
            Product__r.Model__r.Caseback__c,
            Product__r.Model__r.Bezel__c,
            Product__r.Boxes__c,
            Product__r.Papers__c,
            Product__r.Model__r.Gender__c,
            Product__r.Watch_Age__c,
            Product__r.Condition__c
            FROM ChannelPost__c
            WHERE Id = :parameters.get('id')
        ];
    }
    
    public String getImageUrl() {
        List<Photograph__c> photos = [SELECT Url__c FROM Photograph__c WHERE Related_Product__c = :ChannelPost.Product__c AND Organization__c = :ChannelPost.Organization__c];
        if (photos.size() == 0) {
            return '';
        }
        return photos[0].Url__c;
    }
}