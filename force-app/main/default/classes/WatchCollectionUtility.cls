public class WatchCollectionUtility {
    public static void removeWatchCollectionByWatchSales(Set<Id> watchSaleIds) {
        try {
            List<Watch_Collection__c> collections = [SELECT Id FROM Watch_Collection__c WHERE Watch_Sale_ID__c IN :watchSaleIds];
            
            if (!collections.isEmpty()) {
                delete collections;
            }
        } catch (Exception e) {
            System.debug('Error removing associated watch collection records - ' + e.getMessage());
            throw new ApplicationException('Error removing associated watch collection records');
        }
    }
    
    public static void createWatchCollectionByOpportunities(Map<Id, Opportunity> opportunityMap) {
        List<Watch_Collection__c> watchCollection = new List<Watch_Collection__c>();
        List<Watch_Sale__c> watchSalesToUpdate = new List<Watch_Sale__c>();
		SavePoint sp = Database.setSavepoint();
        
        try {
            List<Watch_Sale__c> watchSales = (List<Watch_Sale__c>)[SELECT Id, Added_to_Collection__c, Opportunity__c, Inventory_Item__c, Inventory_Item__r.Model__c, Inventory_Item__r.Watch_Brand__c 
                                                                   FROM Watch_Sale__c 
                                                                   WHERE Opportunity__c IN :opportunityMap.keySet()];

            for (Watch_Sale__c ws : watchSales) {
                Opportunity opp = opportunityMap.get(ws.Opportunity__c);
                
                if (! ws.Added_to_Collection__c 
                    && opp != null 
                    && ws.Inventory_Item__c != null 
                    && ws.Inventory_Item__r.Model__c != null 
                    && ws.Inventory_Item__r.Watch_Brand__c != null) {
                        
                    //Build new watch collection record
                    Watch_Collection__c wc = new Watch_Collection__c();
                    wc.Account__c = opp.AccountId;
                    wc.Brand__c = ws.Inventory_Item__r.Watch_Brand__c;
                    wc.Model__c = ws.Inventory_Item__r.Model__c;
                    wc.Purchased_From__c = 'Govberg';
                    wc.Watch_Sale_ID__c = Id.ValueOf(ws.Id);
                    watchCollection.add(wc);
                    
                    //Marked the record as transferred
                    ws.Added_to_Collection__c = true;
                	watchSalesToUpdate.add(ws);
                }
            }
            
            if (!watchCollection.isEmpty()) {
                insert watchCollection;
                update watchSalesToUpdate;
            }
            
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug('Error creating associated watch collection records - ' + e.getMessage());
            throw new ApplicationException('Error creating associated watch collection records');
        }
    }
}