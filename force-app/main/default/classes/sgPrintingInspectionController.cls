public class sgPrintingInspectionController  extends sgPrintingControllerBase {
    
    public class Checkpoint {
        public Integer OrderNum {get;set;}
        public String Title {get;set;}
        public String Status {get;set;}
        public String Findings {get;set;}
        public Boolean Approval {get;set;}
        public Decimal CostToRepair {get;set;}
    }

	Final String DEFAULT_TEMPLATE_TYPE = 'generic';
    
    public Inspection__c Inspection{get;set;}
    public Product2 Product{get;set;}
    public List<Checkpoint> Checkpoints{get;set;}
    
    public sgPrintingInspectionController() {
        super();
        
        if (templateType == null) {
            templateType = DEFAULT_TEMPLATE_TYPE;
        }
        
        Inspection = [
            SELECT Name, 
            CreatedDate, 
            Inspector_Initials__c, 
            Team_Member__r.Name,
            Related_Inventory__c
            FROM Inspection__c 
            WHERE Id = :parameters.get('id')
        ];
        Product = [
            SELECT Name,
            Inventory_Id__c,
            Watch_Brand__c,
            Model__r.Name,
            Watch_Reference__c,
            Serial__c,
            Watch_Age__c,
            Warranty_Type__c
            FROM Product2
            WHERE Id = :Inspection.Related_Inventory__c
        ];
        Checkpoints = getCheckpoints();
    }
        
    public Boolean getIsApproved() {
        return templateType == 'approved';
    }
 
    List<Checkpoint> getCheckpoints() {
        List<Checkpoint> checkpointList = new List<Checkpoint>();
        
        List<Surge_Inspection_Checkpoint_Mapping__c> mappings = [
            SELECT Order__c,
            Title__c,
            Field_Prefix__c
            FROM Surge_Inspection_Checkpoint_Mapping__c
            ORDER BY Order__c
        ];
        
        //get a list of the field names we need from the inspection object
        List<String> fieldNames = new List<String>();
        for(Surge_Inspection_Checkpoint_Mapping__c mapping : mappings) {
            String prefix = mapping.Field_Prefix__c;
            fieldNames.add(prefix + '_Approval__c');
            fieldNames.add(prefix + '_Cost__c');
            fieldNames.add(prefix + '_Status__c');
            fieldNames.add(prefix + '_Value__c');
        }
        //generate and run a query against the inspection object
        String soql = 'SELECT ' + String.join(fieldNames, ',') + ' FROM Inspection__c WHERE Id = \'' + parameters.get('id') + '\' LIMIT 1';
        Map<String, Object> inspectionMap = ((sObject)Database.query(soql)).getPopulatedFieldsAsMap();
        
        //add checkpoints
        for(Surge_Inspection_Checkpoint_Mapping__c mapping : mappings) {
            String prefix = mapping.Field_Prefix__c;
            
            Checkpoint checkpoint = new Checkpoint();
            checkpoint.OrderNum = (Integer)mapping.Order__c;
            checkpoint.Title = mapping.Title__c;
            checkpoint.Status = (String)inspectionMap.get(prefix + '_Status__c');
            checkpoint.Findings = (String)inspectionMap.get(prefix + '_Value__c');
            checkpoint.Approval = (Boolean)inspectionMap.get(prefix + '_Approval__c');
            checkpoint.CostToRepair = (Decimal)inspectionMap.get(prefix + '_Cost__c');
            
            checkpointList.add(checkpoint);
        }

        return checkpointList;
    }
    
}