public class OverrideWatchCollectionExtension {
	private ApexPages.StandardController sController;
    public boolean ShowModelOverride { get; set;}
    public string ModelName {get;set;}
    
     public OverrideWatchCollectionExtension(ApexPages.StandardController sc) {
     	sController=sc;
     }
    
    public void toggleModelDisplay() {
        Watch_Collection__c wc = (Watch_Collection__c)sController.getRecord();
        wc.Model__c = null;
        wc.Model_Name__c = '';
         
        ShowModelOverride = true;      
    }
    
    public PageReference save() {
        PageReference pageRef = null;
        Watch_Collection__c wc = (Watch_Collection__c)sController.getRecord();
        
        try {
            pageRef = sController.save();
            
            //Redirect to associated Account on success
            if (!ApexPages.hasMessages()) {
            	pageRef = new PageReference('/' + wc.Account__c);
            	pageRef.setRedirect(true);
            }
        } catch (Exception e) {
            pageRef = null;
            System.debug('Error saving watch sale OverrideWatchCollectionExtension.save - ' + e.getMessage());
            
            if (!ApexPages.hasMessages()) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            	ApexPages.addMessage(myMsg); 
            }             
        }    
        
        return pageRef;
    }
    
    public PageReference saveNew() {
        PageReference pageRef = ApexPages.currentPage();
        pageRef.setRedirect(true);
        
        save();
                       
        return pageRef;
    }
}