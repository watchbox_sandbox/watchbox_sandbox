@isTest
public class sgTestClassResolver {

    static testmethod void resolveClass_found() {
        //arrange
        //
        sgClassResolver resolver = new sgClassResolver();
        
        //act
        //
        Type result = resolver.resolveClass('sgTestClassResolver.testClass');
        
        //assert
        //
        System.assertEquals('sgTestClassResolver.testClass', result.getName());
    }
    
    static testmethod void executeClass_success() {
        //arrange
        //
        sgClassResolver resolver = new sgClassResolver();
        
        //act
        //
        Object result = resolver.executeClass('sgTestClassResolver.testClass');
        
        //assert
        //
        System.assertEquals('testClass:[]', String.valueOf(result));
    }
    
    static testmethod void executeClass_missing() {
        //arrange
        //
        sgClassResolver resolver = new sgClassResolver();
        
        //act
        //
        Exception resEx;
        try {
            resolver.executeClass('testClass2');
        }
        catch(Exception ex) {
            resEx = ex;
        }
        //assert
        //
        System.assertEquals('sgClassResolver.ClassNotExistsException', resEx.getTypeName());
    }
    
    static testmethod void classExists() {
        //arrange
        //
        sgClassResolver resolver = new sgClassResolver();
        
        //act
        //
        Boolean result1 = resolver.classExists('sgTestClassResolver.testClass');
        Boolean result2 = resolver.classExists('sgTestClassResolver.testClass2');
        
        //assert
        //
        System.assertEquals(true, result1);
        System.assertEquals(false, result2);
    }
    
    //*******************************************************
    // Support 
    public class testClass {}
    
}