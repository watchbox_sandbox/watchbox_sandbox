public class sgServicing {
    public static string CompleteStatus {
        get {
            return 'QC Complete';
        }
    }
    
	public static List<string> Statuses {
        get {
            List<string> allStatuses = new List<string>{
                CompleteStatus
            };
                
            allStatuses.addAll(CreationStatuses);
            
            return allStatuses;
        }
    }
    
    public static List<string> CreationStatuses {
        get {
            return new List<string> {
                'Outsource',
                'Decasing',
                'Waiting For Parts',
                'Polishing',
                'Overhaul',
                'Recasing',
                'Straps/Links',
                'Photography',
                'Quality Control'
            };
        }
    }
    
    public static void statusChange(List<Product2> products, Map<Id, Product2> oldMap) {        
        Set<string> s = new Set<string>();
        s.addAll(CreationStatuses);
        
        List<Servicing__c> servicings = new List<Servicing__c>();
        for (Product2 p : products) {
            string newVal = p.Status__c;
	        string oldVal = oldMap.get(p.Id).Status__c;
	
            //system.debug(newVal + ' ' + oldVal);
            
            //if the status has changed, the new status isn't complete and neither are already in servicing
            if (
                newVal != oldVal && 
                newVal != CompleteStatus && 
                oldVal != CompleteStatus &&
                s.Contains(newVal) &&
                !s.contains(oldVal)
            ) {
            	servicings.add(CreateServicingRecord(p));    
            }
        }
        
        insert servicings;
    }
    
    private static Servicing__c CreateServicingRecord(Product2 p) {
        Servicing__c s = new Servicing__c(
            Product__c = p.Id
        );
        
        return s;
    }
}