public class WatchmakingTriggerHandler extends TriggerHandler {	
    
    public override void afterInsert() {
        updateInventory();
    }
    
    public override void afterUpdate() {
        updateInventory();
    }
    
    private static void updateInventory() {
        Id[] productIds = new List<Id>();
        List<Watchmaking__c> matchedWithProducts = new List<Watchmaking__c>();
        
        //get id of any watchmaking record where inventory field is not undefined and add to array
        for(Watchmaking__c wm : (List<Watchmaking__c>)trigger.New) {
            if(wm.Inventory__c != null){
                system.debug('Inventory field: '+wm.Inventory__c);
                productIds.add(wm.Inventory__c);
                matchedWithProducts.add(wm);
            }
         
            setProductFields(productIds, matchedWithProducts);  
        }
    }
    
    public static void setProductFields(List<Id> productIds, List<watchmaking__c> watchmakingRecords){
        //query fields to be updated from product records matching ids captured in the watchMaking Trigger
        List<Product2> updatedInventory = new List<Product2>();
        List<Product2> matchingInventory = [SELECT id, Shipping_Status_Aesthetics__c, 
                                            Shipping_Status_Mechanical__c FROM Product2
                                            WHERE id IN :productIds];
                
        //iterate through watchMaking records and Inventory records looking for linked records, then update
        //the product records with information from the watchMaking records
        system.debug('watchMakingRecords:'+watchMakingRecords[0]);
        for(watchmaking__c wm : watchMakingRecords){
            for(Product2 p2 : matchingInventory){
                if(wm.Inventory__c == p2.id){
                    p2.Shipping_Status_Mechanical__c = wm.Mechanical__c;
                    p2.Shipping_Status_Aesthetics__c = wm.Aesthetics__c;
                    updatedInventory.add(p2);
                }
            }
        }
        
        update updatedInventory;
    }
}