@isTest
public class sgTestPrintingController {

    static testMethod void generateDocument() {
    	//arrange
    	//
    	Opportunity opp = new Opportunity(Name = 'Name', StageName = 'Closed Lost', Loss_Reason__c = 'Other', CloseDate = Date.today());
        insert opp;
        
        //act
        //
        Test.startTest();
        String attachmentId = sgPrintingController.GenerateDocument('invoice', 'type', opp.Id, 'Govberg');
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('00P', attachmentId.substring(0, 3));
    }
    
}