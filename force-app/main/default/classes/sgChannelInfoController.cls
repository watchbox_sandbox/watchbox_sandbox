@RestResource(urlMapping='/ChannelInfo/*')
global class sgChannelInfoController {
    @HttpGet
    global static List<sgChannelInfoDTO> get() {
        //run the custom auth check
        sgAuth.check();
        //create the list of channel info
        List<sgChannelInfoDTO> channels = new List<sgChannelInfoDTO>();
        //get the global picklist for channels
        List<String> channelNames = sgUtility.getPicklistValues('ChannelPost__c', 'Channel__c');
        //get the custom setting for the url
        sg_resources__c res = sg_resources__c.getValues('Web Resources');
        
        //loop through the channel picklist
        //get the name and then generate the image url
        //then create a sgChannelInfoDTO object
        for(String name : channelNames) {
            if (name != 'Test') {
                sgChannelInfoDTO chInfo = new sgChannelInfoDTO();
                chInfo.Name = name;
                chInfo.ImageUrl = res.Url__c + '/images/channel-logos/' + name.toLowerCase().replaceAll('\\s|[_-]', '') + '-logo.png';
                chInfo.Visible = false;
                chInfo.Post = false;
                channels.add(chInfo);
            }
        }
        //return the list
        return channels;
    }
}