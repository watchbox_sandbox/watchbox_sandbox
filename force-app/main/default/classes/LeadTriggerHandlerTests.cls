@isTest
public class LeadTriggerHandlerTests {
    @testSetup
    static void setup() {
        TestFactory.createLeadGenLogSettings();
    }
    
    static testmethod void processIncomingLeads_foundEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
    }
    
    static testmethod void processIncomingLeads_foundSecondaryEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'wrong@unittest.com';
        account.Secondary_Email__c = 'test@unittest.com';
        insert account;
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
 //     System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundNoPrimarySecondaryEmailMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Secondary_Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundPhoneMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Phone = '1234567890'; 
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
   //     System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundMobileMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Mobile__c = '1234567890';         
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
   //     System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_foundNoPhoneMobileMatch_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = null;
        account.Phone = '5555555555';
        account.Mobile__c = '1234567890';  
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
      //  System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_noMatches_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';        
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
    //    System.assertEquals(null, lgl.Matched_Account__c);
    }
    
    static testmethod void processIncomingLeads_createAccount_test() {       
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
        System.assertEquals(null, lgl.Matched_Account__c);
        
        Account account = [SELECT Id, Lead_Gen_Log_External_Id__c FROM Account LIMIT 1];
        System.assertNotEquals(null, account.Lead_Gen_Log_External_Id__c);
    }
    
    static testmethod void processIncomingLeads_matchedEmailAccountOpportunity_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
//      System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
        
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
//      System.assertEquals(account.Id, opp.AccountId);
    }
    
    static testmethod void processIncomingLeads_matchedPhoneAccountOpportunity_test() {
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Phone = '1234567890'; 
        insert account;
        
        TestFactory.createLead();
        Lead lgl = [SELECT ID, Matched_Account__c FROM Lead LIMIT 1];
        System.assertEquals(string.ValueOf(account.Id), lgl.Matched_Account__c);
        
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
        System.assertEquals(account.Id, opp.AccountId);
    }
    
    static testmethod void processingIncomingLeads_multiple_test() {
        List<Lead> leads = new List<Lead>();
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        
        Account a = TestFactory.buildAccount();
        a.First_Name__c = 'Test';
        a.Last_Name__c = 'User';
        a.Phone = '1234567890'; 
        insert a;
        
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';
        leads.add(lgl);
        
        Lead lead = new Lead();
        lead.Email = 'test1234@unittest.com';
        lead.FirstName = 'Test1234';
        lead.LastName = 'User1234';
        lead.Phone = '123-456-7890';
        lead.Company = 'WatchBox';
        lead.Images__c = 'https://www.govbergwatches.com/wp-content/uploads/wws_uploads/11e3ff37378fa6904dafd6bfef8733be.jpg,https://www.govbergwatches.com/wp-content/uploads/wws_uploads/11e3ff37378fa6904dafd6bfef8733be.jpg';
        leads.add(lead);
        
        insert leads;
        
        List<Opportunity> opps = [SELECT Id FROM Opportunity];
        List<Account> accounts = [SELECT Id FROM Account];
        
   //   System.assertEquals(2, opps.size());
   //   System.assertEquals(2, accounts.size(), 'No additional accounts should be created');
    }
    
    static testmethod void getDealScheduleOwnerId_noResults_test() {
        Account account = TestFactory.createAccount();
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c ds = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(null, ds);
    }
    
    static testmethod void getDealScheduleOwnerId_future_test() {
        Account account = TestFactory.createAccount();
        Deal_Schedule__c ds = TestFactory.buildDealSchedule(account, null);
        ds.Date__c = DateTime.now().addDays(1);
        insert ds;
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(null, result, 'Future date should not be returned');
    }
    
    static testmethod void getDealScheduleOwnerId_multiple_test() {
        Account account = TestFactory.createAccount();
        User user = TestFactory.createSalesUser();
        Deal_Schedule__c ds = TestFactory.buildDealSchedule(account, user);
        ds.Date__c = DateTime.now().addDays(1);
        insert ds;
        
        Deal_Schedule__c ds2 = TestFactory.buildDealSchedule(account, user);
        ds2.Date__c = DateTime.now();
        insert ds2;
        
        Deal_Schedule__c ds3 = TestFactory.buildDealSchedule(account, user);
        ds3.Date__c = DateTime.now().addDays(-1);
        insert ds3;
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(ds2.Id, result.Id, 'Incorrect schedule returned');
    }
    
    static testmethod void getDealScheduleOwnerId_success_test() {
        Account account = TestFactory.createAccount();       
        Deal_Schedule__c ds = TestFactory.createDealSchedule(account, null);
        
        LeadGenLogTriggerHandler lth = new LeadGenLogTriggerHandler();
        Deal_Schedule__c result = lth.getDealScheduleOwnerId(account.Id);
        
        System.assertEquals(ds.Id, result.Id, 'Incorrect schedule returned');
    }
    
    /*static testmethod void processingIncomingLeads_ardmoreLeadSource_test() {
        LeadGenLog_Settings__c settings = TestFactory.getLeadSettings();
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';
        lgl.Lead_Source__c = 'Ardmore Retail Checkin';
        insert lgl;
        
        Opportunity opp = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        //  System.assertEquals((Id)settings.Lead_Source_Ardmore_User_Id__c,'00550000005vbPXAAY');
    } */
    
    static testmethod void processingIncomingLeads_walnutLeadSource_test() {
        LeadGenLog_Settings__c settings = TestFactory.getLeadSettings();
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';       
        lgl.Lead_Source__c = 'Walnut Retail Checkin';
        insert lgl;
        
        Opportunity opp = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
        //    System.assertEquals((Id)settings.Lead_Source_Walnut_User_Id__c, '00550000005vbPXAAY');
    }
    
    
    static testmethod void processingIncomingLeads_watchBoxApp_testBoxPaper() {
        
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';   
        lgl.Boxes_and_Papers__c = '11';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
    //    System.assertEquals(opp.Boxes_and_Papers__c,'Box & Papers');  
        
    }
    static testmethod void processingIncomingLeads_watchBoxApp_testBox() {
        
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='10';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        //System.assertEquals(opp.Boxes_and_Papers__c,'Box only');  
        
    }
    static testmethod void processingIncomingLeads_watchBoxApp_testPaper() {
        
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';   
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='01';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        //System.assertEquals(opp.Boxes_and_Papers__c,'Papers Only');  
        
    }
    
    static testmethod void processingIncomingLeads_watchBoxApp_Neither() {
        Lead lgl = new Lead();
        lgl.Email = 'test@unittest.com';
        lgl.FirstName = 'Test';
        lgl.LastName = 'User';
        lgl.Phone = '123-456-7890';
        lgl.Company = 'WatchBox';           
        lgl.Lead_Source__c = 'Watchbox App - Sell Your Watch';
        lgl.Boxes_and_Papers__c='00';
        lgl.Landing_Page__c = 'https://www.thewatchbox.com/?clickid=2S52rL2%3Azwq-TF5xKcT5s3mgUkgzTcT8Ny9OzU0&irgwc=1&ircid=8206&irtrackid=14518&irpid=1294415';
        insert lgl;
        Opportunity opp = [SELECT Id, Boxes_and_Papers__c FROM Opportunity LIMIT 1];
        //System.assertEquals(opp.Boxes_and_Papers__c,'Neither');  
    }
    
    static testmethod void afterInsert_phoneFormatted_test() {                        
        TestFactory.createLead();
        Lead lgl = [SELECT Id, Phone FROM Lead LIMIT 1];
      //  System.assertEquals('1234567890', lgl.Phone);    
    }
    
    static testmethod void LeadGenLogDecoratorTest(){
        LeadGenLogDecorator leadD = new LeadGenLogDecorator();
        leadD.Lead = TestFactory.createLeadGenLog();
        leadD.Account = TestFactory.buildAccount();
        leadD.Opportunity = TestFactory.buildOpportunity();
		leadD.LeadRecord = TestFactory.createLead();            
            
    } 
    Static testmethod void TestException() {
        try{
            Lead leadTest = new Lead();
            leadTest.FirstName ='Test';
            leadTest.LastName = 'TestDemo';
            leadTest.Company = 'DemoCompany';
            insert leadTest;
        } catch(Exception e){
        }
    }
}