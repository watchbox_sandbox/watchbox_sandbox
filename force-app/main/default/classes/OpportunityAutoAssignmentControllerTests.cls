@isTest
public class OpportunityAutoAssignmentControllerTests {
    static testmethod void autoAssign_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        User user = TestFactory.createSalesUser();
        
        PageReference pageRef = Page.OpportunityAutoAssignment; 
        pageRef.getParameters().put('userid', String.valueOf(user.Id));
        pageRef.getParameters().put('opportunityid', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        
        OpportunityAutoAssignmentController controller = new OpportunityAutoAssignmentController();
        PageReference destRef = controller.autoAssign();
        
        System.assertNotEquals(null, destRef, 'Page reference should be set to opportunity');
        
        Opportunity result = [SELECT OwnerId FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(user.Id, result.OwnerId, 'Opportunity not assigned');
    }
    
    static testmethod void autoAssign_noUser_test() {
        PageReference pageRef = Page.OpportunityAutoAssignment; 
        pageRef.getParameters().put('userid', '');
        pageRef.getParameters().put('opportunityid', '123');
        
        OpportunityAutoAssignmentController controller = new OpportunityAutoAssignmentController();
        PageReference destRef = controller.autoAssign();
        
        System.assert(ApexPages.hasMessages(), 'Page should have an error message');
    }
    
    static testmethod void autoAssign_noOpportunity_test() {
        PageReference pageRef = Page.OpportunityAutoAssignment; 
        pageRef.getParameters().put('userid', '1234');
        pageRef.getParameters().put('opportunityid', '');
        
        OpportunityAutoAssignmentController controller = new OpportunityAutoAssignmentController();
        PageReference destRef = controller.autoAssign();
        
        System.assert(ApexPages.hasMessages(), 'Page should have an error message');
    }
    
    static testmethod void autoAssign_exception_test() {
        PageReference pageRef = Page.OpportunityAutoAssignment; 
        pageRef.getParameters().put('userid', '1234');
        pageRef.getParameters().put('opportunityid', '1234');
        
        OpportunityAutoAssignmentController controller = new OpportunityAutoAssignmentController();
        PageReference destRef = controller.autoAssign();
        
        System.assert(ApexPages.hasMessages(), 'Page should have an error message');
    }
}