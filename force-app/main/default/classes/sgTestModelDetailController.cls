@isTest
public class sgTestModelDetailController {
    static testMethod void get() {
        Model__c p = new Model__c(
        	Name = 'Test Model',
            Verified__c = true
        );
        
        insert p;
        
        Product2 p2 = new Product2(
            Name = 'Test Product',
        	Model__c = p.Id,
            Retail__c = 1,
            Intake_Date__c = Date.today(),
            IsActive = true
        );
        insert p2;
        
        RestRequest r = new RestRequest();
        r.params.put('query', p.Name);
        RestContext.request = r;
        
        List<sgModelDetail> model = sgModelDetailController.get();
        system.debug(model);
        system.assertEquals(p.Id, model.get(0).Fields.get('Id'));
    }
}