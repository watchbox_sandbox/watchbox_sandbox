public class IntakeTriggerHandler extends TriggerHandler {
    Map<Id, Watch_Purchase__c> WatchPurchaseMap {get;set;}
    Map<Id, Brand_object__c> BrandMap {get;set;}
    Map<Id, Model__c> ModelMap {get;set;}
    
    public override void afterInsert() {
        generateInventory();
    }
    
    public override void beforeUpdate() {
        generateInventory();
    }
    
    private void generateInventory() {
        System.debug('Entering IntakeTriggerHandler.generateInventory()');
        List<Product2> productsToInsert = new List<Product2>();
        List<Intake__c> intakes = (List<Intake__c>)Trigger.new;
        
        Set<Id> wpSet = new Set<Id>();
        Set<Id> modelSet = new Set<Id>();
        Set<Id> brandSet = new Set<Id>();                 
        
        //Build Watch Purchases to fetch
        for (Intake__c intake : intakes) {
            if (intake.Assign_Inventory__c && intake.Product__c == null) {
                if (intake.Watch_Purchase__c != null) 
                    wpSet.add(intake.Watch_Purchase__c);                
                
                if (intake.Watch_Model__c != null) 
                    modelSet.add(intake.Watch_Model__c);                
                
                if (intake.Watch_Brand__c != null) 
                    brandSet.add(intake.Watch_Brand__c);            
            }
        }                    
        
        //Fetch associated watch purchase data
        WatchPurchaseMap = new Map<Id, Watch_Purchase__c>([SELECT Id, Asking__c, Cost__c, MSP__c, Retail__c, 
                                                           Origination_Opportunity__c, Origination_Opportunity__r.Account.Name
                                                           FROM Watch_Purchase__c 
                                                           WHERE Id IN :wpSet]);    
        
        //Fetch needed Brand data
        BrandMap = new Map<Id, Brand_object__c>([SELECT Id, Name FROM Brand_object__c WHERE Id IN :brandSet]);
        
        //Fetch needed Model data
        ModelMap = new Map<Id, Model__c>([SELECT Id, Name, Reference__c, Case_Size__c, Case_Material__c, Dial_Color__c, Bracelet_Material__c, Movement_Type__c 
                                          FROM Model__c 
                                          WHERE Id IN :modelSet]);
        
        for (Intake__c intake : intakes) {
            //Check for flag and required data
            if (intake.Assign_Inventory__c && intake.Product__c == null) {                                
                //Build product
                Product2 product = buildInventory(intake);
                
                if (product != null)
                	productsToInsert.add(product);
            }
        }
                         
        if (!productsToInsert.isEmpty()) {
            insert productsToInsert;
            
            //Update Intakes with newly created products
            
            Map<Id, Intake__c> intakeMap;
            List<Intake__c> intakesToUpdate = new List<Intake__c>();
            
            if (Trigger.isUpdate) {
                intakeMap = new Map<Id, Intake__c>();
            	intakeMap.putAll(intakes);
            } else {                                
                intakeMap = new Map<Id, Intake__c>([SELECT Id, Product__c FROM Intake__c WHERE Id IN :Trigger.newMap.keySet()]);
            }
                
            for (Product2 product : productsToInsert) {
                System.debug('Get Intake from map - ' + product.Intake_Id__c);
                Intake__c intake = intakeMap.get(product.Intake_Id__c);
                System.debug('Assigning product to intake - '+ product.Id);
                intake.Product__c = product.Id;
                intakesToUpdate.add(intake);
            }
            
            if (!intakesToUpdate.isEmpty() && !Trigger.isUpdate)
                update intakesToUpdate;
        }                 	
    }
    
    private Product2 buildInventory(Intake__c intake) {
        System.debug('Entering IntakeTriggerHandler.buildInventory()');
        Watch_Purchase__c wp = WatchPurchaseMap.get(intake.Watch_Purchase__c);
        Brand_object__c brand = BrandMap.get(intake.Watch_Brand__c);
        Model__c model = ModelMap.get(intake.Watch_Model__c);
        
        if (wp == null) {
            System.debug('Watch Purchase not set on intake, product/inventory generation aborted - ' + intake.Id);
            return null;
        }
        
        if (model == null) {
            System.debug('Model not set on intake, product/inventory generation aborted - ' + intake.Id);
            return null;
        }
        
        if (brand == null) {
            System.debug('Brand not set on intake, product/inventory generation aborted - ' + intake.Id);
            return null;
        }
        
        Product2 product = new Product2();
        product.Intake_Id__c = String.valueOf(intake.Id);
        product.ASK__c = wp.Asking__c;               	
        product.Condition__c = 'Pre-Owned';
        product.Cost__c = wp.Cost__c;        
        product.Description = intake.Description__c;
        product.MSP__c = wp.MSP__c;
        product.Origination_Type__c = 'Direct Purchase';
        product.Product_Type__c = 'Pre-Owned';        
        product.Retail__c = wp.Retail__c;
        product.Serial__c = intake.Serial__c;
        product.Status__c = 'Available';
        product.Watch_Brand__c = intake.Watch_Brand__c;
        product.Model__c = intake.Watch_Model__c;        
        
        if (wp.Origination_Opportunity__c != null) {
            product.Deal__c = wp.Origination_Opportunity__c;
            product.Purchased_From__c = wp.Origination_Opportunity__r.Account.Name;
        }        
        
        //Build Name
        string name = brand.Name;
        name += ' ' + model.Name;
                
        if (!string.isBlank(model.Reference__c)) 
            name += ' Ref. ' + model.Reference__c;        
        
        if (!string.isBlank(intake.Watch_Serial__c)) 
            name += ' # ' + intake.Watch_Serial__c;                   
        
        if (model.Case_Size__c != null) 
            name += ' ' + model.Case_Size__c;        
        
        if (model.Case_Material__c != null) 
            name += ' ' + model.Case_Material__c;        
        
        if (model.Dial_Color__c != null) 
            name += ' ' + model.Dial_Color__c;        
        
        if (model.Bracelet_Material__c != null) 
            name += ' - ' + model.Bracelet_Material__c;        
        
        if (model.Movement_Type__c != null) 
            name += ' ' + model.Movement_Type__c;                
        
        //Assign name
        product.Name = name;
        
        if (intake.Boxes__c) 
            product.Boxes__c = 'Yes';
                
        if (intake.Time_Completed__c != null) 
            product.Intake_Date__c = intake.Time_Completed__c.date();
                
        if (intake.Papers__c) 
            product.Papers__c = 'Yes';        
        
        return product;
    }
}