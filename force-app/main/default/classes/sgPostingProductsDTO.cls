global class sgPostingProductsDTO {
    public String Id {get;set;}
    public String InventoryId {get;set;}
    public String Location {get;set;}
    public String Organization {get;set;}
    public List<sgChannelInfoDTO> Channels{get;set;}
}