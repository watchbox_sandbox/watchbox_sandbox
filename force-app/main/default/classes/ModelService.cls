global class ModelService {
    webservice static void setModelAsVerified(Id modelId) {
        try {
            //Retrieve model for update
            Model__c model = [SELECT Id, Verified__c FROM Model__c WHERE Id =: modelId];            
            
            //Mark as verified
            model.Verified__c = true;
            update model;
        } catch (Exception e) {
            System.debug('Error while marking model as verified Id - ' + modelId + ' - Message - ' + e.getMessage());
        }
    }   
    
    webservice static void setModelEbl(Id modelId) {
        try {
            //Retrieve model for update
            Model__c model = [SELECT Id, EBL__c FROM Model__c WHERE Id =: modelId];            
            
            //Mark EBL
            model.EBL__c = true;
            update model;
        } catch (Exception e) {
            System.debug('Error while marking model EBL Id - ' + modelId + ' - Message - ' + e.getMessage());
        }
    }   
}