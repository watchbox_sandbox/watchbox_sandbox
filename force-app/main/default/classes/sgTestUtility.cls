@isTest
public class sgTestUtility {
    
    ///
    // To test the getPicklistValues method we need to
    // choose an arbitrary object to lookup
    // I (Rodger) decided to use the one I know we control
    // the Channel global picklist used on the ChannelPost__c object
    static testMethod void getPicklistValues() {
        // Arrange
        // 
        String objectName = 'ChannelPost__c';
        String fieldName = 'Channel__c';
        Integer expectedCount = 13; //this will need to be updated if values are added or removed from the picklist
        
        // Act
        // 
        List<String> values = sgUtility.getPicklistValues(objectName, fieldName);
        
        // Assert
        // 
        System.assertEquals(expectedCount, values.size());
    }
    
    static testMethod void ApplyData() {
        // Arrange
        // 
        String template = 'This is a {!test} of the {!apply} {!value} with an {!account.Name}';
        Map<String, Object> data = new Map<String, Object> 
        { 
            'test' => 'test', 
            'apply' => 'ApplyData', 
            'value' => 'utility method',
            'account' => new Account(Name = 'Account')
        };
        
        // Act
        // 
        String result1 = sgUtility.ApplyData(template, data);
        String result2 = sgUtility.ApplyData('{!Name}', new Account(Name = 'Account'));
        
        // Assert
        // 
        System.assertEquals('This is a test of the ApplyData utility method with an Account', result1);
        System.assertEquals('Account', result2);
    }
    
    @isTest(SeeAllData=true)
    static void GetType() {
        //arrange
        //
        String strObj = 'test';
        Integer intObj = 10;
        Double dblObj = 10.0;
        Decimal decObj = 1.0;
        Date dateObj = Date.today();
        Time timeObj = Time.newInstance(1, 1, 1, 1);
        DateTime dateTimeObj = DateTime.now();
        Blob blobObj = Blob.valueOf(strObj);
        Boolean boolObj = false;
        Id idObj = [SELECT Id FROM Account LIMIT 1].Id;
        Long longObj = 10000;
        List<String> listObj = new List<String>();
        Map<String,String> mapObj = new Map<String,String>();
        sObject obj = [SELECT Id FROM Account LIMIT 1];

        //act
        //assert
        System.assertEquals('string', sgUtility.GetType(strObj));
        System.assertEquals('integer', sgUtility.GetType(intObj));
        System.assertEquals('decimal', sgUtility.GetType(dblObj)); //double and decimal are instances of each other, decimal appears first so that's what is chosen
        System.assertEquals('decimal', sgUtility.GetType(decObj));
        System.assertEquals('date', sgUtility.GetType(dateObj));
        System.assertEquals('time', sgUtility.GetType(timeObj));
        System.assertEquals('datetime', sgUtility.GetType(dateTimeObj));
        System.assertEquals('blob', sgUtility.GetType(blobObj));
        System.assertEquals('boolean', sgUtility.GetType(boolObj));
        System.assertEquals('id', sgUtility.GetType(idObj));
        System.assertEquals('long', sgUtility.GetType(longObj));
        System.assertEquals('list', sgUtility.GetType(listObj));
        System.assertEquals('map', sgUtility.GetType(mapObj));
        System.assertEquals('sobject', sgUtility.GetType(obj));
    }
    
    static testMethod void Lookup() {
        //arrange
        //
        String key1 = 'test.account.Name';
        String key2 = 'test.opportunity.Name';
        String key3 = 'test.account.Name.invalid';
        Map<String,Object> testMap = new Map<String, Object>{
            'test' => new Map<String,sObject>{
                'account' => new Account(Name='Test')
            } 
        };
        
        //act
        //
        String result1 = (String)sgUtility.Lookup(key1, testMap);
        String result2 = (String)sgUtility.Lookup(key2, testMap);
        String result3 = (String)sgUtility.Lookup(key3, testMap);
        
        //assert
        //
        System.assertEquals('Test', result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
    }
    
    static testMethod void QueryStringtoMapWithValues() {
        //arrange
        //
        String queryString = 'name1=value1&novalue&name2=value2';
        
        //act
        //
        Map<String,String> result = sgUtility.QueryStringtoMap(queryString);
        
        //assert
        //
        System.assertEquals('value1', result.get('name1'));
        System.assertEquals('value2', result.get('name2'));
        System.assertEquals('', result.get('novalue'));
    }

    static testMethod void QueryStringtoMapEmpty() {
        //arrange
        String queryString = '';

        //act
        Map<String,String> result = sgUtility.QueryStringtoMap(queryString);

        //assert
        System.assertEquals(null, result);
    }

    static testMethod void secondsBetween() {
        DateTime startDate = system.now();
        Datetime endDate = startDate.addDays(1);
        
        integer elapsed = sgUtility.secondsBetween(startDate, endDate);
        
        system.assertEquals(86400, elapsed);
        
    }
}