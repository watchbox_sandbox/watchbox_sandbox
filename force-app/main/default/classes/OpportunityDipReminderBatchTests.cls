@isTest
public class OpportunityDipReminderBatchTests {
    static testmethod void execute_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        opp.Deal_Status__c = 'DIP';
        update opp;
        
        Test.startTest();
        OpportunityFieldHistory objectHistory = new OpportunityFieldHistory(Field = 'Deal_Status__c',OpportunityId=opp.id);
        insert objectHistory;
        
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);
        Test.stopTest();
        
        Task result = [SELECT Id, Status FROM Task WHERE WhatId =:opp.Id];
        
        System.assertEquals('Open', result.Status);
    }
    
    static testmethod void execute_multiple_test() {
        Opportunity opp = TestFactory.createOpportunity();
        opp.Deal_Status__c = 'DIP';
        update opp;
        
        Opportunity opp2 = TestFactory.createOpportunity();
        opp2.Deal_Status__c = 'DIP';
        update opp2;
        
        Test.startTest();
        OpportunityFieldHistory objectHistory = new OpportunityFieldHistory(Field = 'Deal_Status__c',OpportunityId=opp.id);
        insert objectHistory;
        
        OpportunityFieldHistory objectHistory2 = new OpportunityFieldHistory(Field = 'Deal_Status__c',OpportunityId=opp2.id);
        insert objectHistory2;
        
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);
        Test.stopTest();
        
        List<Task> results = [SELECT Id, Status FROM Task];
        
        System.assertEquals(2, results.size());
    }
    
    static testmethod void execute_noDipStatus_test() {
        Opportunity opp = TestFactory.createOpportunity();
        
        Test.startTest();
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);
        Test.stopTest();
        
        List<Task> results = [SELECT Id, Status FROM Task];
        
        System.assertEquals(0, results.size());        
    }
    
    static testmethod void execute_noDuplicate_test() {
        Opportunity opp = TestFactory.createOpportunity();
        opp.Deal_Status__c = 'DIP';
        update opp;
        
        Date dt = Date.today();
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.WhatId = opp.Id;
        t.Subject = opp.Name + ' DIP Reminder';
        t.ReminderDateTime = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 9, 0, 0);  
        t.Priority = 'Normal';
        t.Status = 'Open';
        insert t;
        
        Test.startTest();
        OpportunityFieldHistory objectHistory = new OpportunityFieldHistory(Field = 'Deal_Status__c',OpportunityId=opp.id);
        insert objectHistory;
        
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);     
        Test.stopTest();
        
        List<Task> results = [SELECT Id, Status FROM Task WHERE WhatId =:opp.Id];
        
        System.assertEquals(1, results.size());
    }
    
    static testmethod void execute_noFieldHistory_test() {
        Opportunity opp = TestFactory.buildOpportunity();
        opp.Deal_Status__c = 'DIP';
        insert opp;
        
        Test.startTest();
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);
        Test.stopTest();
        
        List<Task> results = [SELECT Id, Status FROM Task];
        
        System.assertEquals(0, results.size());   
    }
}