@RestResource(urlMapping='/Intakes/*')
global class sgIntakeController {
    global class IntakeInspection {
        public sgIntakeData Intakes {get; set;}
        public sgInspectionData Inspections {get; set;}        
    }
    
	@HttpGet
    global static IntakeInspection get() {
        sgAuth.check();
        
        IntakeInspection i = new IntakeInspection();
        i.Intakes = new sgIntakeData();
        i.Inspections = new sgInspectionData();
        
        return i;
    }
    
    global class PostResponse {
        public string message {get; set;}
        public Inspection__c inspection {get; set;}
    }
    
    @HttpPost
    global static PostResponse post(string id, string status, Intake__c intake, List<string> fields) {
        sgAuth.requireRole('Admin,Operations');
        
        Set<String> fieldNames = schema.describeSObjects(new String[] {'Intake__c'})[0].fields.getMap().keyset();
        List<string> updateableFields = new List<string>();
        Set<string> postedFields = new Set<string>();
        postedFields.addAll(fields);
        for (string f : sgIntakeData.Fields) {
            if (fieldNames.contains(f.toLowerCase()) && postedFields.contains(f) && !f.contains('__r')) {
                updateableFields.add(f);
            }
        }
		
		Intake__c p = Database.query('select Intake_Status__c, ' + string.join(updateableFields, ', ') + ', Deal__r.Type, Deal__r.Contact_Record__c, Product__r.RFID__c, Product__r.Model__c from Intake__c where Id = :id limit 1');   

        if (p.Intake_Status__c == null) {
            p.Intake_Status__c = 'New';
        }
        
        if (sgIntakeData.transitionAllowed(p.Intake_Status__c, status) || intake.Is_An_Issue__c) {
            p.Intake_Status__c = status;
            
            if (intake != null) {
                for (string f : updateableFields) {
                    if (!f.contains('__r')) {
                    	p.put(f, intake.get(f));
                    }
                }
            
                if (p.Deal__r != null && intake.Deal__r != null) {
                    p.Deal__r.Type = intake.Deal__r.Type;
                    p.Deal__r.Contact_Record__c = intake.Deal__r.Contact_Record__c;
                    
                    update p.Deal__r;
                }
                
                 if (p.Product__r != null && intake.Product__r != null) {
                    p.Product__r.RFID__c = intake.Product__r.RFID__c;
                    update p.Product__r;
                 }
            }

            if (intake != null && intake.Is_An_Issue__c) {
                Intake__c emailIntake = [select Id, Deal__r.Owner.Email, Watch_Model__r.Name, Reference__c, Is_An_Issue_Text__c from Intake__c where Id = :p.Id limit 1];
                
                sgEmail em = new sgEmail();
                em.IntakeIssue(emailIntake.Team_Member__r.Work_Email__c, emailIntake.Watch_Model__r.Name, emailIntake.Reference__c, emailIntake.Is_An_Issue_Text__c);
            }
            
            p.Id = id;
            update p;
			
            PostResponse pr = new PostResponse();
            
            if (p.Intake_Status__c == 'Complete') {
                //Inspection__c i = sgIntakeData.CreateInspection(
                //    sgIntakeData.CreateProduct(p), p
                    
                //);
                
                //pr.inspection = i;
            }
            
            pr.message = p.Id + ' moved to ' + status;
            
            return pr;
        }
        
        throw new sgException('Cannot move item in ' + p.Intake_Status__c + ' to ' + status);
    }
    
    @HttpPut
    global static string put(string id, string field, string val) {
        sgAuth.requireRole('Admin,Operations');
        
        Intake__c c = (Intake__c) Database.query('select Id, ' + field + ' from Intake__c where Id = :id limit 1').get(0);

	    Schema.SObjectType t = Schema.getGlobalDescribe().get('Intake__c');
	    Schema.DescribeSObjectResult r = t.getDescribe();
	    Schema.DescribeFieldResult f = r.fields.getMap().get(field).getDescribe();

        if (f.getType() == Schema.DisplayType.Boolean){
            c.put(field, val.equals('TRUE'));
            update c;
        } 

        return 'updated ' + id;
    }
}