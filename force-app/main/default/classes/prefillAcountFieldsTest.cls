@isTest
public class prefillAcountFieldsTest {

    public static testMethod void testPopulateAccountFields(){
        Account testAcc = dataFactory.buildAccount();
        insert testAcc;
        ApexPages.StandardController controller = new ApexPages.StandardController(testAcc);
        PageReference prefill = page.vfPrefillAccountFields;
        test.setCurrentPage(prefill);
        prefillAccountFields p = new prefillAccountFields(controller);
        PageReference result = p.populateAccountFields();
        test.startTest();
        System.assertNotEquals(null, result);
        test.stopTest();
    }
}