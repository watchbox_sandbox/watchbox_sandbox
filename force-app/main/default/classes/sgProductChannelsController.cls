@RestResource(urlMapping='/ProductChannels/*')
global class sgProductChannelsController {
    @HttpGet
    global static List<sgProductChannelsDTO> get() {
        //run the custom auth check
        sgAuth.check();
        //get the ids from the querystring parameters
        List<String> products = getProductIdList();
        //create a list to hold the productchannel records
        List<sgProductChannelsDTO> productChannels = new List<sgProductChannelsDTO>();
        //loop through each id
        for(String id : products) {
            productChannels.add(translateToProductChannels(id));
        }
        //return the results
        return productChannels;
    }
    
    static List<String> getProductIdList () {
        RestRequest req = RestContext.request;
        Map<String,String> params = req.params;
        List<String> products = params.get('products').split(',');
        return products;
    }
    
    static sgProductChannelsDTO translateToProductChannels(String id) {
        sgProductChannelsDTO productChannels = new sgProductChannelsDTO();
        productChannels.ProductId = id;
        productChannels.Channels = new List<sgChannelInfoDTO>();
        //lookup the id and get the channelinfo related list
        List<ChannelPost__c> posts = [SELECT Channel__c, Post__c, Post_Date__c, Product__c, Visible__c FROM ChannelPost__c WHERE Product__c = :id];
        System.debug('posts');
        System.debug(posts);
        //loop through the channels
        for(ChannelPost__c post : posts) {
            productChannels.Channels.add(translateToChannelInfo(post));
        }
        return productChannels;
    }
    
    static sgChannelInfoDTO translateToChannelInfo(ChannelPost__c post) {
        sgChannelInfoDTO info = new sgChannelInfoDTO();
        info.Name = post.Channel__c;
        info.Post = post.Post__c;
        info.Visible = post.Visible__c;
        info.PostDate = post.Post_Date__c;
        return info;
    }
}