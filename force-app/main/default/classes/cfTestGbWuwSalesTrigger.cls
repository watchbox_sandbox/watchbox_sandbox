@isTest
public class cfTestGbWuwSalesTrigger {
    @isTest static void TestInsertGbWuwSales() {
        // Setup
        List<GBWUW_Sales__c> sampleUploads = GetGbWuwSalesSamples(); // Pending Batch Test
        
        System.debug(sampleUploads);
        
        // Perform
        Test.startTest();
        Database.SaveResult[] results = Database.insert(sampleUploads, true);
        Test.stopTest();
        
        System.debug(results);
        
        // Verify Insertion
        System.assert(results[0].isSuccess());
        
        List<Id> resultIds = new List<Id>();
        
        for (Database.SaveResult sr :results) {
            resultIds.add(sr.getId());
        }
        
        Map<String, GBWUW_Sales__c> uploadsMap = new Map<String, GBWUW_Sales__c>();
            
        List<GBWUW_Sales__c> uploads = [
            SELECT 
                Id, Name, VendorNum__c, Reference__c, ItemNum__c, Description__c, Loc__c, ASCInventoryID__c, 
                Trans_Date__c, SlspID__c, Customer_Number__c, Cost__c, Extension_Retail__c, Actual_Sale_Price__c, 
                DaysToSell__c, Date_Inventoried__c, Product_Type__c, Brand__c, Team_Member__c, Location__c, Model__c, 
                Transaction_Number__c, DocType__c, GBCustomer__c, SalesUserID__c, NCCustomerID__c, WUWInventoryID__c, 
                WUWManID__c, WUWItemCust8__c, WUWIsGBVirtual__c, Account__c, Model__r.Reference__c
            FROM GBWUW_Sales__c
            WHERE Id IN :resultIds LIMIT 10
        ];
        
        for (GBWUW_Sales__c upload :uploads) {
            if (upload.ASCInventoryID__c != null) uploadsMap.put(upload.ASCInventoryID__c, upload);
            if (upload.WUWInventoryID__c != null) uploadsMap.put(upload.WUWInventoryID__c, upload);
        }
        
        System.debug(uploadsMap.values());
        
        GBWUW_Sales__c gbSample = uploadsMap.get('3296845'); // for general conditions
        GBWUW_Sales__c wuwSample = uploadsMap.get('31513');
        GBWUW_Sales__c preOwnedWuwSample = uploadsMap.get('31514'); // for Pre-Owned type
        GBWUW_Sales__c preOwnedSample = uploadsMap.get('3279692'); // for Pre-Owned type
        GBWUW_Sales__c closeoutSample = uploadsMap.get('2896728'); // for Closeout type
        
        // References and corresponding values should be resolved after insert
        System.assert(gbSample.Product_Type__c != null, 'Product Type assignment');
        System.assert(gbSample.Brand__c != null, 'Watch Brand assignment');
        System.assert(gbSample.Location__c != null, 'Location assignment');
        System.assert(gbSample.Model__c != null, 'Model assignment');
        System.assert(gbSample.Team_Member__c != null, 'Team Member assignment');
        System.assert(gbSample.GBCustomer__c != null, 'GB Customer assignment');
        // ---
        System.assert(wuwSample.Brand__c != null, 'Watch Brand assignment');
        System.assert(wuwSample.Location__c != null, 'Location assignment');
        System.assert(wuwSample.Team_Member__c != null, 'Team Member assignment');
        System.assert(wuwSample.Account__c != null, 'WUW Customer assignment');
        // ---
        
        System.assertEquals(wuwSample.Product_Type__c, 'Closeout', 'Product Type expected as [Closeout]');
        System.assertEquals(preOwnedWuwSample.Product_Type__c, 'Pre-Owned', 'Product Type expected as [Pre-Owned]');
        
        System.assertEquals(gbSample.Product_Type__c, 'Regular Goods', 'Product Type expected as [Regular Goods]');
        System.assertEquals(gbSample.Model__r.Reference__c, 'A10350', 'Model correct value');
        
        System.assertEquals(preOwnedSample.Product_Type__c, 'Pre-Owned', 'Product Type on second sample expected as [Pre-Owned]');
        System.assertEquals(closeoutSample.Product_Type__c, 'Closeout', 'Product Type on third sample expected as [Closeout]');

		System.assertEquals(gbSample.Cost__c, -3093, 'Cost expected to be truncated');
		System.assertEquals(gbSample.Extension_Retail__c, -5946, 'Extension Retail Price expected to be truncated');
		System.assertEquals(gbSample.Actual_Sale_Price__c, 16843, 'Actual Sale expected to be truncated');
        
		System.assertEquals(gbSample.Date_Inventoried__c, Date.valueOf('2016-05-05'), 'Date Inventoried expected to be 2015-05-05 (TransDate - DaysToSell)');
        // ----------
    }
    
    @testSetup static void Init() {
        // Setup test data
        insert GetSampleBrands();
        insert GetSampleLocations();
        insert GetSampleModels();
        insert GetSampleAscGroupCodes();
        insert GetSampleTeamMembers();
        insert GetSampleGbCustomers();
        insert GetSampleAccounts();
        
        System.debug([
            SELECT 
                Id, Name, ASC_Man_ID__c, Brand_Description__c, WUW_Brand_Description__c 
            FROM Brand_object__c WHERE ASC_Man_ID__c != NULL]);
        System.debug([
            SELECT 
                Id, Name, LocNum__c, Segment__c, Telephone_Number__c, Manager_Name__c
            FROM Location__c LIMIT 10]);
        System.debug([
            SELECT 
                Id, Name, Case_Material__c, Case_Size__c, Reference__c, model_brand__c, model_brand__r.ASC_Man_ID__c
            FROM Model__c LIMIT 10]);
        System.debug([
            SELECT 
                Id, ItemNum__c, Model__r.Name, Watch_Brand__r.ASC_Man_ID__c, 
                    Reference__c, Description__c, SHWatchID__c, Active_Inventory__c, VendorID__c
            FROM ASC_Group_Codes__c LIMIT 10]);
        System.debug([
            SELECT 
                Id, Name, First_Name__c, Last_Name__c, GB_ASC_Sale_ID__c
            FROM Team_Member__c LIMIT 10]);
    }
    
    static List<Brand_object__c> GetSampleBrands() {
        return new List<Brand_object__c> {
          new Brand_object__c (
              Name = 'Test Brand', ASC_Man_ID__c = 'BREI', NC_Man_ID__c = '123', Brand_Description__c = 'Testing brand'
          )
        };
    }
    static List<Location__c> GetSampleLocations() {
        return new List<Location__c> {
          new Location__c ( Name = 'Govberg Office', LocNum__c = 11 ), 
          new Location__c ( Name = 'Watchuwant', LocNum__c = 31 )  
        };
    }
    static List<Model__c> GetSampleModels() {
        Brand_object__c brand = [SELECT Id FROM Brand_object__c WHERE ASC_Man_ID__c = 'BREI' LIMIT 1];
        return new List<Model__c>{
            new Model__c (
                Name = 'Windrider Wings Automatic', Case_Material__c = 'Stainless Steel', 
                Case_Size__c = 38, Reference__c = 'A10350', model_brand__c = brand.Id
            ),
            new Model__c (
                Name = 'Zeitwerk 18K Yellow Gold 42mm', Case_Material__c = 'Yellow Gold', 
                Case_Size__c = 42, Reference__c = '140.021'
            )
        };
    }
    static List<ASC_Group_Codes__c> GetSampleAscGroupCodes() {
        Model__c model = [SELECT Id, model_brand__c FROM Model__c WHERE model_brand__r.ASC_Man_ID__c = 'BREI' LIMIT 1];
        return new List<ASC_Group_Codes__c>{
            new ASC_Group_Codes__c(
                ItemNum__c = 'BREI107192', Model__c = model.Id, Watch_Brand__c = model.model_brand__c, 
                Reference__c = 'AB01146B/M524', Description__c = 'SS CHRM44-GOVBERGblk/GRPH', 
                SHWatchID__c = '46922', Active_Inventory__c = true, VendorID__c = '101085'
            )
        };
    }
    static List<Team_Member__c> GetSampleTeamMembers() {
        return new List<Team_Member__c>{
            new Team_Member__c (First_Name__c = 'TmGbFirst', Last_Name__c = 'TmGbLast', GB_ASC_Sale_ID__c = '43'),
            new Team_Member__c (First_Name__c = 'TmWuwFirst', Last_Name__c = 'TmWuwLast', NCUserID__c = 11)
        };
    }
    static List<GBCustomer__c> GetSampleGbCustomers() {
        return new List<GBCustomer__c>{
            new GBCustomer__c (First_Name__c = 'GbFirst', Last_Name__c = 'GbLast', CustomerNumber__c = '12345')
        };
    }
    static List<Account> GetSampleAccounts() {
        return new List<Account>{
            new Account (
                Name = 'WUWFirst WUWLast', Last_Name__c = 'Test', Phone = '123-456-7890', WUW_NC_Customer_ID__c = '12426'
            )
        };
    }
    
    static List<GBWUW_Sales__c> GetGbWuwSalesSamples() {
        return new List<GBWUW_Sales__c> {
            new GBWUW_Sales__c (
                Loc__c = '11', VendorNum__c = '101085', SlspID__c = '43', Customer_Number__c = '12345',
                Reference__c = 'A1332016/G698', ItemNum__c = 'BREI107192', ASCInventoryID__c = '3296845',
                Description__c = 'SS SO HERIT CHR-SLV/BLUE', Cost__c = -3093.1897, Extension_Retail__c = -5946.84, Actual_Sale_Price__c = 16843.9, 
                Trans_Date__c = Date.valueOf('2016-05-25'), DaysToSell__c = '20'),
            new GBWUW_Sales__c (
                Loc__c = '31', WUWInventoryID__c = '31513', Cost__c = 2820, SalesUserID__c = '11', NCCustomerID__c = '12426',
            	WUWItemCust8__c = 'Govberg Jewelers', WUWIsGBVirtual__c = false, WUWManID__c = '123'),
            new GBWUW_Sales__c (
                Loc__c = '31', WUWInventoryID__c = '31514', Cost__c = 2820, SalesUserID__c = '11', NCCustomerID__c = '12426',
            	WUWItemCust8__c = 'Govberg Jewelers', WUWIsGBVirtual__c = true),
            new GBWUW_Sales__c (
                Loc__c = '11', VendorNum__c = '101085', 
                Reference__c = 'A45320B9/C902', ItemNum__c = 'BREI309859', ASCInventoryID__c = '3279692',
                Description__c = 'SS GALACTIC44-METALLICBLU', Cost__c = 2820),
            new GBWUW_Sales__c(
                Loc__c = '11', VendorNum__c = '102125', SlspID__c = '43',
                Reference__c = 'AB01146B/M524', ItemNum__c = 'BREI107192', ASCInventoryID__c = '2896728',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68),
            new GBWUW_Sales__c(
                Loc__c = '11', VendorNum__c = '101085', 
                Reference__c = 'AB01146B/M524', ItemNum__c = 'PANA307192', ASCInventoryID__c = '2896736',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68),
            new GBWUW_Sales__c(
                Loc__c = '11', VendorNum__c = '101085', 
                Reference__c = 'AB01146B/M524', ItemNum__c = 'BREI107192', ASCInventoryID__c = '2900264',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68)
        };
    }
}