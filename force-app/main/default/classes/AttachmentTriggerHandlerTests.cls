@isTest
public class AttachmentTriggerHandlerTests {

    public static testMethod void testNoShipmentIds(){
        Account testAcc = datafactory.buildAccount();
        insert testAcc;
        Blob bodyBlob = Blob.valueOf('Blahbity body');
        Attachment testAttachment = new Attachment(
        body = bodyBlob,
        name = 'Attachment Name',
        ParentId = testAcc.id);
        insert testAttachment;
        system.assertequals(1,[SELECT count() FROM Attachment]);
    }
    
    /*public static testMethod void testThrowException(){
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        zkfedex__ShipmatePreference__c preference = dataFactory.buildPreference();
        insert preference;
        zkfedex__shipment__c shipment = dataFactory.buildShipment();
        insert shipment;
        Blob bodyBlob = Blob.valueOf('Blahbity body');
        Attachment testAttachment = new Attachment(
        body = bodyBlob,
        name = 'Attachment Name',
        ParentId = shipment.id);
        try{
          insert testAttachment;  
        }catch(Exception e){
            system.assert(e.getMessage()!=null);
        }
        
        //system.assertEquals(0,[SELECT count() FROM Attachment]);
        
    }
    */
    /*public static testMethod void testShippingAttachment(){
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        zkfedex__ShipmatePreference__c preference = dataFactory.buildPreference();
        insert preference;
        zkfedex__shipment__c shipment = dataFactory.buildShipment();
        shipment.zkfedex__RecipientOpportunity__c = testOpp.id;
        shipment.fedex_Shipments_One_Click__c = testOpp.id;
        insert shipment;
        system.debug('Shipment: '+shipment);
        Blob bodyBlob = Blob.valueOf('Blahbity body');
        Attachment testAttachment = new Attachment(
        body = bodyBlob,
        name = 'Attachment Name',
        ParentId = shipment.id);
        system.assertEquals(1, [SELECT count() FROM zkfedex__Shipment__c]);
        try{
          insert testAttachment;  
        }catch(exception e){
            system.debug('Error message: '+e.getMessage());
        }
        
        system.assertEquals(1,[SELECT count() FROM Attachment]);
        //system.assertNotEquals([SELECT id FROM Attachment WHERE parentId = :testOpp.id].id, [SELECT id FROM Attachment WHERE parentId = :shipment.id].id);
        //system.assertEquals([SELECT id, body FROM Attachment WHERE parentId = :testOpp.id].body, [SELECT id, body FROM Attachment WHERE parentId = :shipment.id].body);
    }
    */
    static testmethod void beforeInsert_accountSuccess_test() {
        Account account = TestFactory.createAccount();
        Attachment a = TestFactory.createAttachment(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =: account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');
    }
    
    static testmethod void beforeInsert_accountNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Attachment a = TestFactory.buildAttachment(contact.Id);
        insert a;
        
        System.assertNotEquals(null, a.Id);
    }
    
    static testmethod void beforeInsert_multipleAttachmentsSameAccount_test() {
        List<Attachment> attachments = new List<Attachment>();
        Account account = TestFactory.createAccount();
        attachments.add(TestFactory.buildAttachment(account.Id));
        attachments.add(TestFactory.buildAttachment(account.Id));
        attachments.add(TestFactory.buildAttachment(account.Id));
        
        insert attachments;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Multiple attachments for same account not updated correctly');               
    }
    
    static testmethod void beforeInsert_existingAccountToggle_test() {
        Account account = TestFactory.buildAccount();
        insert account;
        
        Attachment a = TestFactory.createAttachment(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Existing value not toggled correctly');   
    }
    
    static testmethod void beforeInsert_opportunitySuccess_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Attachment a = TestFactory.createAttachment(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Value should be true');
    }
    
    static testmethod void beforeInsert_opportunityNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Attachment a = TestFactory.buildAttachment(null);
        a.ParentId = contact.Id;
        insert a;
        
        System.assertNotEquals(null, a.Id);
    }
    
    static testmethod void beforeInsert_multipleAttachmentsSameOpportunity_test() {
        List<Attachment> attachments = new List<Attachment>();
        Opportunity opp = TestFactory.createOpportunity();
        attachments.add(TestFactory.buildAttachment(opp.Id));
        attachments.add(TestFactory.buildAttachment(opp.Id));
        attachments.add(TestFactory.buildAttachment(opp.Id));
        
        insert attachments;
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Multiple attachments for same opportunity not updated correctly');               
    }
    
    static testmethod void beforeInsert_existingOpportunityToggle_test() {
        Opportunity opp = TestFactory.buildOpportunity();
        insert opp;
        
        Attachment attachment = TestFactory.createAttachment(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Existing value not toggled correctly');   
    }
    
    static testmethod void beforeInsert_opportunityAccount_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Attachment a = TestFactory.createAttachment(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c, Account.Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
                
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Value should be true');
        System.assertEquals(System.today(), result.Account.Last_Activity_Date__c, 'Value should be true');
    }
}