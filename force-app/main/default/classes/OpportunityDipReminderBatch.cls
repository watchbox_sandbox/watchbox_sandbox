global class OpportunityDipReminderBatch implements Database.Batchable<sObject> {
	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, StageName, OwnerId, Name, 
                                         (SELECT Id, Subject, Status FROM Tasks WHERE Subject LIKE '%DIP Reminder' AND Status = 'Open'),
                                         (SELECT Field,Id,NewValue,OldValue,OpportunityId FROM Histories WHERE Field = 'Deal_Status__c' AND CreatedDate >= YESTERDAY)
										 FROM Opportunity WHERE Deal_Status__c = 'DIP']);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> opps){
        System.debug('Entering OpportunityDipReminderBatch.execute');
        List<Task> tasksToInsert = new List<Task>();
        
        try {
            for (Opportunity opp : opps) {    
                if (!opp.Histories.isEmpty()) {
                    if (opp.Tasks.isEmpty()) {
                        Date dt = Date.today();
                        Task t = new Task();                    
                        t.OwnerId = opp.OwnerId;
                        t.WhatId = opp.Id;                    
                        t.Subject = opp.Name + ' - DIP Reminder';
                        t.ReminderDateTime = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 9, 0, 0);  
                        t.Priority = 'Normal';
                        t.Status = 'Open';
                        t.IsReminderSet = true;
                        t.ActivityDate = Date.today();
                        tasksToInsert.add(t);
                    }    
                }
            }
            
            if (!tasksToInsert.isEmpty()) 
                database.insert(tasksToInsert, false);            
        } catch (Exception e) {
            System.debug('Error during OpportunityDipReminderBatch - ' + e.getMessage() + ' - ' + e.getStackTraceString());
            throw e;
        }        
    }
    
    global void finish(Database.BatchableContext BC){
		
    }
}