global class sgChannelInfoDTO {
    public String Name {get;set;}
    public String ImageUrl {get;set;}
    public Boolean Visible {get;set;}
    public Boolean Post {get;set;}
    public Date PostDate {get;set;}
    public Date PostedDate {get;set;}
    public Date VisibleDate {get;set;}
}