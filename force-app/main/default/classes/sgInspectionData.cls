global class sgInspectionData {
    public List<Map<string, string>> Products {get; set;}
    
    
    public List<string> FieldBases = new List<string> {
        	'Water_Resistance',
            'Crown_Tube',
            'Freedom_Weight',
            'Power_Reserve_Indicator',
            'Alignment_Of_Hands',
            'Condition_Of_Dial',
            'Date_Change',
            'Aperture_Window',
            'Rapid_Date_Function',
            'Chronograph_Function_Start_Stop',
            'Chronograph_Function_Minute',
            'Chronograph_Function_Reset',
            'Rate_Amplitude_OFF',
            'Rate_Amplitude_ON',
            'Movement_Performance',
            'Case',
            'Crystal',
            'Antiglare',
            'Bezel',
            'Bezel_Rotation',
            'Bracelet_Strap',
            'Bracelet_Direction',
            'Bracelet_Strap_Length',
            'Links_Balanced',
            'Ease_Of_Pin',
            'Crown_Guard',
            'Closing_Clasp',
            'Refinish_Case'
    };
                
    
    public List<string> BuildFields() {
        List<string> suffixes = new List<string> {
        	'Approval',
            'Cost',
            'Status',
            'Value'
        };
        
        List<string> fields = new List<string>();
        for (string f : FieldBases) {
            for (string suffix : suffixes) {
            	fields.add(f + '_' + suffix + '__c');
            }
        }

        
        return fields;
    }
    
    public List<string> AdditionalFields {
        get {
            return new List<string> {
                'Id',
                'Picked_Up_By__c',
                'Picked_Up_By__r.Name__c',
                
                'Related_Inventory__c',
                'Related_Inventory__r.Inventory_ID__c',
                'Related_Inventory__r.Model__r.AWS_Image_Array__c',
                'Related_Inventory__r.AWS_Web_Images__c',
                'Related_Inventory__r.Model__r.Name',
                
                'Team_Member__c',
                'Team_Member__r.Name__c',
                'Team_Member__r.Profile_Picture__c',                
                'Mark_Issue__c',
                
                'Mark_For_Pickup__c',
                'Request_Approval__c',
                'Sales_Approved__c',
                
                'TimeInspected__c',
                'Time_Picked_Up__c',
                'Time_Sales_Approved__c',
                'Inspection_Start_Time__c',
                'Cumulative_Inspection_Time__c',

                
                'Intake__r.WalkIn__c',
                'Intake__r.Deal__r.Type',
                'Intake__r.Reference__c',
                'Intake__r.LastModifiedDate',
                'LastModifiedDate',
                'Estimated_Weeks_For_Delivery__c',
                'Issue_Type__c',
                'Issue_Notes__c',
                'Type_Of_Repair_Needed__c',
                'Suggested_Repair_Vendor__c',
                'Inspection_Results__c',
                'Intake__r.Id',
                'Intake__r.Is_Active__c',
                'Intake__r.AWS_Web_Images__c'
            };
        }
    }

    public Map<string, List<string>> FieldOptions {
        get {
        	Map<string, List<string>> options = new Map<string, List<string>>();
            
            for (string f : FieldBases) {
             	options.put(f, sgUtility.getPicklistValues('Inspection__c', f + '_Value__c'));
            }
            
            options.put('Type_Of_Repair_Needed__c', sgUtility.getPicklistValues('Inspection__c', 'Type_Of_Repair_Needed__c'));
            options.put('Issue_Type__c', sgUtility.getPicklistValues('Inspection__c', 'Issue_Type__c'));
            options.put('Inspection_Results__c', sgUtility.getPicklistValues('Inspection__c', 'Inspection_Results__c'));
            
            return options;
    	}
    }
    
    public sgInspectionData() {
        
        List<string> AllFields = BuildFields();
        AllFields.addAll(AdditionalFields);
        {
            string s = '';
        }    
        string query = 'select ' + string.join(AllFields, ',') + ' from Inspection__c ' +
            		   'where (Sales_Approved__c != true and Team_Member__c in (null, \'' + sgAuth.user.Id + '\') and Intake__c != null and Intake__r.Is_Active__c = true ';
		
        
        Map<string, string> reqParams = RestContext.request.params;
        List<string> paramNames = new List<string>{
            'Watch_Brand__r.Name',
        	'Deal__r.Type',
        	'Deal__r.Salesperson__r.Name__c'
		};
            
        Map<string, string> params = new Map<string, string>();
        for (string param : paramNames) {
            if (reqParams.containsKey(param)) {
        		params.put('Intake__r.' + param, reqParams.get(param));
            }
        }
        
        if (reqParams.containsKey('Product__r.Location__r.Name')) {
        	params.put('Related_Inventory__r.Location__r.Name', reqParams.get('Product__r.Location__r.Name'));
        }


        query = + sgUtility.applyFilters(query, params) + ')';
        
		string inspectionId = 'inspectionId';
        if (reqParams.containsKey(inspectionId)) {
        	query += ' or Id = \'' + reqParams.get(inspectionId) + '\' ';
            reqParams.remove(inspectionId);
        }
        
        query += ' order by Intake__r.Walkin__c desc, Intake__r.Deal__r.Type desc, Related_Inventory__r.Cost__c desc nulls last limit 100';
            //throw new sgexception(query);

        Products = sgSerializer.serializeFromQuery(query);

        for (Map<string, string> p : Products) {
            string status = 'Queue';
            
            if (p.get('Request_Approval__c') == 'true') {
                status = 'Approve';
            } else if (p.get('Mark_For_Pickup__c') == 'true') {
                status = 'Pickups';
            }
            
            p.put('Status__c', status);
            
            String id = p.get('Id');
            p.put('Inspections__rId', id);
            
            p.put('ModelImage__c', sgUtility.getMainImage(p.get('Related_Inventory__rModel__rAWS_Image_Array__c')));
            p.remove('Related_Inventory__rModel__rAWS_Image_Array__c');
        }
    }
}