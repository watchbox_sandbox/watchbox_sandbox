global class sgModelDetail {
    public Map<string, string> Fields {get; set;}
    
    public List<Map<string, string>> Products {get; set;}
}