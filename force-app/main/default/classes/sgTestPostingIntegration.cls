@isTest
public class sgTestPostingIntegration {

    static testmethod void initialize() {
        //arrange
        //
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        //sgEndpoint endpoint = sgEndpoint.Test();
        system.debug(endpoint);
        sgPostingIntegrationNoOverride integration = new sgPostingIntegrationNoOverride();
        integration.Name = 'Test';
        integration.Endpoint = endpoint;
        integration.ChannelPost = new ChannelPost__c();
        createServiceSettings(integration.Name);
            
        //act
        //
        integration.Initialize();
        
        //assert
        //
        System.assertEquals(1, integration.Services.size());
    }
    
    static testmethod void noOverrides_failure() {
        //arrange
        //
        Integer excepCount = 0;
        sgPostingIntegrationNoOverride integration = new sgPostingIntegrationNoOverride();
            
        //act
        //
        try { integration.SetVisible(); } catch (Exception ex) { excepCount++; }
        try { integration.SetPost(); } catch (Exception ex) { excepCount++; }
        try { integration.RemoveVisible(); } catch (Exception ex) { excepCount++; }
        try { integration.RemovePost(); } catch (Exception ex) { excepCount++; }
        try { integration.DoUpdate(); } catch (Exception ex) { excepCount++; }
        
        //assert
        //
        System.assertEquals(5, excepCount);
    }
    
    static testmethod void activityLog() {
        //arrange
        //
        sgPostingIntegrationNoOverride integration = new sgPostingIntegrationNoOverride();
        ChannelPost__c channel = new ChannelPost__c (
        	Name = 'Watchbox',
            Channel__c = 'Watchbox'
        );
        insert channel;
        integration.ChannelPost = channel;
        
        //act
        //
        integration.AddActivity('subject', 'description', 'Closed');
        integration.AddActivity('subject2', 'description', 'Closed');
        integration.SaveActivityLog();
        
        //assert
        //
        List<Task> activities = [SELECT Id FROM Task WHERE WhatId = :channel.Id];
        System.assertEquals(2, activities.size());
    }
    
    //*****************************************************************************
    // Support
    
    public class sgPostingIntegrationNoOverride extends sgPostingIntegration {
        
    }
    
    public class MockEndpoint implements System.StubProvider {
        
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            system.debug('in mock endpoint');
            
            return null;
        }

    }
    
    static void createServiceSettings(String name) {
        Surge_Integration_Settings__c setting = new Surge_Integration_Settings__c(
            Name = name,
            Integration__c = name,
            Organization__c = 'Watchbox',
            Service__c = 'Test',
            Path__c = '/test',
            Parameters__c = 'test=test'
        );
        
        insert setting;
    }
}