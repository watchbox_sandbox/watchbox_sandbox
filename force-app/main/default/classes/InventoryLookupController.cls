public class InventoryLookupController {
    private static final string SOLD_STATUS = 'Sold';
    private static final string HOLD_STATUS = 'On Hold';
    public List<Product2> Products {get; set;}
    public string SearchTerm { get; set;}   

    public PageReference searchProducts() {
        Products = new List<Product2>();
        
        try {
            string sosl = 'FIND :SearchTerm IN ALL FIELDS RETURNING Product2(Id, Name, Inventory_ID__c WHERE Status__c !=:SOLD_STATUS AND Calculated_Status__c !=:HOLD_STATUS ORDER BY NAME) LIMIT 100';
         	System.debug('Executing InventoryLookupController.searchProducts with SearchTerm - ' + SearchTerm);
            
            Search.SearchResults searchResults = Search.find(sosl);
            List<Search.SearchResult> productList = searchResults.get('Product2');
            
            for (Search.SearchResult searchResult : productList) { 
                Product2 product = (Product2)searchResult.getSObject(); 
                Products.add(product);
            } 
        } catch (Exception e) {
            System.debug('Error searching products in InventoryLookupController.searchProducts - ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);     
        }       

        return null;
    }
}