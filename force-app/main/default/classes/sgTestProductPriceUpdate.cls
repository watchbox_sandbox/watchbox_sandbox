@isTest
public class sgTestProductPriceUpdate {

    static testMethod void updateProduct() {
        //arrange
        //
        Product2 product = new Product2(
        	Name = 'test'
        );
        insert product;
        
        ChannelPost__c channel = new ChannelPost__c (
        	Channel__c = 'Watchbox',
            Product__c = product.Id
        );
        insert channel;
        
        //act
        //
        Test.startTest();
        product.ASK__c = 1000;
        update product;
        Test.stopTest();
        
        //assert
        //
        channel = [SELECT Update__c FROM ChannelPost__c WHERE Id = :channel.Id];
        System.assertEquals(true, channel.Update__c);
    }
    
}