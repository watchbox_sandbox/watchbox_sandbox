@isTest
public class createDealShipmentTest {
    
    static testMethod void testDealShipment(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference shipmentPage = page.vfDealShipmentConnector;
            
            //create necessary test records
           // zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
           // insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id, shipping_Paid_By__c = 'WUW', Why_did_we_pay_for_shipping__c = 'Testing');
            insert testReturn;
          //  zkfedex__CustomAddressSource__c cas = new zkfedex__CustomAddressSource__c(zkfedex__ShipmentObjectLookupField__c = 'Fedex_Shipments_One_Click__c');
          //  insert cas;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(shipmentPage);
            shipmentPage.getParameters().put('id',testOpp.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
            createDealShipment cds = new createDealShipment(controller);
            PageReference result = cds.shipFromDealRecord();
            system.assertNotEquals(null, result);
            //system.assertEquals(1, [SELECT count() FROM zkfedex__Shipment__c ]);
        test.stopTest();
    }
    static testMethod void testDealShipmentNeg(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference shipmentPage = page.vfDealShipmentConnector;
            
          //create necessary test records
          //  zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
          //  insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id);
            insert testReturn;
         //   zkfedex__CustomAddressSource__c cas = new zkfedex__CustomAddressSource__c(zkfedex__ShipmentObjectLookupField__c = 'Fedex_Shipments_One_Click__c');
         //   insert cas;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(shipmentPage);
            shipmentPage.getParameters().put('id',testOpp.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
            createDealShipment cds = new createDealShipment(controller);
            PageReference result = cds.shipFromDealRecord();
            system.assertNotEquals(null, result);
         //   system.assertEquals(0, [SELECT count() FROM zkfedex__Shipment__c ]);
        test.stopTest();
    }
}