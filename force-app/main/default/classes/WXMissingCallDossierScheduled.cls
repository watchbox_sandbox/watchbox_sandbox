global class WXMissingCallDossierScheduled implements Schedulable{
    public Integer startIndex = 0;
    public Integer endIndex = 0;
    public Integer Noofrecords = 0;
    
    global void execute(SchedulableContext sc) {
        WealthXIndexSettings__c wxIndex = WealthXIndexSettings__c.getValues('WXIndexMissingIdxRange');
        startIndex = Integer.valueOf(wxIndex.Start_Index__c);
        endIndex = Integer.valueOf(wxIndex.End_Index__c);
        Noofrecords  = Integer.valueOf(wxIndex.No_of_Records__c);
        
        batchWXMissingCallDossier batch = new batchWXMissingCallDossier(startIndex,endIndex);
        database.executeBatch(batch,1);
        startIndex = endIndex  + 1;
        endIndex = startIndex + Noofrecords;
        
        wxIndex.Start_Index__c = String.valueOf(startIndex);
        wxIndex.End_Index__c = String.valueOf(endIndex);
        update wxIndex;
    }
}