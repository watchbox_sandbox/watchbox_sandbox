@isTest
public class ModelServiceTests {
    static testmethod void setModelAsVerified_success_test() {
        Model__c model = TestFactory.createModel();               
        ModelService.setModelAsVerified(model.Id);
        
        Model__c result = [SELECT Id, Verified__c FROM Model__c WHERE Id =: model.Id];
        
        System.assert(result.Verified__c, 'Record not updated to verified');
    }
    
    static testmethod void setModelEbl_success_test() {
        Model__c model = TestFactory.createModel();               
        ModelService.setModelEbl(model.Id);
        
        Model__c result = [SELECT Id, EBL__c FROM Model__c WHERE Id =: model.Id];
        
        System.assert(result.EBL__c, 'Record not updated EBL');
    }
}