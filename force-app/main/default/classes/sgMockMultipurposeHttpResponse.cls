///
// This is a multipurpose mocked HttpResponse
//  Set the StatusCode, Body, Headers or even a Mock Exception
//  and the response will return those values
global class sgMockMultipurposeHttpResponse implements HttpCalloutMock {
	public class MockException extends Exception {}
    
    public Integer StatusCode {get;set;}
    public String Body {get;set;}
    public Map<String,String> Headers {get;set;}
    public String MockExceptionMessage {get;set;}
    
    public HTTPRequest LastRequest {get;set;}
    
    public sgMockMultipurposeHttpResponse() {}
    
    public sgMockMultipurposeHttpResponse(Integer statusCode, String body, Map<String,String> headers) {
        this.StatusCode = statusCode;
        this.Body = body;
        this.Headers = headers;
    }
    
    public sgMockMultipurposeHttpResponse(String exceptionMessage) {
        this.MockExceptionMessage = exceptionMessage;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        //set the last request
        LastRequest = req;
        
        //if we have a MockExceptionMessage then we are mocking an exception, throw it
        if (this.MockExceptionMessage != null) {
            throw new MockException(MockExceptionMessage);
        }
        
        HttpResponse res = new HttpResponse();       
        //status code
        if (StatusCode != null) {
            res.setStatusCode(StatusCode);
        }
        //body
        if (Body != null) {
            
        	res.setBody(Body);
        }
        //headers
        if (Headers != null) {
            for(String key : Headers.keySet()) {
                res.setHeader(key, Headers.get(key));
            }
        }
        return res;
    }
    
}