global class OpportunityContactUpdateBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id,Contact_Record__c,accountId
                                         FROM Opportunity WHERE Contact_Record__c =: null]);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> listOpportunity){
        
        
        Map<Id,List<Opportunity>> oppAccountMap = new Map<Id,List<Opportunity>>();
        Map<Id,Opportunity> oppMap=new  Map<Id,Opportunity>();
        try{
            if(listOpportunity!= null && listOpportunity.size()>0){
                for(Opportunity opp: listOpportunity){
                    if(opp.accountId!=null){
                        if(oppAccountMap.containsKey(opp.accountId)){
                            oppAccountMap.get(opp.accountId).add(opp);
                        }
                        else{
                            oppAccountMap.put(opp.accountId,new list<Opportunity>{opp});
                        }
                        
                    }
                }
                system.debug('map----'+oppAccountMap);
                
                List<Contact> listContact=[select id,accountId from Contact where accountId IN: oppAccountMap.keySet()];
                system.debug('---listContct'+listContact);
                
                for(Contact contact: listContact){
                    List<Opportunity> opp = oppAccountMap.get(contact.accountId);
                    for(Opportunity o :oppAccountMap.get(contact.accountId) ){
                        o.Contact_Record__c=contact.Id;
                        oppMap.put(o.Id,o);
                        //oppSet.add(opp);
                    }
                }
                system.debug('----oppMap------'+oppMap);
                
                
                if(oppMap!=null && oppMap.size()>0){
                    list<Opportunity> lstopp = new list<Opportunity>();
                    lstopp.addAll(oppMap.values());
                    update lstopp;
                    system.debug('----list----'+lstopp);
                    
                }
                
                
            }
            
        }catch (Exception e) {
            System.debug('Error during OpportunityContactUpdateBatch - ' + e.getMessage() + ' - ' + e.getStackTraceString()+ ' - '+e.getLineNumber());
            throw e;
        }   
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}