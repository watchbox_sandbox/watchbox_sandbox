public class WatchSaleController {
    public Watch_Sale__c watchSale {get;set;}
    public String queryString {get;set;}
    public String oppId {get;set;}
    
    private ApexPages.StandardController myStdController;
    
    public WatchSaleController(ApexPages.StandardController controller){
        
        watchSale=new Watch_Sale__c();
        watchSale = (Watch_Sale__c) controller.getRecord();
        oppId = apexpages.currentpage().getparameters().get('oppid');
        
        myStdController=controller;
        system.debug('oppid-----'+oppId);
        if(oppId!=null )
            watchSale.Opportunity__c=oppId.trim();
        PageReference thisPage = ApexPages.currentPage();
        if(thisPage <> NULL && thisPage.getUrl() <> NULL){
            List<String> url = thisPage.getUrl().split('\\?');
            if(url <> NULL && url.size()>2){
                queryString = url[1];
            }
        }
        
    }
    
    public pagereference save(){
        pagereference oppPage;
        system.debug('watch Sale record::'+watchSale);
        try{
            if (watchSale.Selling_below_MSP__c=='Yes' && watchSale.Reason_for_Selling_Below_MSP__c == null){                                                 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Reason for Selling is mandatory.'));
                opppage=null;
            }else if(watchSale.MSP__c == null || watchSale.MSP__c ==0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'WB Offer is mandatory.'));
                opppage=null;                
            }else if(watchSale.Client_s_Offer__c == null|| watchSale.Client_s_Offer__c ==0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Client\'s Offer is mandatory.'));
                opppage=null;                
            }
            else{
                insert watchSale;
                oppPage= new pagereference('/'+oppId);
                oppPage.setRedirect(true);
            }
            
        }
        catch(System.DMLException e) {
            Integer numErrors = e.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDmlMessage(i)));
            }
        }
        system.debug('opppage save:::'+oppPage);
        return oppPage;
    }
    public pagereference cancelButton(){
        pagereference oppPage= new pagereference('/'+oppId);
        System.debug('oppPage----------'+oppPage);
        //oppPage = new ApexPages.StandardController(origination).view();
        oppPage.setRedirect(true);
          system.debug('opppage cancel:::'+oppPage);
        return oppPage;
    }
    
    @AuraEnabled
    public static List<String> saleBrandValues(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Watch_Sale__c.Sale_Brand__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static void addWatchSale(Watch_Sale__c watchSaleobj,Id oppid){
        system.debug('inside save----------'+watchSaleobj);
        watchSaleobj.Opportunity__c=oppid;
        insert watchSaleobj;
        system.debug('after save');
        
        
    }
    @AuraEnabled
    public static Opportunity getOpportunityName(Id oppId){
        Opportunity opp= [Select name from Opportunity where id=:oppId];
        return opp;
    }
}