global class sgPicklistDTO {

    global SelectOptions selectOptions { get; set; }
    global List<Picklist> picklists { get; set; }


    global class SelectOptions
    {
        global List<string> speed {get;set; }
        global List<string> type {get;set; }
    }

    global class Picklist
    {
        global string id { get; set; }
        global string type { get; set; }
        global string priority { get; set; }
        global string destination { get; set; }
        global string shipping { get; set; }
        global string deal { get; set; }
        global string watchMaking { get; set; }
        global string siteToSite { get; set; }
        global string retrn { get; set; }
        global List<PicklistDetail> details { get; set; }
    }

    global class PicklistDetail
    {
        global string id { get; set; }
        global string productId { get; set; }
        global string inventoryId { get; set; }
        global string brandName { get; set; }
        global decimal cost { get; set; }
        global string imageUrl { get; set; }
        global string reference { get; set; }
        global string location { get; set; }
    }
}