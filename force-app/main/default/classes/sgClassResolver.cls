///
// A simple utility for resolving and instanciating classes by name
// 
public class sgClassResolver {
    public class ClassNotExistsException extends Exception {}
    
    ///
    // Returns the Type represented by `name`
    // This is an object method (rather than static)
    //  so it can be mocked
    //
    public Type resolveClass(String name) {
        return Type.forName(name);
    }
    
    ///
    // Returns an instance of a Class represented by `name`
    // The class must have a public parameterless constructor
    // This is an object method (rather than static)
    //  so it can be mocked
    //  
    public Object executeClass(String name) {
        //get the type
        Type t = resolveClass(name);
        if (t == null) {
            throw new ClassNotExistsException('Class ' + name + ' does not exist');
        }
        //create and return an instance
        return t.newInstance();
    }
    
    ///
    // Returns true if the class exists
    // 
    public Boolean classExists(String name) {
        //get the type
        Type t = resolveClass(name);
        if (t != null) {
            return true;
        }
        return false;
    }
}