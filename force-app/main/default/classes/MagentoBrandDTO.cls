public with sharing class MagentoBrandDTO {
    public String sfId; // SF_ID__c
    public String name; // Name = 'Brand One',
    public String description; // Brand_Description__c = 'Brand One description.',
    public String slug; // URL_Slug__c = 'brand-one',
    public Boolean active; // WUW_Website_Active__c = true,
    public String hex; // Brand_Hex__c
    public String keynoteVideoUrl; // Brand_Keynote_Video__c
    public String watchImageUrl; // Brand_Main_Watch_Image__c
    public String logoImageUrl; // Logo_Image_URL__c
}