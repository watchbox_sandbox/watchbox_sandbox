public abstract class sgPostingIntegrationMagento extends sgPostingIntegration {
    public class NoMagentoProductsReturnedException extends Exception {}

    public virtual Integer getWebsiteId() {
        return 0; // this method should be overridden.
    }

    public override void SetPost() {
        MagentoProductDTO productToPost = MagentoProductUpdate.productToDTO([SELECT Id FROM Product2 WHERE Id=:this.ChannelPost.Product__c]);

        productToPost.websiteId = getWebsiteId();
        // Don't modify isActive by default.

        postProduct(productToPost);
    }

    public override void SetVisible() {
        MagentoProductDTO productToPost = MagentoProductUpdate.productToDTO([SELECT Id FROM Product2 WHERE Id=:this.ChannelPost.Product__c]);

        productToPost.websiteId = getWebsiteId();
        productToPost.isActive = true;

        postProduct(productToPost);
    }

    public override void RemoveVisible() {
        MagentoProductDTO productToPost = MagentoProductUpdate.productToDTO([SELECT Id FROM Product2 WHERE Id=:this.ChannelPost.Product__c]);

        productToPost.websiteId = getWebsiteId();
        productToPost.isActive = false;

        postProduct(productToPost);
    }

    public override void RemovePost() {
        super.RemovePost(); // not implemented
    }

    public override void DoUpdate() {
        SetPost();
    }

    public void postProduct(MagentoProductDTO productToPost) {
        postProducts(new List<MagentoProductDTO>{productToPost});
    }
    public void postProducts(List<MagentoProductDTO> pdtos) {
        String payload = MagentoProductUpdate.productDTOsToDTOJson(pdtos);

        Blob ret = this.Endpoint.Send('/rest/V1/salesforce/product/sync', 'POST', Blob.valueOf(payload), null, null);
        String entityId = getMagentoProductIdFromResponse(ret.toString());

        this.ChannelPost.External_Id__c = entityId;
        this.ChannelPost.External_Id_Name__c = 'entity_id';
        this.Endpoint.UpdateContext('External_Id__c', entityId);
        this.Endpoint.UpdateContext('External_Id_Name__c', 'entity_id');
    }

    public String getMagentoProductIdFromResponse(String response) {
        String entityId;

        try {
            List<Object> decoded = (List<Object>)JSON.deserializeUntyped(response);
            Map<String, Object> product = (Map<String, Object>)decoded[0];
            entityId = (String)product.get('entity_id');
        }
        catch (Exception ex) {
            throw new NoMagentoProductsReturnedException('no products returned');
        }

        if (entityId == null || entityId.length() < 1) {
            throw new NoMagentoProductsReturnedException('no products returned');
        }

        return entityId;
    }
}