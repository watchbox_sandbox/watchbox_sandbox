@isTest
public class UpdateWatchBoxLoanLeadTest {

    @isTest static void testUpdateOpportuntiy() {
        Integer count = randomWithLimit(1000);
        String requestUriLabel = System.Label.requestUri;
        RestRequest request = new RestRequest();
        request.requestUri = requestUriLabel;
        request.httpMethod = 'Post';
        request.requestBody = Blob.valueOf('[{"WhatWouldLike":null,"WhatBringsYouInToday":null,"WatchReference":null,"WatchModel":null,"WatchImages":null,"WatchBrand":null,"watchBoxDeal":null,"UnitPrice":null,"TypeofProduct":null,"TypeofMetal":null,"TransactionType":null,"SubmissionDate":null,"StageName":"Final Offer Sent","SKU":null,"SignupWinAWatch":null,"SellWatchSerial":null,"SellModel":null,"SellBrand":null,"SalePrice":null,"ReferringURL":null,"ReceiveUpdates":null,"Quantity":null,"Phone":null,"Papers":null,"PageURL":null,"PageID":null,"opportunityid":"a0i0U000002bu1dQAA","Opportunity":null,"NumberofWatchInCollection":null,"NotesDevelopment":null,"NewWatchModel":null,"NameYourPriceASK":null,"ModelNumber":null,"MatchedAccount":null,"MarketingBrand":"WatchBox","LeadSource":null,"LastServiceDate":null,"LastName":null,"LandingPage":null,"irtrackid":null,"irpid":null,"irclickid":null,"ircid":null,"IPCountry":null,"IPAddress":null,"InventoryID":null,"interestedinReferenceNumber":null,"InterestedinProductUrl":null,"InterestedinProductListprice":null,"InterestedinProduct":null,"interestedinModel":null,"interestedinBrand":null,"InitialDealDetails":null,"Images":null,"GCLID":null,"GAVisits":null,"GATerm":null,"GaSource":null,"GASegment":null,"GAMedium":null,"GAgclid":null,"GAContent":null,"GACampaign":null,"FullName":"Govberg Diksha 25 December  12/18/2018","FirstName":null,"Email":null,"EbayUserName":null,"DialDescription":null,"Comments":null,"CloseDate":"2019-03-31","BoxOnly":null,"BoxesandPapers":null,"Boxes":null,"appcampaignid":null,"AdditionalInfo":null}]');
        RestContext.request = request;
        UpdateWatchBoxLoanLead.responseWrapper respWrapper = new UpdateWatchBoxLoanLead.responseWrapper();
        UpdateWatchBoxLoanLead.responseWrapper testResponseWrapper1 = UpdateWatchBoxLoanLead.OpportunityCreationLogic();
    }
    
    public static Integer randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }
    
    
    @isTest static void testUpdateAccount() {
        TestFactory.createLeadGenLogSettings(); 
    	LeadGenLog_Settings__c settings = TestFactory.getLeadSettings();
        Lead_Gen_Log__c leadgen = TestFactory.buildLeadGenLog();
        leadgen.Lead_Source__c = 'WatchLoans';
        insert leadgen;
        OpportunityWrapperr OWrapp = new OpportunityWrapperr();
            OWrapp.FirstName = 'TestFirstName';
            OWrapp.LastName = 'TestLastName';
            OWrapp.MarketingBrand = 'WatchBox';
            OWrapp.WhatWouldLike = 'I\'d like to get a loan';    
            OWrapp.Email = 'testdemo@gmail.com';   
            OWrapp.StageName = 'Application Complete';   
            OWrapp.LandingPage   = 'https://www.thewatchbox.com/get-a-loan/';
            OWrapp.Quantity = '1';
            OWrapp.SellBrand = 'Rolex';
            OWrapp.Boxes = 'Test';
            OWrapp.NameYourPriceASK ='10.10';
            OWrapp.Images = 'https://www.thewatchbox.com/wp-content/uploads/ninja-forms/10/1542648403611561642613.jpg';
            OWrapp.UnitPrice = '0';
        	OWrapp.CloseDate = '2019-01-01';
        	OWrapp.opportunityid = leadgen.Id;
        List<OpportunityWrapperr> oppWrappList = new List<OpportunityWrapperr>();
        oppWrappList.add(OWrapp);
        UpdateWatchBoxLoanLead.responseWrapper response = new UpdateWatchBoxLoanLead.responseWrapper();  
        
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
        OWrapp.StageName = 'Assets Received';
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
        OWrapp.StageName = 'Asset not shipped';
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
        OWrapp.StageName = 'Asset Shipped Back';
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
        OWrapp.StageName = 'Final Offer Sent';
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
        OWrapp.StageName = 'Transaction Complete';
        LACGov_WBSyncService.updateOpportunity(oppWrappList, response);
    }
}