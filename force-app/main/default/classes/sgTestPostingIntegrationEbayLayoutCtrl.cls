@isTest
public class sgTestPostingIntegrationEbayLayoutCtrl {

    static testMethod void construct() {
        //arrange
        //
        Organization__c org = new Organization__c (
            Name='Watchbox'
        );
        insert org;
        Product2 product = new Product2 (
            Name = 'test'
        );
        insert product;
        Photograph__c photo = new Photograph__c (
            Related_Product__c = product.Id,
            Organization__c = org.Id,
            Url__c = 'testurl'
        );
        insert photo;
        
        ChannelPost__c cp = new ChannelPost__c(
            Name='Watchbox',
            Channel__c='Watchbox',
            Organization__c=org.Id,
            Product__c = product.Id
        );
        insert cp;
        
        PageReference page = ApexPages.currentPage();
        Map<String, String> parameters = page.getParameters();
        parameters.put('id', cp.Id);
        //act
        //
        sgPostingIntegrationEbayLayoutController ctrl = new sgPostingIntegrationEbayLayoutController();
        String url = ctrl.getImageUrl();
        
        //assert
        //
        System.assertEquals(cp.Id, ctrl.ChannelPost.Id);
        System.assertEquals('testurl', url);
    }
    
}