public class insertBidOfferTriggerHandler extends TriggerHandler {
    public static Boolean isFirstTime = true;
    public override void afterInsert() {
        system.debug('Inside insert');
        Set<id> watchSaleIdsforBid=new Set<id>();
        Set<id> watchSaleIdsforOffer=new Set<id>();
        
        
        for(Watch_Sale__c watchsale: (List<Watch_Sale__c>)Trigger.new) {
            if (!string.isBlank(watchsale.Reference_number__c)){
                if(watchsale.Client_s_Offer__c!=null ) {
                    watchSaleIdsforBid.add(watchsale.id);
                }
                if(watchsale.MSP__c!=null){
                    watchSaleIdsforOffer.add(watchsale.id);
                }
            }
        }  
        system.debug(':::watchSaleIdsforBid'+watchSaleIdsforBid);
        system.debug(':::watchSaleIdsforOffer'+watchSaleIdsforOffer);
        
        if(watchSaleIdsforBid.size() > 0){
            insertBid(watchSaleIdsforBid);
        }
        if(watchSaleIdsforOffer.size() > 0){
            insertOffer(watchSaleIdsforOffer);
        }        
    }
    
    
    public override void beforeUpdate() {
        Set<id> watchSaleIdsforBid=new Set<id>();
        Set<id> watchSaleIdsforOffer=new Set<id>();      
        Map<Id, Watch_Sale__c>  newWatchSaleMap = (Map<Id, Watch_Sale__c>) Trigger.newMap;
        Map<Id, Watch_Sale__c> oldWatchSaleMap= (Map<Id,Watch_Sale__c>) Trigger.oldMap;
        
        system.debug(':::newWatchSaleMap'+newWatchSaleMap.values());
        system.debug(':::oldWatchSaleMap'+oldWatchSaleMap);
        
        for(Watch_Sale__c newWatchsale: newWatchSaleMap.values()){
           
            if (newWatchsale.Reference_Number__c!=oldWatchSaleMap.get(newWatchsale.Id).Reference_Number__c 
                    && !string.isBlank(newWatchsale.Reference_number__c)){
                system.debug('referencenumber updated');
                if(newWatchsale.Client_s_Offer__c!=null ) {
                    watchSaleIdsforBid.add(newWatchsale.id);
                }
                if(newWatchsale.MSP__c!=null){
                    watchSaleIdsforOffer.add(newWatchsale.id);
                }
            }
            if(newWatchsale.Client_s_Offer__c!=oldWatchSaleMap.get(newWatchsale.Id).Client_s_Offer__c 
                    && newWatchsale.Client_s_Offer__c!=null ) {
                system.debug('Update watchSaleIdsforBid');
                watchSaleIdsforBid.add(newWatchsale.id);
            }
            if(newWatchsale.MSP__c!=oldWatchSaleMap.get(newWatchsale.Id).MSP__c 
                    && newWatchsale.MSP__c!=null){
                    
                system.debug('Update watchSaleIdsforOffer');
                watchSaleIdsforOffer.add(newWatchsale.id);
            }
        }
        if(watchSaleIdsforBid.size() > 0){
            insertBid(watchSaleIdsforBid);
        }
        if(watchSaleIdsforOffer.size() > 0){
            insertOffer(watchSaleIdsforOffer);
        } 
    }
    
    Public static void insertBid(set<id> watchSaleid){
        List<Bid__c> listBid = new List<Bid__c>();
        List<Watch_Sale__c> WatchSaleList=[SELECT Watch_Model__c, Id, Name, MSP__c, Sale_Brand__c,Sale_Model__c, 
                                           Web_price__c, Box_Paper__c, Condition__c,Reference_Number__c,
                                           Brand_Family__c, Brand_Family_Id__c,Reference_Number_Id__c,
                                           Client_s_Offer__c,Inventory_ID__c,Opportunity__r.IP_Country__c 
                                           FROM Watch_Sale__c
                                           WHERE id IN:watchSaleid];
                                           
        for(Watch_Sale__c watchSale:WatchSaleList){
            Bid__c bid=new Bid__c();
            bid.Box_Paper__c=watchSale.Box_Paper__c;
            bid.Brand_Family__c=watchSale.Brand_Family__c;
            bid.Client_s_Offer__c=watchSale.Client_s_Offer__c;
            bid.Condition__c=watchSale.Condition__c;
            bid.Inventory_ID__c=watchSale.Inventory_ID__c;
            bid.Reference_Number__c=watchSale.Reference_Number__c;
            bid.Sale_Brand__c=watchSale.Sale_Brand__c;
            bid.Sale_Model__c=watchSale.Sale_Model__c;
            bid.Watch_Sale__c=watchSale.Id;
            bid.Brand_Family_Id__c=watchSale.Brand_Family_Id__c;
            bid.Reference_Number_Id__c=watchSale.Reference_Number_Id__c;
            bid.Region__c=watchSale.Opportunity__r.IP_Country__c;
            listBid.add(bid);   
        }
        if(listBid.size() > 0){
            insert listBid;
        }
    }
        
    Public static void insertOffer(set<id> watchSaleid){
        List<Offer__c> listOffer = new List<Offer__c>();
        List<Watch_Sale__c> WatchSaleList=[SELECT Watch_Model__c, Id, Name, MSP__c, Sale_Brand__c,Sale_Model__c, 
                                           Web_price__c, Box_Paper__c, Condition__c,Reference_Number__c,
                                           Brand_Family__c, Brand_Family_Id__c,Reference_Number_Id__c,
                                           Client_s_Offer__c,Inventory_ID__c,Opportunity__r.IP_Country__c 
                                           FROM Watch_Sale__c
                                           WHERE id IN:watchSaleid];
                                           
        for(Watch_Sale__c watchSale:WatchSaleList){
            Offer__c offer=new Offer__c();
            offer.Box_Paper__c=watchSale.Box_Paper__c;
            offer.Brand_Family__c=watchSale.Brand_Family__c;
            offer.Brand_Family_Id__c=watchSale.Brand_Family_Id__c;
            offer.Condition__c=watchSale.Condition__c;
            offer.Reference_Number__c=watchSale.Reference_Number__c;
            offer.Reference_Number_Id__c=watchSale.Reference_Number_Id__c;
            offer.Region__c=watchSale.Opportunity__r.IP_Country__c;
            offer.Inventory_ID__c=watchSale.Inventory_ID__c;
            offer.Sale_Brand__c=watchSale.Sale_Brand__c;
            offer.Sale_Model__c=watchSale.Sale_Model__c;
            offer.WB_Offer__c=watchSale.MSP__c;
            offer.Watch_Sale__c=watchSale.id;
            listOffer.add(offer);
        }
        if(listOffer.size() > 0){
            insert listOffer;
        }        
    }
}