@isTest 
public class OriginationControllerTest 
{
	static testmethod void processWatchpurchase(){
         Opportunity opp =TestFactory.createOpportunity();
        Watch_Purchase__c  wpur = new Watch_Purchase__c();
        wpur.Origination_Opportunity__c = opp.id;
        wpur.WB_Trade_Offer__c=443;
        wpur.WB_Last_Purchase_Price__c=3353;
        insert wpur;
        PageReference pageRef = page.OriginationNewPage;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(wpur.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(wpur);
        OriginationController oc  = new OriginationController(sc);
        oc.oppId=opp.id;
     	oc.save();
        oc.cancelButton();
        List<String> sr =  OriginationController.originationBrandValues();
        
        Opportunity opp1 = OriginationController.getOpportunityName(opp.Id);
        
	}
   static testmethod void processopportunities_error() {
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.IsNoteAttached__c='N';
        opp.CloseDate=System.today().addMonths(1); 
        opp.Last_Activity_Date__c=Date.today().addDays(-1);
        opp.Deal_Status__c='Deal Lost';
       	opp.Loss_Reason__c='Couldn\'t Agree on Price';
       opp.Denied_By__c='Brian Govberg';
       opp.Lead_Source__c='Govbergwatches.com - Sell your watch';
       opp.Price_Difference__c='Less than $500';
       opp.Type_of_Transaction_Initial__c='Sale';
        insert opp;
       Watch_Purchase__c  orig = new Watch_Purchase__c();
        orig.Origination_Opportunity__c = opp.id;
        PageReference pageRef = page.OriginationNewPage;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(orig.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(orig);
        OriginationController originationobj  = new OriginationController(sc);
       	originationobj.oppId=opp.id;
        originationobj.origination=orig;
      	originationobj.save();
     
       
        
    }
}