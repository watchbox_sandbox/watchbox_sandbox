@isTest
public class sgTestProductChannelsController {

    static testmethod void getProductChannels() {
        //arrange
        //
        String ids = addProducts(2);
        setupRequestContext(ids);
        
        //act
        //
        List<sgProductChannelsDTO> productChannels = sgProductChannelsController.get();
        
        //assert
        //
        System.assert(productChannels.size() == 2);
        System.assert(productChannels[0].Channels.size() == 2);
    }
    
    static void setupRequestContext(String products) {
        RestContext.request = new RestRequest();
        RestContext.request.params.put('products', products);
    }
    
    static String addProducts(Integer count) {
        String ids = '';
        //loop `count` times
        for(Integer i = 0; i < count; i++) {
            //create the product
            Product2 product = addProduct();
            //add some channels
            addChannelInfo(product, 'Watch U Want', 'Watchbox', true, false);
            addChannelInfo(product, 'Govberg', 'Govberg', false, true);
            ids += (product.Id + '').left(15) + ',';
        }
        return ids.left(ids.length()-1);
    }
    
    static Product2 addProduct() {
        Product2 product = new Product2(
            Name = 'name'
        );
        Database.insert(product);
        return product;
    }
    
    static void addChannelInfo(Product2 product, String name, String channel, Boolean visible, Boolean post) {
        ChannelPost__c chpost = new ChannelPost__c(
            Name = name,
            Product__c = product.Id,
            Channel__c = channel,
            Post__c = post,
            Visible__c = visible
        );
        Database.insert(chpost);
    }
}