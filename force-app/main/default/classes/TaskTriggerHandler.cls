/*
Task Trigger handler for generating deal for call-rail,intercom. Email Notifications.
*/
public class TaskTriggerHandler extends TriggerHandler {

    public static Boolean codeCoverage = false;
    public LeadGenLog_Settings__c Settings {get;set;}
    public Set<Id> leadToClone = new Set<Id>();
    public Map<Id, Account> accountMap = new Map<Id,Account>();
    public Map<Id, Account> dealAccountMap = new Map<Id,Account>();
    public Map<Id, Account> oppAccountMap = new Map<Id,Account>();
    Map<Id, Contact> contactMap = new Map<Id,Contact>();
    Map<Id, Lead> leadMap = new Map<Id,Lead>();
    Set<Id> whoIds = new Set<Id>();
    Set<Id> whoContactIds = new Set<Id>();
    Set<Id> whoLeadIds = new Set<Id>();
    public override void beforeInsert() {
          Settings = getLeadSettings();
        System.debug('beforeInsert***');
        Map<Id, Account> accountUpdateMap = new Map<Id, Account>();
        Map<Id, Opportunity> opportunityUpdateMap = new Map<Id, Opportunity>();
        Map<Id, Contact> contactUpdateMap = new Map<Id, Contact>();        
        Set<Id> whatIds = new Set<Id>();
        // Set<Id> DealWhatId = new Set<Id>();
        List<Task> taskToClone = new List<Task> (); 
        List<Task> taskToCloneForIntercom = new List<Task> (); 
        List<Opportunity> intercomCloneOpportunity = new List<Opportunity>();
        
        Map<Id,List<Task>> leadTaskMap = new Map<Id,List<Task>>(); //Lead map
        String whoid = '';
        String whatid = '';
        for (Task t : (List<Task>)Trigger.new) { 
            System.debug('(List<Task>)Trigger.new)****'+Trigger.new);
            if(t == null)
                continue;
            if (t.WhatId != null)
                whatIds.add(t.WhatId);
            whatid = t.WhatId;
            whoid=t.WhoId;
            
            /* Code to check if the activity history has been added from the call-rail ASK*/
            if(whatid !=null && whatid!='' && whatid.contains('001') && (t.Status == 'In Progress') && (t.Subject != Null && t.Subject =='Call')){
                taskToClone.add(t);
            }
            /* Code to check if the activity history has been added from the intercom*/
            
            
            if(whoid !=null && whoid!='' && whoid.contains('003') && (t.Status == 'Completed') && (t.Subject != Null && t.Subject.containsIgnoreCase('Intercom chat with'))) {
                taskToCloneForIntercom.add(t);
            }
           
            
            System.debug('whatIds****'+whatIds);
            if (t.WhoId != null)
                whoIds.add(t.WhoId);
            
        }
        System.debug('taskToCloneForIntercom***'+taskToCloneForIntercom);
        List<Id> leadIds = new List<Id>(); //Lead id's to create deal for
        List<Account> accountRecords = new List<Account>(); //Account id's to craete deal for
        if (!whatIds.isEmpty()) {
            
            accountMap = new Map<Id, Account>([SELECT Id, Last_Activity_Date__c,Lead_Gen_Log_External_Id__c FROM Account WHERE Id IN :whatIds]);                        
            // oppaccountMap = new Map<Id, Account>([SELECT Id, Last_Activity_Date__c,First_Name__c,Lead_Gen_Log_External_Id__c,Last_Name__c,GA_Campaign__c,GA_Content__c,GA_Medium__c,GA_Segment__c,Ga_Source__c,GA_Term__c,GA_Visits__c,Page_ID__c FROM Account WHERE Id IN :DealWhatId]);                        
            if (!accountMap.isEmpty()) {
                for (Task t : (List<Task>)Trigger.new) {
                    if (t.WhatId != null) {
                        Account account = null;
                        //First check in existing list
                        account = accountUpdateMap.get(t.WhatId);
                        
                        //If already in updates
                        if (account == null) {
                            account = accountMap.get(t.WhatId);
                            
                            if (account != null && account.Last_Activity_Date__c != System.today()) {
                                account.Last_Activity_Date__c = System.today(); 
                                accountUpdateMap.put(account.Id, account);
                            }                                
                        }
                    }                    
                }
                //Update accounts
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
            
            Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Last_Activity_Date__c, Account.Id, Account.Last_Activity_Date__c 
                                                                            FROM Opportunity WHERE Id IN :whatIds]);
            accountUpdateMap = new Map<Id, Account>();
            if (!opportunityMap.isEmpty()) {
                for (Task t : (List<Task>)Trigger.new) {
                    if (t.WhatId != null) {
                        Opportunity opp = null;
                        //First check in existing list
                        opp = opportunityUpdateMap.get(t.WhatId);
                        
                        //If already in updates
                        if (opp == null) {
                            opp = opportunityMap.get(t.WhatId);
                            
                            if (opp != null && opp.Last_Activity_Date__c != System.today()) {
                                opp.Last_Activity_Date__c = System.today();                                 
                                opportunityUpdateMap.put(opp.Id, opp);
                                
                                if (opp.Account != null && opp.Account.Last_Activity_Date__c != System.today()) {
                                    Account account = new Account
                                        (
                                            Id=opp.Account.Id, 
                                            Last_Activity_Date__c = System.today()
                                        );
                                    
                                    accountUpdateMap.put(account.Id, account);
                                }
                            }                                
                        }
                    }                    
                }
                
                //Update opportunities
                if (!opportunityUpdateMap.isEmpty())
                    update opportunityUpdateMap.values();
                
                //Update accounts for opportunity touches
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }   
            List<Opportunity> cloneOpportunity = new List<Opportunity>();
            
            
            //Insert deal created from the account records 
            for(Task TaskRecord : taskToClone) {
                Opportunity opportunity=createDeal(TaskRecord);
                cloneOpportunity.add(opportunity);
            }
            
            
            System.debug('cloneOpportunity after insert****'+cloneOpportunity);
            if(cloneOpportunity.size() > 0)
                insert cloneOpportunity;
            
            
        }  
        
        if (!whoIds.isEmpty()) { 
            
            Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, Last_Activity_Date__c, Account.Id, Account.Last_Activity_Date__c 
                                                                FROM Contact WHERE Id IN :whoIds]);
            accountUpdateMap = new Map<Id, Account>();
            
            if (!contactMap.isEmpty()) {
                for (Task t : (List<Task>)Trigger.new) {
                    if (t.WhoId != null) {
                        Contact c = null;
                        //First check in existing list
                        c = contactUpdateMap.get(t.WhoId);
                        
                        //If already in updates
                        if (c == null) {
                            c = contactMap.get(t.WhoId);
                            
                            if (c != null && c.Last_Activity_Date__c != System.today()) {
                                c.Last_Activity_Date__c = System.today();                                 
                                contactUpdateMap.put(c.Id, c);
                                
                                if (c.Account != null && c.Account.Last_Activity_Date__c != System.today()) {
                                    Account account = new Account
                                        (
                                            Id=c.Account.Id, 
                                            Last_Activity_Date__c = System.today()
                                        );
                                    
                                    accountUpdateMap.put(account.Id, account);
                                }
                            }                                
                        }
                    }                    
                }
                
                //Update contacts
                if (!contactUpdateMap.isEmpty())
                    update contactUpdateMap.values();
                
                //Update accounts for contact touches
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
            /* create new Deal for the Intercom activity history */
            system.debug('--whoids----'+whoIds);
            
            for(Task TaskRecord : taskToCloneForIntercom) {
                Opportunity opportunity = createDeal(TaskRecord);
                intercomCloneOpportunity.add(opportunity);
            }
            
            System.debug('intercomCloneOpportunity after insert****'+intercomCloneOpportunity);
            if(intercomCloneOpportunity.size() > 0)
                insert intercomCloneOpportunity;
            System.debug('intercomCloneOpportunity after insert****'+intercomCloneOpportunity);
            
        }
    } 
    /* Generic method to create new Deal (CallRail/Intercom)  */
    public Opportunity createDeal(task taskObj){
        String whoid='';
        
        Opportunity opp = new Opportunity();
        for (Task t : (List<Task>)Trigger.new) { 
            whoid=t.WhoId;
            if (whoid != null && whoid.contains('003')){
                whoContactIds.add(whoid);
            }
        }
        
        contactMap = new Map<Id, Contact>([SELECT Id, name,AccountId,Account.Lead_Source__c FROM Contact WHERE Id In :whoContactIds]);
        if(!whoContactIds.isEmpty()){
            
            opp.Name = contactMap.get(taskObj.WhoId).name;
            opp.Lead_Source__c = 'Intercom Chat';
            opp.AccountId=contactMap.get(taskObj.WhoId).AccountId;
            opp.Initial_Deal_Details__c=taskobj.Description;
            if (!string.isBlank(Settings.Intercom_Intercom_Id__c)) {
            opp.OwnerId=settings.Intercom_Intercom_Id__c;
            }
        }
        
        
        else{
            system.debug('inside call rail condition');
            opp.Name =taskObj.LastName__c;
            String LandingPage = taskObj.Description;
            if(LandingPage != null && LandingPage != '' && LandingPage.contains('clickid')) {
                String landingUrl = EncodingUtil.urlDecode(LandingPage,'UTF-8');
                System.debug('landingUrl*****'+landingUrl);
                List<String> params = landingUrl.split('&');
                System.debug('params*****'+params);
                Map<String, String> mapData = new Map<String, String>();
                for (String param : params)
                {
                    String name = param.split('=')[0];
                    String value = param.split('=')[1];
                    System.debug('name***'+name);
                    System.debug('value***'+value);
                    mapData.put(name, value);
                }
                System.debug('mapData***'+mapData); 
                opp.irclickid__c = mapData.values()[0];
                opp.ircid__c = mapData.get('ircid');
                opp.irtrackid__c = mapData.get('irtrackid');
                opp.irpid__c = mapData.get('irpid');
                opp.Lead_Source__c = 'Call_Rail_ImpactRadius';
            }  else if(taskObj.utm_campaign__c!=null && taskObj.utm_campaign__c.containsIgnoreCase('Impact Radius Affiliate - Broad')){
                system.debug('inside call railDirect  condition');
                opp.Lead_Source__c = 'Call_Rail Impact Direct';
            }else {
                opp.Lead_Source__c = 'Call_Rail';
            }
            opp.AccountId = taskObj.WhatId;
            System.debug('opp******data******'+opp);
        } 
        
        opp.CloseDate = System.today();
        opp.StageName = 'DIP';
        opp.GA_Campaign__c = taskObj.utm_campaign__c;
        opp.GA_Medium__c = taskObj.utm_medium__c;
        opp.GA_Source__c = taskObj.utm_source__c;
        opp.Source__c = taskObj.source_name__c;
        opp.GA_Term__c = taskObj.utm_term__c;
        opp.GA_gclid__c = taskObj.gclid__c;
        opp.Referring_URL__c = taskObj.referring_url__c;
        
        
        return opp;
        
    }
     @TestVisible
    private LeadGenLog_Settings__c getLeadSettings() {
        LeadGenLog_Settings__c settings = LeadGenLog_Settings__c.getValues('LeadTrigger');  
        
        return settings;
    }
}