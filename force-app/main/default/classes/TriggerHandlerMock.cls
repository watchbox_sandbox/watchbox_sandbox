@isTest
public class TriggerHandlerMock extends TriggerHandler {
    
    public void TestBaseMethods(){
        beforeInsert();
        afterInsert();
        beforeUpdate();
        afterUpdate();
        beforeDelete();
        afterDelete();
    }     
}