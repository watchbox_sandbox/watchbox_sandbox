@isTest
public class sgTestPrintingRestController {

    static testMethod void get() {
        //arrange
        //
        Intake__c intake = createIntake();
        createOrg('Govberg');
        
        RestContext.request = new RestRequest();
        RestContext.request.params.put('id', intake.Id);
        RestContext.request.params.put('templateName', 'intake');
        RestContext.request.params.put('organization', 'Govberg');
        
        //act
        //
        String result = sgPrintingRestController.get();
        
        //assert
        //
        System.assertNotEquals(null, result);
    }
    
    //**********************************************************
    // helper methods
    
    static Intake__c createIntake() {
        Intake__c intake = new Intake__c (
        	
        );
        insert intake;
        return intake;
    }
    
    static void createOrg(String name) {
        Organization__c org = new Organization__c(
        	Name = name
        );
        insert org;
    }
    
}