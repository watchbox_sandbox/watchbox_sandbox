public class SendEmailTrigger_ClayHandler {
    
    public void sendEmailOnAccountOwnerChange(List<Account> listAccount,Map<Id,Account> accountMap){
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<ID>ownerids=new List<ID>();
        List<ID>accids=new List<ID>();
        List<String> sendCC = new List<String>();
        List<String> sendTo = new List<String>();
        Map<Id,User> mapUsers=new Map<Id,User>();
        List<User> newAccUsers=new List<User>();
        List<User> accusers=new List<User>();
        Account oldAccount;
        
        for (Account newAccount : listAccount ) {
            if(accountMap.containsKey(newAccount.Id)){
                oldAccount =accountMap.get(newAccount.Id);
            }
            if (oldAccount != null && newAccount.ownerid != oldAccount.ownerid && oldAccount.OwnerId==Label.ClayUserID){
                ownerids.add(newAccount.ownerid);  
                accids.add(newAccount.id);
                sendTo.add(newAccount.Email__c);
            }
        }
        system.debug('--ownerids--'+ownerids);
        system.debug('--accids--'+accids);
        if(ownerids != null && ownerids.size()>0){
            
            newAccUsers=[select name,id,email,firstName,phone,MobilePhone from user where id in:ownerids];
            system.debug('-------------newAccUsers------'+newAccUsers);
            if(newAccUsers != null && newAccUsers.size()>0){
                for(User u : newAccUsers){
                    sendCC.add(u.Email);
                    mapUsers.put(u.id,u);
                }
            }
        }
        List<String> bccEmails= new List<String>();
        bccEmails.add('rafaeld@govbergwatches.com');
        
        if(mapUsers!=null){
            for(User u: mapUsers.values()){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSenderDisplayName('Clay Cooper');
                mail.setSubject('A message from Clay Cooper at Govberg WatchBox');
                
                String body= 'To my valued clients,<br><br> <p>I am writing to inform you that effective today, I will be leaving Govberg Jewelers to pursue a new career in a new industry..<p></br><p>While I am excited to begin this new chapter in my life, a big part of me is sad to be saying goodbye to the amazing friends and clients that I have had the pleasure of serving over the past years.</p></br><p> Rest assured you will continue to receive the same white glove service and attention you deserve. My esteemed colleague '+mapUsers.get(u.id).name+' will be taking over your account and I am extremely confident that you will get along wonderfully.'+u.FirstName+' is extremely knowledgeable and shares our same passion for fine timepieces. In the next couple of days '+u.FirstName+ ' will be reaching out to you via phone or email to introduce themselves  and help facilitate any pending or new watch needs that you may have. </p></br><p>It has been a true pleasure serving you, and I wish you all the best!</p></br><p>I have CC’ed '+u.FirstName+' on this email and have included all of their contact information below:</p></br>'+u.name+'<br/>';
                //  if(u.MobilePhone!=null)
                //   	body =body +'Cell: '+u.MobilePhone+'<br/>';
                if(u.phone!=null)
                    body=body+'Office: '+u.phone+'<br/>';
                if(u.Email!=null)
                    body= body+ 'Email: '+u.email+'<br/><br/>';
                body=body+'Warmest Regards,<br/><br/><br/>Clay Cooper';
                mail.setToAddresses(sendTo);
                mail.setHtmlBody(body);
                mail.setBccAddresses(bccEmails);
                mail.setCCAddresses(sendCC);
                mails.add(mail);
                try{
                    Messaging.sendEmail(mails);
                }
                catch(Exception e){
                    system.debug('-------------exception------'+e);
                }
            }
        }
        
        
    }
    
    
}