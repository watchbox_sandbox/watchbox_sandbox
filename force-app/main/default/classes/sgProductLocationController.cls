@RestResource(urlMapping='/ProductLocation')
global class sgProductLocationController {

    @HttpGet
    global static List<sgProductLocationDTO> get() {
        List<sgProductLocationDTO> prodLocs = new List<sgProductLocationDTO>();
        
        //get the locations
        List<Location__c> locations = [SELECT Id, Name FROM Location__c WHERE Name LIKE 'Govberg%' OR Name LIKE 'WatchUWant%'];
        
        //add each location to the string list
        for (Location__c location : locations) {
            prodLocs.add(translateToProductLocation(location));
        }

        return prodLocs;
    }
    
    static sgProductLocationDTO translateToProductLocation(Location__c location) {
        sgProductLocationDTO product = new sgProductLocationDTO();
        product.Id = location.Id;
        product.Location = location.Name;
        
        return product;
    }
}