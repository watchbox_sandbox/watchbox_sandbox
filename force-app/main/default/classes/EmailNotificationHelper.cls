global class EmailNotificationHelper Implements Schedulable
{
    
    //List of users to which email is sent
    public static List<String> listofUsersToEmail;
    
    global void execute(SchedulableContext sc)
    {	
        try{
            sendMailForDueTasks();
        }
        catch (Exception e) {
            System.debug('Error during EmailNotificationHelper - ' + e.getMessage());
        }
    }
    
    public void sendMailForDueTasks()
    {	
        
        Date todayDate = Date.today() ;        
        List<Task> taskList= [SELECT Id,ActivityDate,whoID,Owner.Name,Subject,Status,WhatId FROM Task WHERE WhatId IN (SELECT Id FROM Opportunity) AND Status='Open' and ActivityDate = :todayDate];                  
        Map<ID,List<Task>> taskNOppMap = new Map<ID,List<Task>>();
        //List<Task> listOfTasks=new List<task>();
        for(Task task:taskList){
            if(!taskNOppMap.containskey(task.WhatId)){
                taskNOppMap.put(task.WhatId, new list<task>{task});
            }
            else{
            	list<Task> listOfTasks = taskNOppMap.get(task.WhatId);
            	listOfTasks.add(task);            
            	taskNOppMap.put(task.WhatId,listOfTasks);
            }
        }
        //List<Opportunity> dealsList=[Select ID,Account_Email4templates__c,Owner.Name,Account.Name from Opportunity where ID=:taskNOppMap.keySet()];        
        //List<Opportunity> oppList= new List<Opportunity>();        
        Map<Id,List<Opportunity>> UserDealsMap = new Map<Id,List<Opportunity>>();
        for(Opportunity opp: [Select ID,Owner.ID,Name,Account.Name from Opportunity where ID=:taskNOppMap.keySet()]){
            if(!UserDealsMap.containskey(opp.Owner.ID)){
                UserDealsMap.put(opp.Owner.ID, new List<Opportunity>{opp});
            }
            else{
            	list<Opportunity> listOfOpps = UserDealsMap.get(opp.Owner.ID);
            	listOfOpps.add(opp);            
            	UserDealsMap.put(opp.Owner.ID,listOfOpps);
            }
        }
        
        OrgWideEmailAddress owe = [SELECT ID,IsAllowAllProfiles,DisplayName,Address FROM OrgWideEmailAddress WHERE Address = 'noreply@govbergwatches.com' LIMIT 1];
        List<Messaging.SingleEmailMessage> taskDueEmailList = new List<Messaging.SingleEmailMessage>();                
        //for(ListofUsers who are opp owners){}
        for(User oppOwner: [Select ID,Email,Name,Profile.Name from User where ID=:UserDealsMap.keySet() and Profile.Name ='Sales']){            
        	Messaging.SingleEmailMessage taskDueEMail = new Messaging.SingleEmailMessage();    
            System.debug('Users'+UserDealsMap.keySet());
            System.debug('Opps'+UserDealsMap.get(oppOwner.id));
            if(oppOwner.Email!=null){
                listofUsersToEmail =new List<String>{oppOwner.Email};
            }
            System.debug('listofUsersToEmail*****'+listofUsersToEmail);
            if(!listofUsersToEmail.isEmpty()){
                taskDueEMail.setToAddresses(listofUsersToEmail);                
            }
            taskDueEMail.setSubject('These are your SalesForce Tasks due today');
            String body = '<body class="setupTab" ><style background-color="#CCCCCC" bEditID="b1st1" bLabel="body" ></style><center ><table border="1" cellpadding="0" width="500" cellspacing="0" id="topTable" height="400" ><tr valign="top"><td align="center"><style background-color="#FFFFFF" bEditID="r1st1" bLabel="header" vertical-align="middle" height="2" text-align="center" ></style><img border="0" bEditID="r1sp1" bLabel="headerImage" id="r1sp1" src="https://govberg--full--c.cs97.content.force.com/servlet/servlet.ImageServer?id=0155000000302rG&oid=00D0U0000008eZk"></img></td></tr><tr><td>';                  
            body += 'Hello ' + oppOwner.Name + ',<br>';
            body += '<br>The following tasks are due today: <br>';
            for(Opportunity opp: UserDealsMap.get(oppOwner.id)){ 
                System.debug('tasks'+taskNOppMap.get(opp.id));
                body += '<br>'  + opp.Name+ '-' + opp.Account.Name +'- '+system.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.ID+'<br>';
                //List<task> tasks=taskNOppMap.values()            
                for(task t:taskNOppMap.get(opp.id)){
                    body += '<ul>' + '<li><a href='+system.URL.getSalesforceBaseUrl().toExternalForm()+'/'+t.ID+'>'+t.subject+'</a></li></ul>';
                }                                            
            }
            body += '</td></tr></table></center></body> ' ;
            taskDueEMail.setHtmlBody(body);
            taskDueEMail.setOrgWideEmailAddressId(owe.Id);
            taskDueEmailList.add(taskDueEMail);
            System.debug('taskDueEmailList'+taskDueEmailList);
        }
        if(!taskDueEmailList.isEmpty()){
            Messaging.sendEmail(taskDueEmailList); 
        }
    }
}