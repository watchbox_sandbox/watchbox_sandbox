public class WatchOriginationTriggerHandler {
    public static Boolean isFirstTime = true;
    public static Boolean isFirstTimeDelete = true;
    public static void countRLonOpportunityUpdate(List<Watch_Purchase__c> watchP){
        try{
            System.debug('watchP------'+watchP);
            Set<Id> oppIds = new Set<Id>();
            for(Watch_Purchase__c wp :watchP){
                if(wp.Origination_Opportunity__c!=null){
                    oppIds.add(wp.Origination_Opportunity__c);
                }
            }        
            map<Id,Double> oppWBMap = new map <Id,Double>(); 
            List<Watch_Purchase__c> wpListForSelect= new List<Watch_Purchase__c>();
            for(Opportunity opp:[Select Id, (Select Id from Origination_Records__r) from Opportunity where id=:oppIds]){
                for(Watch_Purchase__c wOrig:opp.Origination_Records__r){
                    wpListForSelect.add(wOrig);
                    System.debug(wOrig);
                }       
            }
            System.debug('wpListForSelect**************'+wpListForSelect);
            for(AggregateResult ar : [SELECT Origination_Opportunity__r.Id dealID,sum(WB_Preferred_Offer__c) totalWBoffer FROM Watch_Purchase__c where Id= :wpListForSelect group by Origination_Opportunity__r.Id]){
                oppWBMap.put((Id)ar.get('dealID'),(Double)ar.get('totalWBoffer'));//change
            } 
            List<Opportunity> oppListToupdate= new List<Opportunity>();
            for(Opportunity opp: [Select ID,Watch_Sale_Records_Count__c,Watch_Origination_Records_Count__c,(Select Id from Watch_Sales__r),(Select Id from Origination_Records__r) from Opportunity where ID=:oppIds]){
                opp.Watch_Sale_Records_Count__c= opp.Watch_Sales__r.size();
                opp.Watch_Origination_Records_Count__c= opp.Origination_Records__r.size();
                opp.Total_WB_Offer_Watch_Originations__c=oppWBMap.get(opp.Id)==null?0:oppWBMap.get(opp.Id) ;
                System.debug('opp.Total_WB_Offer_Watch_Originations__c'+opp.Total_WB_Offer_Watch_Originations__c);
                if(opp.Watch_Sale_Records_Count__c!=null && opp.Watch_Sale_Records_Count__c!=null){
                    if(opp.Watch_Sale_Records_Count__c > 0 && opp.Watch_Origination_Records_Count__c == 0){
                        opp.Type_of_Transaction_Final__c='Sale';
                    }
                    if(opp.Watch_Sale_Records_Count__c == 0 && opp.Watch_Origination_Records_Count__c > 0){
                        opp.Type_of_Transaction_Final__c='Origination';
                    }
                    if(opp.Watch_Sale_Records_Count__c > 0 && opp.Watch_Origination_Records_Count__c > 0){
                        opp.Type_of_Transaction_Final__c='Trade';
                    }
                }
                oppListToupdate.add(opp);
            }
            if(!oppListToupdate.isEmpty()){
                update oppListToupdate; 
            }
        }
        catch (Exception e) {
            System.debug('Error during WatchOriginationTriggerHandler - ' + e.getMessage());
            for (Watch_Purchase__c record : (List<Watch_Purchase__c>)Trigger.new) {
                System.debug('Error during WatchOriginationTriggerHandler - ' + e.getLineNumber());
                record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
            }   
        }
    }
}