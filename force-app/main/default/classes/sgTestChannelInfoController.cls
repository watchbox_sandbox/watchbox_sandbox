@isTest(SeeAllData=true)
public class sgTestChannelInfoController {

    static testMethod void getChannelInfo() {
        //arrange
        List<String> channelNames = sgUtility.getPicklistValues('ChannelPost__c', 'Channel__c');
        sg_resources__c res = sg_resources__c.getValues('Web Resources');
        
        //act
        List<sgChannelInfoDTO> channels = sgChannelInfoController.get();
        
        //assert
        system.assertEquals(channelNames.size(), channels.size());
        system.assert(channels[0].ImageUrl.contains(res.Url__c));
    }
    
}