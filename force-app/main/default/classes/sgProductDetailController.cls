@RestResource(urlMapping='/ProductDetails/*')
global class sgProductDetailController {
    @HttpGet
    global static sgProductDetail get() {
        sgAuth.check();
        
        List<string> columns = new List<string> {'Id',
            'Age__c',
            'ASK__c',
            'Boxes__c',
            'Condition__c',
            'Consignment_Date__c',
            'Cost__c',
            'CreatedDate',                
            'Date_On_Warranty_Card__c',
            'Days_Inventoried__c',
            'Description',
            'Hold_Byline__c',
            'Inventory_ID__c',
            'Inventory_Type__r.Name',
            'LastModifiedDate',
            'Location__r.Name',
            'Model__c',
            'Model__r.Case_Material__c',
            'Model__r.Case_Size__c',
            'Model__r.Model_Brand__r.Name',
            'Model__r.Model_Write_Up__c',
            'Model__r.Movement_Type__c',
            'Model__r.Name',
            'Model__r.Reference__c',
            'Model__r.SH_Watch_ID__c',
            'MSP__c',
            'Name',
            'Original_Dealer_Name__c',
            'Origination_Type__c',
            'Papers__c',
            'Posted_On_WUW__c',
            'Product_Type__c',
            'Retail__c',
            'Status__c',
            'Watch_Brand__r.Name',
            'WUW_eBay_Listing_ID__c',    
            'WUW_Date_Created__c',
            'WUW_MSP_Date__c',
            'WUW_Notes__c',
            'WUW_Sold_Price__c',
            'WUW_URL_Alias__c',
            'AWS_Web_Images__c'
       	};
           
        string query = 'select ' + string.join(columns, ', ') + ' from Product2';

        Map<string, string> params =  RestContext.request.params.clone();
        
        if (!params.containsKey('limit')) {
            params.put('limit', '1');
        }
        
        sgProductDetail sg = new sgProductDetail();

        sg.Fields = sgSerializer.serializeFromQuery(query, params).get(0);
        
        logs(sg);
        notes(sg);
        intakeInspections(sg);
		video(sg);
        posting(sg);
        landedCosts(sg);
        
        return sg;
    }
    
    private static string queryId(sgProductDetail sg) {
        return '\'' + string.escapeSingleQuotes(sg.Fields.get('Id')) + '\'';
    }
    
    private static void logs(sgProductDetail sg) {
    	List<Map<string, string>> log = sgSerializer.serializeFromQuery(
            'select Field__c, NewValue__c, OldValue__c, Author__c, CreatedDate from Log__c where Product__c = ' + queryId(sg) + ' order by CreatedDate asc'
        );
        sg.Log = log;
        
        //set the last updated field based on the log if there is one
        if (log.size() > 0) {
            sg.Fields.put('LastModifiedDate', log.get(log.size() - 1).get('CreatedDate'));
        }
    }
    
    private static void notes(sgProductDetail sg) {
        List<Map<string, string>> notes = sgSerializer.serializeFromQuery(
            'select body from note where ParentId = ' + queryId(sg) + ' order by CreatedDate asc'
        );
        sg.Notes = notes;
    }
    
    private static void intakeInspections(sgProductDetail sg) {
        List<string> intakeFields = new List<string>{
            'Id',
            'Strap_Good__c',
            'Boxes_Good__c',
            'Straight_To_Post__c',
            'Warranty_Card_Date__c',
            'Time_Completed__c',
            'Intake_Name__c'
		};
            
        List<string> inspectionFields = new List<string> {
            'Id',
        	'Parts_Needed__c',
            'Overhaul_Needed__c',
            'Outsource_Work__c',
            'Type_Of_Repair_Needed__c',
            'Total_Cost_Of_Repairs__c',
            'TimeInspected__c',
            'Inspection_Name__c'
        };
        Product2 ii = (Product2) Database.query('select Id, (select ' + string.join(intakeFields, ', ') + 
            ' from Intakes__r), (select ' + string.join(inspectionFields, ', ') + ' from Inspections__r)' + 
            ' from Product2 where id = ' + queryId(sg)).get(0);
        
        if (ii.Intakes__r.size() > 0) {
            for (string f : intakeFields) {
                string mapF = f.replace('.', '');
                string value = string.valueOf(ii.Intakes__r.get(0).get(mapF));
                if (mapF == 'Id') {
                    mapF = 'Intakes__rId';
                }
                sg.Fields.put(mapF, value);
            }
        }
        
        if (ii.Inspections__r.size() > 0) {
            for (string f : inspectionFields) {
                string mapF = f.replace('.', '');
                string value = string.valueOf(ii.Inspections__r.get(0).get(mapF));
                if (mapF == 'Id') {
                    mapF = 'Inspections__rId';
                }
                sg.Fields.put(mapF, value);
            }
        }
    }
    
    private static void video(sgProductDetail sg) {
        try {
            Video__c v = [select YTVideoID__c from Video__c where Watch_Model__c = :sg.Fields.get('Model__c') limit 1];
            sg.Fields.put('Video_URL__c', 'https://www.youtube.com/embed/' + v.YTVideoID__c);
        } catch (Exception e) {
            
        }
    }
    
    private static void posting(sgProductDetail sg) {
        List<string> fields = new List<string>{'Id', 
            'Channel__c', 
            'External_ID__c', 
            'External_Id_2__c', 
            'External_ID_Name__c', 
            'External_ID_2_Name__c'
        };

        try {
        	List<ChannelPost__c> posts = Database.query('select ' + string.join(fields, ', ') + ' from ChannelPost__c where Product__c = ' + queryId(sg));
			
			sg.Posts = new List<sgProductDetail.Post>();
            
            for (ChannelPost__c p : posts) {
                if (p.Channel__c == 'Ebay') {
                    sg.Fields.put('Ebay_ID__c', p.External_ID__c);
                }
                
                sgProductDetail.Post post = new sgProductDetail.Post();
                post.Name = p.Channel__c;
               	post.ExternalId = p.External_Id__c;
                
                sg.Posts.add(post);

            }
        } catch (Exception e) {}
    }
    
    private static void buyDowns(sgProductDetail sg) {
        integer count = Database.countQuery('select count() from Buy_Down__c where ');
        sg.Fields.put('Buy_Downs__c', string.valueOf(count));
    }
    
    private static void landedCosts(sgProductDetail sg) {
        string inventoryId = queryId(sg);
        AggregateResult[] ar = Database.query('select sum(Expense_Cost__c) cost from Landed_Cost__c where InventoryID__c = ' + inventoryId);

        sg.Fields.put('Landed__c', string.valueOf(ar[0].get('cost')));
    }
}