global class sgBatchStatusUpdateDTO {
    global String inventoryId {get;set;}
    global Double cost {get;set;}
    global Decimal daysIn {get;set;}
    global String salesPerson {get;set;}
    global Decimal dayIn {get;set;}
    global String origination {get;set;}
    global string brandName {get; set;}
    global string reference {get; set;}
    global string name {get; set;}
    global string mainImage {get; set;}
    global string status {get; set;}
    global string productId {get; set;}
}