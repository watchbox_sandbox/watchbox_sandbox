global class sgGovbergBatchMagentoSync implements Database.Batchable<sObject>, Database.AllowsCallouts, Queueable{
    global final string query = 'select Id, Product__c from GB_API_Queue__c where Product__r.Model__r.Model_Series__c != null';

    global Database.QueryLocator start(Database.BatchableContext context){
        return Database.getQueryLocator(query);
    }

    global final static string endpoint = '/rest/V1/salesforce/product/sync';
    global void execute(Database.BatchableContext context, List<GB_API_Queue__c> queue){
        List<string> ids = sgUtility.extract('Product__c', queue);
        
       	List<MagentoProductDTO> dtos = MagentoProductUpdate.productsToDTO(ids, true);
        
        sgPostingIntegrationWatchboxHk hk = new sgPostingIntegrationWatchboxHk();
        for (MagentoProductDTO p : dtos) {
            p.websiteId = hk.getWebsiteId();
            
            if (p.photos != null && p.photos.size() > 1) {
            	string first = p.photos[0];
                p.photos[0] = p.photos[1];
                p.photos[1] = first;
            }
        }
        
        system.debug(dtos.size());
        system.debug(dtos.get(0));
        
        String payload = MagentoProductUpdate.productDTOsToDTOJson(dtos);

        sgEndpoint mage = new sgEndpoint('Watchbox Hong Kong', 'Watchbox');
		
        if (!Test.isRunningTest()) {
            mage.Authenticate();
            
            mage.Send(endpoint, 'POST', Blob.valueOf(payload), null, null);
        }
    }
    
    public static void StartBatch() {
        System.enqueueJob(new sgGovbergBatchMagentoSync());
    }
    
    public void execute(QueueableContext context) { 
		setting.Magento_Batch_ID__c = Database.executeBatch(new sgGovbergBatchMagentoSync(), integer.valueOf(Setting.Magento_Batch_Size__c));
        update setting;
    }

    global static sgSettings__c settingP;
    global static sgSettings__c Setting {
        get {
            if (settingP == null) {
                string env = sgUtility.isSandbox() ? 'surge' : 'watchdms';
                
                settingP = [select Govberg_Chunk_Size__c, Govberg_Current_Page__c, Magento_Batch_Id__c, Magento_Batch_Size__c from sgSettings__c where Name = :env limit 1];
            }
            
            return settingP;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        //clear out the queue
        delete [select Id from GB_API_Queue__c];
        
        //and the batch id so sgGovbergJsonHandler can resume
        setting.Magento_Batch_ID__c = null;
        update setting;
    }
}