global class OpportunityNewPageController {
    
    private ApexPages.StandardController myStdController;
    public String queryString {get;set;}
    public String name {get;set;}
    public Opportunity opp {get;set;}
    public List<Schema.FieldSetMember> fieldName {get;set;}
    public Schema.FieldSetMember fieldList {get;set;}
    public Boolean demo {get;set;}
    public Boolean profileFlag {get;set;}
    public Boolean approvalStatusFlag {get;set;}
    public String attributeValue {get;set ;}
    public boolean watchPurchasedValue {get;set;}
    public Double expectedCost{get;set;}
    public Double currentCost{get;set;}
    public String selectedApprover{get;set;}
    public Double approvedAmount{get;set;}
    //Added by Kirti
    public List<Watch_Sale__c> memberAddList {get;set;}
    public Integer count {get;set;} 
    public Integer oCount {get;set;}
    public Watch_Sale__c wc;
    public List<WatchSaleWrapper> watchSaleWrapperList {get;set;}
    public List<OriginationWrapper> originationWrapperList {get;set;}
    public String delegatedUser;
    public Id oppId;
    
    public List<SelectOption> getApproverOptions() {
        List<SelectOption> countryOptions = new List<SelectOption>();
        countryOptions.add(new SelectOption('-None-','-None-'));
        countryOptions.add(new SelectOption('Shannon Beck','Shannon Beck'));
        countryOptions.add(new SelectOption('Mike Manjos','Mike Manjos'));
        countryOptions.add(new SelectOption('George Mayer','George Mayer'));
        countryOptions.add(new SelectOption('Brian Govberg','Brian Govberg'));
        countryOptions.add(new SelectOption('Josh Thanos','Josh Thanos'));
        
        return countryOptions;
    }
    
    public void ApproverAmountSection() {
        opp.Approver__c=selectedApprover;
        if(selectedApprover == '-None-'){
            selectedApprover = 'none' ;  
        }
        
        system.debug('---***---'+selectedApprover);
    }
    
    public OpportunityNewPageController(ApexPages.StandardController controller){


        approvalStatusFlag=false;
        User u = [select Id, username,Profile.name from User where Id = :UserInfo.getUserId()];
        system.debug('u details'+u.profile.name);
        if(u.Profile.name.EqualsIgnoreCase('Administrator')){
            approvalStatusFlag=true;
        }else{
            approvalStatusFlag=false;
        }
        System.debug('OpportunityNewPageController******');
        
        fieldName = new List<Schema.FieldSetMember>();
        List<RecordType> recordTypeNameList=new List<RecordType>();
        opp = (Opportunity) controller.getRecord();
        System.debug(opp);
        if(opp.RecordTypeId!=null){     
            recordTypeNameList=[SELECT name from RecordType where id =:opp.RecordTypeId];
        }
        else{
            Id id = apexpages.currentpage().getparameters().get('opp4_lkid');
            //Id oppId = apexpages.currentpage().getparameters().get('opp4_lkid');
            system.debug('--##--'+id);
            if(id!=null)
                opp.AccountId=id;
            recordTypeNameList=[SELECT name from RecordType where name='Scratch Pad'];
        }
        
        if(recordTypeNameList!=null && recordTypeNameList.size()>0){
            name= recordTypeNameList[0].name;
        }
        system.debug('---'+opp.RecordTypeId);
        opp.StageName = 'DIP';
        opp.CloseDate=Date.today();
        opp.Probability=25;
        String userid = Userinfo.getUserId();
        opp.OwnerId=userid;
        if(opp.id != null && opp.Test__c== null) {
            delegatedUser  = OpportunityNewPageSelector.getOpp(opp.OwnerId);
        }
        
        System.debug(opp.Test__c );
        system.debug(delegatedUser);
        if(delegatedUser == null && opp.Test__c== null ) {
            Id delegatedUserId =  OpportunityNewPageSelector.getDelayedManagerId(userid);
            delegatedUser =  OpportunityNewPageSelector.getManagerName(delegatedUserId);
            opp.Test__c = delegatedUser;
        }
        System.debug(opp.Test__c);
        opp.Images__c='';
        opp.Watch_Images__c='';
        myStdController = controller;
        
        PageReference thisPage = ApexPages.currentPage();
        if(thisPage <> NULL && thisPage.getUrl() <> NULL){
            List<String> url = thisPage.getUrl().split('\\?');
            if(url <> NULL && url.size()>2){
                queryString = url[1];
            }
        }
        //Added by Kirti
        
        memberAddList = new List<Watch_Sale__c>();
        count = 1;
        oCount = 1;
        wc = new Watch_Sale__c();
        Watch_Purchase__c oc = new Watch_Purchase__c();
        watchSaleWrapperList = new List<WatchSaleWrapper>();
        originationWrapperList =  new List<OriginationWrapper>();
        
        if(Opp.id != null) {
            List<Watch_Sale__c> watchSaleDetailList = OpportunityNewPageSelector.getWatchSaleList(Opp.id);
            List<Watch_Purchase__c> originateDetailList = OpportunityNewPageSelector.getOriginationList(Opp.id);
            if(watchSaleDetailList != null && watchSaleDetailList.size() > 0) {
                
                for(Watch_Sale__c wathcSaleObj :watchSaleDetailList) {
                    watchSaleWrapperList.add(new WatchSaleWrapper(count,wathcSaleObj));
                    count++;
                    
                }
                count--;
            }else {
                
                watchSaleWrapperList.add(new WatchSaleWrapper(1,wc));
            }
            if(originateDetailList != null && originateDetailList.size() > 0) {
                //integer i = 1;
                for(Watch_Purchase__c originateObj :originateDetailList) {
                    originationWrapperList.add(new OriginationWrapper(oCount,originateObj));
                    oCount++;
                }
                oCount--;
            }else {
                originationWrapperList.add(new OriginationWrapper(1,oc));
            }
            
        }else {
            watchSaleWrapperList.add(new WatchSaleWrapper(1,wc));
            originationWrapperList.add(new OriginationWrapper(1,oc));
        }
    }
    
    public void RenderSec() {

        opp.Is_ref_currently_in_stock__c=demo;
        if(demo == true){
            demo = true ;  
        }
        if(demo == false){
            demo = false ;
        }
        
    }
    public void DiffAttributesSection() {

        opp.Attributes_Differ__c=attributeValue;
        if(attributeValue == 'none'){
            attributeValue = 'none' ;  
        }
        
        system.debug('---***---'+attributeValue);
    }
    
    public void RenderWatchPurchasedSection() {
           system.debug('inside RenderWatchPurchasedSection');
        opp.New_watch_purchased_from_Govberg_WB__c=watchPurchasedValue;
        if(watchPurchasedValue == true){
            watchPurchasedValue = true ;  
        }
        if(watchPurchasedValue == false){
            watchPurchasedValue = false ;
        }
        
    }
    
    public PageReference saveandnew(){
        PageReference pr=null;
        system.debug('approvedAmount---'+approvedAmount);
        system.debug('selectedApprover---'+selectedApprover);
        try {
            if(expectedCost>=currentCost && (opp.Notes_Development__c=='' || opp.Notes_Development__c==null)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Note is required when Expected cost is greater than or equal to Current adjustment Cost'));
            }
            else if(selectedApprover!=null && selectedApprover!='' && selectedApprover!='none' && approvedAmount==null || approvedAmount==0.0 ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please provide approver amount.'));
            }
            else if(opp.Type_of_Transaction_Initial__c != null 
                    && (opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Sale') || opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Trade'))
                    && (watchSaleWrapperList.size() == 0 || watchSaleWrapperList[0].watchSale.Sale_Brand__c == null
                        || watchSaleWrapperList[0].watchSale.Sale_Brand__c == '')
                    && name != null && (name.equalsIgnoreCase('Scratch Pad') || name.equalsIgnoreCase('Deals Standard'))) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide the Sale brand value.'));   
                    }
            else if(opp.Type_of_Transaction_Initial__c != null 
                    && (opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Origination') || opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Trade')) && 
                    (originationWrapperList.size() == 0 || originationWrapperList[0].origination.Origination__c == null
                     || originationWrapperList[0].origination.Origination__c == '') 
                    && name != null && (name.equalsIgnoreCase('Scratch Pad') || name.equalsIgnoreCase('Deals Standard'))) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide the Origination brand value.')); 
                        
                    }
            else{
                opp.Expected_Total_Cost__c=expectedCost;
                opp.Current_Adjusted_Cost__c=currentCost;
                opp.Approved_Amount__c=approvedAmount;
                System.debug(opp.Test__c);
                if(opp.Test__c == null) {
                    opp.Test__c = delegatedUser;
                } 
                upsert opp;
                System.debug(opp);
                //Added by Kirti
                if(opp.id != null){
                    {
                        insertWatchSaleAndOrgList(opp.id);
                    }
                }
                System.debug(opp);
                Schema.DescribeSObjectResult describeResult = myStdController.getRecord().getSObjectType().getDescribe();
                pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e?' + queryString);
                pr.setRedirect(true);
            }
            
            
        } catch(System.DMLException e) {
            Integer numErrors = e.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDmlMessage(i)));
            }
        }
        return pr;  
    }
    public pagereference save(){

        PageReference oppPage=null;
       
        // String recordTypeName = OpportunityNewPageSelector.getRecordTypeName(opp.recordTypeId);
        // System.debug(recordTypeName);
        //if()
        try {
            system.debug(expectedCost>=currentCost);
            system.debug(opp.Notes_Development__c=='' || opp.Notes_Development__c==null);
            system.debug(opp);
            
            if(expectedCost>=currentCost && (opp.Notes_Development__c=='' || opp.Notes_Development__c==null)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Note is required when Expected cost is greater than or equal to Current adjustment Cost'));
            } else if(selectedApprover!=null && selectedApprover!='' && selectedApprover!='none' && approvedAmount==null || approvedAmount==0.0 ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please provide approver amount.'));
            }
            
            else if(opp.Type_of_Transaction_Initial__c != null 
                    && (opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Sale') || opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Trade'))
                    && (watchSaleWrapperList.size() == 0 || watchSaleWrapperList[0].watchSale.Sale_Brand__c == null
                        || watchSaleWrapperList[0].watchSale.Sale_Brand__c == '')
                    && name != null && (name.equalsIgnoreCase('Scratch Pad') || name.equalsIgnoreCase('Deals Standard'))) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide the Sale brand value.'));   
                    }
            else if(opp.Type_of_Transaction_Initial__c != null 
                    && (opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Origination') || opp.Type_of_Transaction_Initial__c.equalsIgnoreCase('Trade')) && 
                    (originationWrapperList.size() == 0 || originationWrapperList[0].origination.Origination__c == null
                     || originationWrapperList[0].origination.Origination__c == '') 
                    && name != null && (name.equalsIgnoreCase('Scratch Pad') || name.equalsIgnoreCase('Deals Standard'))) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide the Origination brand value.'));   
                    }
            else{
                
                opp.Expected_Total_Cost__c=expectedCost;
                opp.Current_Adjusted_Cost__c=currentCost;
                opp.Approved_Amount__c=approvedAmount;
                System.debug(opp.Test__c);
                if(opp.Test__c == null) {
                    opp.Test__c = delegatedUser;
                } 
                /*if(watchSaleWrapperList.size() == 1) {
if((watchSaleWrapperList[0].watchSale.Inventory_ID__c == null || watchSaleWrapperList[0].watchSale.Inventory_ID__c =='')
&& (watchSaleWrapperList[0].watchSale.Sale_Brand__c == null)
&& (watchSaleWrapperList[0].watchSale.Sale_Model__c == null || watchSaleWrapperList[0].watchSale.Sale_Model__c!='')
&& (watchSaleWrapperList[0].watchSale.MSP__c == null || watchSaleWrapperList[0].watchSale.MSP__c ==0.00)
&& (watchSaleWrapperList[0].watchSale.Client_s_Offer__c == null)
&& (watchSaleWrapperList[0].watchSale.Notes__c == null || watchSaleWrapperList[0].watchSale.Notes__c == '')) {
opp.Total_Sale_Watches__c = 0;   
}
}else {
opp.Total_Sale_Watches__c = watchSaleWrapperList.size(); 
}*/
                
                upsert opp;
                //Added by Kirti
                if(opp.id != null){
                    insertWatchSaleAndOrgList(opp.id);
                }
                System.debug(opp);
                oppPage = new ApexPages.StandardController(opp).view();
                oppPage.setRedirect(true);
                
            }
            
        }
        catch(System.DMLException e) {
            Integer numErrors = e.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDmlMessage(i)));
            }
        }
        return oppPage;
    }
    //Added by Kirti
    public void AddRowSale(){
        count = count+1;
        Watch_Sale__c wc = new Watch_Sale__c();
        watchSaleWrapperList.add(new WatchSaleWrapper(count,wc));
        System.debug(watchSaleWrapperList);
    } 
    public void AddRowOriginate(){
        oCount = oCount+1;
        Watch_Purchase__c oc = new Watch_Purchase__c();
        originationWrapperList.add(new OriginationWrapper(oCount,oc));
    }  
    public void insertWatchSaleAndOrgList(Id OppId) {

        List<Watch_Sale__c> watchSaleList = new List<Watch_Sale__c>();
        List<Watch_Purchase__c> orginationList = new List<Watch_Purchase__c>();
        if(watchSaleWrapperList != null && watchSaleWrapperList.size() > 0) {
            
            for(WatchSaleWrapper ws :watchSaleWrapperList) {
                System.debug(ws.watchSale.Sale_Brand__c);
                if((ws.watchSale.Inventory_ID__c == null || ws.watchSale.Inventory_ID__c =='')
                   && (ws.watchSale.Sale_Brand__c == null)
                   && (ws.watchSale.Sale_Model__c == null || ws.watchSale.Sale_Model__c!='')
                   && (ws.watchSale.MSP__c == null || ws.watchSale.MSP__c ==0.00)
                   && (ws.watchSale.Client_s_Offer__c == null)
                   && (ws.watchSale.Notes__c == null || ws.watchSale.Notes__c == '')) {
                       continue;}
                if( ws.watchSale.id == null) {
                    ws.watchSale.Opportunity__c = opp.id;
                }
                watchSaleList.add(ws.watchSale);
                
            }
            if(watchSaleList.size() > 0) {
                upsert watchSaleList;
            }
            
        }
        if(originationWrapperList != null && originationWrapperList.size() > 0) {
            
            for(OriginationWrapper os :originationWrapperList) {
                if(os.origination.Client_s_Desired_Value__c == null 
                   && (os.origination.Attributes_differ_from_original_watch__c == null)
                   && (os.origination.Origination_Model__c == null || os.origination.Origination_Model__c=='')
                   && (os.origination.Origination_Opportunity__c == null)
                   && (os.origination.Origination__c == null)
                   && (os.origination.Origination_Value_Offered__c == null)
                   && (os.origination.Notes__c == null || os.origination.Notes__c == '')
                   && (os.origination.New_watch_purchased_from_Govberg_WB__c == false)
                   && (os.origination.Is_ref_currently_in_stock__c == false)){
                       continue;}
                if( os.origination.id == null) {
                    
                    os.origination.Origination_Opportunity__c = opp.id;
                }
                orginationList.add(os.origination);
                
            }
            if(orginationList.size() > 0) {
                upsert orginationList;
            }
            
        } 
        opp.Total_Sale_Watches__c = watchSaleList.size(); 
        opp.Total_Origination_Watches__c = orginationList.size();
        upsert opp;
    }
    
}