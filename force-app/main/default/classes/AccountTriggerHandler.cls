public class AccountTriggerHandler extends TriggerHandler {
    private static final decimal RANKING_DENOMINATOR = 13;
    
	public override void beforeInsert() { 
        for (Account a : (List<Account>)Trigger.new) {
            calculateRanking(a);
            
            //Format phone numbers
        	/*Following code is commented as per the requirement by Demandware on 11/10/2019
        	 a.Phone = PhoneNumberFormatter.stripCharacters(a.Phone);
            a.Mobile__c = PhoneNumberFormatter.stripCharacters(a.Mobile__c);
            a.Office_Phone__c = PhoneNumberFormatter.stripCharacters(a.Office_Phone__c);
            a.Home_Phone__c = PhoneNumberFormatter.stripCharacters(a.Home_Phone__c);*/
        }               
    }
    
    public override void beforeUpdate() {
        System.debug('Entering AccountTriggerHandler.beforeUpdate');
                
    	for (Account a : (List<Account>)Trigger.new) {
            calculateRanking(a);  
            
            Account oldAccount = (Account)Trigger.oldMap.get(a.Id);
			
            if (a.OwnerId != oldAccount.OwnerId) {
                System.debug('Updating previous owner to - ' + oldAccount.OwnerId);              
                a.Previous_Owner__c = oldAccount.OwnerId;
            } else if (a.OwnerId == UserInfo.getUserId()) { 
                System.debug('Updating Last Activity Date since change made by owner');
                a.Last_Activity_Date__c = System.today(); 
            }
            
            //Format phone numbers
        	/*Following code is commented as per the requirement by Demandware on 11/10/2019
        	  a.Phone = PhoneNumberFormatter.stripCharacters(a.Phone);
            a.Mobile__c = PhoneNumberFormatter.stripCharacters(a.Mobile__c);*/
        }
    }
    
    private void calculateRanking(Account account) {
        System.debug('Entering calculateRanking for account - ' + account);
        
        decimal points = 0;
        
        if (!string.isBlank(account.First_Name__c) && !string.isBlank(account.Last_Name__c)) {
            points++;
            System.debug('Ranking Calculation - First and last name populated');
        }            
        
        if (!string.isBlank(account.BillingCity) && !string.isBlank(account.BillingState) && !string.isBlank(account.BillingPostalCode) && !string.isBlank(account.BillingStreet)){
            points++;
            System.debug('Ranking Calculation - Address populated');
        }
        
        if (!string.isBlank(account.Phone)){
            points++;
            System.debug('Ranking Calculation - Phone populated');
        }
        
        if (!string.isBlank(account.Email__c)){
            points++;
            System.debug('Ranking Calculation - Email populated');
        }
        
        if (!string.isBlank(account.Rating)){
            points++;
            System.debug('Ranking Calculation - Rating populated');
        }
        
        if (account.Number_of_Watches_in_Collection__c != null && account.Number_of_Watches_in_Collection__c > 0){
            points++;
            System.debug('Ranking Calculation - Number of watches in collection populated');
        }
        
        if (!string.isBlank(account.Watchbox_App_Signed_Up__c)){
            points++;
            System.debug('Ranking Calculation - Watchbox app sign up populated');
        }
        
        if (!string.isBlank(account.Interests_Hobbies__c)){
            points++;
            System.debug('Ranking Calculation - Interestes and hobbies populated');
        }
        
        if (account.Birthday__c != null){
            points++;
            System.debug('Ranking Calculation - DOB populated');
        }
        
        if (!string.isBlank(account.Industry)){
            points++;
            System.debug('Ranking Calculation - Industry populated');
        }
        
        if (!string.isBlank(account.Title__c)){
            points++;
            System.debug('Ranking Calculation - Title populated');
        }
        
        if (!string.isBlank(account.Where_else_do_you_buy_watches__c)){
            points++;
            System.debug('Ranking Calculation - Other buying locations populated');
        }
        
        if (account.Receive_Updates__c){
            points++;
            System.debug('Ranking Calculation - Receive updates populated');
        }               
        
        account.Ranking_Score__c = points/RANKING_DENOMINATOR; 
    }
}