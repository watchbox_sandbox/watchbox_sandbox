@isTest
public class sgTestPrintingInspectionController {

    static testMethod void construct() {
        //arrange
        //
        createOrg('Govberg');
        createCheckpointSetting('Test1', 'Alignment_Of_Hands');
        createCheckpointSetting('Test2', 'Aperture_Window');
        Inspection__c inspection = createInspection('test');

        Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('id', inspection.Id);
        parameters.put('organization', 'Govberg');
        
        //act
        //
        sgPrintingInspectionController ctl = new sgPrintingInspectionController();
        Boolean approved = ctl.getIsApproved();
        
        //assert
        //
        System.assertEquals(2, ctl.Checkpoints.size());
        System.assertEquals(false, approved);
    }
    
    //**********************************************************
    // helper methods
    
    static Inspection__c createInspection(String name) {
        Inspection__c insp = new Inspection__c (
            Related_Inventory__c = createProduct().Id
        );
        insert insp;
        return insp;
    }
    
    static Product2 createProduct() {
        Product2 prod = new Product2(
        	Name = 'test'
        );
        insert prod;
        return prod;
    }
    
    static void createOrg(String name) {
        Organization__c org = new Organization__c(
        	Name = name
        );
        insert org;
    }
    
    static void createCheckpointSetting(String name, String prefix) {
        Surge_Inspection_Checkpoint_Mapping__c setting = new Surge_Inspection_Checkpoint_Mapping__c(
        	Name = name,
            Title__c = 'title',
            Field_Prefix__c = prefix,
        	Order__c = 1
        );
        insert setting;
    }
    
}