global class OpportunityDipReminderScheduled implements Schedulable{
	global void execute(SchedulableContext sc) {
        OpportunityDipReminderBatch batch = new OpportunityDipReminderBatch();
        database.executeBatch(batch);
    }
}