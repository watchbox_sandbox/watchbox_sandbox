@isTest
public class sgTestPrintingInvoiceController {

    static testMethod void construct() {
        //arrange
        //
        createOrg('Govberg');
        Opportunity deal = createDeal('Test1');
        createWatchSale(deal.Id);
        createWatchPurchase(deal.Id);
        Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('id', deal.Id);
        parameters.put('organization', 'Govberg');
        
        //act
        //
        sgPrintingInvoiceController ctl = new sgPrintingInvoiceController();
        Boolean isInternal = ctl.getIsInternal();
        
        //assert
        //
        System.assertNotEquals(null, ctl.Deal);
        System.assertNotEquals(null, ctl.Watches);
        System.assertNotEquals(null, ctl.Trades);
        System.assertNotEquals(null, ctl.IncomingPayments);
        System.assertEquals(false, isInternal);
    }
    
    //*************************************************************
    // helper methods
    
    static Opportunity createDeal(String name) {
        Opportunity opp = new Opportunity(
        	Name = name,
            StageName = 'Closed Lost', //closed won threw a field validation exception
            Loss_Reason__c = 'Other', //and a reason included
            CloseDate = Date.today()
        );
        insert opp;
        return opp;
    }
    
    static void createWatchSale(String oppId) {
        Watch_Sale__c watchSale = new Watch_Sale__c(
        	Opportunity__c = oppId,
            Sale_Amount__c = 1000
        );
        insert watchSale;
    }
    
    static void createWatchPurchase(String oppId) {
        Watch_Purchase__c watchPurchase = new Watch_Purchase__c(
        	Origination_Opportunity__c = oppId,
            Cost__c = 1000, 
            Asking__c = 1000, 
            MSP__c = 1000,
            Model__c = createModel().Id
        );
        insert watchPurchase;
    }
    
    static void createIncomingPayment(String oppId) {
        Incoming_Payment__c payment = new Incoming_Payment__c(
        	Opportunity__c = oppId
        );
        insert payment;
    }
    
    static Model__c createModel() {
        Model__c model = new Model__c(
        
        );
        insert model;
        return model;
    }
    
    static void createOrg(String name) {
        Organization__c org = new Organization__c(
        	Name = name
        );
        insert org;
    }
    
}