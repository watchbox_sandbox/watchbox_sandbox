@isTest
public class NoteBatchForOpportunityTest {
    
    static testmethod void note_attached_test() {
        Opportunity opp=TestFactory.createOpportunity();
        Note n=TestFactory.createNote(opp.id);
        Test.startTest();
        NoteBatchForOpportunity batch = new NoteBatchForOpportunity();
        database.executeBatch(batch);
  		Test.stopTest();
        
        Opportunity opportunity = [SELECT Id, IsNoteAttached__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(opportunity.IsNoteAttached__c,'Y');        
    }
    
    static testmethod void note_notattached_test() {
        Opportunity opp=TestFactory.createOpportunity();
        Test.startTest();
        NoteBatchForOpportunity batch = new NoteBatchForOpportunity();
        database.executeBatch(batch);
  		Test.stopTest();
        
        Opportunity opportunity = [SELECT Id, IsNoteAttached__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(opportunity.IsNoteAttached__c,'N');        
    }

}