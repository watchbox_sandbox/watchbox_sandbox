@isTest
public class sgTestPostingQueueable {

    static testMethod void queueJob() {
        //arrange
        //
        createServiceSettings('Watchbox');
        Product2 product = createProduct();
        ChannelPost__c channelPost = createChannelPost('Watchbox', true, true, Date.today(), null, null, false, product);
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(400, null, null));
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('Watchbox', env, 'token');
        
        //act
        //
        Test.startTest();
        String jobId = System.enqueueJob(new sgPostingQueueable(channelPost.Product__c, channelPost.Channel__c));
        Test.stopTest();
        
        //assert
        //
        //TODO: figure out what to assert
    }
    
    //**********************************************************
    // Support methods
    // 

    public class MockClassResolver implements System.StubProvider {
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (stubbedMethodName == 'resolveClass') {
                return Type.forName('MockPostingIntegration');
            }
            else {
                return new sgPostingIntegrationTest();
            }
        }
    }
    
    public class MockEndpoint implements System.StubProvider {
        
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            
            return null;
        }

    }
    
    static ChannelPost__c createChannelPost(String name, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate, Boolean priceUpdate, Product2 product) {
        ChannelPost__c cp = new ChannelPost__c(
            Name = name,
            Channel__c = name,
            Post__c = post,
            Post_Date__c = postDate,
            Posted_Date__c = postedDate,
            Visible__c = visible,
            Visible_Date__c = visibleDate,
            Update__c = priceUpdate,
            Product__c = product.Id,
            Is_Processing__c = true
        );
        insert cp;
        return cp;
    }

    static Surge_Endpoint_Settings__c createFakeSetting(String name, String env, String authMode) {
        Surge_Endpoint_Settings__c setting = new Surge_Endpoint_Settings__c();
        setting.Environment__c = env;
        setting.Name = setting.Endpoint_Name__c = name;
        setting.Auth_Mode__c = authMode;
        setting.Auth_Url__c = 'https://test.authendpoint/?clientId={clientId}/';
        setting.Client_ID__c = '12345';
        setting.Client_Secret__c = '67891';
        setting.Password__c = 'password';
        setting.User_Name__c = 'username';
        setting.Auth_Response_Key__c = 'access_token';
        setting.Endpoint_Url__c = 'https://test.endpoint/';
        insert setting;
        return setting;
    }
    
    static void createServiceSettings(String name) {
        Surge_Integration_Settings__c setting = new Surge_Integration_Settings__c(
            Name = name,
            Integration__c = name,
            Organization__c = 'Watchbox',
            Service__c = 'Watchbox',
            Path__c = '/test',
            Parameters__c = 'test=test'
        );
        
        insert setting;
    }
    
    static Product2 createProduct() {
        Product2 product = new Product2 (
        	Name = 'test'
        );
        insert product;
        return product;
    }
    
}