public class OpporunityContactUpdate {
    
    public void updateContacts(List<Opportunity> listOpportunity){
        system.debug('--inside handler-------');
        List<Opportunity> oppList=new List<Opportunity>();
        Map<Id,Opportunity> oppAccountMap = new Map<Id,Opportunity>();
        if(listOpportunity!=null && listOpportunity.size()>0)
        {
            system.debug('--inside list-------');
            for(Opportunity opp: listOpportunity){
                if(opp.accountId!=null){
                    oppAccountMap.put(opp.accountId,opp);
                }
            }
            List<Contact> listContact=[select id,accountId from Contact where accountId IN: oppAccountMap.keySet()];
            system.debug('opp list-----'+listContact);
            for(Contact c: listContact){
                Opportunity opp = oppAccountMap.get(c.accountId);
                opp.Contact_Record__c=c.Id;
            }
        }
        
    }
    
}