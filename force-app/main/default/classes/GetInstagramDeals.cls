global class GetInstagramDeals implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        System.debug('Incoming email message - ' + email);
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Account matchedAccount = null;
        string toAddress = email.toAddresses[0];
        string bccAddress = '';
        if(!Test.isRunningTest()){
            for (Messaging.InboundEmail.Header header : email.headers) {
                if (header.name == 'Received') {
                    bccAddress = header.value;
                    bccAddress = bccAddress.substringBetween('<','>');
                }
            }            
        }
        List<Account> accounts = [SELECT Id, Name, OwnerId FROM Account WHERE Email__c =: toAddress];
        system.debug('accounts@@@'+accounts);
        User fromUser = getUserByEmail(email.fromAddress);
        system.debug('fromUser@@@'+fromUser);
        if (!accounts.isEmpty()){
            matchedAccount = accounts[0];            
        }
        if (matchedAccount == null) {                                
            matchedAccount = new Account();                
            matchedAccount.Name = toAddress;
            matchedAccount.Last_Name__c = toAddress;
            matchedAccount.Email__c = toAddress;
            matchedAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('People').getRecordTypeId();
            matchedAccount.Status__c = 'Email Lead';
            
            if (fromUser != null)
                matchedAccount.OwnerId = fromUser.Id;
            
            upsert matchedAccount;
        } 
        system.debug('Inserted Account @@@'+matchedAccount);
        //create deal for account
        
        Opportunity opp=new Opportunity();
        opp.Name=toAddress;
        opp.AccountId=matchedAccount.id;
        opp.CloseDate=system.today()+30;
        opp.StageName='New';
        opp.Initial_Deal_Details__c=email.plainTextBody;
        insert opp;
        system.debug('Inserted Opportunity @@@'+opp);
        
        //Build email message 
        EmailMessage message = new EmailMessage();
        message.RelatedToId = matchedAccount.Id;
        message.Subject = email.subject;
        message.ToAddress = toAddress;
        message.bccAddress = bccAddress;
        message.FromAddress = email.fromAddress;
        message.FromName = email.fromName;            
        
        if (!string.isBlank(email.htmlBody) && email.htmlBody.length() > 130000) {            
            message.HtmlBody = email.htmlBody.substring(0, 130000);            
        } else {
            message.HtmlBody = email.htmlBody;
        }              
        
        if (email.ccAddresses != null && email.ccAddresses.size() > 0)
            message.CcAddress = string.join(email.ccAddresses, ',');
        
        message.Incoming = false;
        message.TextBody = email.plainTextBody;
        message.Status = '3';
        insert message;  
        createAttachments(email, message,opp.Id);
        
        
        
        return result;
    }
    private User getUserByEmail(string emailAddress) {
        User user = null;
        
        try {
            user = [SELECT Id FROM User WHERE Email =: emailAddress LIMIT 1];
        } catch (Exception e) {
            System.debug('No user found in handleInboundEmail.getUserByEmail - ' + emailAddress + ' - ' +e.getMessage());
        }
        
        return user;
    }
    private void createAttachments(Messaging.InboundEmail email, EmailMessage message,id oppid) {
        List<Attachment> attachments = new List<Attachment>();
        
        //Process Binary attachments
        if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                try {
                    if (email.binaryAttachments[i].filename != null) { 
                        Attachment binaryAttachment = new Attachment(
                            ParentId = oppid,                        
                            Name = email.binaryAttachments[i].filename,
                            Body = email.binaryAttachments[i].body);
                        attachments.add(binaryAttachment);                        
                    }
                } catch (Exception e) {
                    System.debug('Error processing binary attachment - ' + email + ' - ' + e.getMessage());
                    throw new ApplicationException(e.getMessage());
                }
            }
        }
        
        //Process Text attachments
        if (email.textAttachments!=null && email.textAttachments.size() > 0) {
            for (integer i = 0 ; i < email.textAttachments.size() ; i++) {
                try {
                    if (email.textAttachments[i].filename != null) { 
                        Attachment textAttachment = new Attachment(
                            ParentId = oppid, 
                            Name = email.textAttachments[i].filename,
                            Body = Blob.valueOf(email.textAttachments[i].body));
                        attachments.add(textAttachment);
                    }               
                } catch (Exception e) {
                    System.debug('Error processing text attachment - ' + email + ' - ' + e.getMessage());
                    throw new ApplicationException(e.getMessage());
                }
            }
        }
        
        if (!attachments.isEmpty())
            insert attachments;
    }
    
}