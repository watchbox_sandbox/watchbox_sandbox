@isTest
public class sgTestPostingIntegrationEbay {

    static testMethod void SetPost_NoProduct() {
        //arrange
        //
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                Blob.valueOf('{"total": 0 }')
            },
            '/Create Product' => new List<Object> {
                Blob.valueOf('')
            },
            '/Get Offer' => new List<Object> {
                Blob.valueOf('{ "offers": [{ "offerId": "12345" }]}')
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        ebay.SetPost();
        Test.stopTest();
        //assert
        //
        System.assertEquals('12345', ebay.ChannelPost.External_Id__c);
        
    }
    
    static testMethod void SetPost_NoOffer() {
        //arrange
        //
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                Blob.valueOf('{"inventoryItems": [{"sku": "54321"}]}')
            },
            '/Get Offer' => new List<Object> {
                Blob.valueOf('{"errors":[{"errorId": 25713,"domain": "API_INVENTORY","subdomain": "Selling","category": "REQUEST","message": "This Offer is not available."}]}')
            },
            '/Create Offer' => new List<Object> {
                Blob.valueOf('{ "offerId": "12345" }')
            },
            '/Get Location' => new List<Object> {
                Blob.valueOf('{"errors": [{"errorId": 25804,"domain": "API_INVENTORY","category": "REQUEST","message": "Input error. Resource not found","parameters": [{"name": "fieldName","value": ""},{"name": "additionalInfo","value": "Resource not found"}]}]}')
            },
            '/Create Location' => new List<Object> {
                Blob.valueOf('')
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        ebay.SetPost();
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('12345', ebay.ChannelPost.External_Id__c);
    }
    
    static testMethod void SetVisible_Error() {
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                Blob.valueOf('{"errors": [{"errorId": 1001,"domain": "OAuth","category": "REQUEST","message": "Invalid access token","longMessage": "Invalid access token. Check the value of the Authorization HTTP request header."}]}')
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        Exception resultEx = null;
        try {
        	ebay.SetVisible();
        }
        catch(Exception ex) {
            resultEx = ex;
        }
        Test.stopTest();
        
        //assert
        //
		System.assertEquals('Error Calling the "Get Product" Service: Invalid access token: Invalid access token. Check the value of the Authorization HTTP request header.', resultEx.getMessage());
    }
    
    static testMethod void SetVisible_Success() {
        //arrange
        //
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                Blob.valueOf('{"inventoryItems": [{"sku": "54321"}]}')
            },
            '/Get Offer' => new List<Object> {
                Blob.valueOf('{ "offers": [{ "offerId": "12345", "status": "UNPUBLISHED" }]}')
            },
            '/Publish Offer' => new List<Object> {
                Blob.valueOf('{"listingId": "223412345678"}')
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        ebay.SetVisible();
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('12345', ebay.ChannelPost.External_Id__c);
    }
    
    static testMethod void RemoveVisible() {
        //arrange
        //
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                Blob.valueOf('{"sku": "54321"}')
            },
            '/Get Offer' => new List<Object> {
                Blob.valueOf('{ "offers": [{ "offerId": "12345", "status": "PUBLISHED" }]}')
            },
            '/Delete Offer' => new List<Object> {
                Blob.valueOf('')
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        ebay.RemoveVisible();
        Test.stopTest();
        
        //assert
        //
        System.assertEquals(null, ebay.ChannelPost.External_Id__c);
    }
    
    static testMethod void RemovePost() {
        //arrange
        //
        sgPostingIntegrationEbay ebay = createEbay(null, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        Exception resultEx = null;
        try {
            ebay.RemovePost();
        }
        catch(Exception ex) {
            resultEx = ex;
        }
        
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('A concrete implementation is required', resultEx.getMessage());
    }
    
    static testMethod void NullResponse() {
        //arrange
        //
        Map<String,List<Object>> responses = new Map<String,List<Object>> {
            '/Get Product' => new List<Object> {
                null
            }
        };
        sgPostingIntegrationEbay ebay = createEbay(responses, true, false, Date.today(), null, null);
        
        //act
        //
        Test.startTest();
        Exception resultEx = null;
        try {
            ebay.SetPost();
        }
        catch(Exception ex) {
            resultEx = ex;
        }
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('Empty response for service Get Product', resultEx.getMessage());
    }
    
    //**********************************************************
    // Support methods
    
    public class MockEndpoint implements System.StubProvider {
        
        Map<String,List<Object>> responses;
        Map<String,Integer> responseCallCount = new Map<String,Integer>();
        
        public MockEndpoint(Map<String,List<Object>> resp) {
            responses = resp;
        }
        
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (stubbedMethodName != 'Authenticate' && stubbedMethodName != 'UpdateContext' && responses != null) {
                String path = (String)listOfArgs[0];
                List<Object> responseList = responses.get(path);
                if (responseList != null) {
                    Integer callCount = getCallCount(path);
                    Object response = responseList[callCount - 1];
                    return response;
                }
            }
            return null;
        }
        
        Integer getCallCount(String path) {
            Integer callCount = 0;
            if (responseCallCount.containsKey(path)) {
                callCount = responseCallCount.get(path);
            }
            callCount++;
            responseCallCount.put(path, callCount);
            return callCount;
        }

    }
    
    static sgPostingIntegrationEbay createEbay(Map<String,List<Object>> responses, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate) {
        String name = 'Ebay';
        createServiceSettings();
        sgPostingIntegrationEbay ebay = new sgPostingIntegrationEbay();
        ebay.Name = name;
        ebay.Endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint(responses));
        ebay.ChannelPost = createChannelPost(name, post, visible, postDate, postedDate, visibleDate);
        Photograph__c photo = createPhotograph(ebay.ChannelPost.Product__c, ebay.ChannelPost.Organization__c);
        ebay.Initialize(); //loads the services list
        return ebay;
    }
    
    static ChannelPost__c createChannelPost(String name, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate) {
        ChannelPost__c cp = new ChannelPost__c(
            Name = name,
            Channel__c = name,
            Organization__c = createOrganization().Id,
            Product__c = createProduct().Id,
            Post__c = post,
            Post_Date__c = postDate,
            Posted_Date__c = postedDate,
            Visible__c = visible,
            Visible_Date__c = visibleDate,
            Is_Processing__c = true
        );
        insert cp;
        return cp;
    }
	
    static Product2 createProduct() {
        Product2 product = new Product2(
        	Name = 'Test'
        );
        insert product;
        return product;
    }
    
    static void createServiceSettings() {
        createServiceSetting('Ebay', 'Get Product');
        createServiceSetting('Ebay', 'Create Product');
        createServiceSetting('Ebay', 'Get Offer');
        createServiceSetting('Ebay', 'Create Offer');
        createServiceSetting('Ebay', 'Get Location');
        createServiceSetting('Ebay', 'Create Location');
        createServiceSetting('Ebay', 'Publish Offer');
        createServiceSetting('Ebay', 'Delete Offer');
    }
    
    static void createServiceSetting(String name, String service) {
        Surge_Integration_Settings__c setting = new Surge_Integration_Settings__c(
            Name = name + ' ' + service,
            Integration__c = name,
            Organization__c = 'Watchbox',
            Service__c = service,
            Path__c = '/' + service,
            Headers__c = 'header=test',
            Parameters__c = 'test=test'
        );
        
        insert setting;
    }
    
    static Photograph__c createPhotograph(String productId, String organizationId) {
        Photograph__c photo = new Photograph__c(
        	Organization__c = organizationId,
            Url__c = '/url',
            Related_Product__c = productId
        );
        insert photo;
        return photo;
    }
    
    static Organization__c createOrganization() {
        Organization__c org = new Organization__c(
            Name = 'Watchbox',
            Organization_Code__c = 'Watchbox'
        );
        insert org;
        return org;
    }
    
}