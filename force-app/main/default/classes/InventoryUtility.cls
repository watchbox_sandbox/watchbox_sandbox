public class InventoryUtility {
	public static void removeHolds(Set<Id> productIds) {
        List<Product2> products = [SELECT Id, Hold_Expiration__c, Status__c FROM Product2 WHERE Id IN :productIds];
        
        for (Product2 p : products) {
            p.Hold_Expiration__c = null;
            
            //TODO: Move to Custom Settings
            if (p.Status__c == 'On Hold') {
                p.Status__c = 'Available';
            }
        }
        
        update products;
    }
}