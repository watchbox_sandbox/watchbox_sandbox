/* Api to update records from the Lux-Asset Salesforce instance*/

@RestResource(urlMapping='/WatchBox/updateLoanLead/*') 
global class UpdateWatchBoxLoanLead {
    
    @HttpPost 
    global static responseWrapper OpportunityCreationLogic() {
       
        RestRequest req = RestContext.request; 
        responseWrapper responseWrapper = new responseWrapper(); 
        String jsonBody = req.requestBody.toString(); 
        System.debug('jsonBody******'+jsonBody);
        Type resultType = Type.forName('OpportunityWrapper');
        //jsonBody = jsonBody.replace('[', '');
        //jsonBody = jsonBody.replace(']', '');
        List<OpportunityWrapperr> OpportunityWrapperrList = new List<OpportunityWrapperr>();
        OpportunityWrapperrList = (List<OpportunityWrapperr>)JSON.deserialize(jsonBody, List<OpportunityWrapperr>.class);
        System.debug('==========> deserialize() results = ' + OpportunityWrapperrList);
        
        try {
            // Update Logic Goes here
            System.debug('in try');
            responseWrapper.message = 'Opportunity Updated Successfully';
            LACGov_WBSyncService.updateOpportunity(OpportunityWrapperrList,responseWrapper);
               
        }catch (Exception exp){
            System.debug('Exp*****exp'+exp);
            System.debug('Exp*****getLineNumber'+exp.getLineNumber());
            System.debug('Exp*****getStackTraceString'+exp.getStackTraceString());
            responseWrapper.message = exp.getMessage();
            responseWrapper.status = 'failure';
        }
        return responseWrapper; //return the JSON response
    }
    
    //wrapper class for the response to an API request
    global class responseWrapper {
        global String status {get;set;} //status string
        global Opportunity Opportunity {get;set;} //18 character Opporutnity record Id
        global String oppId {get; set;}
        global String OpportunityProductId {get;set;} //18 character OpportunityProduct record Id
        global String ContactId{get;set;} // ContactId
        global String AccountId{get;set;} // AccountId
        global String message{get;set;} // message strubg
        global String oppName{get;set;} 
        
        
        //constructor
        global responseWrapper() {
            this.status = 'success';
            this.Opportunity = new Opportunity();
            this.oppId = '';
            this.OpportunityProductId = '';
            this.ContactId = '';
            this.AccountId = '';
            this.message = '';
            this.oppName = '';
        }
    }        
}