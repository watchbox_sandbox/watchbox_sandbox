public with sharing class MagentoSeriesSerializer {
    public static MagentoSeriesDTO seriesToDTO(Series__c providedSeries) {
        // Re-fetch to get all of our required properties.
        Series__c series = providedSeries;
        /*
        [
            SELECT
                Model__r.Model_Series__r.Id,
                Model__r.Model_Series__r.Series_Description__c,
                Model__r.Model_Series__r.Series_Quote__c,
                Model__r.Model_Series__r.URL_Slug__c,
                Model__r.Model_Series__r.WUW_Website_Active__c,
                Model__r.Model_Series__r.Watch_Brand__c,
                Model__r.Model_Series__r.Name
            FROM Series__c
            WHERE Id = :providedSeries.Id
        ];
		*/
        
        MagentoBrandDTO bdto = MagentoBrandSerializer.brandToDTO(series.Watch_Brand__r);

        // Create an empty DTO instance.
        MagentoSeriesDTO sdto = new MagentoSeriesDTO();

        // Fill er up.
        sdto.sfId = series.Id;
        sdto.description = series.Series_Description__c;
        sdto.quote = series.Series_Quote__c;
        sdto.slug = series.URL_Slug__c;
        sdto.active = true; //if the product is coming through, it's active //series.WUW_Website_Active__c;
        sdto.brandId = series.Watch_Brand__c;
        sdto.name = series.Name;
        sdto.brand = bdto;

        return sdto;
    }

    public static String seriesToDTOJson(List<Series__c> series) {
        List<MagentoSeriesDTO> sdtos = new List<MagentoSeriesDTO>();

        for (Series__c s: series) {
            MagentoSeriesDTO sdto = MagentoSeriesSerializer.seriesToDTO(s);
            sdtos.add(sdto);
        }

        Map<String, List<MagentoSeriesDTO>> batchData = new Map<String, List<MagentoSeriesDTO>>{
        	'series' => sdtos
        };

        String dataString = JSON.serialize(batchData);

        return dataString;
    }
}