@isTest
public class sgTestLoginController {
    static testMethod void login() {
        string password = 'test';
        
        Team_Member__c t = sgDataSeeding.TeamMembers().get(0);
        t.Password__c = password;
        sgAuth.setPasswordHash(t);
        
        insert t;
        
        Map<string, string> response = sgLoginController.login(t.Work_Email__c, password);
        
        system.assert(response != null);
        system.assert(response.get('Token').length() == 128);
    }
    
    static testMethod void badlogin() {
        string password = 'test';
        string badPassword = 'nope';
        
        Team_Member__c t = sgDataSeeding.TeamMembers().get(0);
        t.Password__c = password;
        sgAuth.setPasswordHash(t);
        
        insert t;
        try {
        	Map<string, string> response = sgLoginController.login(t.Work_Email__c, badPassword);
            system.assert(false);
        } catch (Exception e) {
            system.assert(true);
        }
    }
}