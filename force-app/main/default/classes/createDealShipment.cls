public class createDealShipment {
public static ApexPages.standardController stdController;
    Public static Id objId;
    public createDealShipment(ApexPages.StandardController controller){
        stdController = controller;
        objId = stdController.getId();
    }
    
    public PageReference shipFromDealRecord(){
        //we're going to generate a shipping label and then tie it to the deal
        //We'll grab the shipmate preferences for setting WUW as sender data(will be reversed because this is an inbound shipment)
        //We'll query the opp to get the account address info for recipient data
        //Create a bulk shipment, a queued shipment, and a package, insert like a turducken
        //process and validate bulk shipment
       /* Id customAddressSourceId = [SELECT id, zkfedex__ShipmentObjectLookupField__c 
                                    FROM zkfedex__CustomAddressSource__c 
                                    WHERE zkfedex__ShipmentObjectLookupField__c = 'Fedex_Shipments_One_Click__c'].id;
        system.debug('ID of custom address source: ' +customAddressSourceId); */
        
        
        Opportunity relatedOpp = [SELECT id, WUW_Phone__c, signature_Required__c,customer_Shipping_City__c, customer_shipping_country__c, 
                                  customer_Shipping_State__c, customer_Shipping_Street__c, 
                                  customer_Shipping_Zip__c, dealID__c, domestic_service_Type__c, Full_Name__c, WUW_Contact__c, 
                                  quantity__c, weight__c, packaging_Type__c, shipment_Type__c,account.id 
                                  FROM Opportunity 
                                  WHERE id = :objId];
       /* 
        zkfedex__ShipmatePreference__c shipmatePref = [SELECT zkfedex__CompanyName__c, zkfedex__ShippingIsResidential__c, zkfedex__ShippingCity__c,
                zkfedex__ShippingCountry__c, zkfedex__SenderEMailDefault__c, zkfedex__SenderPhoneDefault__c,
                zkfedex__ShippingState__c, zkfedex__ShippingStreet__c, zkfedex__ShippingPostalCode__c, 
                zkfedex__FedExAccountNumber__c, zkfedex__BillingCountry__c, 
                zkfedex__DropoffTypeDefault__c, zkfedex__LabelImageTypeDefault__c
        FROM zkfedex__ShipmatePreference__c
        Limit 1];
        
         zkfedex__BulkShipment__c bulkShipment = new zkfedex__BulkShipment__c (
        zkfedex__ShipmatePreference__c = shipmatePref.Id
        );
        insert bulkShipment;
        Date shipdate;
        Datetime dt = System.now();
        
        if(dt.format('EEEE') !='Sunday'){
            shipdate = date.today();
        }else{
            shipDate = date.today()+1;
        }
        List<zkfedex__QueuedShipment__c> queuedShipmentsToInsert = new List<zkfedex__QueuedShipment__c>();
        zkfedex__QueuedShipment__c queuedShipment = new zkfedex__QueuedShipment__c (
            zkfedex__BulkShipment__c = bulkShipment.Id,
            zkfedex__ShipDate__c = shipDate,
            zkfedex__DropoffType__c = 'Regular Pickup',
            zkfedex__LabelImageType__c = shipmatePref.zkfedex__LabelImageTypeDefault__c,
            zkfedex__ServiceType__c = 'Domestic: FedEx Priority Overnight',
            zkfedex__PackagingType__c = 'Your Packaging',
            zkfedex__WeightDimensionUnits__c = 'LB / IN',      
            zkfedex__SenderName__c = shipmatePref.zkfedex__CompanyName__c,
            zkfedex__SenderIsResidential__c = shipmatePref.zkfedex__ShippingIsResidential__c,
            zkfedex__SenderCity__c = shipmatePref.zkfedex__ShippingCity__c,
            zkfedex__SenderCompany__c = shipmatePref.zkfedex__CompanyName__c,
            zkfedex__SenderCountry__c = shipmatePref.zkfedex__ShippingCountry__c,
            zkfedex__SenderEmail__c = shipmatePref.zkfedex__SenderEMailDefault__c,
            zkfedex__SenderPhone__c = shipmatePref.zkfedex__SenderPhoneDefault__c,
            zkfedex__SenderState__c = shipmatePref.zkfedex__ShippingState__c,
            zkfedex__SenderStreet__c = shipmatePref.zkfedex__ShippingStreet__c,
            zkfedex__SenderPostalCode__c = shipmatePref.zkfedex__ShippingPostalCode__c,
            

            // recipient info
            zkfedex__RecipientIsResidential__c = false,
            zkfedex__RecipientPhone__c  = relatedOpp.WUW_Phone__c,
            zkfedex__RecipientCity__c =  relatedOpp.customer_Shipping_City__c,
            zkfedex__RecipientCountry__c = 'US',
            zkfedex__RecipientName__c = relatedOpp.Full_Name__c,
            zkfedex__RecipientState__c = relatedOpp.customer_Shipping_State__c,
            zkfedex__RecipientStreet__c = relatedOpp.Customer_Shipping_Street__c,
            zkfedex__RecipientPostalCode__c = relatedOpp.customer_Shipping_Zip__c,
            zkfedex__PaymentType__c = 'SENDER',
            
            
            
            //general info
            zkfedex__Account__c = relatedOpp.Account.id,
            zkfedex__Opportunity__c  = relatedOpp.id,
            zkfedex__ReferenceYourReference__c = relatedOpp.DealID__c,
            zkfedex__CustomAddressSource__c = customAddressSourceId,
            zkfedex__CustomAddressSourceRecordId__c = relatedOpp.id
            
        );

        insert queuedShipment;         
        zkfedex__QueuedPackage__c queuedPackage = new zkfedex__QueuedPackage__c (
                zkfedex__DeclaredValue__c = 20,
                zkfedex__Weight__c = 4,
                zkfedex__QueuedShipment__c = queuedShipment.Id);          
           
         

        insert queuedPackage;
        String bulkShipmentBatch = zkfedex.BulkShipmentInterface.processBulkShipment(bulkShipment.Id);
        system.debug(bulkShipmentBatch);
        */
        PageReference refreshObj = new PageReference('/apex/shippingLabelLoadingScreen?id='+objId);
        refreshObj.setRedirect(true);
        return refreshObj;
    }
}