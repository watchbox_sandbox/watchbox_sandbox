@istest
public class AssignmentComponentControllerTests {

    static testmethod void hasUsers_success_test() {
        TestFactory.createSalesUser();
        
        AssignmentComponentController acc = new AssignmentComponentController();
        boolean result = acc.hasUsers;
        
        System.assert(result, 'Sales user should exist');
    }
}