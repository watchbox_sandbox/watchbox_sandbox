@isTest
public class LeadGenLogDetailsAPITest {
    @testSetup
    static void setup() {
        TestFactory.createLeadGenLogSettings();
    }
    
    static testmethod void nolead_test() {
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/leadGenLog/info';
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        LeadGenLogDetailsAPI.getLeadGenLogInfo();
        Test.stopTest();  
    }
     static testmethod void lead_test() {
        
        Test.startTest();
       
        Lead_Gen_Log__c lgl = new Lead_Gen_Log__c();
        lgl.Email__c = 'test@unittest.com';
        lgl.First_Name__c = 'Test';
        lgl.Last_Name__c = 'User';
        lgl.Phone__c = '123-456-7890';
        lgl.Lead_Source__c='Thewatchbox.com- Sell your watch';
        insert lgl;   
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/leadGenLog/info';
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        LeadGenLogDetailsAPI.getLeadGenLogInfo();
        Test.stopTest();  
    }
    
}