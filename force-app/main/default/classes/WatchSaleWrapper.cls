public class WatchSaleWrapper {
     public integer counter {get;set;}
     public Watch_Sale__c watchSale {get;set;} 
    
    public WatchSaleWrapper(integer counter,Watch_Sale__c watchSale) {
       
        this.counter = counter;
        this.watchSale = watchSale;
    }

}