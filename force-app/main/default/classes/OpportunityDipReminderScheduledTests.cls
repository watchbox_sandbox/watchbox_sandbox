@isTest
public class OpportunityDipReminderScheduledTests {
    static testmethod void execute_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        opp.Deal_Status__c = 'DIP';
        update opp;
        
        Test.startTest();
        OpportunityFieldHistory objectHistory = new OpportunityFieldHistory(Field = 'Deal_Status__c',OpportunityId=opp.id);
        insert objectHistory;
        
        SchedulableContext sc = null;
		OpportunityDipReminderScheduled schedule = new OpportunityDipReminderScheduled();
		schedule.execute(sc); 
        Test.stopTest();        
        
        Task result = [SELECT Id, Status FROM Task WHERE WhatId =:opp.Id];
        
        System.assertEquals('Open', result.Status);               
    }
}