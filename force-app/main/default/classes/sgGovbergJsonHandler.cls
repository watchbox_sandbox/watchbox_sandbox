/*
 * 
 * System.schedule('Govberg Import 1', '0 0 * * * ?', new sgGovbergJsonHandler());
 * System.schedule('Govberg Import 2', '0 10 * * * ?', new sgGovbergJsonHandler());
 * System.schedule('Govberg Import 3', '0 20 * * * ?', new sgGovbergJsonHandler());
 * System.schedule('Govberg Import 4', '0 30 * * * ?', new sgGovbergJsonHandler());
 * System.schedule('Govberg Import 5', '0 40 * * * ?', new sgGovbergJsonHandler());
 * System.schedule('Govberg Import 6', '0 50 * * * ?', new sgGovbergJsonHandler());
 */


public class sgGovbergJsonHandler implements Schedulable {
    public static boolean Schedule = true;
    
    private static Map<string, string> Models = new Map<string, string>();
    
    private static final string endpoint = 'https://api2.govbergwatches.com/shop/';
    
    private static string MissingModel = [select Id from Model__c where Name = 'Missing ASC Group Code Model'].Id;
    
    private static Map<string, Map<string, string>> PicklistMaps = new Map<string, Map<string, string>>{
        'Watch_Bracelet__c' => new Map<string, string>(),
        'Watch_Case_Material__c'=> new Map<string, string>(),
		'Watch_Dial_Color__c'=> new Map<string, string>()
    };
    private static void SetPicklistMap() {
        for (string field : PicklistMaps.keySet()) {
            for (string s : sgUtility.getPicklistValues('Product2', field)) {
                PicklistMaps.get(field).put(s, '');
            }
        }
    }
    
    //get the number of results to return and the current page of the api
    private static sgSettings__c Setting;
    static {
        string env = sgUtility.isSandbox() ? 'surge' : 'watchdms';
        
		Setting = [select Magento_Batch_ID__c, Govberg_Chunk_Size__c, Govberg_Current_Page__c from sgSettings__c where Name = :env limit 1];        
        
        SetPicklistMap();
    }
    
    //increment the current page in below logic, save it here
    private static void UpdateSetting() {
        update Setting;
    }
    
    //append pagination info
    private static string GetEndpoint() {
        return endpoint + '?per_page=' + Setting.Govberg_Chunk_Size__c + '&page=' + Setting.Govberg_Current_Page__c;
    }
    
    public void execute(SchedulableContext context) {
        Sync();
    }
    
    @future(callout=true)
    public static void Sync() {
        List<sgGovbergJsonData.Data> data = new List<sgGovbergJsonData.Data>();
        
        //currently processing the queue
        if (Setting.Magento_Batch_ID__c != null) {
            return;
        }
        
        data = sgGovbergJsonData.get(GetEndpoint()).Data;
            
        //we've reached the end (no more products), reset the current page counter
        if (data.size() == 0) {
            Setting.Govberg_Current_Page__c = 0;
            
            Finish();
        } else {
            List<Product2> products = JsonToProducts(data);
            
            system.debug('products to insert: ' + products.size());
            
            upsert products GB_Inventory_ID__c;
            
            QueueProducts(products);
            
            UpsertPhotographs(data, products);
            
            //we didn't get a full chunk's worth, so we've reached the end
            if (data.size() < Setting.Govberg_Chunk_Size__c) {
                Setting.Govberg_Current_Page__c = 0;
                
                Finish();
            } else {
	            Setting.Govberg_Current_Page__c++;
            }
        }
        
        UpdateSetting();
    }
    
    public static List<Product2> JsonToProducts(List<sgGovbergJsonData.Data> jsonList) {
        ModelsLookup(jsonList);
        
        List<Product2> products = new List<Product2>();
        
        for (sgGovbergJsonData.Data d : jsonList) {
            products.add(ToProduct(d));
        }

        return products;
    }
    
    private static Product2 ToProduct(sgGovbergJsonData.Data d) {
        string boxesPapers = GetAttribute(d, 'Box/Papers');
        boxesPapers = boxesPapers != null ? boxesPapers : ''; 

        decimal price = !string.isBlank(d.price) ? decimal.valueOf(d.price) : 0;
        
        string model = Models.containsKey(d.sku) ? Models.get(d.sku) : MissingModel;
        
        if (model == MissingModel) {
            //system.debug('gb id ' + d.id + ' missing model');
        }
        
        Product2 p = new Product2(
            GB_Inventory_ID__c = string.valueOf(d.id),
            GB_Asc_Group_Code__c = d.sku,
        	Name = d.title,
            Description = d.description_plaintext,
            ASK__c = !string.isBlank(d.sale_price) ? decimal.valueOf(d.sale_price) : price,
            Retail__c = !string.isBlank(d.retail_price) ? decimal.valueOf(d.regular_price) : price,
            Cost__c = price, //iffy on mapping
            Product_Type__c = 'Pre-Owned',
            Model__c = model,
            IsActive = true,
            
            //attributes
            Condition__c = GetAttribute(d, 'Condition'), //product type?
            Watch_Gender__c = GetAttribute(d, 'Gender'),
            Watch_Case_Material__c = GetAttribute(d, 'Material'),
            Boxes__c = boxesPapers.containsIgnoreCase('Box') ? 'Yes' : 'No',
            Papers__c = boxesPapers.containsIgnoreCase('Paper') ? 'Yes' : 'No',
            Watch_Movement_Type__c = GetAttribute(d, 'Movement'),
            Watch_Case_Color__c = GetAttribute(d, 'Case Color'),
            Watch_Dial_Color__c = GetAttribute(d, 'Dial Color'),
            Watch_Bracelet__c = GetAttribute(d, 'Band Material'),
            Warranty_Type__c = GetAttribute(d, 'Warranty')
            
            //***unmapped attributes***
            //Reference Number //model
            //Forums Rating
            //Case Size
            //Hour Markers
            //Display
            //Watch Shape
            //Caseback
            //Complication
            //Water Resistance
            //Band Color
            //Buckle Type
            //Style
            //Country/Region
            //Year of Manufacture
            
        );
        
        return p;
    }
    
    private static string GetAttribute(sgGovbergJsonData.Data d, string key) {
        for (sgGovbergJsonData.Attributes a : d.attributes) {
            if (a.title == key) {
                string value = a.Values.size() > 0 ? a.Values.get(0).title : null;
                
                Type attr = Type.forName('sgGovbergJsonHandler.' + key.replaceAll(' ', ''));
                
                if (attr != null) {
                    value = ((IAttribute) attr.newInstance()).Transform(value);
                }
                
                
                return value;
            }
        }
        
        return null;
    }
    
    //this interface allows an attribute's value to be transformed, looking for a class named for the attribute
    public interface IAttribute {
        string Transform(string val);
    }
    
    public class Warranty implements IAttribute{
        public string Transform(string val) {
            string picklist = null;
            
            val = val.toLowerCase();
            
            if (val.contains('month')) {
                picklist = '15 Month Govberg';
            } else if (val.contains('majority')) {
                picklist = 'Majority Time Remaining';
            } else if (val.contains('time')) {
                picklist = 'Time Remaining';
            } else if (val.contains('no')) {
                picklist = 'No Warranty';
            }
            
            return picklist;
        }
    }
	
	//there seem to be an endless amount of new and slightly different values for this attr
	//so if it's not in the current value set, call it "other"    
    public class BandMaterial implements IAttribute{      
        public string Transform(string val) {
            string picklist = val;
            
            if (val == null || !sgGovbergJsonHandler.PicklistMaps.get('Watch_Bracelet__c').containsKey(val)) {
            	picklist = 'Other';    
            }   
            
            return picklist;
        }
    }
    
    public class Material implements IAttribute{      
        public string Transform(string val) {
            string picklist = val;
            
            if (val == null || !sgGovbergJsonHandler.PicklistMaps.get('Watch_Case_Material__c').containsKey(val)) {
            	picklist = 'Other';    
            }   
            
            return picklist;
        }
    }
    
    public class DialColor implements IAttribute{      
        public string Transform(string val) {
            string picklist = val;
            
            if (val == null || !sgGovbergJsonHandler.PicklistMaps.get('Watch_Dial_Color__c').containsKey(val)) {
            	picklist = 'Other';    
            }   
            
            return picklist;
        }
    }
    
    public class Gender implements IAttribute {
        public string Transform(string val) {
            string picklist = null;
            
            val = val.toLowerCase();
            
            if (val.contains('men')) {
                picklist = 'Male';
            } else if (val.contains('ladie') || val.contains('lady') || val.contains('female')) {
                picklist = 'Female';
            } else if (val.contains('male')) {
                picklist = 'Male';
            }
            
            return picklist;
        }
    }
    
    /*
    public class Movement implements IAttribute{
        public string Transform(string val) {
            string picklist = val;
            
            val = val.toLowerCase();
            
            if (val.contains('automatic')) {
                picklist = 'Automatic';
            } else if (val.contains('hand')) {
                picklist = 'Manual';
            } else if (val.contains('quartz')) {
                picklist = 'Quartz';
            }
            
            return picklist;
        }
    }
	*/
    
    /*
    public class DialColor implements IAttribute{
        public string Transform(string val) {
            string picklist = val;
            
            val = val.toLowerCase();
            
            if (val.contains('charcoal')) {
                picklist = 'Charcoal';
            }
            
            return picklist;
        }
    }
	*/
    
    //this will set the private member Models using the in clause to compare against a yet-to-be-created external id field referencing the govberg db
    private static Map<string, string> BrandNameLookup = new Map<string, string>();
    private static void ModelsLookup(List<sgGovbergJsonData.Data> jsonList) {
        List<string> govbergModelIds = new List<string>();
        
        for (sgGovbergJsonData.Data d : jsonList) {
            govbergModelIds.add(d.sku);
        }
        system.debug(govbergModelIds);
        List<ASC_Group_Codes__c> modelIds = [select ItemNum__c, Model__c, Model__r.Model_Brand__r.Name from ASC_Group_Codes__c where ItemNum__c in :govbergModelIds limit :govbergModelIds.size()];
        
        for (ASC_Group_Codes__c a : modelIds) {
            Models.put(a.ItemNum__c, a.Model__c);
            BrandNameLookup.put(a.Model__c, a.Model__r.Model_Brand__r.Name);
        }
        
        system.debug(Models);
    }
    
    private static void UpsertPhotographs(List<sgGovbergJsonData.Data> data, List<Product2> productsList) {
        DeletePhotographs(productsList);
        
        Map<string, sObject> products = sgUtility.MapBy('GB_Inventory_ID__c', productsList);
        
        Map<string, Photograph__c> photos = new Map<string, Photograph__c>();
        
        for (sgGovbergJsonData.Data d : data) {
            integer j = 0;
            for (sgGovbergJsonData.Images i : d.images) {
                if (i.id == '') {
                    continue;
                }
                
                //apparently there are dupes in the photo id, so some product is getting shorted
                photos.put(i.id, new Photograph__c(
                    Related_Product__c = products.get(string.valueOf(d.id)).Id,
                    GB_ID__c = decimal.valueOf(i.id),
                    Url__c = i.url,
                    Display_Order__c = j++
                ));
            }
        }
        
        system.debug('photo size: ' + photos.values().size());
        
        upsert photos.values() GB_ID__c;
    }
    
    private static void DeletePhotographs(List<Product2> productsList) {
        List<string> ids = sgUtility.extract('id', productsList);
        
        List<Photograph__c> photos = [select Id from Photograph__c where Related_Product__c in :ids];
        
        delete photos;
    }
    
    //the only way to tell if a product is no longer active is that it doesn't show up in the api
    //but since we have to make multiple api calls to get all products, we won't know until it's
    //we've gotten all products, so we store the ones that came through the api until then.
    private static void QueueProducts(List<Product2> products) {
        List<GB_API_Queue__c> q = new List<GB_API_Queue__c>();
        for (Product2 p : products) {
            if (p.Model__c != MissingModel && AllowedBrand(p)) {
                q.add(new GB_API_Queue__c(
                    Product__c = p.Id
                ));
            } 
        }
        
        insert q;
    }
    
    public static Set<string> allowedBrands {
        get {
        	return new Set<string>{
                'rolex', 
                'audemars piguet',
                'a. lange & sohne',
                'omega',
                'panerai',
                'jaeger leCoultre',
                'jaeger-leCoultre',
                'f.f. journe',
                'cartier',
                'breitling',
                'bell & ross',
                'richard mille'
            };
		}
    }
    
    public static boolean AllowedBrand(Product2 p) {
        string brandName = BrandNameLookup.containsKey(p.Model__c) ? BrandNameLookup.get(p.Model__c) : '';
        
        if (brandName == null) {
            return false;
        }
        
        boolean allowed = allowedBrands.contains(brandName.toLowerCase());
        
        //system.debug(p.Model__c + ' ' + brandName + ' allowed: ' + allowed + ' / missing model: ' + p.Model__c == MissingModel);

        return allowed;
    }
    
    public static void Finish() {
        system.debug('schedule: ' + Schedule);
        if (!Schedule) {
            return;
        }
        
        List<GB_API_Queue__c> qq = [select Product__c from GB_API_Queue__c];
        
        Set<string> productIds = new Set<string>();
        
        for (GB_API_Queue__c q : qq) {
            productIds.add(q.Product__c);
        }
        
        List<Product2> products = [select Id, IsActive, Model__r.Model_Brand__r.Name from Product2 where IsActive = true and  GB_Inventory_ID__c != null];
        
        List<Product2> updates = new List<Product2>();
        
        for (Product2 p : products) {
            string brand = p.Model__r.Model_Brand__r.Name;
            brand = brand == null ? '' : brand.toLowerCase();
            
            //this product is currently active, but shouldn't be
            //and also in the allowed brands list, otherwise it's not even in magento, so don't send it over
            if (allowedBrands.contains(brand) && p.IsActive && !productIds.contains(p.Id)) {
                p.IsActive = false;
                
                //these products need to be de-activated in sf
                updates.add(p);
                
                system.debug(p.Id + ' is being deactivated');
                
                //and magento
                qq.add(new GB_API_Queue__c(
                	Product__c = p.Id
                ));
            }
        }
        
        update updates;
        upsert qq;
        
        //now get rid of the api queue to start over again
        //delete qq;
        
        sgGovbergBatchMagentoSync.StartBatch();
    }

}