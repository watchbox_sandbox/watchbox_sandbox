@RestResource(urlMapping='/Products/*')
global class sgProductController {
    @HttpGet
    global static List<Map<string, string>> get() {
        sgAuth.check();

        List<string> columns = new List<string> {'Id', 
            'Inventory_ID__c',
            'Name',
            'Model__c',
            'Model__r.Name',
            'Model__r.Model_Brand__r.Name',
            'Model__r.Reference__c',
            'Model__r.AWS_Image_Array__c',
            'Product_Type__c',
            'Status__c',
            'Hold_Byline__c',
            'Location__r.Name',
            'Days_Inventoried__c',
            'Inventory_Type__r.Name',
            'WUW_Notes__c',
            'Cost__c',
            'ASK__c',
            'Watch_Brand__r.Name',
            'Watch_Reference__c',
            'WUW_Sold_Price__c',
            'Retail__c',
            'MSP__c',
            'Model__r.SH_Watch_ID__c',
            'AWS_Web_Images__c',
            'Boxes__c',
            'Papers__c',
            'Price_Range__c',
            'Price_Change_Timeframe__c',
            'Price_Change_Days__c',
            'Origination_Type__c',
            'Sales_Person__c',
            'Fully_Listed_Days__c'
       	};

        string query = 'select ' + string.join(columns, ', ') + ' from Product2';
        
        Map<string, string> params =  RestContext.request.params.clone();        
        
        //this isn't ideal, but the filter is checked when on "All Inventory", so put no restrictions on it then
        if (!params.containsKey('IsActive') || params.get('IsActive') == 'false') {
        	params.put('IsActive', 'true');
        } else {
            params.remove('IsActive');
        }
       	
        if (!params.containsKey('limit')) {
            params.put('limit', '50');
        }

        if (!params.containsKey('orderBy')) {
            params.put('orderBy', 'name');
        }
        
        if (params.containsKey('Location__r.Name') && params.get('Location__r.Name') == 'GB Inventory') {
            params.remove('Location__r.Name');
            params.put('IsGBInventory__c', 'true');
        }
        
        if (params.containsKey('CurrentYear')) {
            params.remove('CurrentYear');
            
            string startDate = '>=' + (Date.today().year()) + '-01-01';
            params.put('group2WUW_Date_Created__c', startDate);
            params.put('group2GB_Date_Inventoried__c', startDate);
        }

        if (params.containsKey('query')) {
            string queryParam = params.get('query');
            params.remove('query');
            
            params.put('group1Name', 'like:' + queryParam);
            params.put('group1Model__r.Name ', 'like:' + queryParam);
            params.put('group1Location__r.Name ', 'like:' + queryParam);
            params.put('group1WUW_Inventory_ID__c ', 'like:' + queryParam);
            params.put('group1GB_Inventory_ID__c ', 'like:' + queryParam);
        
        }
        
        //throw new sgException(sgUtility.applyFilters(query, params));
    
	    if (params.containsKey('count')) {
            params.remove('count');
            params.remove('limit');
            params.remove('offset');
            params.remove('orderBy');
            
            query = 'select count() from Product2';

            // return new List<Map<string, string>>{
            //     new Map<string, string> {
            //         'count' => sgUtility.applyFilters(query, params)
            //     }
            // };
            
            integer count = Database.countQuery(sgUtility.applyFilters(query, params));
            
            return new List<Map<string, string>>{
                new Map<string, string> {
                    'count' => string.valueOf(count),
                    'query' => sgUtility.applyFilters(query, params)
                }
            };
    	}
        
        List<Map<string, string>> products = sgSerializer.serializeFromQuery(query, params);
        
        for (Map<string, string> p : products) {
            string image = sgUtility.getMainImage(p.get('AWS_Web_Images__c'));
            p.put('MainImage__c', image);
            p.remove('AWS_Web_Images__c');

            if (image == '') {
                image = sgUtility.getMainImage(p.get('Model__rAWS_Image_Array__c'));
            }
            
			p.put('GridImage__c', image);
            p.remove('Model__rAWS_Image_Array__c');
        }
        
        return products;
    }
    
    @HttpPost
    global static string post(string id) {
        throw new sgException('Action no longer supported.');
        /*
        sgAuth.check();
        
        Product2 p = [select Id, Status__c, Previous_Status__c, Held_By__c, Held_By_Date__c from Product2 where Id = :id limit 1];
        
        string currentStatus = p.Status__c != null ? p.Status__c : 'Other';
            
        if (p.Status__c == 'On Hold') {    
            p.Status__c = p.Previous_Status__c;
            p.Previous_Status__c = currentStatus;
    
            p.Held_By__c = null;
            p.Held_By_Date__c = null;
        } else {
            p.Status__c = 'On Hold';
            p.Previous_Status__c = currentStatus;
            
            p.Held_By__c = sgAuth.user.Id;
            p.Held_By_Date__c = Date.today();         
        }
            
        update p;
        
        p = [select Hold_Byline__c from Product2 where Id = :id limit 1];

        return p.Hold_Byline__c;
		*/
    }
    
    @HttpPut
    global static string put(string id, string field, string val) {
        sgAuth.check();
        
        if (field == 'Cost__c') {
            throw new sgException('Cost may not be edited');
        }
        
        if (!sgAuth.hasRole('Admin, Operations, Sales Manager')) {
            throw new sgException('Only admin and operations roles may edit pricing');
        }
        
        Product2 p = (Product2) Database.query('select Id, ' + field + ' from Product2 where Id = :id limit 1').get(0);
        
        if (field.containsIgnoreCase('date') || field.containsIgnoreCase('posted')) {
            p.put(field, sgUtility.parseDate(val));
        } else {
            Set<string> decimals = new Set<string> {
                'cost__c',
                'retail__c',
                'ask__c',
                'msp__c'
            };
                
            if (decimals.contains(field.toLowerCase())) {
                p.put(field, Decimal.valueOf(val));   
            } else {
                p.put(field, val);
            }
        }
            
        update p;

        return field.replace('__c', '').replace('_', ' ') + ' updated';
    }
}