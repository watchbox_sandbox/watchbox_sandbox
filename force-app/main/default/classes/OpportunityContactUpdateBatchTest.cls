@isTest
public class OpportunityContactUpdateBatchTest {    
       
    static testmethod void execute_success_test() {
        Account account = new Account();
        account.First_Name__c = 'Test';
        account.name = 'Test User';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;

        
        Account account2 = new Account();
        account2.First_Name__c = 'Test2';
        account2.Last_Name__c = 'User2';
        account2.name = 'Test2 User2';
        account2.Email__c = 'test2@unittest2.com';
        insert account2;
        
        Opportunity opp=new  Opportunity ();
        opp.AccountId=account.Id;
        opp.Name = 'Test';
		opp.StageName = 'Test';
        opp.CloseDate=System.today().addMonths(1); 
       	insert opp;
        
        Opportunity opp2=new  Opportunity ();
        opp2.AccountId=account2.Id;
        opp2.Name = 'Test2';
		opp2.StageName = 'Test2';
        opp2.CloseDate=System.today().addMonths(5); 
       	insert opp2;
        
        Test.startTest();
        OpportunityContactUpdateBatch batch = new OpportunityContactUpdateBatch();
        database.executeBatch(batch);               
        Test.stopTest();
        
        Opportunity opportunity = [SELECT Id, OwnerId,Contact_Record__c FROM Opportunity where id=:opp.id ];
        System.assertNotEquals(opportunity.Contact_Record__c,null);
    }
}