@isTest 
public class WatchSaleControllerTest 
{
    
    static testmethod void processopportunities() {
        Opportunity opp =TestFactory.createOpportunity();
        
        Watch_Sale__c  wstest = new Watch_Sale__c();
        wstest.Opportunity__c = opp.id;
        wstest.Client_s_Offer__c=454;
        wstest.MSP__c=546;
        wstest.Selling_below_MSP__c='Yes';
        wstest.Reason_for_Selling_Below_MSP__c='Duplicate watch';
        
        insert wstest;
        PageReference pageRef = page.WatchSaleNewPage;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(wstest.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(wstest);
        WatchSaleController wsc  = new WatchSaleController(sc);
        wsc.save();
        wsc.cancelButton();
        List<String> sr =  WatchSaleController.saleBrandValues();
        
        Opportunity opp1 = WatchSaleController.getOpportunityName(opp.Id);
       
        
    }
       
     static testmethod void processopportunities_error() {
        Opportunity opp =TestFactory.createOpportunity();
        
        Watch_Sale__c  wstest = new Watch_Sale__c();
        wstest.Opportunity__c = opp.id;
        wstest.Client_s_Offer__c=454;
        wstest.MSP__c=546;
        wstest.Selling_below_MSP__c='Yes';
        wstest.Reason_for_Selling_Below_MSP__c=null;
 
        PageReference pageRef = page.WatchSaleNewPage;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(wstest.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(wstest);
        WatchSaleController wsc  = new WatchSaleController(sc);
         wsc.watchSale=wstest;
        wsc.save();
     
       
        
    }
    
    
}