@RestResource(urlMapping='/ModelDetails/*')
global class sgModelDetailController {
    @HttpGet
    global static List<sgModelDetail> get() {
        sgAuth.check();
        
        List<string> columns = new List<string> {'Id', 
            'SH_Watch_ID__c',
            'Name',
            'Reference__c',
            'Model_Write_Up__c',
            'DMS_Model_ID__c',
            'LastModifiedDate',
            'Description__c',
            'Gender__c',
            'Watch_Case_Shape__c',
            'Case_Size__c',
            'Tonneau_Case_Size__c',
            'Case_Material__c',
            'Case_Color__c',
            'Bezel__c',
            'Caseback__c',
            'Bracelet_Material__c',
            'Watch_Display_Type__c',
            'Dial_Color__c',
            'Dial_Type__c',
            'Movement_Type__c',
            'Complications__c',
            'Water_Resistance_Rating__c',
            'AWS_Image_Array__c',
            'Model_Brand__r.Name',
            'Model_Series__r.Name',
            'Also_Known_As__c'
        };

        Map<string, string> params =  RestContext.request.params.clone();
        
        if (params.containsKey('id')) {
            params.put('Model__r.id', params.get('id'));
            params.remove('id');
        } else {
            params.put('Model__r.Verified__c ', 'true');
        }
        
        //open param is used only on the frontend to open a model on page load
        params.remove('open');
                    
 		if (!params.containsKey('id')  && params.containsKey('query')) {
            string queryParam = params.get('query');
            
            params.put('group1Model__r.Name', 'like:' + queryParam);
            params.put('group1Model__r.Reference__c ', 'like:' + queryParam);
            params.put('group1Model__r.SH_Watch_ID__c ', 'like:' + queryParam);
            params.put('group1Model__r.DMS_Model_ID__c ', 'like:' + queryParam);
            params.put('group1Model__r.Also_Known_As__c ', 'like:' + queryParam);
            params.put('group1Model__r.Also_Known_As_2__c ', 'like:' + queryParam);
            params.put('group1Model__r.model_series__r.Name ', 'like:' + queryParam);
        }
        
        params.remove('query');
        
        string searchQuery = sgUtility.applyFilters('select count(Id) c, Model__c from Product2 where IsActive = true', params) + 
            ' group by Model__c order by count(Id) desc limit 50';
        //throw new sgexception(searchQuery);
        AggregateResult[] searchResults = Database.query(searchQuery);
        
        List<sgModelDetail> details = new List<sgModelDetail>();
        
        Map<string, integer> searchModelIds = new Map<string, integer>();
        for(AggregateResult a : searchResults) {
            searchModelIds.put((string) a.get('Model__c'), (integer) a.get('c'));
        }
        List<string> modelIds = new List<string>();
        modelIds.addAll(searchModelIds.keySet());
        
		Map<string, string> inactiveParams = new Map<string, string>();
        for (string k : params.keySet()) {
            inactiveParams.put(k.replace('Model__r.', ''), params.get(k));
        }
        
        string inactiveQuery = sgUtility.applyFilters('select Id from Model__c', inactiveParams) + 
            ' and Id not in (\'' + string.join(modelIds, '\', \'') + '\') limit 50';
              
        //throw new sgexception(inactiveQuery);
        List<string> inactiveResults = sgUtility.extract('Id', Database.query(inactiveQuery));
        modelIds.addAll(inactiveResults);
        
        
        //if there are no results the below product query with no ids in the "in" clause takes forever
        if (modelIds.size() == 0) {
            return details;
        }
        
        string query = 'select ' + string.join(columns, ', ') + ' from Model__c where Id in (\'' + string.join(modelIds, '\', \'') + '\')';

        List<Model__c> models = Database.query(query);
        
        
        List<string> productColumns = new List<string>{'Id', 
            'Inventory_ID__c',
            'IsActive',
            'Model__c', 
            'Ask__c', 
            'Cost__c', 
            'MSP__c', 
            'Retail__c', 
            'Days_Inventoried__c', 
            'Location__r.Name', 
            'Status__c', 
            'Purchase_Date__c',
            'Boxes__c', 
            'Papers__c', 
            'Inventory_Type__r.Name',
            'Product_Type__c',
            'Last_30_Days__c',
            'Date_Created__c',
            'Sold_For__c',
            'Days_To_Sell__c',
            'Origination_Type__c'
        };
            
        string productsQuery = 'select ' + string.join(productColumns, ', ') + ' from Product2 where Model__c in (\'' 
            + string.join(modelIds, '\', \'') + '\')';
        
        List<Video__c> videosList = [select Watch_Model__c, YTVideoId__c from Video__c where Watch_Model__c in :modelIds];
        Map<string, sObject> videos = sgUtility.MapBy('Watch_Model__c', videosList);
            
        List<Map<string, string>> products = sgSerializer.serializeFromQuery(productsQuery, new Map<string, string>());
        
        for (Model__c m : models) {
			sgModelDetail d = new sgModelDetail();
              
            d.Products = new List<Map<string, string>>();
            
            decimal retail = 0;
            integer count = 0;
            integer purchased = 0;
            integer last30 = 0;
            decimal averageDays = 0;
            
            for (Map<string, string> p : products) {
                if (p.get('Model__c') == m.Id) {
                    if (p.get('Last_30_Days__c') == 'true') {
                        last30++;
                    }
                    
                    if (p.get('IsActive') == 'False') {
                        purchased++;
                    } else {
                        count++;
                        
                        string pRetail = p.get('Cost__c');
                    	retail += Decimal.valueOf(pRetail != null && pRetail != '' ? pRetail : '0');
                    }
                    
                    string daysIn = p.get('Days_In_Inventory__c');
                    
                    if (daysIn != null && daysIn != '') {
                    	averageDays += Decimal.valueOf(daysIn);
                    }
                    
                    d.Products.add(p);
                }
            }
            
            integer divisor = count > 0 ? count : 1;
            
            d.Fields = sgSerializer.serialize(m, columns);
            
            d.Fields.put('In_Stock', string.valueOf(count));
            d.Fields.put('Inventory_Cost', string.valueOf(retail));
            d.Fields.put('Retail', string.valueOf(retail / divisor));
            d.Fields.put('Average_Days', string.valueOf(averageDays / divisor));
            d.Fields.put('Last_30_Days', string.valueOf(last30));
            d.Fields.put('Purchased', string.valueOf(purchased));
            d.Fields.put('ActiveInventory', string.valueOf(searchModelIds.containsKey(m.Id) ? searchModelIds.get(m.Id) : 0));
            
            d.Fields.put('MainImage__c', sgUtility.getMainImage(m.AWS_Image_Array__c));
            //d.Fields.remove('AWS_Image_Array__c');
            
            if (videos.containsKey(m.Id)) {
                d.Fields.put('Video_URL__c', 'https://www.youtube.com/embed/' + ((Video__c) videos.get(m.Id)).YTVideoId__c);
            }
            
            details.add(d);
        }
        
        return details;
    }
}