@isTest
public class sgTestAuth {
    static testMethod void check() {
        string token = sgUtility.generateRandomString(10);
        
        Team_Member__c t = sgDataSeeding.TeamMembers().get(0);
        
        t.Token__c = token;
        t.Token_Expiration__c = system.now().addDays(10);
        
        insert t;
        
        try {
            sgAuth.skipForTest = false;
            
        	sgAuth.check(token);
            
            system.assert(true);
        } catch (Exception e) {
            system.assert(false);
        }
    }
    static testMethod void authed() {
        string token = sgUtility.generateRandomString(10);
        
        Team_Member__c t = sgDataSeeding.TeamMembers().get(0);
        
        t.Token__c = token;
        t.Token_Expiration__c = system.now().addDays(10);
        
        insert t;
        
        system.assert(sgAuth.authed(token));
    }
    
    static testMethod void notFound() {
        string token = sgUtility.generateRandomString(10);
        
        Team_Member__c t = sgDataSeeding.TeamMembers().get(0);
        
        t.Token__c = token;
        t.Token_Expiration__c = system.now().addDays(10);
        
        insert t;
        
        try {
            sgAuth.skipForTest = false;
            
        	sgAuth.check(sgUtility.generateRandomString(10));
            
            system.assert(false);
        } catch (Exception e) {
            system.assert(true);
        }
    }
    
    static testMethod void hashPassword() {
        string salt = sgUtility.generateRandomString(10);
        string password = sgUtility.generateRandomString(10);
        string saltedPassword = password + salt;
        
        string hashed = EncodingUtil.base64Encode(crypto.generateDigest('SHA-512', Blob.valueOf(saltedPassword)));
        
        string authPassword = sgAuth.hashPassword(password, salt);
        
        system.assertEquals(hashed, authPassword);
    
    }
    
    static testMethod void hasRoles() {
        List<Role__c> roles = sgDataSeeding.Roles();
        insert roles;
        
        Team_Member__c t = sgAuth.user;
        t.Role__c = roles.get(0).Id;
        update t;
        
        system.assert(sgAuth.hasRole('Admin, test'));
    }
    
    static testMethod void resetToken() {
        Team_Member__c t = new Team_Member__c();
        
        sgAuth.resetToken(t);
        
        system.assert(t.Reset_Token__c.length() > 0);
    }
}