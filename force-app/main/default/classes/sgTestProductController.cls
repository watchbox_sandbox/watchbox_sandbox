@isTest
public class sgTestProductController {
    static testMethod void get1() {
        Product2 p = new Product2(
        	Name = 'Test Product',
            IsActive = true
        );
        
        insert p;
        
        RestRequest r = new RestRequest();
        r.params.put('id', p.Id);
        RestContext.request = r;
        
        List<Map<string, string>> product = sgProductController.get();
        
        system.assertEquals(p.Id, product.get(0).get('Id'));
    }

    static testMethod void get2() {
        Product2 p = new Product2(
        	Name = 'Test Product',
            IsActive = true
        );
        
        insert p;

        RestRequest r = new RestRequest();
        r.params.put('query', p.Name);
        RestContext.request = r;
        
        List<Map<string, string>> product = sgProductController.get();
        
        system.assertEquals(p.Id, product.get(0).get('Id'));
    }
    /*
    static testMethod void post1() {
        Product2 p = new Product2(
        	Name = 'Test Product',
            Status__c = 'Available'
        );
        
        insert p;
        
        sgProductController.post(p.Id);
        
        Product2 p2 = [select Id, Status__c, Previous_Status__c from Product2 limit 1];
        
        system.assertEquals('Available', p2.Previous_Status__c);
    }
    
    static testMethod void post2() {
        Product2 p = new Product2(
        	Name = 'Test Product',
            Status__c = 'On Hold',
            Previous_Status__c = 'Available'
        );
        
        insert p;
        
        sgProductController.post(p.Id);
        
        Product2 p2 = [select Id, Status__c, Previous_Status__c from Product2 limit 1];
        
        system.assertEquals('Available', p2.Status__c);
    }
    */
    
    static testMethod void post3() {
        try {
            sgProductController.post('');
            
            system.assert(false);
        } catch (Exception e) {
            system.assert(true);
        }
    }
    
    static testMethod void put() {
        Product2 p = new Product2(
        	Name = 'Test Product',
            Status__c = 'Available'
        );
        
        insert p;
        
        string cost = '1.01';
        
        sgProductController.put(p.Id, 'MSP__c', cost);
        
        Product2 p2 = [select Id, MSP__c from Product2 limit 1];
        
        system.assertEquals(cost, string.valueOf(p2.MSP__c));
    }
}