global class sgProductChannelsDTO {
    public String ProductId {get;set;}
    public List<sgChannelInfoDTO> Channels {get;set;}
}