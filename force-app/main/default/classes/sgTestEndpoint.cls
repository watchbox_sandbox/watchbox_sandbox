@isTest
public class sgTestEndpoint {

    static testmethod void construct_missingSetting() {
        //arrange
        //
        Exception testEx;
        
        //act
        //
        try {
            new sgEndpoint('noname', 'Watchbox');
        }
        catch(Exception ex) {
            testEx = ex;
        }
        
        //assert
        //
        System.assert(testEx != null);
    }
    
    static testmethod void construct() {
        //arrange
        //
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'standard');
        
        //act
        //
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');

        //assert
        //
        System.assert(endpt != null);
    }
    
    static testmethod void authenticate_oauthBadrequest() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'oauth');
        //create a mock that has a status code of 400
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(400, null, null));
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        
        //act
        //
        Exception resultEx;
        try {
            Test.startTest(); //required due to the DML above and using a callout
            endpt.Authenticate();
            Test.stopTest();
        } 
        catch(Exception ex) {
            resultEx = ex;
        }
        
        //assert
        //
        System.assertEquals(true, resultEx.getMessage().contains('Status Code: 400'));
    }

    static testmethod void authenticate_magentotokenBadrequest() {
        // Arrange
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'magentotoken');
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(400, null, null));
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');

        // Act
        Exception resultEx;
        try {
            Test.startTest();
            endpt.Authenticate();
            Test.stopTest();
        }
        catch(Exception ex) {
            resultEx = ex;
        }

        // Assert
        System.assertEquals(true, resultEx.getMessage().contains('Status Code: 400'));
    }

    static testmethod void authenticate_oAuthConnectionException() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'oauth');
        //create a mock connection that will throw an error
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse('Mock Exception'));
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        
        //act
        //
        Exception resultEx;
        try {
            Test.startTest(); //required due to the DML above and using a callout
            endpt.Authenticate();
            Test.stopTest();
        } 
        catch(Exception ex) {
            resultEx = ex;
        }
        
        //assert
        //
        System.assertEquals(true, resultEx.getMessage().contains('Connection Issue'));
    }
    
    static testmethod void authenticate_oAuthSuccess() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'oauth');
        String body = 'access_token=faketoken';
        Map<String,String> headers = new Map<String, String>();
        headers.put('Content-Type', 'plain text');
        //create a mock connection that returns a valid response
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, body, headers));
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        endpt.Authenticate();
        String token = endpt.AccessToken;
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('faketoken', token);
    }

    static testmethod void authenticate_magentotokenAuthLocked() {
        // If authentication is already taking place (maybe in another thread/worker/?
        // auth should throw an exception

        // Arrange
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        Surge_Endpoint_Settings__c setting = createFakeSetting('test', env, 'magentotoken');
        setting.Is_Authenticating__c = true;
        update setting;
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');

        // Act
        Exception resultEx;
        try {
            Test.startTest();
            endpt.Authenticate();
            Test.stopTest();
        }
        catch (Exception ex) {
            resultEx = ex;
        }

        // Assert
        System.assertEquals(true, resultEx.getMessage().contains('authrorization is in progress'));
    }

    static testmethod void authenticate_magentotokenAuthFail() {
        // Arrange
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'magentotoken');
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');

        String body = '{"message":"You did not sign in correctly or your account is temporarily disabled."}';
        Map<String,String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, body, headers));

        // Act
        Exception resultEx;
        try {
            Test.startTest();
            endpt.Authenticate();
            Test.stopTest();
        }
        catch (Exception ex) {
            resultEx = ex;
        }

        // Assert
        System.assertEquals(true, resultEx.getMessage().contains('cannot get token'));
    }

    static testmethod void authenticate_magentotokenSetTokenSuccess() {
        // Arrange
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'magentotoken');
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');

        String body = '"faketoken"';
        Map<String,String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, body, headers));

        // Act
        Test.startTest();
        endpt.Authenticate();
        Surge_Endpoint_Settings__c savedSetting = Surge_Endpoint_Settings__c.getInstance('test');
        Test.stopTest();

        // Assert
        System.assertEquals('faketoken', endpt.AccessToken);
        // Until i figure out how to save the token after the future txn is done, this will fail...
        //System.assertEquals('faketoken', savedSetting.Token_Part1__c);
    }

    static testmethod void authenticate_oAuthResponseMissingTokenName() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'oauth');
        String body = 'different_name=faketoken';
        Map<String,String> headers = new Map<String, String>();
        headers.put('Content-Type', 'plain text');
        //create a mock connection that returns a valid response
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, body, headers));
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        
        //act
        //
        Exception resultEx;
        try {
            Test.startTest(); //required due to the DML above and using a callout
            endpt.Authenticate();
            Test.stopTest();
        }
        catch (Exception ex) {
            resultEx = ex;
        }
        //assert
        //
        System.assertEquals('sgEndpoint.InvalidAuthenticationResponseException', resultEx.getTypeName());
    }
    
    static testmethod void authenticate_oAuthJsonSuccess() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('test', env, 'oauth');
        String body = '{"access_token":"faketoken"}';
        Map<String,String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        //create a mock connection that returns a valid response
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, body, headers));
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        endpt.Authenticate();
        Test.stopTest();
        
        //assert
        //
        System.assertEquals('faketoken', endpt.AccessToken);
    }
    
    static testmethod void get_success() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        Surge_Endpoint_Settings__c settings = createFakeSetting('test', env, 'oauth');
        //create a mock that has a status code of 400
        String returnPayload = 'payload';
        sgMockMultipurposeHttpResponse mock = new sgMockMultipurposeHttpResponse(200, returnPayload, null);
        Test.setMock(HttpCalloutMock.class, mock);
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        //create call settings
        String path = '/method';
        String params = 'param1=value1';
        String headers = 'Content-Type=application/json';
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        Blob result = endpt.get(path, params, headers);
        Test.stopTest(); 
        
        //assert
        //
        System.assertEquals(settings.Endpoint_Url__c + 'method?param1=value1', mock.LastRequest.getEndpoint());
        System.assertEquals(returnPayload, result.toString());
    }
    
    static testmethod void post_success() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        Surge_Endpoint_Settings__c settings = createFakeSetting('test', env, 'oauth');
        //create a mock that has a status code of 400
        String returnPayload = 'payload';
        sgMockMultipurposeHttpResponse mock = new sgMockMultipurposeHttpResponse(200, returnPayload, null);
        Test.setMock(HttpCalloutMock.class, mock);
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        //create call settings
        String path = '/method';
        Blob payload = Blob.valueOf('send payload');
        String params = 'param1=value1';
        String headers = 'Content-Type=application/json';
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        Blob result = endpt.post(path, payload, params, headers);
        Test.stopTest(); 
        
        //assert
        //
        System.assertEquals(payload, mock.LastRequest.getBodyAsBlob());
        System.assertEquals('POST', mock.LastRequest.getMethod());
    }
    
    static testmethod void put_success() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        Surge_Endpoint_Settings__c settings = createFakeSetting('test', env, 'oauth');
        //create a mock that has a status code of 400
        sgMockMultipurposeHttpResponse mock = new sgMockMultipurposeHttpResponse(200, null, null);
        Test.setMock(HttpCalloutMock.class, mock);
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        //create call settings
        String path = '/method';
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        Blob result = endpt.put(path, null, null, null);
        Test.stopTest(); 
        
        //assert
        //
        System.assertEquals('PUT', mock.LastRequest.getMethod());
    }
    
    static testmethod void delete_success() {
        //arrange
        //
        //create a fake setting that uses oauth
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        Surge_Endpoint_Settings__c settings = createFakeSetting('test', env, 'oauth');
        //create a mock that has a status code of 400
        sgMockMultipurposeHttpResponse mock = new sgMockMultipurposeHttpResponse(200, null, null);
        Test.setMock(HttpCalloutMock.class, mock);
        //instanciate the endpoint class
        sgEndpoint endpt = new sgEndpoint('test', 'Watchbox');
        //create call settings
        String path = '/method';
        
        //act
        //
        Test.startTest(); //required due to the DML above and using a callout
        Blob result = endpt.del(path, null, null);
        Test.stopTest(); 
        
        //assert
        //
        System.assertEquals('DELETE', mock.LastRequest.getMethod());
    }
    
    //*************************************************************************
    // Helper methods
    static Surge_Endpoint_Settings__c createFakeSetting(String name, String env, String authMode) {
        Surge_Endpoint_Settings__c setting = new Surge_Endpoint_Settings__c();
        setting.Environment__c = env;
        setting.Name = setting.Endpoint_Name__c = name;
        setting.Auth_Mode__c = authMode;
        setting.Auth_Url__c = 'https://test.authendpoint/?clientId={clientId}/';
        setting.Auth_Method__c = '';
        setting.Auth_Headers__c = '';
        setting.Auth_Response_Key__c = 'access_token';
        setting.Client_ID__c = '12345';
        setting.Client_Secret__c = '67891';
        setting.Password__c = 'password';
        setting.User_Name__c = 'username';        
        setting.Endpoint_Url__c = 'https://test.endpoint/';
        setting.Organization__c = 'Watchbox';

        if (authMode == 'magentotoken') {
            setting.Token_ttl__c = 14400000; // 4 hours
            setting.Is_Authenticating__c = false;
        }

        insert setting;
        return setting;
    }
}