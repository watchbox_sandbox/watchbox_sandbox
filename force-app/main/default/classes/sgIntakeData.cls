global class sgIntakeData {
    public Map<string, string> Brands {get; set;}
    public List<string> Originations {get; set;}
    public Map<string, string> SalesAssociates {get; set;}
    public Map<string, string> Locations {get; set;}
    public Map<string, string> Customers {get; set;}
    public Map<string, string> Emails {get; set;}
    
    public List<Map<string, string>> Products {get; set;}
    
    public static List<string> Fields {
        get {
            return new List<string>{'Id',
                'Intake_Status__c',
                'Reference__c',
                'Product__r.Id',
                'Product__r.AWS_Web_Images__c',
                'Product__r.Cost__c',
                'Product__r.MSP__c',
                'Product__r.Ask__c',
                'Product__r.Origination_Type__c',
                'Product__r.Original_Dealer_Name__c',
                'Product__r.Inventory_ID__c',
                'Product__r.Description',
                'Product__r.Papers__c',
                'Product__r.Boxes__c',
                'Product__r.Model__c',
                'Product__r.Model__r.Name',
                'Product__r.Watermark_Index__c',
                'Watch_Model__c',    
                'Watch_Model__r.Name',
                'Watch_Model__r.AWS_Image_Array__c',
                'Product__r.Name',
                'Product__r.Product_Type__c',
                'Team_Member__c',
                'Team_Member__r.Name__c',
                'Team_Member__r.Profile_Picture__c',
                'Tracking_Number__c',
                'Brand__c',
                'Watch_Brand__c',
                'Watch_Brand__r.Name',
                'LastModifiedDate',
                
                'Is_An_Issue__c',
                'Is_An_Issue_Text__c',
                'Strap_Good__c',
                'Boxes_Good__c',
                'Straight_To_Post__c',
    
                'Serial__c',
                'Serial_Verification__c',
                'Watch_Year__c',
                'Year_Verification__c',
                'Warranty_Card_Date__c',
                'Warranty_Card_Verification__c',
                    
                'Boxes__c',
                'Papers__c',
                'CSA_Ran__c',
                'Suspicious__c',
                    
                'Triple_Box_Set__c',
                'Inner_Box_Set__c',
                'Outer_Box_Set__c',
                'Instruction_Booklet__c',
                'Warranty_Card_Certificate__c',
                'Service_Booklet__c',
                'Chronometer_Certificate_Booklet__c',
                'Special_Edition_Certificate__c',
                'Additional_Strap__c',
                'Extra_Straps__c',
                'Extra_Links__c',
                'Screwdriver__c',
                'Tool_Stylus__c',
                'Polishing_Cloth__c',
                'Hangtag__c',
                'Bezel_Protector__c',
                'Document_Holder__c',
                //'Notes_Missing_Items__c',
				'More_Items_To_Come__c',
                'WalkIn__c',
                'Deal__r.Salesperson__r.Name__c',
                'Deal__r.Salesperson__r.Profile_Picture__c',
                'Deal__r.Account.Name',
                'Deal__r.Type',
                'Deal__r.Contact_Record__c',
                'Is_Active__c'
            };
        }
    }
    
    public static List<string> Transitions {
        get {
            return new List<string> {
                'New',
                'In Progress',
                'Complete',
                'Pickups',
                'Approve',
                'Approved'
            };
    	}
	}
    public static boolean transitionAllowed(Id id, string status) {
        Product2 p = [select id, Intake_Inspection__c from Product2 where Id = :id limit 1];
        
        return transitionAllowed(p.Intake_Inspection__c, status);
    }
    
    public static boolean transitionAllowed(string oldStatus, string newStatus) {
        
        
        integer index = sgUtility.indexOf(Transitions, oldStatus);
        if (index == -1) {
            return false;
        } else if (newStatus == Transitions.get(index + 1)) {
            return true;
        }
        
        return false;
    }
    
    public sgIntakeData() {    
		Map<string, string> reqParams = RestContext.request.params.clone();
        reqParams.remove('inspectionId');
        
        Products = AsProducts();
        
        //explicitly send empty paramaters so that the request parameters are not used
        Map<string, string> lookupParams = new Map<string, string>();
        Brands = sgLookupController.get('Brand_object__c', 'id', 'name', lookupParams);
        Originations = sgUtility.getPicklistValues('Opportunity', 'Type');
        Locations = sgLookupController.get('Location__c', 'id', 'name', lookupParams);
        Customers = sgLookupController.get('Contact','id', 'name', lookupParams);   
        Emails = sgLookupController.get('Contact','id', 'email', lookupParams); 
        
        //get team members who have a product in the queues
        Map<string, string> salesAssociatesParams = new Map<string, string>{
            //'orderBy' => 'Name__c'
        };
        
        Map<string, Product2> intakeProducts = new Map<string, Product2>();
            
        for (Map<string, string> p : Products) {
            string heldBy = p.get('Team_Member__c');
            if (heldBy != null) {
            	salesAssociatesParams.put('group1' + 'Id', heldBy);
            }
            
            p.put('Status__c', p.get('Intake_Status__c'));
            p.remove('Intake_Inspection_Status__c');
            p.put('MainImage__c', sgUtility.getMainImage(p.get('Product__rAWS_Web_Images__c')));
            
            p.put('ModelImage__c', sgUtility.getMainImage(p.get('Watch_Model__rAWS_Image_Array__c')));
            p.remove('Watch_Model__rAWS_Image_Array__c');
            
            //have to create missing products here so photos can be uploaded at intake form
            //since dml cannot happen prior to a callout
            if (p.get('Product__rId') == '') {
                intakeProducts.put(p.get('Id'), new Product2(
                    Name = 'Intake Product',
                    IsActive = true
                ));
            }
        }
        
        insert intakeProducts.values();
        List<Intake__c> intakes = [select Id, Product__c from Intake__c where Id in :intakeProducts.keySet()];
        for (Intake__c i : intakes) {
            i.Product__c = intakeProducts.get(i.Id).Id;
        }
        update intakes;
        
        SalesAssociates = sgLookupController.get('Team_Member__c', 'id', 'name__c', salesAssociatesParams);
    }
    
    public static Product2 CreateProduct(Intake__c intake) {
        system.debug(intake.Product__r.Model__c);
        Model__c m = [select Name from Model__c where Id = :intake.Product__r.Model__c limit 1];
        Product2 p = new Product2 (
            Id = intake.Product__c,
            Name = m.Name,
            Model__c = intake.Product__r.Model__c,
            Watch_Brand__c = intake.Watch_Brand__c,
            Serial__c = intake.Serial__c,
            Boxes__c = intake.Boxes__c ? 'Yes' : 'No',
            Papers__c = intake.Papers__c ? 'Yes' : 'No', 
            WUW_Date_Created__c = Date.today(),
            Product_Type__c = 'Regular Goods',
            AWS_Web_Images__c = intake.AWS_Web_Images__c,
            IsActive = true
        );
        
        upsert p;
        
        p = [select Id, WUW_Inventory_ID__c, Auto_Inventory_ID__c from Product2 where Id = :p.Id limit 1];
        
        intake.Product__c = p.Id;
        update intake;
        
        p.WUW_Inventory_ID__c = p.Auto_Inventory_ID__c;
        update p;
        
		return p;
    }
    
    public static Inspection__c CreateInspection(Product2 p, Intake__c n) {
        Inspection__c i = new Inspection__c(
        	Related_Inventory__c = p.Id,
            //Team_Member__c = n.Team_Member__c,
            Intake__c = n.Id
        );
        
        insert i;
        
        return i;
    }
    
    public static List<Map<string, string>> AsProducts() {
        return AsProducts(null);
    }
    
    public static List<Map<string, string>> AsProducts(Map<string, string> additional) {
        Map<string, string> params = RestContext.request.params.clone();
        params.remove('inspectionId');
        
        if (additional != null) {
            for (string k : additional.keySet()) {
                params.put(k, additional.get(k));
            }
        }
        
        string query = 'select ' + string.join(Fields, ', ') + ' from Intake__c';
        
        params.put('limit', '100');
        
		params.put('Is_Active__c', 'true');  
        
        return sgSerializer.serializeFromQuery(query, params);
    }
}