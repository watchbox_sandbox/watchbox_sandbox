@isTest
public class opportunityTriggerTest {
/*
    public opportunityTriggerTest(){
        //empty constructor
    }
    
    public static testMethod void updateParentAccount_test(){
        //create necessary test records and variables
        User u1 = dataFactory.buildSecondaryUser();
        insert u1;
        User operations = dataFactory.testOperationsUser();
        insert operations;
        Account testAcc = dataFactory.buildAccount();
        Opportunity testOpp = dataFactory.buildOpportunity();
        testAcc.ownerId = operations.id;
        testOpp.ownerId = operations.id;
        insert testAcc;
        testOpp.AccountId = testAcc.id;
        insert testOpp;
        system.debug('Original account owner: ' + testAcc.ownerId);
        system.debug('Origian opp owner: ' + testOpp.OwnerId);
        system.debug('Operations Id: '+ operations.id);
        system.debug('Operations Name: '+operations.LastName);
        Brand_Object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries;
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Watch_Purchase__c testWP = dataFactory.buildWatchPurchase(testopp);
        testWp.model__c = testModel.id;
        testWP.Brand__c = testBrand.id;
        insert testWP;
        testOpp.OwnerId = u1.id;
        update testOpp;
        system.debug('New opp owner: ' + testOpp.OwnerId);
        system.debug('User 1 id: '+u1.id);
        test.startTest();
        Account checkAccount = [SELECT id, ownerId, name FROM Account WHERE name = 'James Jones'];
        	system.assertEquals(checkAccount.OwnerId, testOpp.OwnerId);
        	system.assertEquals(testOpp.OwnerId, u1.id);
        test.stopTest();
    }
    */
 static testmethod void deleteOpportunity_removeHold_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Product2 product = TestFactory.createProduct();
        Watch_Sale__c watchSale = TestFactory.createWatchSale(product, opp);
        
        product.Hold_Expiration__c = DateTime.now().addDays(1);
        update product;
        
        delete opp;
        
        Product2 result = [SELECT Id, Hold_Expiration__c, Status__c, Calculated_Status__c FROM Product2 WHERE Id =:product.Id];
        
        System.assertEquals(null, result.Hold_Expiration__c);
        System.assertNotEquals('On Hold', result.Calculated_Status__c);
    
    }
    static testmethod void UpdateOppEventDateFieldTest() {
        Opportunity opp = TestFactory.createOpportunity();
        set<id> oppids=new set<id>();
        oppids.add(opp.id);
         test.startTest();
        UpdateOppEventDateField.updateEventDateField(oppids );
        test.stopTest();
    
    }
}