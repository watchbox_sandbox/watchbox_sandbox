@isTest
public class OverrideWatchPurchaseExtensionTests {
    static testmethod void populateImages_success_test() {
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(null);
        Model__c model = TestFactory.createModel();
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wp);
        OverrideWatchPurchaseExtension extension = new OverrideWatchPurchaseExtension(sController); 
        extension.ModelId = (string)model.Id;
        extension.populateImages();
        
        System.assertEquals(1, extension.ImageURLs.size());
    }
    
    static testmethod void populateImages_exception_test() {
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wp);
        OverrideWatchPurchaseExtension extension = new OverrideWatchPurchaseExtension(sController); 
        extension.ModelId = 'invalid';
        extension.populateImages();
        
        System.assert(ApexPages.hasMessages()); 
        System.assertEquals(0, extension.ImageURLs.size());    
    }
    
    static testmethod void toggleModelDisplay_success_test() {
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(wp);
        OverrideWatchPurchaseExtension extension = new OverrideWatchPurchaseExtension(sController); 
        extension.toggleModelDisplay();
        
        System.assert(extension.ShowModelOverride); 
        System.assertEquals(0, extension.ImageURLs.size());  
    }
}