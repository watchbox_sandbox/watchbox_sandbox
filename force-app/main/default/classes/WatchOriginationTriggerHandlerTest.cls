@isTest
public class WatchOriginationTriggerHandlerTest {
    
   @testSetup static void test_SetUp() {

       
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp1';
        opp.StageName = 'New';
         opp.CloseDate = Date.today();
        
        insert opp;

        List<Watch_Purchase__c> list_WatchPurchase = new List<Watch_Purchase__c>();
        Watch_Purchase__c origination = new Watch_Purchase__c();
        origination.Origination_Opportunity__c = opp.id;
        origination.Origination_Value_Offered__c = 10.0;
        origination.Client_s_Desired_Value__c = 10.0;
        origination.WB_Last_Purchase_Price__c = 10.0;
       origination.Origination__c='Chopard';
        list_WatchPurchase.add(origination);
                
        Watch_Purchase__c origination1 = new Watch_Purchase__c();
        origination1.Origination_Opportunity__c = opp.id;
        origination1.Origination_Value_Offered__c = 20.0;
        origination1.Client_s_Desired_Value__c = 20.0;
        origination1.WB_Last_Purchase_Price__c = 20.0;
       	origination1.Origination__c='Chopard';
        list_WatchPurchase.add(origination1);
               insert list_WatchPurchase;
       
   }
    
    
    
    static testMethod void countRLonOpportunityUpdatetest(){
      
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp2';
        opp.StageName = 'New';
         opp.CloseDate = Date.today();
        
        insert opp;
        
        Watch_Purchase__c origination = new Watch_Purchase__c();
        origination.Origination_Opportunity__c = opp.id;
        origination.Origination_Value_Offered__c = 10.0;
        origination.Client_s_Desired_Value__c = 10.0;
        origination.WB_Last_Purchase_Price__c = 10.0;
        origination.Origination__c='Chopard';
         insert origination;
     
        
     }
    static testMethod void countRLonOpportunityUpdatetesFalse(){
               
     Watch_Purchase__c origination1 = [select id from Watch_Purchase__c limit 1 ];
    delete origination1;
        
        }
    }