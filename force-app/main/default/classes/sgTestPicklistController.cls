@isTest
global class sgTestPicklistController {
    static testMethod void get() {
        RestContext.request = new RestRequest();

        string type ='SALE';
        string priority ='High';
        string speed = 'Domestic: FedEx 2Day';

        RestContext.request.addParameter('type', type);

        RestContext.request.addParameter('speed', speed);

        Picklist__c picklist = new Picklist__c (
            Shipping_Speed2__c = speed, 
            Picklist_Type__c = type);

        insert picklist;

        Picklist_Detail__c picklistDetail = new Picklist_Detail__c (
            Related_Picklist__c = picklist.id
        );

        insert picklistDetail;

        object o = sgPicklistController.get();

        system.assert(o != null);
    }
}