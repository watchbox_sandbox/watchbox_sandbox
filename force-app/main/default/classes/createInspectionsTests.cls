@isTest
public class createInspectionsTests {

    public static testMethod void testOverloadedCreateInspections(){
        //create necessary test records
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Intake__c testIntake = dataFactory.buildIntake(testOpp);
        insert testIntake;
        Brand_object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries; 
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Product2 testProduct = dataFactory.buildProduct(testModel.Id);
        insert testProduct;
        //create new instance of createInspections and run test with intake record present
        createInspections ci = new CreateInspections();
        
        
        //now we run the whole process over again, only this time without an intake record present
        ////create necessary test records
        Opportunity testOpp2 = dataFactory.buildOpportunity();
        insert testOpp2;
        
        Brand_object__c testBrand2 = dataFactory.buildBrand();
        insert testBrand2;
        Series__c testSeries2 = dataFactory.buildSeries(testBrand2.id);
        insert testSeries2; 
        Model__c testModel2 = dataFactory.buildModel(testBrand2.id, testSeries2.id);
        insert testModel2;
        Product2 testProduct2 = dataFactory.buildProduct(testModel2.Id);
        insert testProduct2;
        //create new instance of createInspections to run test with intake record not present
        createInspections ci2 = new CreateInspections();
        
        // and a third time for an instance where there is already 1 inspection for each intake
        //create necessary test records
        Opportunity testOpp3 = dataFactory.buildOpportunity();
        insert testOpp3;
        Intake__c testIntake3 = dataFactory.buildIntake(testOpp);
        insert testIntake3;
        Brand_object__c testBrand3 = dataFactory.buildBrand();
        insert testBrand3;
        Series__c testSeries3 = dataFactory.buildSeries(testBrand.id);
        insert testSeries3; 
        Model__c testModel3 = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel3;
        Product2 testProduct3 = dataFactory.buildProduct(testModel.Id);
        insert testProduct3;
        //we'll also add a pre-existing inspection to hit code that creates a collection of existing inspections
        Inspection__c testInspection = dataFactory.buildInspection(testOpp3.id, testIntake3.id);
        insert testInspection;
        //create new instance of createInspections and run test with intake record present
        createInspections ci3 = new CreateInspections();
        
        
        
        //now we run all our tests at once
        test.startTest();
        ci.createInspectionRecords(testOpp.Id);
        ci2.createInspectionRecords(testOpp2.Id);
        ci3.createInspectionRecords(testOpp3.Id);
        test.stopTest();
    }
    public static testMethod void testCreateInspectionsByPageRef(){
        test.startTest();
        PageReference inspectionPage = page.vfInspectionConnector;
        //create necessary test records
        //create necessary test records
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Intake__c testIntake = dataFactory.buildIntake(testOpp);
        insert testIntake;
        Brand_object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries; 
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Product2 testProduct = dataFactory.buildProduct(testModel.Id);
        insert testProduct;
        //set test page and create new instance of createInspections class and new standard controller
        test.setCurrentPage(inspectionPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
        createInspections ci = new CreateInspections(controller);
        //create page reference to capture result
        PageReference result = ci.createInspectionRecords();
        PageReference result2 = ci.createInspectionRecords();
        //make sure there is data return
        system.assertNotEquals(null, result);
    }
    public static testMethod void testCreateReturnInspectionsByPageRef(){
        test.startTest();
        PageReference inspectionPage = page.vfcreateReturnInspectionsConnector;
        //create necessary test records
        
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Return__c testReturn = new Return__c(deal__c = testOpp.id);
        insert testReturn;
        Intake__c testIntake = dataFactory.buildIntake(testOpp);
        testIntake.Return__c = testReturn.id;
        insert testIntake;
        Brand_object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries; 
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Product2 testProduct = dataFactory.buildProduct(testModel.Id);
        insert testProduct;
        //set test page and create new instance of createInspections class and new standard controller
        test.setCurrentPage(inspectionPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(testReturn);
        createReturnInspections ci = new CreateReturnInspections(controller);
        //create page reference to capture result
        PageReference result = ci.createInspectionsForReturns();
        PageReference result2 = ci.createInspectionsForReturns();
        //make sure there is data return
        system.assertNotEquals(null, result);
    }
}