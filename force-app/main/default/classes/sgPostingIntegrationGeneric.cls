public class sgPostingIntegrationGeneric extends sgPostingIntegration {
    
    public override void SetPost() {
        this.AddActivity('Posting Requested', 'Please add the product to the "' + this.Name + '" system', 'Open');
    }
    
    public override void SetVisible() {
        this.AddActivity('Posting Visible Requested', 'Please add the product to the "' + this.Name + '" system (if not already) and make it available for purchase', 'Open');
    }

    public override void RemoveVisible() {
        this.AddActivity('Posting Remove Visibility Requested', 'Please remove the purchase availability from the "' + this.Name + '" system (if it is visible)', 'Open');
    }
    
    public override void RemovePost() {
        this.AddActivity('Posting Removal Requested', 'Please remove the product from the "' + this.Name + '" system (if it has been posted)', 'Open');
    }
    
    public override void DoUpdate() {
        this.AddActivity('Price Update Request', 'Please update the price in the "' + this.Name + '" system (if it has been posted)', 'Open');
    }
    
}