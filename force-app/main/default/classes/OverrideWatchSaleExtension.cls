public class OverrideWatchSaleExtension {
    private static final string ON_HOLD = 'On Hold';
	private ApexPages.StandardController sController;
    public Product2 DisplayProduct {get; set;}
    public Id ProductId {get; set; }
    public string ProductName {get; set;}
    public List<string> ImageURLs {get; set;}
  
     public OverrideWatchSaleExtension(ApexPages.StandardController sc) {
     	sController=sc;
        DisplayProduct = new Product2();
     }
      
     public void populateInventory() {
         Watch_Sale__c ws = (Watch_Sale__c)sController.getRecord();
         ImageURLs = new List<string>();
         
         try {
             DisplayProduct = [SELECT Id, AWS_Web_Images__c, Watch_Reference__c, ASK__c, Description, MSP__c, Name, Cost__c, Calculated_Status__c 
                               FROM Product2 
                               WHERE Id =:ws.Inventory_Item__c];
             
             ws.Inventory_Item__c = DisplayProduct.Id;
             
             if (DisplayProduct != null && !string.isEmpty(DisplayProduct.AWS_Web_Images__c)) {
                 for (string url : DisplayProduct.AWS_Web_Images__c.split(',')) {
                     if (!string.isBlank(url) && !url.contains('http')) {
                         url = 'http://' + url;
                     }
                     
                     if (!string.isBlank(url)) {
                     	ImageUrls.add(url);
                     }
                 }
             }
         } 
         catch (Exception e) {
             System.debug('Error getting Product for display in OverrideWatchSaleExtension.InventoryPopulated - ' + e.getMessage());
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error retrieving inventory item, please contact your administrator');
             ApexPages.addMessage(myMsg); 
         }                 
     }
    
    public PageReference addToDeal() {
        PageReference pageRef = null;
        SavePoint sp = Database.setSavepoint();
        Watch_Sale__c ws = (Watch_Sale__c)sController.getRecord();
        
        try {            
            Product2 product = [SELECT Id, AWS_Web_Images__c, Watch_Reference__c, ASK__c, Description, MSP__c, Name, Cost__c, Calculated_Status__c, Inventory_ID__c, Hold_Expiration__c
                                FROM Product2 
                                WHERE Id =:ws.Inventory_Item__c FOR UPDATE];
            
            if (product.Calculated_Status__c != ON_HOLD) {                
                ws.ASK__c = product.ASK__c;
                ws.Cost__c = product.Cost__c;
                ws.MSP__c = product.MSP__c;
                                                                                              
                Holds__c hold = createHold(ws, product);
                updateInventory(product);

                sController.save();
                
                //Redirect to associated Opportunity on success
                pageRef = new PageReference('/' + ws.Opportunity__c);
                pageRef.setRedirect(true);                
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Selected item is already on hold');
             	ApexPages.addMessage(myMsg); 
            }
            
        } catch (Exception e) {     
            Database.rollback(sp);
            pageRef = null;
            System.debug('Error saving watch sale OverrideWatchSaleExtension.addToDeal - ' + e.getMessage());
            
            if (!ApexPages.hasMessages()) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            	ApexPages.addMessage(myMsg); 
            }                        
        }
        
        return pageRef;
    }
    
    @TestVisible
    private Holds__c createHold(Watch_Sale__c ws, Product2 product) {        
        Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Id =: ws.Opportunity__c];
        
        Holds__c hold = new Holds__c();
        hold.MSP__c = ws.MSP__c;
        hold.Sale_Amount__c = ws.Sale_Amount__c;
        hold.Opportunity__c = ws.Opportunity__c;
        hold.Inventory_Number__c = product.Id;
        hold.Holder_Owner__c = UserInfo.getUserId();
        hold.Name = opp.Name + '-' + product.Inventory_ID__c;
        
        insert hold;
        return hold;
    }
    
    @TestVisible
    private void updateInventory(Product2 product) {
        product.Hold_Expiration__c = DateTime.Now().addDays(1);
        update product;
    }
}