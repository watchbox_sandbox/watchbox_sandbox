public class OverrideWatchPurchaseExtension {
    private ApexPages.StandardController sController;
    public List<string> ImageURLs {get; set;}
    public string ModelId {get; set;}
    public boolean ShowModelOverride { get; set;}
    
     public OverrideWatchPurchaseExtension(ApexPages.StandardController sc) {
     	sController=sc;
     }
    
    public void toggleModelDisplay() {
        Watch_Purchase__c wp = (Watch_Purchase__c)sController.getRecord();
        wp.Model__c = null;
        wp.Model_Name__c = '';
         
        ShowModelOverride = true;
        ImageURLs = new List<string>();        
    }
    
    public PageReference save() {
        PageReference pageRef = null;
        Watch_Purchase__c wp = (Watch_Purchase__c)sController.getRecord();
        
        try {
            sController.save();
            
            //Redirect to associated Opportunity on success
            if (!ApexPages.hasMessages()) {
                pageRef = new PageReference('/' + wp.Origination_Opportunity__c);
                pageRef.setRedirect(true);
            }
        } catch (Exception e) {
            pageRef = null;
            System.debug('Error saving watch sale OverrideWatchPurchaseExtension.save - ' + e.getMessage());
            
            if (!ApexPages.hasMessages()) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            	ApexPages.addMessage(myMsg); 
            }             
        }    
        
        return pageRef;
    }
    
    public void populateImages() {
                        
        try {
            ImageURLs = new List<string>();
            Id mId = (Id)ModelId;        
            Model__c model = [SELECT Id, AWS_Image_Array__c FROM Model__c WHERE Id =: mId];
            
            if (model != null && !string.isEmpty(model.AWS_Image_Array__c)) {
                for (string url : model.AWS_Image_Array__c.split(',')) {
                    if (!string.isBlank(url) && !url.contains('http')) {
                        url = 'http://' + url;
                    }
                    
                    if (!string.isBlank(url)) {
                        ImageUrls.add(url);
                    }
                }
            }        
        } catch (Exception e) {
             System.debug('Error getting Images for display in OverrideWatchPurchaseExtension.populateImages - ' + e.getMessage());
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error retrieving images, please contact your administrator');
             ApexPages.addMessage(myMsg); 
        }           
    }
}