@RestResource(urlMapping='/BatchStatusUpdate/*')
global class sgBatchStatusUpdateController {
    
    @HttpGet
    global static void get() {
      RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
        }
        
        string id = RestContext.request.params.get('id');
        
		List<Product2> products = [select Inventory_Id__c,
                                       Cost__c,
                                       Days_Inventoried__c,
                                       Held_By__c,
                                       Days_In_Inventory__c,
                                       Origination_Type__c,
                                   	   Watch_Reference__c,
                                       Watch_Brand__c,
                                   	   Name,
                                   	   AWS_Web_Images__c,
									   Status__c,
                                   	   Id
                                   from Product2
                             	   where Inventory_Id__c = :id Limit 1];

        if(products.isEmpty())
        {
            res.statusCode = 404;    
        } 
        else
        {
            sgBatchStatusUpdateDTO batchStatusUpdatedto = new sgBatchStatusUpdateDTO();
            Product2 p = products[0];
            
            batchStatusUpdatedto.InventoryId = p.Inventory_Id__c;
            batchStatusUpdatedto.Cost = p.Cost__c;
            batchStatusUpdatedto.DaysIn = p.Days_Inventoried__c;
            batchStatusUpdatedto.SalesPerson = p.Held_By__c;
            batchStatusUpdatedto.DayIn = p.Days_In_Inventory__c;
            batchStatusUpdatedto.Origination = p.Origination_Type__c;
            batchStatusUpdatedto.BrandName = p.Watch_Brand__c;
            batchStatusUpdatedto.Reference = p.Watch_Reference__c;
            batchStatusUpdatedto.Name = p.Name;
            batchStatusUpdatedto.MainImage = sgUtility.getMainImage(p.AWS_Web_Images__c);
            batchStatusUpdatedto.Status = p.Status__c;
            batchStatusUpdatedto.ProductId = p.Id;
            
            res.responseBody = Blob.valueOf(JSON.serialize(batchStatusUpdatedto));
            
            res.statusCode = 200;    
        }
    }
    
    @HttpPatch
    global static void patch(List<Product2> products, String newNote)
    {
        update products;
        
        //create a notes record for each product
        if (newNote != null) {
            List<Note> notes = new List<Note>();
            for(Product2 product : products) {
                Note note = new Note(
                    ParentId = product.Id,
                    Title = 'Status Updated to ' + product.Status__c,
                    Body = newNote
                );
                notes.add(note);
            }
            insert notes;
        }
    }

}