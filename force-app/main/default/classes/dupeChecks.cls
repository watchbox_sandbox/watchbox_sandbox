public class dupeChecks {
	    public static void dupeWebCheck(List<Contact> triggerNew, List<Opportunity> newDeals){
        system.debug('dupeWebCheck method fired');
        Set<Id> newIds = New Set<Id>();
        List<Opportunity> moveDeals = New List<Opportunity>();
        List<Contact> deleteCons = New List<Contact>();
        for(Contact cons : triggerNew){
            newIds.add(cons.Id);
        }
        List<Contact> oldCons = [SELECT Id, Email FROM Contact WHERE Id NOT IN :newIds];
        List<Contact> newCons = [SELECT Id, Email FROM Contact WHERE Id IN :newIds];
        
        
        for(Contact con : oldCons)
        {
            for(Contact con2: newCons){
                If((con.Email == con2.Email && con.Id != con2.Id)){
                    system.debug(con2.Id);
                    system.debug(con.Id);
                    deleteCons.add(con2);
                    For(opportunity opp : newDeals){
                        if(opp.contact_Record__c == con2.Id){
                            opp.contact_Record__c = con.Id;
                            moveDeals.add(opp);
                        }
                    }
                }
            }
        }
        Update moveDeals;
        system.debug('Passed moveDeals update command');
        system.debug(deleteCons);
            Delete deleteCons;
        
    }
    
    public static void dupeCheck(List<Contact> triggerNew){
        
            Set<String> newEmails = New Set<String>();
            For(Contact con : triggerNew){
                newEmails.add(con.email);
            }
            List<Contact> duplicates = [SELECT Id, Email FROM Contact WHERE Email IN :newEmails];
            for(contact con : triggerNew){
                for(contact con2 : duplicates){
                    if(con.email == con2.email && con.Id != con2.Id){
                        con.email.addError('Duplicate email exists. Please update original record');
                    }
                }
            }
            
        }
}