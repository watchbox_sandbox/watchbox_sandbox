public class modelsByBrand {
    Public String brand{get;set;}
    Public String filterBy{get;set;}
    Public String filterWuwBy{get;set;}
    Public String wildFilter;
    Public String isVerified{get;set;}
    Public String brandName{get;set;}
    Public brand_object__c findBrand;
    Public Boolean checkVerified;
    Public List<model__c> models{get;set;}
    Public List<wuw_website_models__c> wuwModels{get;set;}
    
 
    public modelsByBrand(){
        //filterBy=null;
        isVerified='all';
        this.brandName = apexpages.currentPage().getParameters().get('name');
        this.filterWuwBy = apexpages.currentPage().getParameters().get('filter');
        filterWuwBy = '%'+filterWuwBy+'%';
        system.debug(filterWuwBy);
        if(this.brandName != null){
            if(this.filterWuwBy != null){
            wuwModels=[SELECT model_name__c, brand__c, reference__c, also_known_as__c, series__c, 
                   google_image__c, images__c FROM wuw_website_models__c WHERE (brand__c=:brandName and model_name__c LIKE :filterWuwBy) OR (brand__c=:brandName and reference__c LIKE :filterWuwBy)];
                system.debug(wuwModels);
            }else{
            
             wuwModels=[SELECT model_name__c, brand__c, reference__c, also_known_as__c, series__c, 
                   google_image__c, images__c FROM wuw_website_models__c WHERE brand__c=:brandName];
                system.debug(wuwModels);
            }
        }
        
    }
    
    public List<SelectOption> getAllBrands(){
        List<SelectOption> theBrands = New List<SelectOption>();
        for(brand_object__c brandObj : [SELECT Id, name FROM brand_object__c ORDER BY name]){
            theBrands.add(new selectOption(brandObj.Id, brandObj.name));
            
        }
        return theBrands;
    }
    public List<SelectOption> getVerifiedStatus(){
        List<SelectOption> verified = New List<SelectOption>();
        	verified.add(new selectOption('all', 'All'));
            verified.add(new selectOption('true', 'Verified'));
        	verified.add(new selectOption('false', 'Unverified'));
        	
        
        return verified;
    }
    
    public pageReference search(){
        findBrand = [SELECT name FROM brand_object__c WHERE name = :brand OR Id = :brand LIMIT 1];
        brandName = findBrand.name;
        //wuwModels=[SELECT model_name__c, brand__c, reference__c, also_known_as__c, series__c, 
                   //google_image__c, images__c FROM wuw_website_models__c WHERE brand__c = :brandName];
        system.debug(wuwModels);
        system.debug(brandName);
        if(isVerified=='all'){
        	if((filterBy == null)||(filterBy=='')){
        		models = [SELECT Id, url_slug__c, sh_image_url__c, name, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE model_brand__c= :brand];
        	} else{
                if(!filterBy.contains('%')){
                    wildFilter = '%'+filterBy+'%';}
            	models=[SELECT Id, name, url_slug__c, sh_image_url__c, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE (model_brand__c=:brand and name LIKE :wildFilter) OR (model_brand__c=:brand and reference__c LIKE :wildFilter)];
        	}
        } else if(isVerified=='true'){
            checkVerified=true;
            if((filterBy == null)||(filterBy=='')){
        		models = [SELECT Id, name, url_slug__c, sh_image_url__c, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE model_brand__c= :brand AND verified__c = :checkVerified];
        	} else{
            	if(!filterBy.contains('%')){
                    wildFilter = '%'+filterBy+'%';}
            	models=[SELECT Id, name, url_slug__c, sh_image_url__c, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE ((model_brand__c=:brand and name LIKE :wildFilter) OR (model_brand__c=:brand and reference__c LIKE :filterBy)) AND( verified__c= :checkVerified)];
        	}
        } else{
            checkVerified=false;
            if((filterBy == null)||(filterBy=='')){
        		models = [SELECT Id, name, url_slug__c, sh_image_url__c, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE model_brand__c= :brand AND verified__c = :checkVerified];
        	} else{
            	if(!filterBy.contains('%')){
                    wildFilter = '%'+filterBy+'%';}
            	models=[SELECT Id, name, url_slug__c, sh_image_url__c, model_brand__c, model_series__c, reference__c, also_known_as__c, verified__c FROM model__c WHERE ((model_brand__c=:brand and name LIKE :wildFilter) OR (model_brand__c=:brand and reference__c LIKE :wildFilter)) AND( verified__c= :checkVerified)];
        	}
        }
        system.debug(models);
        system.debug(models.size());
        
       return null;
    }
    
    public pageReference save(){
        update models;
        return null;
    }
    
	    
       
}