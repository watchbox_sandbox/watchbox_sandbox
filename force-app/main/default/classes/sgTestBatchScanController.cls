@isTest
public class sgTestBatchScanController {
    static List<Product2> setup() {
        List<Product2> products = sgDataSeeding.ProductsWithBrands();
        string l = sgAuth.user.Location__c;
        
        for (Product2 p : products) {
            p.Location__c = l;
        }
        
        insert products;
        
        return products;
    }
    
    static testMethod void get() {
        List<Product2> products = setup();
        
        Batch_Scan__c batch = sgBatchScanController.post();
        
        sgBatchScanController.Scans scan = sgBatchScanController.get();
        
        system.assertEquals(products.size(), scan.CurrentScan.Batch_Details__r.size());
    }
    
    static testMethod void post() {
        List<Product2> products = setup();
        
        Batch_Scan__c batch = sgBatchScanController.post();
        
        system.assertEquals(1, [select Id from Batch_Scan__c].size());
        system.assertEquals(products.size(), batch.Batch_Details__r.size());
    }
    
    static testMethod void put() {
        List<Product2> products = setup();
        
        Batch_Scan__c batch = sgBatchScanController.post();
        
        List<string> ids = new List<string>{
            batch.Batch_Details__r.get(0).Id
        };
            
        string scanType = 'Manual';
        
        sgBatchScanController.put(ids, scanType);
        
        Batch_Detail__c batchDetail = [select Id, Scanned__c, Scan_Type__c from Batch_Detail__c where Id = :ids.get(0) limit 1];
        
        system.assertEquals(true, batchDetail.Scanned__c);
        system.assertEquals(scanType, batchDetail.Scan_Type__c);
    }
    
    static testMethod void patch() {
        List<Product2> products = setup();
        
        Batch_Scan__c postBatch = sgBatchScanController.post();
        sgBatchScanController.patch(postBatch.Id);
        
        Batch_Scan__c batch = [select Id, Scan_Completed__c from Batch_Scan__c limit 1];
        
        system.assert(batch.Scan_Completed__c != null);
    }
    
}