public class OriginationWrapper {
     public integer counter {get;set;}
     public Watch_Purchase__c origination {get;set;} 
    
    public OriginationWrapper(integer counter,Watch_Purchase__c origination) {
       
        this.counter = counter;
        this.origination = origination;
    }

}