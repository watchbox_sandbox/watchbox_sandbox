@isTest
global class sgTestInventoryPhotographyController {
    
    static testMethod void get() {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();   
		
        RestContext.request = new RestRequest();
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, null, null));
        
        Photograph__c photo = new Photograph__c(Url__c = 'test');

        insert photo;
        
        Organization__c org = new Organization__c(Name = 'test');

        insert org;

        object o = sgInventoryPhotographyController.get();

        system.assert(o != null);
    }
    
    @isTest static void post() {
         Product2 product = new Product2(Name = 'test');
         
         insert product;

		Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, null, null));
         
        object url = sgInventoryPhotographyController.post('123',product.Id,'123','123',null,null);
       
        system.assert(url != null);
    }
    
    @isTest static void post_photo_insert() {
         Product2 product = new Product2(Name = 'test');
         
         insert product;

        Photograph__c photo = new Photograph__c(Url__c = 'test');

        insert photo;
        
		Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(200, null, null));
         
        object url = sgInventoryPhotographyController.post('123',product.Id,'123','123',null,photo.id);
        
        system.assert(url != null);

    }
    
    static testMethod void remove() {
        
        Product2 product = new Product2(Name = 'test', AWS_Web_Images__c = 'http');
         
        insert product;
        
        Photograph__c photo = new Photograph__c(Url__c = 'test');

        insert photo;
        
        RestContext.request = new RestRequest();
        
        RestContext.request.addParameter('productId', product.Id);
        
        RestContext.request.addParameter('url','http');
        
        RestContext.request.addParameter('photographId', photo.id);
        
        sgInventoryPhotographyController.remove();
        
        system.assert(product != null);
    }
    
    static testMethod void remove_url_midstring() {
        
        Product2 product = new Product2(Name = 'test', AWS_Web_Images__c = 'test,http');
         
        insert product;
        
        Photograph__c photo = new Photograph__c(Url__c = 'test');

        insert photo;
        
        RestContext.request = new RestRequest();
        
        RestContext.request.addParameter('productId', product.Id);
        
        RestContext.request.addParameter('url','http');
        
        RestContext.request.addParameter('photographId', photo.id);
        
        sgInventoryPhotographyController.remove();
        
        system.assert(product != null);
    }    
    
    static testMethod void patch() {
        Product2 product = new Product2(Name = 'test');
         
        insert product;
        
        List<Photograph__c> photos = new List<Photograph__c>();
        
        sgInventoryPhotographyController.patch(photos, 'c,s,v', product.Id);
         
        system.assert(product != null);
    }
    
}