public class sgPrintingIntakeController extends sgPrintingControllerBase {

    public Intake__c Intake{get;set;}
    
    public sgPrintingIntakeController() {
        super();
        
        Intake = [
            SELECT Name,
            CreatedDate,
            Owner.Name,
            Product__r.Inventory_Id__c,
            Brand__c,
            Watch_Model__r.Name,
            Reference__c,
            Watch_Serial__c,
            Watch_Year__c,
            Boxes__c,
            Papers__c
            FROM Intake__c
            WHERE Id = :parameters.get('id')
        ];
    }
    
}