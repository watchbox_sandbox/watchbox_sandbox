@isTest
public class IntakeTriggerHandlerTests {
    static testmethod void afterInsert_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Intake__c intake = TestFactory.createIntake(opp, wp);
        
        Product2 product = [SELECT Id, ASK__c FROM Product2 LIMIT 1];      
        Intake__c result = [SELECT Id, Product__c FROM Intake__c WHERE Id =: intake.Id];
        
        System.assertEquals(TestFactory.TEST_AMOUNT, product.ASK__c);
        System.assertEquals(product.Id, result.Product__c);
    }
    
    static testmethod void afterInsert_nullWatchPurchase_test() {
        Opportunity opp = TestFactory.createOpportunity();        
        Intake__c intake = TestFactory.buildIntake(opp, null);
        
        intake.Watch_Purchase__c = null;
        insert intake;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');
        
        System.assertEquals(0, count, 'No product should exist');               
    }
    
    static testmethod void afterInsert_nullBrand_test() {
        Opportunity opp = TestFactory.createOpportunity();        
        Intake__c intake = TestFactory.buildIntake(opp, null);
        
        intake.Watch_Brand__c = null;
        insert intake;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');
        
        System.assertEquals(0, count, 'No product should exist');       
    }
    
    static testmethod void afterInsert_nullModel_test() {
        Opportunity opp = TestFactory.createOpportunity();        
        Intake__c intake = TestFactory.buildIntake(opp, null);
        
        intake.Watch_Model__c = null;
        insert intake;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');
        
        System.assertEquals(0, count, 'No product should exist');       
    }
    
    static testmethod void afterInsert_noAssignFlag_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Intake__c intake = TestFactory.buildIntake(opp, wp);
        intake.Assign_Inventory__c = false;
        
        insert intake;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');
        
        System.assertEquals(0, count, 'No product should exist');        
    }
    
    static testmethod void afterInsert_bulk_test() {
        List<Intake__c> intakesToInsert = new List<Intake__c>();
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        
        for (integer i = 0; i < 10; i++) {			
            Intake__c intake = TestFactory.buildIntake(opp, wp);
            intakesToInsert.add(intake);
        }
        
        insert intakesToInsert;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');
        
        System.assertEquals(10, count);
    }
    
    static testmethod void afterInsert_existingInventory_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Product2 product = TestFactory.createProduct();
        
        Intake__c intake = TestFactory.buildIntake(opp, wp);
        intake.Product__c = product.Id;
        insert intake;
        
        integer count = Database.countQuery('SELECT count() FROM Product2');        
        System.assertEquals(1, count, 'No new products should have been generated'); 
        
        Intake__c result = [SELECT Id, Product__c FROM Intake__c WHERE Id =: intake.Id];
        
        System.assertEquals(product.Id, result.Product__c, 'Original product should still be associated');
    }
    
    static testmethod void beforeUpdate_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Intake__c intake = TestFactory.buildIntake(opp, wp);
        intake.Assign_Inventory__c = false;
        insert intake;        
        
        integer count = Database.countQuery('SELECT count() FROM Product2');        
        System.assertEquals(0, count, 'No new products should have been generated');
        
		intake.Assign_Inventory__c = true;
        update intake;
        
        Product2 product = [SELECT Id, ASK__c FROM Product2 LIMIT 1];      
        Intake__c result = [SELECT Id, Product__c FROM Intake__c WHERE Id =: intake.Id];
        
        count = Database.countQuery('SELECT count() FROM Product2');        
        System.assertEquals(1, count, 'New product should have been generated');
        
        System.assertEquals(product.Id, result.Product__c);        
    }
}