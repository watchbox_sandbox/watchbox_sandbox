@isTest
public class ContactPhoneEmailCtrlTest {
    
     public static testmethod void TestContactPhoneEmailCtrl() {
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.Email = 'test@test.com';
        contact.Secondary_Email__c = 'test123@test.com';
        insert contact;
        ApexPages.StandardController controller = new ApexPages.StandardController(contact);
        ContactPhoneEmailCtrl cds = new ContactPhoneEmailCtrl(controller);
        cds.EditPhone();
        cds.Save();
        cds.Cancel();
     }    
}