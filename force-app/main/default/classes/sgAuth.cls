public class sgAuth {
    public static boolean skipForTest = true;
    public static boolean skip = false;
    
    private static boolean ran = false;
    private static Team_Member__c privateUser;
        public static Team_Member__c user {
            get {
                if (Test.isRunningTest()) {
                    if (ran) {
                        return [select Id, First_Name__c, Last_Name__c, Role__r.Name, Location__c, Location__r.Name from Team_Member__c where Id = :privateUser.Id limit 1];
                    }
                    
                    Location__c l = new Location__c(Name = 'Test');
                    insert l;
                    
                    Team_Member__c t = new Team_Member__c(
                        First_Name__c = 'Test',
                        Last_Name__c = 'User',
                        IsActive__c = true,
                        Location__c = l.Id
                    );  
                    insert t;
                    
                    privateUser = t;
                    
                    ran = true;
                }
                
                return privateUser;
    		}
        }
    
    public static string token {
        get {
            if (skip || (skipForTest && Test.isRunningTest())) {
                return '';
            }
            
            return RestContext.request.headers.get('sgtoken');
        }
    }
    
    public static void check() {
        check(token);
    }
    public static void check(string passedToken) {
        if (skip || (skipForTest && Test.isRunningTest())) {
            return;
        }
        
        if (!authed(passedToken)) {
            throw new sgException('User is not authorized');
        }
    }
    
    public static boolean hasRole(string roles) {
        if (Test.isRunningTest() && skipForTest) {
			return true;    
       	}
        
        check();
		
        if (user.Role__c == null) {
            return false;
        }
        
       	List<string> roleList = roles.split(',');
       
       	for (string r : roleList) {
        	if (user.Role__r.Name.toLowerCase() == r.trim().toLowerCase()) {
            	return true;
           	}
       	}
       
       	return false;
	}
    
    public static void requireRole(string roles) {
        check();
        
        if (!hasRole(roles)) {
            throw new sgException('Must have one of the following roles: ' + roles);
        }
    }
    
    public static boolean authed(string passedToken) {
        try {
	        privateUser = [
                select Id, First_Name__c, Last_Name__c, Work_Email__c, Token__c, Token_Expiration__c,
                Group__r.Name, Role__r.Name, Location__c, Location__r.Name
                from Team_Member__c 
                where IsActive__c = true and Token__c = :passedToken and Token_Expiration__c > :system.now()
            ];
            
            return true;
        } catch (Exception e) {
        	return false;       
        }
        
        return false;
    }
    
    public static string hashPassword(string inputPassword, string salt) {
        string saltedPassword = inputPassword + salt;
        
        return EncodingUtil.base64Encode(crypto.generateDigest('SHA-512', Blob.valueOf(saltedPassword)));
    }
    
    public static void setPasswordHash(Team_Member__c u) {
        string salt = sgUtility.generateRandomString();
        
        string hashed = hashPassword(u.Password__c, salt);
        
        u.Salt__c = salt;
        u.Password__c = hashed;
    }
    
    public static void resetToken(Team_Member__c u) {
        u.Token__c = null;
        u.Token_Expiration__c = null;
        u.Reset_Token__c = sgUtility.generateRandomString(128);
    }
}