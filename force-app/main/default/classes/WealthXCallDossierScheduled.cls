global class WealthXCallDossierScheduled implements Schedulable{
    public Integer startIndex = 0;
    public Integer endIndex = 0;
    public Integer Noofrecords = 0;
    public Integer maxIndexSize = 10;
    public Integer maxBatchSize = 2;
    global void execute(SchedulableContext sc) {
        WealthXIndexSettings__c wxIndex = WealthXIndexSettings__c.getValues('WealthXIndex');
        startIndex = Integer.valueOf(wxIndex.Start_Index__c);
        endIndex = Integer.valueOf(wxIndex.End_Index__c);
        Noofrecords  = Integer.valueOf(wxIndex.No_of_Records__c);
        maxIndexSize = Integer.valueOf(wxIndex.Max_Index_Size__c);
        maxBatchSize = Integer.valueOf(wxIndex.Max_Batch_Size__c);
        for(Integer i=1;i<=maxBatchSize; i++){
            
            batchWealthXCallDossier batch = new batchWealthXCallDossier(startIndex,endIndex);
            database.executeBatch(batch,1);
            startIndex = endIndex  + 1;
            endIndex = startIndex + (maxIndexSize - 1);
        }
        
        wxIndex.Start_Index__c = String.valueOf(startIndex);
        wxIndex.End_Index__c = String.valueOf(endIndex);
        update wxIndex;
        String nextFireTime = '0 0/4 * 1/1 * ? *';
        Datetime sysTime = System.now().addSeconds(60);      
        String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        List<CronTrigger> ct = new List<CronTrigger>([SELECT Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE CronJobDetail.Name like 'WealthXCallDossier%']);
        if(ct.size() > 0){
            system.abortJob(ct[0].id);
        }
        System.schedule('WealthXCallDossier', chronExpression, new WealthXCallDossierScheduled());
    }
}