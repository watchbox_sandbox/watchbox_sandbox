@isTest
public class WatchmakingTriggerHandlerTests {

    public static testMethod void testProductUpdateClass(){
        //create necessary test records 
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Brand_object__c testBrand = dataFactory.buildBrand();
        insert testBrand;
        Series__c testSeries = dataFactory.buildSeries(testBrand.id);
        insert testSeries;
        Model__c testModel = dataFactory.buildModel(testBrand.id, testSeries.id);
        insert testModel;
        Product2 testProd = dataFactory.buildProduct(testModel.id);
        insert testProd;
        Watchmaking__c testWatchMaking = dataFactory.buildWatchmaking(testProd.id);
        test.StartTest();
        	insert testWatchmaking;
        	system.debug('testWatchmaking Id:'+testWatchmaking.id);
        	system.assertNotEquals(null, testWatchmaking.id);
        test.stopTest();        
    }
}