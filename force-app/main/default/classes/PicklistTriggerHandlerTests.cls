@isTest
public class PicklistTriggerHandlerTests {
    static testmethod void beforeInsert_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Picklist__c picklist = TestFactory.createPicklist(null, opp.Id);
        
        Picklist__c result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(account.Id, result.Account__c, 'Account not populated correctly');        
    }
    
    static testmethod void beforeUpdate_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Picklist__c picklist = TestFactory.createPicklist(null, null);
        
        Picklist__c result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(null, result.Account__c, 'Account should be null');  
        
        //Update picklist with a deal/associated account
        picklist.Deal__c = opp.Id;
        update picklist;
        
        result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(account.Id, result.Account__c, 'Account not populated correctly');    
    }
    
    static testmethod void beforeUpdate_removeDeal_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Picklist__c picklist = TestFactory.createPicklist(null, opp.Id);
        
        Picklist__c result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(account.Id, result.Account__c, 'Account not populated correctly');  
        
        //Remove deal
        picklist.Deal__c = null;
        update picklist;
        
        result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(null, result.Account__c, 'Account not cleared correctly'); 
    }
    
    static testmethod void beforeUpdate_changeDeal_test() {
        //Initial data set
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);  
        
        //Secondary data set to switch to
        Account newAccount = TestFactory.createAccount();
        Opportunity newOpp = TestFactory.createOpportunity(newAccount);
        
        Picklist__c picklist = TestFactory.createPicklist(null, opp.Id);        
        
        //Update picklist with a different deal/associated account
        picklist.Deal__c = newOpp.Id;
        update picklist;
        
        Picklist__c result = [SELECT Id, Account__c FROM Picklist__c WHERE Id =: picklist.Id];
        
        System.assertEquals(newAccount.Id, result.Account__c, 'Account not changed correctly');    
    }
    
    static testmethod void afterInsert_success_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        //Create products
        Product2 p1 = TestFactory.createProduct();
        Product2 p2 = TestFactory.createProduct();
        Product2 p3 = TestFactory.createProduct(); 
        //Create watch sales
        Watch_Sale__c ws1 = TestFactory.createWatchSale(p1, opp);
        Watch_Sale__c ws2 = TestFactory.createWatchSale(p2, opp);
        Watch_Sale__c ws3 = TestFactory.createWatchSale(p3, opp);
        
        Picklist__c picklist = TestFactory.createPicklist(null, opp.Id);
        
        integer count = Database.countQuery('SELECT count() FROM Picklist_Detail__c');
        
        System.assertEquals(3, count, 'Picklist Detail not generated correctly');
    }
    
    static testmethod void afterInsert_noInventory_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        //Create exception case
        Watch_Sale__c ws = TestFactory.buildWatchSale(null, opp);
        ws.Inventory_Item__c = null;
        insert ws;
        
        Picklist__c picklist = TestFactory.createPicklist(null, opp.Id);
        
        integer count = Database.countQuery('SELECT count() FROM Picklist_Detail__c');
        
        System.assertEquals(0, count, 'Picklist Detail record incorrectly generated');
    }
}