@RestResource(urlMapping='/Search/*')
global class sgSearchController {
    private static sgSearchResult sr = new sgSearchResult();
    
    @HttpGet
    global static sgSearchResult get() {
        sgAuth.check();
        
        Map<string, string> params = RestContext.request.params.clone();
        params.put('orderBy', 'name');
        
        string query = params.get('query');
        params.remove('query');
        
        params.put('limit', '3');
        
        Map<string, string> brandParams = params.clone();
        //brandParams.put('limit', '5');
        brandParams.put('group1Name', 'like:' + query);
        //brandParams.put('group1WUW_Brand_Description__c', 'like:' + query);
        string brandsQuery = 'select Id, Name, (select Id from Models__r), (select Cost__c, IsActive from Products__r), ' +
            '(select Days_In_Inventory__c, Product__c  from Purchase_Details__r) from Brand_object__c';
		
        List<Brand_object__c> brandsList = sgUtility.query(brandsQuery, brandParams);
        List<Map<string, string>> brands = new List<Map<string, string>>();
        
        for (Brand_object__c b : brandsList) {
            decimal inventory = 0;
            for (Product2 p : b.Products__r) {
                if (p.IsActive) {
                	inventory += (p.Cost__c != null ? p.Cost__c : 0);
                }
            }
            
            decimal avgDays = 0;
            for (Purchase_Detail__c p : b.Purchase_Details__r) {
                avgDays += p.Product__c != null ? p.Days_In_Inventory__c : 0;
            }
            
            integer pdSize = b.Purchase_Details__r.size();
            Map<string, string> brand = new Map<string, string>{
            	'Id' => b.Id,
                'Name' => b.Name,
                'ModelsCount' => String.valueOf(b.Models__r.size()),
                'ProductsCount' => String.valueOf(b.Products__r.size()),
                'SoldCount' => String.valueOf(pdSize),
                'Inventory' => String.valueOf(inventory),
                'AvgDays' => String.valueOf((avgDays / (pdSize > 0 ? pdSize : 1)).setScale(2))
                
            };
                
            brands.add(brand);
        }
        sr.Brands = brands;
        
        
        
        
        Map<string, string> modelParams = params.clone();
        modelParams.put('limit', '5');
        modelParams.put('Verified__c ', 'true');
        
        modelParams.put('group1Name', 'like:' + query);
        modelParams.put('group1Reference__c ', 'like:' + query);
        modelParams.put('group1SH_Watch_ID__c ', 'like:' + query);
        modelParams.put('group1DMS_Model_ID__c ', 'like:' + query);
        modelParams.put('group1Also_Known_As__c ', 'like:' + query);
        modelParams.put('group1Also_Known_As_2__c ', 'like:' + query);
        modelParams.put('group1model_series__r.Name ', 'like:' + query);
        
        string modelQuery = 'select Id, Name, Reference__c, AWS_Image_Array__c, model_series__r.Name, DMS_Model_ID__c, SH_Watch_ID__c from Model__c';
		sr.Models = sgSerializer.serializeFromQuery(modelQuery, modelParams);

        List<string> modelIds = new List<string>();
        for (Map<string, string> m : sr.Models) {
            modelIds.add(m.get('Id'));
            
           
            m.put('MainImage__c', sgUtility.getMainImage(m.get('AWS_Image_Array__c')));
            //m.remove('AWS_Image_Array__c');
        }
        
        Map<string, string> productCount = new Map<string, string>();
        for (AggregateResult aggRes : [select Model__c, IsActive, count(Id) numProducts from Product2 where Model__c in :modelIds group by Model__c, IsActive]) {
            productCount.put((string) aggRes.get('Model__c') + '-' + string.valueOf(aggRes.get('IsActive')), String.valueOf((integer) aggRes.get('numProducts')));
        }
        
        for (Map<string, string> m : sr.Models) {
            for (string bool : new List<string> {'true', 'false'}) {
            	string key = (string) m.get('Id') + '-' + bool;
                
                string quantity = productCount.get(key);
            	m.put('quantity' + (bool == 'true' ? 'Active' : 'Sold'), quantity != null ? quantity : '0');
            }
        }

        Map<string, string> productParams = params.clone();
        productParams.put('limit', '5');
        productParams.put('IsActive', 'true');
        productParams.put('group1Name', 'like:' + query);
        productParams.put('group1Model__r.Name ', 'like:' + query);
        productParams.put('group1Location__r.Name ', 'like:' + query);
		productParams.put('group1WUW_Inventory_ID__c ', 'like:' + query);
        productParams.put('group1GB_Inventory_ID__c ', 'like:' + query);
        
        string productQuery = 'select Id, Name, Inventory_ID__c, Cost__c, AWS_Web_Images__c from Product2';
		sr.Products = sgSerializer.serializeFromQuery(productQuery, productParams);
        
        for (Map<string, string> p : sr.Products) {
            p.put('MainImage__c', sgUtility.getMainImage(p.get('AWS_Web_Images__c')));
            p.remove('AWS_Web_Images__c');
        }

        
        if (sr.Brands.size() == 0 && sr.Models.size() == 0 && sr.Products.size() == 0) {
            Map<string, string> userParams = params.clone();
            userParams.put('group1First_Name__c', 'like:' + query);
            userParams.put('group1Last_Name__c', 'like:' + query);
            userParams.put('group1Work_Email__c', 'like:' + query);
            userParams.put('IsActive__c', 'true');        
            string userQuery = 'select Id, Profile_Picture__c, Name, First_Name__c, Last_Name__c, Work_Email__c, Group__r.Name from Team_Member__c';
            sr.Users = sgSerializer.serializeFromQuery(userQuery, userParams);
            
            Map<string, string> vendorParams = params.clone();
            vendorParams.put('WUW_NC_Vendor_ID__c', '!null');
            vendorParams.put('name', 'like:' + query);
            string vendorQuery = 'select Id, Name, Phone, Email__c from Account';
            sr.Vendors = sgSerializer.serializeFromQuery(vendorQuery, vendorParams);
            
            Map<string, string> customerParams = params.clone();
            customerParams.put('WUW_NC_Customer_ID__c', '!null');
            customerParams.put('name', 'like:' + query);
            string customerQuery = 'select Id, Name, Phone, Email__c from Account';
            sr.Customers = sgSerializer.serializeFromQuery(customerQuery, customerParams);
        }
        
        return sr;
    }
}