@isTest
public class sgTestIntakeController {
    static testMethod void get() {
        List<Product2> products = sgDataSeeding.ProductsWithBrands();
        insert products;
        
        List<Team_Member__c> members = sgDataSeeding.TeamMembers();
        insert members;
        
        Intake__c i = new Intake__c (
            Is_Active__c = true,
        	Intake_Status__c = 'New',
            Team_Member__c = members.get(0).Id,
            Product__c = products.get(0).Id
        );
        
        insert i;
        system.debug([select id, isactive from product2]);
        
        RestContext.request = new RestRequest();
        
        sgIntakeController.IntakeInspection sid = sgIntakeController.get();
        
        system.assertEquals(1, sid.Intakes.Products.size());
        system.assertEquals(1, sid.Intakes.SalesAssociates.size());
    }
    
    static testMethod void post1() {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();        
        
        Intake__c p = new Intake__c();
        
        insert p;
        
        p.Intake_Status__c = 'New';   
        update p;
                
        RestContext.request = new RestRequest();
        
        //try {
            Intake__c i = new Intake__c ( Boxes__c = True );
            sgIntakeController.post(p.Id, 'In Progress', i, new List<string>{'Boxes__c'});
            
            system.assert(true);
        //} catch (Exception e) {
            //system.debug(e.getMessage());
            
            //system.assert(false);
        //}
    }
    
    //not allowed to move from incoming to queue, should throw exception
    static testMethod void post2() {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();        
        
        Intake__c p = new Intake__c();
        
        insert p;
        
        p.Intake_Status__c = 'New';
        update p;
		
        RestContext.request = new RestRequest();
        
        try {
            sgIntakeController.post(p.Id, 'Complete', null, new List<string>{'Boxes__c'});
            
            system.assert(false);
        } catch (Exception e) {
            system.assert(true);
        }
    }
    
    static testMethod void put() {
        Intake__c i = new Intake__c();
        
        insert i;
        
        sgIntakeController.put(i.Id, 'Name', 'test');
    }
    
    static testMethod void createProduct() {
        List<Product2> products =  sgDataSeeding.ProductsWithBrands();
        
        Product2 p1 = products.get(0);
        insert p1;
        
        system.debug(p1);
        
        Intake__c i = new Intake__c(
            Product__c = p1.Id,
        	Watch_Model__c = p1.Model__c
        );
        
        insert i;
        
        i = [select Id, Product__c, Product__r.Model__c, Watch_Brand__c, Serial__c, Boxes__c, Papers__c, AWS_Web_Images__c from Intake__c where Id = :i.Id limit 1];
        
        system.debug(i);
        
        Product2 p = sgIntakeData.CreateProduct(i);
        
        sgIntakeData.CreateInspection(p, i);
    }
}