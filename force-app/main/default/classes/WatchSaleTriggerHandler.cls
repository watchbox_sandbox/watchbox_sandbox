public class WatchSaleTriggerHandler extends TriggerHandler { 
    public static Boolean isFirstTime = true;
    public override void afterUpdate() {
        Set<Id> productIdsForRelease = new Set<Id>();
        
        for (Id saleId : Trigger.newMap.keySet()) {            
            Watch_Sale__c newSale = (Watch_Sale__c)Trigger.newMap.get(saleId);
            Watch_Sale__c oldSale = (Watch_Sale__c)Trigger.oldMap.get(saleId);  
            
            if (newSale.Inventory_Item__c != oldSale.Inventory_Item__c) {
                if (oldSale.Inventory_Item__c != null) {
                    productIdsForRelease.add(oldSale.Inventory_Item__c);
                }                
            }  
        }
        
        if (!productIdsForRelease.isEmpty()) {
            InventoryUtility.removeHolds(productIdsForRelease);
        }
    }
    
    public override void afterDelete() {
        Set<Id> productIdsForRelease = new Set<Id>();
        Set<Id> watchSaleIds = new Set<Id>();
        
        for (Watch_Sale__c ws : (List<Watch_Sale__c>)trigger.old) {
            //Add inventory items to release holds on
            if (ws.Inventory_Item__c != null) {
               productIdsForRelease.add(ws.Inventory_Item__c);
            }
            
            //Add associated watch collection records to remove
            if (ws.Added_to_Collection__c) {
                watchSaleIds.add(ws.Id);
            }
        }
        
        //Remove holds
        if (!productIdsForRelease.isEmpty()) {
        	InventoryUtility.removeHolds(productIdsForRelease);
    	}
        
        //Remove associated watch collection records
        if (!watchSaleIds.isEmpty()) {
            WatchCollectionUtility.removeWatchCollectionByWatchSales(watchSaleIds);
        }
    }
    
    public override void beforeUpdate() {
        List<Watch_Sale__c> wsWithRelatedInventory = new List<Watch_Sale__c>();
        for(Watch_Sale__c ws : (List<Watch_Sale__c>)trigger.new) {
            Watch_Sale__c oldWS = (Watch_Sale__c)trigger.oldMap.get(ws.id);
            if(ws.Inventory_Item__c != oldWS.Inventory_Item__c) {
                wsWithRelatedInventory.add(ws);
            }
        /*if(ws.MSP__c==null){
                ws.MSP__c=0;
        }
        if(ws.Client_s_Offer__c==null){
                ws.Client_s_Offer__c=0;
        }*/
        }
        
        if(!wsWithRelatedInventory.isEmpty()) {
            relocateDeal(wsWithRelatedInventory);
        }
    }
    
    public override void beforeInsert() {
        List<Watch_Sale__c> wsWithRelatedInventory = new List<Watch_Sale__c>();      
        for(Watch_Sale__c ws : (List<Watch_Sale__c>)trigger.new){
            if(ws.Inventory_Item__c != null) {
                wsWithRelatedInventory.add(ws);
            }
        }
        
        if(!wsWithRelatedInventory.isEmpty()){
            relocateDeal(wsWithRelatedInventory);
        }
    }        
    
    //this class will make sure that watch sales and inventory look up to the same deal
    private static void relocateDeal(List<Watch_Sale__c> watchSales){
        List<Id> inventoryIds = new List<Id>();
        List<Product2> updatedInventory = new List<Product2>();
        
        for(Watch_Sale__c ws : watchSales){
            inventoryids.add(ws.Inventory_Item__c);
            system.debug(inventoryIds);
        }
        
        List<Product2> relatedInventory = [SELECT id, deal__c FROM Product2 WHERE id IN :inventoryIds];
        
        for(Watch_Sale__c watchSale : watchSales){
            for(Product2 product : relatedInventory){
                if(watchSale.Inventory_Item__c == product.Id){
                    product.Deal__c = watchSale.Opportunity__c;
                    updatedInventory.add(product);
                    system.debug('Updated Inventory: '+updatedInventory);
                }
            }            
        }
        
        update updatedInventory;
    }
    
    
    public static void countRLonOpportunityUpdate(List<Watch_Sale__c> watchSales){
        try{
            Set<Id> oppIds = new Set<Id>();
            for(Watch_Sale__c ws :watchSales){
                oppIds.add(ws.Opportunity__c);
            }
            //List<Watch_Sale__c> wsList= [SELECT Id,Opportunity__c FROM Watch_Sale__c where Id= :WSIds];
            map<Id,Double> oppCOMap = new map <Id,Double>();
            map<Id,Double> oppWBMap = new map <Id,Double>();
             map<Id,Double> oppWebPriceMap = new map <Id,Double>();
            //Map<ID,List<Watch_Sale__c>> WSaleNOppMap = new Map<ID,List<Watch_Sale__c>>();
            List<Watch_Sale__c> wsListForSelect= new List<Watch_Sale__c>();
            for(Opportunity opp:[Select Id, (Select Id from Watch_Sales__r) from Opportunity where id=:oppIds]){
                for(Watch_Sale__c wsale:opp.Watch_Sales__r){
                    wsListForSelect.add(wsale);
                    System.debug(wsale);
                }                        
            }
            System.debug('wsListForSelect'+wsListForSelect);
            for(AggregateResult ar : [SELECT Opportunity__r.Id dealID,sum(Client_s_Offer__c) totalCOffer,sum(MSP__c) totalWBoffer,sum(Web_price__c) totalWebPrice FROM Watch_Sale__c where Id=:wsListForSelect group by Opportunity__r.Id]){
                System.debug('ar-----'+ar);
                oppCOMap.put((Id)ar.get('dealID'),(Double)ar.get('totalCOffer'));
                oppWBMap.put((Id)ar.get('dealID'),(Double)ar.get('totalWBoffer'));
                oppWebPriceMap.put((Id)ar.get('dealID'),(Double)ar.get('totalWebPrice'));
                system.debug('oppCOMap'+oppCOMap);
                system.debug('oppWBMap'+oppWBMap);
            } 
            List<Opportunity> oppListToupdate= new List<Opportunity>();
            for(Opportunity opp: [Select ID,Watch_Sale_Records_Count__c,Watch_Origination_Records_Count__c,(Select Id from Watch_Sales__r),(Select Id from Origination_Records__r) from Opportunity where ID=:oppIds]){
                opp.Watch_Sale_Records_Count__c= opp.Watch_Sales__r.size();
                opp.Watch_Origination_Records_Count__c= opp.Origination_Records__r.size();
                system.debug('oppWBMap value'+oppCOMap.get(opp.Id));
                system.debug('oppWBMap value'+oppWBMap.get(opp.Id));
                opp.Total_Client_Offer_Watch_Sales__c=oppCOMap.get(opp.Id)==null?0.00:oppCOMap.get(opp.Id) ;
                opp.Total_WB_Offer_Watch_Sales__c=oppWBMap.get(opp.Id)==null?0.00:oppWBMap.get(opp.Id) ;
                opp.Total_Web_Price__c=oppWBMap.get(opp.Id)==null?0.00:oppWebPriceMap.get(opp.Id) ;
                
                if(opp.Watch_Sale_Records_Count__c!=null && opp.Watch_Sale_Records_Count__c!=null){
                    if(opp.Watch_Sale_Records_Count__c > 0 && opp.Watch_Origination_Records_Count__c == 0){
                        opp.Type_of_Transaction_Final__c='Sale';
                    }
                    if(opp.Watch_Sale_Records_Count__c == 0 && opp.Watch_Origination_Records_Count__c > 0){
                        opp.Type_of_Transaction_Final__c='Origination';
                    }
                    if(opp.Watch_Sale_Records_Count__c > 0 && opp.Watch_Origination_Records_Count__c > 0){
                        opp.Type_of_Transaction_Final__c='Trade';
                    }
                }
                oppListToupdate.add(opp);
            }
            if(!oppListToupdate.isEmpty()){
                update oppListToupdate; 
            }
        }
        catch (Exception e) {
            System.debug('Error during WatchOriginationTriggerHandler - ' + e.getMessage());
            for (Watch_Sale__c record : (List<Watch_Sale__c>)Trigger.new) {
                System.debug('Error during WatchOriginationTriggerHandler - ' + e.getLineNumber());
                record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
            }   
        }
        
        
    }
    
}