@isTest
public class sgTestServicingController {
    public static List<Product2> products;
    public static List<Servicing__c> servicings;
    
    public static void setup() {
        setup(sgServicing.CompleteStatus);
    }
    public static void setup(string status) {
        RestContext.request = new RestRequest();
        
        products = sgDataSeeding.ProductsWithBrands();
        insert products;
        
		servicings = new List<Servicing__c>();
        
        for (Product2 p : products) {
            p.Status__c = status;
            p.Location__c = sgAuth.user.Location__c;
            
            servicings.add(new Servicing__c(
            	Product__c = p.Id
            ));
        }
        
        update products;
        
        insert servicings;
    }
    
    static testMethod void get() {
		setup();
        
        List<Map<string, string>> get = sgServicingController.get();
        
        system.assertEquals(servicings.size(), get.size());
    }
    
    static testMethod void post() {
 		setup();
        
        Servicing__c s = servicings.get(0);
	
        string oldStatus = 'Outsource';
		string newStatus = 'Decasing';
        string stopfield = oldStatus + 'EndDate__c';

        SgServicingController.patch(s.Id, oldStatus, 'Started');
        
        
        SgServicingController.post(s.Id, oldStatus, newStatus);
        
        string sId = s.Id;
		s = Database.query('select Id, Product__r.Status__c, ' + stopField + ' from Servicing__c where Id = :sId');
        
        system.assert(s.get(stopField) != null);
        system.assertEquals(newStatus, s.Product__r.Status__c);
    }
    
    static testMethod void put() {
		setup();

        Role__c role = new Role__c(
        	Name = 'Watchmaker'
        );
        insert role;
        
		Team_Member__c tm = new Team_Member__c(
        	Location__c = products.get(0).Location__c,
            Role__c = role.Id
        );        
        insert tm;
        
        Servicing__c s = servicings.get(0);
        
        system.assertEquals(null, s.Watchmaker__c);
        
        sgServicingController.put(s.Id, tm.Id);
        
        s = [select Id, Watchmaker__c from Servicing__c where Id = :s.Id];
        
        system.assertEquals(tm.Id, s.Watchmaker__c);
    }
    
    static testMethod void patch() {
        setup();
        
        Servicing__c s = servicings.get(0);
		
		string status = 'Outsource';
        string startfield = status + 'StartDate__c';
        string stopfield = status + 'EndDate__c';
        
        system.assertEquals(null, s.get(startField));
        
        SgServicingController.patch(s.Id, status, 'Started');
        
		string sId = s.Id;
		s = Database.query('select Id, ' + startField + ' from Servicing__c where Id = :sId');
        
        system.assert(s.get(startField) != null);
        
        
        SgServicingController.patch(s.Id, status, 'Stopped');
        
		s = Database.query('select Id, ' + stopField + ' from Servicing__c where Id = :sId');
        
        system.assert(s.get(stopField) != null);
    }
    
    //Servicing2 Controller

    static testMethod void post2() {
        setup();
        
        Map<string, integer> ordering = new Map<string, integer>();
        integer i = 0;
        for (Servicing__c s : servicings) {
            ordering.put(s.Id, i++);
        }
        
        sgServicing2Controller.post(ordering);
        
        List<Servicing__c> ordered = [select Id, Order__c from Servicing__c];
        for (Servicing__c o : ordered) {
            system.assertEquals(ordering.get(o.Id), o.Order__c);
        }
    }
    
    static testMethod void put2() {
        setup();
        
        Servicing__c s = servicings.get(0);
        
        string note = 'test note';
        
        sgServicing2Controller.put(s.Id, note, null);
        
        Note n = [select Id, ParentId, Body from Note];
        
        system.assertEquals(s.Id, n.ParentId);
        system.assertEquals(note, n.Body);
    }
    
    static testMethod void patch2() {
    	setup();
        
        Product2 p = products.get(0);
        
        sgServicing2Controller.patch(p.Id);
        
        p = [select Id, QC_Failed__c from Product2 where Id = :p.Id];
        
        system.assertEquals(true, p.QC_Failed__c);
    }
    
    //Servicing record creation trigger

    static testMethod void goodStatus() {
        setup('Available');
        
        Product2 p = products.get(0);
        
        p.Status__c = sgServicing.CreationStatuses.get(0);
        update p;
        
        system.assertEquals(servicings.size() + 1, Database.countQuery('select count() from Servicing__c'));
    }
    
    static testMethod void noChange() {
        setup('Available');
        
        Product2 p = products.get(0);
        
        p.Status__c = sgServicing.CreationStatuses.get(0);
        update p;
        
        p.Status__c = sgServicing.CreationStatuses.get(1);
        update p;
        
        system.assertEquals(servicings.size() + 1, Database.countQuery('select count() from Servicing__c'));
    }
    
    static testMethod void completed() {
       setup('Available');
        
        Product2 p = products.get(0);
        
        p.Status__c = sgServicing.CreationStatuses.get(0);
        update p;
        
        p.Status__c = sgServicing.CompleteStatus;
        update p;
        
        system.assertEquals(servicings.size() + 1, Database.countQuery('select count() from Servicing__c'));
    }
}