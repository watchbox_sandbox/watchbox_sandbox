@IsTest
public class JSONBranchfamilyTest {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '['+
        '    {'+
        '        \"id\": \"be96b631-049d-4a63-b9e4-5508d8c31fe6\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969546982,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969546982,'+
        '        \"name\": \"WWI\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"c2363621-7e7a-4456-b7ac-d08a34f193e1\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969542577,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969542577,'+
        '        \"name\": \"Vintage\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"319127d1-04b3-4145-af6e-760b749a2861\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969539054,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969539054,'+
        '        \"name\": \"Regulateur\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"c52bfe77-3b0f-4bca-960f-db082e356a40\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969536103,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969536103,'+
        '        \"name\": \"Officer\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"6145c482-e88e-47c6-b49b-175038307d32\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969526452,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969526452,'+
        '        \"name\": \"BR-126\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"294778ce-581e-48a4-9203-eccf9509ac24\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969519851,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969519851,'+
        '        \"name\": \"BR-X1\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"aff926de-bd14-43f0-833c-8d1e04de8e40\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969515775,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969515775,'+
        '        \"name\": \"BR-S\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"9a306871-3b82-40ab-a1bb-29b2b3bfe497\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969504239,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969504239,'+
        '        \"name\": \"BR-03\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"b3c545ba-6ffe-4982-841f-2c3342d9da6d\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969500213,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969500213,'+
        '        \"name\": \"BR-02\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"5c46bf26-47e6-46da-8039-c0441de0fde0\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969496033,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969496033,'+
        '        \"name\": \"BR-01\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"79a3c926-5511-4cc6-b109-f27b09a6cc42\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969491540,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969491540,'+
        '        \"name\": \"BR-V2-94\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"f85bf30a-d893-4f9d-894d-08182aaa2cbf\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969484543,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969484543,'+
        '        \"name\": \"BR-V2-92\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"ccc21ff4-1599-4fe6-9e55-21b9954d4e2c\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969478365,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969478365,'+
        '        \"name\": \"BR-123\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    },'+
        '    {'+
        '        \"id\": \"68294119-fe90-4448-89be-262bc806af14\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560969473058,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560969473058,'+
        '        \"name\": \"Aviation\",'+
        '        \"brandId\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\"'+
        '    }'+
        ']';
        List<JSONBranchfamily> r = JSONBranchfamily.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSONBranchfamily objJSONBranchfamily = new JSONBranchfamily(System.JSON.createParser(json));
        System.assert(objJSONBranchfamily != null);
        System.assert(objJSONBranchfamily.id == null);
        System.assert(objJSONBranchfamily.creator == null);
        System.assert(objJSONBranchfamily.creationInstant == null);
        System.assert(objJSONBranchfamily.modifier == null);
        System.assert(objJSONBranchfamily.modifiedInstant == null);
        System.assert(objJSONBranchfamily.name == null);
        System.assert(objJSONBranchfamily.brandId == null);
    }
}