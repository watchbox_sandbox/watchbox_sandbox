public class sgUtility {
    
    ///
    // Takes a querystring like string value and creates a map
    //
    public static Map<String,String> QueryStringtoMap(String value) {
        if (value != null && value.length() > 0) {
            Map<String, String> mapValue = new Map<String, String> ();
            String[] parts = value.split('&');
            for(String part : parts) {
                List<String> keyValue = part.split('=', 2);
                if (keyValue.size() == 1) {
                    keyValue.add('');
                }
                mapValue.put(keyValue[0], keyValue[1]);
            }
            return mapValue;
        }
        return null;
    }
    
    ///
    // Updates the {!key} values in the `template` with the values in `data`
    // 
    public static String ApplyData(String template, Map<String, Object> data) {
        if (template == null || template.length() == 0) {
            return null;
        }
        String result = template;
        if (data != null && data.size() > 0 && HasKeys(template)) {
            Matcher keyMatcher = sgUtility.GetKeyMatcher(template);
            while (keyMatcher.find()) {
                String key = keyMatcher.group(1);
                String value = String.valueOf(Lookup(key, data));
                if (value == null) {
                    continue;
                }
                String replaceKey = keyMatcher.group(0);
                result = result.replace(replaceKey, value);
            }
        }
        return result;
    }
    public static String ApplyData(String template, sObject data) {
        return ApplyData(template, data.getPopulatedFieldsAsMap());
    }
    
    ///
    // Returns a Matcher for the template using {!key} as the pattern
    //
    public static Matcher GetKeyMatcher(String template) {
        Pattern keyPattern = Pattern.compile('\\{!([^}]+)\\}');
        return keyPattern.matcher(template);
    }
    
    ///
    // Checks the `template` string for instances of {!key}
    //
    public static Boolean HasKeys(String template) {
        Matcher keyMatcher = sgUtility.GetKeyMatcher(template);
        if (keyMatcher.find()) {
            return true;
        }
        return false;
    }
    
    ///
    // Removes any instance of {!key}
    //
    public static String RemoveKeys(String template) {
        if (template == null || template == '') {
            return '';
        }
        Matcher keyMatcher = sgUtility.GetKeyMatcher(template);
        String result = template;
        while (keyMatcher.find()) {
            String replaceKey = keyMatcher.group(0);
            result = result.replace(replaceKey, '');
        }
        return result;
    }
    
    ///
    // Using the `key`, in dot notation, finds the value in the Map `data`
    //  works with everything except standard classes, which we can't get meta data for
    //  
    public static Object Lookup(String name, Map<String, Object> data) {
        String[] nameParts = name.split('\\.');
        
		Object value = null;
        for (String key : nameParts) {
            //if the data is null the we've not found the key
            if (data == null) {
                return null;
            }
            //lookup the member at the key
            value = data.get(key);
            if (value == null) {
                return null;
            }
            //get the type and process according to the type
            String oType = GetType(value);
            if (oType == 'sobject') {
                data = ((sObject)value).getPopulatedFieldsAsMap();
                value = (Object)data; //this could be the final key, in which case the return value is the data
            }
            else if (oType == 'map') {
                data = (Map<String,Object>)value;
                value = (Object)data; //this could be the final key, in which case the return value is the data
            }
            else {
                value = String.valueOf(value);
                data = null; //set data to null, if this isn't the last key then we didn't find what we wanted
            }
        }
        return value;
    }
    
    ///
    // Returns the type of the object 
    //  https://success.salesforce.com/ideaView?id=08730000000l9wHAAQ
    // 
    public static string GetType(Object o) {
        if(o==null) return '';
        if(o instanceof SObject)            return 'sobject'; 
        if(o instanceof Boolean)            return 'boolean';
        if(o instanceof Id)                 return 'id';
        if(o instanceof String)             return 'string';
        if(o instanceof Blob)               return 'blob';
        if(o instanceof Date)               return 'date';
        if(o instanceof Datetime)           return 'datetime';
        if(o instanceof Time)               return 'time';
        if(o instanceof String)             return 'string';
        if(o instanceof Integer)            return 'integer';
        if(o instanceof Long)               return 'long';
        if(o instanceof Decimal)            return 'decimal';
        if(o instanceof Double)             return 'double';
        if(o instanceof List<object>)       return 'list';
        if(o instanceof Map<String,Object>)	return 'map';
        return 'object';
    }
    
    public static List<sObject> query(string query, Map<string, string> reqParams) {
    	query = applyFilters(query, reqParams);    
        
        return Database.query(query);
    }
    
    public static List<sObject> query(string query, Map<string, string> reqParams, boolean onlyMine) {
        query = applyFilters(query, reqParams, onlyMine);
        
        return Database.query(query);
    }
    
    public static boolean checkOnlyMine(string query) {
        string obj = parseQueryObject(query);
        
        if (obj == null) {
            return false;
        }
        
        return false;
        //return sgBaseController.hasPermission('OnlyMine ' + obj);
    }
    
    public static string parseQueryObject(string query) {
        if (query.containsIgnoreCase('from user')) {
            return 'Users';
        }
        
        string startToken = 'from fcem_';
        string endToken = '__c';
                
        integer startIndex = query.toLowerCase().indexOf(startToken);
        integer endIndex = query.toLowerCase().indexOf(endToken, startIndex);
        
        if (startIndex < 0 || endIndex < 0) {
            return null;
        }

        string obj = query.substring(startIndex + startToken.length(), endIndex);
        obj = obj.replace('_', '')  + 's';
        
        return obj;
    }
    
    public static Map<string, string> params = null;
    public static string applyFilters(string query, Map<string, string> reqParams) {
    	boolean onlyMine = checkOnlyMine(query);    

        return applyFilters(query, reqParams, onlyMine);
    }
    
    public static string applyFilters(string query, Map<string, string> reqParamsPassed, boolean onlyMine) {
        Map<string, string> reqParams = reqParamsPassed.clone();
        
        //add the ownerid if it's missing to all queries
        //if (!query.containsIgnoreCase('ownerid') && !query.containsIgnoreCase('from user')) {
        //    query = query.replace('select', 'select OwnerId, ');
        //}
        
        //these should not be treated as where clause fields
        Set<string> exclusions = new Set<string> {
                'orderby',
                'limit',
                'offset',
                'objectname',
				'key',
                'value',
                'sgtoken',
				'args'
        };
        
        if (reqParams != null) {
            params = reqParams;
            system.debug(reqParams);
        } else {
            params = new Map<string, string>();
        }
        
        boolean hasWhere = false;

		//add a where clause if it's missing
        if (!query.toLowerCase().contains('where')) {
            query += ' where';
        } else {
            hasWhere = true;
        }
        
        //users can only see records they own
        if (!query.containsIgnoreCase('from user') && onlyMine) {
        	query += ' OwnerId = \'' + UserInfo.getUserId() + '\'';
            
            hasWhere = true;
        }
        
        
        //auto add all clauses that aren't part ofthe ordering/limits
        string joiner = ' AND';
        integer groupInProgress = 0;
        boolean paramAdded = false;

        Map<string, string> originalParams = params.clone();

        for (string key : originalParams.keySet()) {
            if (!exclusions.contains(key.toLowerCase())) {
                if (Pattern.matches('group\\d{1}.*', key)) {
                    //if a param has already been added to this query, join the ( or ) clause with another 'AND'
                    if (paramAdded && groupInProgress == 0) {
                        query += ' AND';
                    }

                    string oldKey = key;
                    string value = params.get(key);

                    key = key.replaceAll('group\\d{1}', '');

                    params.remove(oldKey);
                    params.put(key, value);
                    
                    if (groupInProgress == 0) {
                        joiner = ' OR';

                        query += ' (';
                    } 

                    groupInProgress++;
                } else if (groupInProgress > 0) {
                    joiner = ' AND';
                    groupInProgress = 0;

                    query +=') ';
                }

                if ((hasWhere && joiner == ' AND') || groupInProgress > 1) {
                    query += joiner;
                }
                query += addClause(key);
                
                hasWhere = true;

                paramAdded = true;
            }
        }

        if (groupInProgress > 0) {
            query += ')';
        }
        
        //close off the real where clause
        if (!hasWhere) {
            query += ' name != \'\'';
        }
        
        if (hasKey('orderBy')) {
            List<string> fields = getKey('orderBy').split(',');
            
            query += ' order by ';
            
            List<string> orders = new List<string>();
            
            for (string field : fields) {
                string direction = 'asc';
                if (field.startsWith('-')) {
                    field = field.substring(1, field.length());
                    direction = 'desc nulls last';
                }
                
                orders.add(field + ' ' + direction);
            }
            
            query += string.join(orders, ', ');
        }
        
        if (hasKey('limit')) {
            query += ' limit ' + integer.valueOf(getKey('limit'));
        }
        
		if (hasKey('offset')) {
            query += ' offset ' + integer.valueOf(getKey('offset'));
        }
        
        system.debug(query);
        
        //throw new sgException(query);

        return query;
    }
    
    public static Boolean hasKey(string key) {
        return params != null && params.get(key) != null;
    } 
    
    public static string getKey(string key) {
        return params != null ? params.get(key) : '';
    }
    
    public static string addClause(string key) {
        string value = getKey(key);

        if (key.startsWithIgnoreCase('date:')) {
            key = key.replace('date:', '');
        }

        string operator = '=';

        if (value.startsWithIgnoreCase('>=')) {
            value = value.replace('>=', '');
            operator = '>=';
        } else if (value.startsWithIgnoreCase('<=')) {
            value = value.replace('<=', '');
            operator = '<=';
        } else if (value.startsWithIgnoreCase('>')) {
            value = value.replace('>', '');
            operator = '>';
        } else if (value.startsWithIgnoreCase('<')) {
            value = value.replace('<', '');
            operator = '<';
        } else if (value.startsWithIgnoreCase('!')) {
            value = value.replace('!', '');
            operator = '!=';
        } else if (value.startsWithIgnoreCase('like:')) {
            value = '%' + value.replace('like:', '') + '%';
            operator = 'LIKE';
        } else if (value.startsWithIgnoreCase('between:')) {
            value = value.replace('between:', '');
            
            string origKey = key;
            string origValue = value;

            if (origValue.contains('-') && origValue.indexOf('-') != 0) {
                List<string> values = origValue.split('-');
            	key = '(' + key + ' >= ' + values.get(0).replace('k', '000') +
                      ' AND ' + key + ' <= ' + values.get(1).replace('k', '000') + ')';
                
                operator = '';
                value = 'ignore';
            } else if (origValue.contains('+')) {
                value = value.replace('k', '000').replace('+', '');
                
                operator = '>=';
            }
        }
        
        return ' ' + string.escapeSingleQuotes(key) + ' ' + operator + ' ' + formatValue(value);
    }
        
    public static string formatValue(string value) {
        if (value == 'ignore') {
            return '';
        }
        
        string valueToCheck = value.toLowerCase();
        
        //treat null string specially
        string enclosing = '\'';
        if (
            valueToCheck.trim().isNumeric() ||
            valueToCheck == 'null' || valueToCheck == 'true' || valueToCheck == 'false' ||
            (valueToCheck.indexOf('-') == 4 && valueToCheck.indexOf('-', 5) == 7)
        ) {
            enclosing = '';
        }
        
        return  enclosing + string.escapeSingleQuotes(value) + enclosing;
    }
    
    public static Boolean isSandbox() {
      return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }

    public static object ifNull(object field, object defaultValue) {
        return field != null ? field : defaultValue;
    }
    
    public static Map<string, sobject> MapBy (string field, List<sobject> objects) {
        Map<string, sobject> mapped = new Map<string, sobject>();
        
        for (sobject obj : objects) {
            mapped.put((string) obj.get(field), obj);
        }
        
        return mapped;
    }
    
    //	--------------------------------------------------------
    //	parseDate; null is invalid Date; yyyy-mm-dd and locale-specific e.g. mm/dd/yyyy or dd/mm/yyyy formats supported
    //	--------------------------------------------------------
    public static Date parseDate(String inDate) {
        Date dateRes = null;
        //	1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy	
        try {
            String candDate	 = inDate.substring(0,Math.min(10,inDate.length())); // grab date portion only m[m]/d[d]/yyyy , ignore time
            dateRes  = Date.parse(candDate);
        } catch (Exception e) {}
    
        if (dateRes == null) {
        //	2 - Try yyyy-mm-dd			
            try {
                String candDate		= inDate.substring(0,10); // grab date portion only, ignore time, if any
                dateRes				= Date.valueOf(candDate);
            } catch (Exception e) {} 
        }
        
        return dateRes;
    }

    //http://salesforce.stackexchange.com/a/4993
    public static List<String> getPicklistValues(String ObjectApi_name, String Field_name){ 
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();

        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject

        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }

        return lstPickvals;
    }
    
    public static string selectStar(string sobjectName) {
        return selectStar(sobjectName, true);
    }
    
    //https://developer.salesforce.com/forums/?id=906F00000008os8IAA
    public static string selectStar(String sobjectName, boolean ignoreSystemFields) {
        Set<String> fieldNames = schema.describeSObjects(new String[] {sobjectName})[0].fields.getMap().keyset();
        List<String> iterableFields = new List<String>(fieldNames);
    
        string query = String.format('SELECT {0} FROM {1}', new String[] {String.join(iterableFields, ','), sobjectName});
        
        if (ignoreSystemFields) {
            query = query.replace('isdeleted,name,currencyisocode,createddate,createdbyid,lastmodifieddate,lastmodifiedbyid,systemmodstamp,connectionreceivedid,connectionsentid,', '');
        }
        
        return query;
    }
    
    public static integer getRandomNumber(integer upperLimitExclusive) {
        return (Math.random() * upperLimitExclusive).intValue();
    }
    
    public static boolean getBoolean() {
        return getRandomNumber(2) < 1 ? false : true;
    }
    
    public static List<string> extract(string fieldName, List<sObject> lst) {
    	return extract(fieldName, lst, true);
    }
    public static List<string> extract(string fieldName, List<sObject> lst, boolean unique) {
        Set<string> uniques = new Set<string>();
        List<string> extracted = new List<string>();
        
        for (sObject obj : lst) {
            string val = (string) obj.get(fieldName);
            
            if (val != null) {
                if (!unique || (unique && !uniques.contains(val))) {
                    uniques.add(val);
                    extracted.add(val);
                }
            }
        }
        
        return extracted;
    }
    
    public static String generateRandomString() {
        return generateRandomString(64);
    }
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz1234567890';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        
        return randStr; 
	}
    
    public static integer indexOf(List<object> lst, object val) {
        for (integer i = 0; i < lst.size(); i++) {
            if (val == lst.get(i)) {
                return i;
            }
        }
        
        return -1;
    }
    
    public static string getMainImage(string image) {
        string prefix = image != null && image != '' && image.startsWith('https://') ? '' : 'https://';
        
        if (image != null && image != '' && image.contains(',')) {
            return prefix + image.split(',').get(0);
        }
        
        return prefix + image;
    }
    
    public static integer secondsBetween(DateTime startDt, DateTime endDt) {
        long startTime = startDt.getTime();
        long endTime = endDt.getTime();
        
        return integer.valueOf(endTime - startTime) / 1000;
    }
}