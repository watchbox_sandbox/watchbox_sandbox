@isTest
public class createIntakesTests {

    /*static testmethod void testIntakesOverloaded(){
        createIntakes createIntake = New createIntakes();
      //  zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
      //      insert testPref;
        Opportunity testOpp = dataFactory.buildOpportunity();
        insert testOpp;
        Watch_Purchase__c watchPurchase = dataFactory.buildWatchPurchase(testOpp);
        insert WatchPurchase;
        test.startTest();
        createIntake.createIntakeRecords(testOpp.id);
        test.stopTest();
        }*/
    static testMethod void testIntakesFromPageRef(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference intakePage = page.vfIntakeconnector;
            
            //create necessary test records
         //   zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
         //   insert testPref;
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Watch_Purchase__c testPurchase = dataFactory.buildWatchPurchase(testOpp);
            insert testPurchase;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(intakePage);
            intakePage.getParameters().put('id',testOpp.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
            createIntakes ci = new createIntakes(controller);
            PageReference result = ci.createIntakeRecords();
            system.assertNotEquals(null, result);
        test.stopTest();
    }
    static testMethod void testReturnIntakesFromPageRef(){
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference intakePage = page.vfcreateReturnIntakesconnector;
            
            //create necessary test records
            Opportunity testOpp = dataFactory.buildOpportunity();
            insert testOpp;
            Return__c testReturn = new Return__c(deal__c = testOpp.id);
            insert testReturn;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(intakePage);
            intakePage.getParameters().put('id',testReturn.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testReturn);
            createReturnIntakes ci = new createReturnIntakes(controller);
            PageReference result = ci.createIntakesForReturns();
            system.assertNotEquals(null, result);
        test.stopTest();
    }
    static testMethod void testNullIdVal(){
        //create necessary test records
        Opportunity testOpp= dataFactory.buildOpportunity();
        insert testOpp;
      //  zkfedex__ShipmatePreference__c testPref = dataFactory.buildPreference();
      //  insert testPref;
        //testing overloaded method with null Id
        id nullId = null;
        createIntakes ci = new createIntakes();
        //ci.createIntakeRecords(nullId);
        //testing PageRef method with null Id
        //
        test.startTest();
            //create new pagereference to intakeConnector
            PageReference intakePage = page.vfIntakeconnector;
            //create necessary test records
            Watch_Purchase__c testPurchase = dataFactory.buildWatchPurchase(testOpp);
            insert testPurchase;
            //set test page and create standard Opportunity controller
            Test.setCurrentPage(intakePage);
            intakePage.getParameters().put('id',null);
            ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
            createIntakes ci2 = new createIntakes();
            createIntakes ci3 = new createIntakes(controller);
            PageReference result = ci2.createIntakeRecords();
            PageReference comparativeResult = ci3.createIntakeRecords();
            system.assertNotEquals(comparativeResult, result);
        test.stopTest();
        
    }
}