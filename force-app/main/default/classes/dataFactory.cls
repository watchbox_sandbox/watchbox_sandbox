@isTest
public class dataFactory {
    
    public static User testOperationsUser(){
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',FirstName='Watchuwant', LastName='Operations', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='operations42@testorg.com');
        return u;
    }
    public static User buildSecondaryUser(){
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', FirstName='Testing', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser16@testorg.com'); 
       
       return u;
    }
    public static Account buildAccount(){
        RecordType  recordType = [SELECT id FROM RecordType WHERE name='People' AND sObjectType='Account' LIMIT 1];
        Account acc = new Account(RecordType=recordType, first_Name__c='James', last_Name__c='Jones', name='Testing', email__c ='testuser@standard.org', phone='5555555555', mobile__c = '5555555555');
        return acc;
    }
    public static List<Contact> consWithDeals(){
        List<Contact> consAndDeals = New List<Contact>();
        for(integer x=0;x<10;x++){
            Contact newCon = New Contact(
            lastName= 'Test' + x,
            IPAddress__c = '281.0.1.1.1.'+x,
            PageId__c = 'f1504-24e001',
            pageURL__c = 'ub.watchuwant.com/sellnew-1',
            pageVariant__c='a',
            submissionDate__c = Date.today(),
            phone = '5555551212',
            email = 'test@testytest'+x+'.com');
            consAndDeals.add(newCon);
        }
        return consAndDeals;
    }
    public static Opportunity buildOpportunity() {
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
             
        opp.CloseDate=System.today().addMonths(1); 
        
        return opp;
    }
    public static Watch_Purchase__c buildWatchPurchase(Opportunity opp){
        
        
        Watch_Purchase__c wp = New Watch_Purchase__c();
        wp.Model_Name__c = 'Test Model';
        wp.Cost__c = 99.99;
        wp.MSP__c = 999.99;
        wp.Asking__c = 799.99;
        wp.Retail__c = 1099.99;
        wp.Origination_Opportunity__c = opp.Id;
        return wp;
    }
    public static Watch_Sale__c buildWatchSale(Opportunity opp, Id prodId){
        Watch_Sale__c ws = New Watch_Sale__c();
        ws.Sale_Amount__c = 99.99;
        ws.Opportunity__c = opp.id;
        ws.Inventory_Item__c = prodId;
        return ws;
    }
    public static Intake__c buildIntake(Opportunity opp){
        Intake__c intake = new Intake__c();
        intake.deal__c = opp.Id;
        intake.Model__c='Model1';
        return intake;
    }
    public static Inspection__c buildInspection(Id oppId, Id intakeId){
        Inspection__c inspection = new Inspection__c();
        inspection.opportunity__c = oppId;
        inspection.Intake__c = intakeId;
        return inspection;
    }
    public static Brand_object__c buildBrand(){
        Brand_object__c brand = new Brand_object__c();
        brand.name = 'TestBrand';
        return brand;
    }
    public static Series__c buildSeries(Id brandId){
        Series__c series = new Series__c();
        series.name = 'Test Series';
        series.Watch_Brand__c = brandId;
        return series;
    }
    public static Model__c buildModel(Id brandId, Id seriesId){
        Model__c model = new Model__c();
        model.Name = 'Model1';
        model.model_brand__c = brandId;
        model.model_series__c = seriesId;
        return model;
    }
    public static Product2 buildProduct(Id modelId){
        Product2 product = new Product2();
        product.Name ='TestProduct';
        product.Model__c = modelId;
        product.Hold_Expiration__c = system.now();
        return product;
    }
    public static Watchmaking__c buildWatchmaking(Id productId){
        Watchmaking__c watchmaking = new Watchmaking__c();
        watchmaking.Type__c = 'Internal';
        watchmaking.Inventory__c = productId;
        watchmaking.Mechanical__c = 'Sell As Is';
        watchmaking.Aesthetics__c = 'Sell As Is';
        return watchmaking;
    }
    
    /*
    public static zkfedex__ShipmatePreference__c buildPreference(){
        zkfedex__ShipmatePreference__c preference=new zkfedex__ShipmatePreference__c(zkfedex__LabelImageTypeDefault__c='PDF',
        zkfedex__FedExAccountNumber__c = '630156343',
        zkfedex__SenderNameDefault__c = 'WUW',
        zkfedex__SenderEmailDefault__c = 'adamnila@aenila.com',
        zkfedex__SendEmailNotificationToRecipient__c=true,
        zkfedex__SendEmailNotificationToShipper__c=false,
        zkfedex__ShippingCity__c='Collierville',
        zkfedex__ShippingCountry__c = 'US',
        zkfedex__ShippingState__c='TN',
        zkfedex__ShippingStreet__c='10 Fed Ex Pkwy',
        zkfedex__ShippingPostalCode__c='38017',
        zkfedex__BillingCity__c='Collierville',
        zkfedex__BillingCountry__c='US',
        zkfedex__BillingState__c='TN',
        zkfedex__BillingStreet__c='10 Fed Ex Pkwy',
        zkfedex__BillingPostalCode__c='38017');
        
        return preference;  
    }
    
    
    public static zkfedex__Shipment__c buildShipment(){
        zkfedex__Shipment__c shipment = new zkfedex__Shipment__c();
        return shipment;
        
    }
    */
}