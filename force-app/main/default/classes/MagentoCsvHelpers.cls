public with sharing class MagentoCsvHelpers {
    public class UnsupportedTranslationException extends Exception {}

    public static String escape(String raw) {
            return raw.escapeCsv();
    }

    public static String translationToCsvRow(Translation__c trans, String language) {
        String row = '';

        if ('chinese_simplified' == language) {
            row += MagentoCsvHelpers.escape(trans.Vocabulary_Word__c);
            row += ',';
            row += MagentoCsvHelpers.escape(trans.Chinese_Simplified_Word__c);
        }

        if ('chinese_traditional' == language) {
            row += MagentoCsvHelpers.escape(trans.Vocabulary_Word__c);
            row += ',';
            row += MagentoCsvHelpers.escape(trans.Chinese_Traditional_Word__c);
        }

        if ('spanish' == language) {
            row += MagentoCsvHelpers.escape(trans.Vocabulary_Word__c);
            row += ',';
            row += MagentoCsvHelpers.escape(trans.Spanish_Word__c);
        }

        if ('german' == language) {
            row += MagentoCsvHelpers.escape(trans.Vocabulary_Word__c);
            row += ',';
            row += MagentoCsvHelpers.escape(trans.German_Word__c);
        }

        if ('french' == language) {
            row += MagentoCsvHelpers.escape(trans.Vocabulary_Word__c);
            row += ',';
            row += MagentoCsvHelpers.escape(trans.French_Word__c);
        }

        if (row != '') {
            return row;
        }

        throw new UnsupportedTranslationException('translation type not supported; supported types are: chinese_simplified, chinese_traditional, spanish, german, french');
    }

    public static Blob translationsToCsv(List<Translation__c> translations, String language) {
        String csv = '';

        for (Translation__c translation : translations) {
            try {
                csv += MagentoCsvHelpers.translationToCsvRow(translation, language);
                csv += '\n';
            } catch (NullPointerException e) {
                // translation doesn't have this language, so skip it.
            }
        }

        return Blob.valueOf(csv);
    }
}