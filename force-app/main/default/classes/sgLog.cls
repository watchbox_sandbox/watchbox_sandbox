public class sgLog {
    public static void product(List<Product2> products, Map<Id, Product2> oldMap) {
		List<string> fields = new List<string> {
            'ASK__c', 
            'Cost__c', 
            'MSP__c', 
            'Retail__c', 
            'WUW_eBay_Listing_Price__c', 
            'WUW_Sold_Price__c',
            'Status__c',
            'QC_Failed__c'
        };  
            
         
        
        List<Log__c> logs = new List<Log__c>();
        
		for (Product2 p : products) {
            try {
                Product2 old = (Product2) oldMap.get(p.Id);
                
                for (string f : fields) {
                    string newVal = string.valueOf(p.get(f));
                    string oldVal = string.valueOf(old.get(f));
    
                    //system.debug(f + ' ' + newVal + ' ' + oldVal);
    
                    if (newVal != oldVal) {
                        Log__c log = log(f, newVal, oldVal);
                        log.Product__c = p.Id;
                        logs.add(log);
                    }          
                }
            } catch (Exception e) {}
        }
        
        if (logs.size() > 0) {
            insert logs;
        }
    }
    
	private static Log__c log(string field, string newVal, string oldVal) {
        string userId = '';
        try {
            userId = sgAuth.user.Id;
        } catch (Exception e) {
            userId = null;
        }
        
    	return new Log__c (
        	Team_Member__c = userId,
            Field__c = field,
            NewValue__c = string.valueOf(newVal),
            OldValue__c = string.valueOf(oldVal)
        );                
    }
}