global class CreateLeadGenLogFromEmail implements Messaging.InboundEmailHandler {
    public final String EMAIL_TYPE_INQUIRY = 'Inquiry regarding';
    public final String EMAIL_TYPE_PRICE_SUGGESTION ='You\'ve received a price suggestion';
    public final String LEAD_SOURCE = 'Chrono24 Watchbox';
    public final String EMAIL_SOURCE = 'TrustedCheckout@chrono24.com';
    public final String EMAIL_SOURCETRAN = 'transaction@chrono24.com';
    public static Boolean doNotCreateAcctFromEmail = false;
    
    public enum EmailType {
            EMAIL_TYPE_INQUIRY,
            EMAIL_TYPE_PRICE_SUGGESTION
            }
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope)
    {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Lead_Gen_Log__c leadGenLogObj = new Lead_Gen_Log__c();
        String subject = '';
        try{
            string emailFrom=email.fromAddress;
            system.debug('emailFrom' + emailFrom ) ; 
            subject = email.subject;
            String[] watchBrand = subject.split(' ');
            system.debug('SUBJECT' + subject ) ; 
            system.debug('condition1' + emailFrom.containsIgnoreCase(String.valueOf(EMAIL_SOURCE))) ;
            system.debug('condition2' + subject.containsIgnoreCase(String.valueOf(EMAIL_TYPE_PRICE_SUGGESTION))) ; 
            
            if(!emailFrom.containsIgnoreCase(String.valueOf(EMAIL_SOURCE)) && subject.containsIgnoreCase(String.valueOf(EMAIL_TYPE_INQUIRY))) {
                inquiryEmail(leadGenLogObj, email.subject, email.plainTextBody, email.fromAddress);
                
            } 
            else if((emailFrom.containsIgnoreCase(String.valueOf(EMAIL_SOURCE)) || emailFrom.containsIgnoreCase(String.valueOf(EMAIL_SOURCETRAN))) && subject.containsIgnoreCase(String.valueOf(EMAIL_TYPE_PRICE_SUGGESTION)))
            {
                
                EMAIL_TYPE_PRICE_SUGGESTION(leadGenLogObj,email.subject,email.plainTextBody,email.fromAddress);
            }
        }catch(Exception e){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string str = System.Label.Email_Id;
            List<String> lstemails = new List<String>();
            String apexLogEmailStr = subject  +'::  <br/> ';
            apexLogEmailStr += e.getmessage() +'::  <br/> ';
            lstemails = str.split(',');
            mail.setToAddresses(lstemails);
            mail.setSaveAsActivity(false);  
            mail.setSubject('Debug Log Deatils for Chrono24');
            mail.setHtmlBody(apexLogEmailStr);
            mails.add(mail);
            Messaging.sendEmail(mails);     
        }        
        return result;
    }
    
    private String getSellBrandName(String[] emailSubjectList) {
        String sellBrandName;
        if(emailSubjectList != null && emailSubjectList.size() > 0) {
            sellBrandName  = emailSubjectList[5] + ' '+ emailSubjectList[6] + ' '+ emailSubjectList[7]+ ' '+ emailSubjectList[8];
        }
        
        return sellBrandName;
    }
    
    private void inquiryEmail(Lead_Gen_Log__c leadGenLogObj, String emailSubject, String emailPlainTextBody, String fromAddress) {
        CreateLeadGenLogFromEmail.doNotCreateAcctFromEmail=true;
        String subject = emailSubject;
        String[] watchBrand = subject.split(' ');
        
        leadGenLogObj.Watch_Brand__c = getSellBrandName(watchBrand).replace('´','').replace('at', '');
        //leadGenLogObj.Watch_Model__c = leadGenLogObj.Watch_Brand__c.substring(leadGenLogObj.Watch_Brand__c.lastIndexOf(' ') + 1);
        //String[] WatchModel=leadGenLogObj.Watch_Brand__c.split('[A-Z0-9]+');
        //String[] Model=WatchBrand.split('[A-Z0-9]');
        String[] Model=leadGenLogObj.Watch_Brand__c.split('[0-9][0-9]+|[A-Z][A-Z]+|[A-Z]+[0-9]+|[0-9]+[A-Z]+');
        leadGenLogObj.Watch_Model__c=leadGenLogObj.Watch_Brand__c.replace(Model[0],'');
        //leadGenLogObj.Watch_Brand__c=leadGenLogObj.Watch_Brand__c.replace(leadGenLogObj.Watch_Model__c,'');
        leadGenLogObj.Watch_Brand__c=leadGenLogObj.Watch_Model__c!=null?leadGenLogObj.Watch_Brand__c.replace(leadGenLogObj.Watch_Model__c,''):leadGenLogObj.Watch_Brand__c;
        leadGenLogObj.Lead_Source__c = LEAD_SOURCE;
        
        //if(email.plainTextBody != Null && email.plainTextBody != ''){
        if(String.isNotBlank(emailPlainTextBody)){
            String[] emailBodyRows = emailPlainTextBody.split('\n');
            System.debug('emailBodyRows'+emailBodyRows);
            
            system.debug(emailBodyRows[13]+'!!!!'+emailBodyRows[14]+'@@@@@'+emailBodyRows[15]+'%%%%'+emailBodyRows[16]);
            leadGenLogObj.Comments__c = emailBodyRows[14];
            leadGenLogObj.Email__c=fromAddress;
            
            for (String bodyRow :emailBodyRows) {
                
                String[] rowContents = bodyRow.split(':');
                String label = rowContents[0].trim();
                System.debug('rowContents.size()'+rowContents.size());
                
                if(rowContents.size() >1){
                    String value = rowContents[1].trim();
                    leadGenLogObj.Sale_Price__c =0;
                    switch on label {
                        when 'Name' {
                            String strr=value.replace('*','');
                            String[] fullName = strr.split(' ');
                            String firstName = fullName.size() > 0 ? fullName[0] : strr;
                            String lastName = fullName.size() > 1 ? fullName[1] :'';
                            leadGenLogObj.Account_Name__c=firstName+' '+lastName;
                            leadGenLogObj.First_Name__c = 'Chrono24 Message';
                            leadGenLogObj.Last_Name__c = lastName+' '+leadGenLogObj.Watch_Brand__c;
                        }
                        when 'Country' {    
                            leadGenLogObj.IP_Country__c = value.replace('*',''); 
                        }
                        when 'Price' {  
                            String orPrice = value.replace('$','').replace('£', '').replace('HK', '').replace('Rs.', '').replace('€', '').replace(',', '');
                            leadGenLogObj.WB_offer__c = Decimal.valueof(orPrice);
                        }
                        when 'Internal number' {    
                            leadGenLogObj.SKU__c = value.replace('*',''); 
                        }
                        
                    }            
                }  
            }
        }
        try{
            insert leadGenLogObj;
        }catch(Exception e){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string str = System.Label.Email_Id;
            List<String> lstemails = new List<String>();
            String apexLogEmailStr = subject  +'::  <br/> ';
            apexLogEmailStr += e.getmessage() +'::  <br/> ';
            lstemails = str.split(',');
            mail.setToAddresses(lstemails);
            mail.setSaveAsActivity(false);  
            mail.setSubject('Debug Log Deatils for Chrono24 : line number 124 : insert falied');
            mail.setHtmlBody(apexLogEmailStr);
            mails.add(mail);
            Messaging.sendEmail(mails);         
        }        
    }
    
    private void EMAIL_TYPE_PRICE_SUGGESTION(Lead_Gen_Log__c leadGenLogObj,String subject, String emailPlainTextBody,string fromAddress)
    {    
        try{
            doNotCreateAcctFromEmail=true;
            if(String.isNotBlank(emailPlainTextBody)){
                
                String eSubject = subject;
                String[] watchBrand = eSubject.split(' ');
                String dealID='';
                String dealIDTC='';
                if(watchBrand.size() > 3){
                    dealID= watchBrand[5].replace('(','');
                    dealIDTC=dealID.replace(')','');
                }
                system.debug(watchBrand.size());
                system.debug(watchBrand);
                String[] emailBodyRows = emailPlainTextBody.split('\n');
                Integer  emailsize = emailBodyRows.size();
                system.debug(emailsize);
                //List<Lead_Gen_Log__c> duplicateLead;
                List<Lead_Gen_Log__c> duplicateLeadList;
                if(dealIDTC !=null && dealIDTC !=''){ 
                   duplicateLeadList= [SELECT Id,Trusted_Checkout_Deal_ID__c FROM Lead_Gen_Log__c WHERE Trusted_Checkout_Deal_ID__c = :dealIDTC];
                }
                if(duplicateLeadList.size() > 0){ 
                    Lead_Gen_Log__c duplicateLead = duplicateLeadList.get(0);
                    List<Lead_Gen_Log__c> leadListToUpdate= new List<Lead_Gen_Log__c>();
                    duplicateLead.Lead_Source__c='Chrono24 Watchbox';
                    for (String bodyRow:emailBodyRows){
                        //for(Lead_Gen_Log__c leadtoupdate:duplicateLead) 
                            String[] rowContents = bodyRow.split(':');
                            String label = rowContents[0].trim();
                            System.debug('rowContents.size()'+rowContents.size());
                            
                            if(rowContents.size() >1){
                                String value = rowContents[1].trim();
                                String[] NameContents = emailBodyRows[2].split(' ');
                                String description = emailBodyRows[26]+ ' \r\n'+emailBodyRows[27]+ ' \r\n'+emailBodyRows[28]+ ' \r\n'+emailBodyRows[29]+ ' \r\n'+emailBodyRows[30]; 
                                System.debug('***************************'+description);
                                //String firstName = NameContents.size() > 1 ? NameContents[1] : NameContents[0];
                                //String lastName = NameContents.size() > 3 ? NameContents[3].replaceAll(':','') : NameContents[2].replaceAll(':','');
                                duplicateLead.Watch_Brand__c =emailBodyRows[26].replaceAll('\\[.*?\\]', '');
                                String[] Model=duplicateLead.Watch_Brand__c.split('[0-9][0-9]+|[A-Z][A-Z]+|[A-Z]+[0-9]+|[0-9]+[A-Z]+');
                                duplicateLead.Watch_Model__c=duplicateLead.Watch_Brand__c.replace(Model[0],'');
                                duplicateLead.Watch_Brand__c=duplicateLead.Watch_Model__c!=null?duplicateLead.Watch_Brand__c.replace(duplicateLead.Watch_Model__c,''):duplicateLead.Watch_Brand__c;
                                //duplicateLead.Watch_Model__c = duplicateLead.Watch_Brand__c.substring(duplicateLead.Watch_Brand__c.lastIndexOf(' ') + 1);
                                duplicateLead.First_Name__c = 'Chrono24 Price Suggestion';
                                duplicateLead.Last_Name__c = dealIDTC+' '+duplicateLead.Watch_Brand__c;
                                duplicateLead.Email__c=fromAddress;
                                duplicateLead.Trusted_Checkout_Deal_ID__c=dealIDTC;
                                duplicateLead.Opportunity_Name__c='Chrono24 Price Suggestion'+' '+dealIDTC+' '+duplicateLead.Watch_Brand__c;
                                switch on label {
                                    when 'Code' {                                    
                                        duplicateLead.SKU__c = value.replace('*','');
                                    }
                                    when 'Price' {  
                                        String orPrice = value.replace('$','').replace('£', '').replace('HK', '').replace('Rs.', '').replace('€', '').replace(',', '');
                                        if(orPrice != Null)
                                        duplicateLead.WB_offer__c = Decimal.valueof(orPrice);
                                    }                            
                                    when 'Buyer\'s suggested price' {   
                                        String orPrice = value.replace('$','').replace('£', '').replace('HK', '').replace('Rs.', '').replace('€', '').replace(',', '');
                                        if(orPrice != Null)
                                        duplicateLead.Sale_Price__c = Decimal.valueof(orPrice);
                                    }
                                    when 'Buyer\'s name' {
                                        String strr=value.replace('*','');
                                        String[] fullName = strr.split(' ');
                                        String firstName = fullName.size() > 0 ? fullName[0] : strr;
                                        String lastName = fullName.size() > 1 ? fullName[1] :'';
                                        duplicateLead.Account_Name__c=firstName+' '+lastName;
                                        system.debug(duplicateLead.Account_Name__c);
                                    }
                                    when 'Country' {    
                                        String countryOrg =value.replace('*','');
                                        duplicateLead.IP_Country__c = countryOrg; 
                                    }
                                    when 'Phone number' {                                   
                                        duplicateLead.Phone__c = value.replace('*','');
                                    }
                                } 
                            }                     
                    }
                    //if(!leadListToUpdate.isEmpty()){
                        update duplicateLead;
                    //}
                }
                else{
                    system.debug('No duplicates avaialable');
                    leadGenLogObj.Lead_Source__c = 'Chrono24 Watchbox';                
                    
                    for (String bodyRow:emailBodyRows)
                    {
                        
                        String[] rowContents = bodyRow.split(':');
                        String label = rowContents[0].trim();
                        System.debug('rowContents.size()'+rowContents.size());
                        
                        if(rowContents.size() >1){
                            String value = rowContents[1].trim();
                            String[] NameContents = emailBodyRows[2].split(' ');
                            String description = emailBodyRows[26]+ ' \r\n'+emailBodyRows[27]+ ' \r\n'+emailBodyRows[28]+ ' \r\n'+emailBodyRows[29]+ ' \r\n'+emailBodyRows[30];
                            System.debug('***************************'+description);
                            //String firstName = NameContents.size() > 1 ? NameContents[1] : NameContents[0];
                            //String lastName = NameContents.size() > 3 ? NameContents[3].replaceAll(':','') : NameContents[2].replaceAll(':','');
                            leadGenLogObj.Watch_Brand__c =emailBodyRows[26].replaceAll('\\[.*?\\]', '');
                            String[] Model=leadGenLogObj.Watch_Brand__c.split('[0-9][0-9]+|[A-Z][A-Z]+|[A-Z]+[0-9]+|[0-9]+[A-Z]+');
                            leadGenLogObj.Watch_Model__c=leadGenLogObj.Watch_Brand__c.replace(Model[0],'');
                            //leadGenLogObj.Watch_Brand__c=leadGenLogObj.Watch_Brand__c.replace(leadGenLogObj.Watch_Model__c,'');
                            leadGenLogObj.Watch_Brand__c=leadGenLogObj.Watch_Model__c!=null?leadGenLogObj.Watch_Brand__c.replace(leadGenLogObj.Watch_Model__c,''):leadGenLogObj.Watch_Brand__c;
                            leadGenLogObj.First_Name__c = 'Chrono24 Price Suggestion';
                            leadGenLogObj.Last_Name__c = dealIDTC+' '+leadGenLogObj.Watch_Brand__c;
                            leadGenLogObj.Email__c=fromAddress;
                            leadGenLogObj.Trusted_Checkout_Deal_ID__c=dealIDTC;
                            leadGenLogObj.Opportunity_Name__c='Chrono24 Price Suggestion'+' '+dealIDTC+' '+leadGenLogObj.Watch_Brand__c;
                            switch on label {
                                when 'Code' {
                                    leadGenLogObj.SKU__c = value.replace('*','');
                                    system.debug(leadGenLogObj.SKU__c);
                                }
                                when 'Price' {  
                                    String orPrice = value.replace('$','').replace('£', '').replace('HK', '').replace('Rs.', '').replace('€', '').replace('kr','').replace('CHF','').replace('¥','').replace('AED','').replace('MX','').replace(',', '');
                                    if(orPrice != Null)
                                    leadGenLogObj.WB_offer__c = decimal.valueof(orPrice.trim());
                                }                            
                                when 'Buyer\'s suggested price' {   
                                    String orPrice = value.replace('$','').replace('£', '').replace('HK', '').replace('Rs.', '').replace('€', '').replace('kr','').replace('CHF','').replace('¥','').replace('AED','').replace('MX','').replace(',', '');
                                    if(orPrice != Null)
                                    leadGenLogObj.Sale_Price__c = Decimal.valueof(orPrice.trim());
                                }
                                when 'Buyer\'s name' {
                                    String strr=value.replace('*','');
                                    String[] fullName = strr.split(' ');
                                    String firstName = fullName.size() > 0 ? fullName[0] : strr;
                                    String lastName = fullName.size() > 1 ? fullName[1] :'';
                                    leadGenLogObj.Account_Name__c=firstName+' '+lastName;
                                    system.debug(leadGenLogObj.Account_Name__c);
                                }
                                when 'Country' {    
                                    String countryOrg =value.replace('*','');
                                    leadGenLogObj.IP_Country__c = countryOrg;
                                }
                                when 'Phone number' {                                   
                                    leadGenLogObj.Phone__c = value.replace('*','');
                                    system.debug(leadGenLogObj.Phone__c);
                                }
                            } 
                        }
                    }
                    insert leadGenLogObj;
                }
            }
        }catch(Exception e){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string str = System.Label.Email_Id;
            List<String> lstemails = new List<String>();
            String apexLogEmailStr = subject  +'::  <br/> ';
            apexLogEmailStr += e.getmessage() +'::  <br/> ';
            lstemails = str.split(',');
            mail.setToAddresses(lstemails);
            mail.setSaveAsActivity(false);  
            mail.setSubject('Debug Log Deatils for Chrono24:Method:EMAIL_TYPE_PRICE_SUGGESTION : insert/update falied');
            mail.setHtmlBody(apexLogEmailStr);
            mails.add(mail);
            Messaging.sendEmail(mails);         
        } 
    }
}