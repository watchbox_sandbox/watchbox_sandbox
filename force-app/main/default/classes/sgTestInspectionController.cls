@isTest
public class sgTestInspectionController {
	static testMethod void get() {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();        
        
        Intake__c intake = new Intake__c(
        	Is_Active__c = true
        );
        insert intake;
        
        List<Team_Member__c> members = sgDataSeeding.TeamMembers();
        insert members;
        
        Inspection__c i = new Inspection__c (
            Intake__c = intake.Id,
            Sales_Approved__c = false
        );
        
        insert i;
        
        
        RestContext.request = new RestRequest();
        
        sgInspectionData sid = sgInspectionController.get();
        system.debug(sid);
        system.assertEquals(1, sid.Products.size());
    }
    
    static testMethod void post1() {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();        
       
        Intake__c intake = new Intake__c();
        insert intake;
        
        Inspection__c p = new Inspection__c(
        	Intake__c = intake.Id
        );
        
        insert p;
        
        update p;
                
        RestContext.request = new RestRequest();
        
        Inspection__c i = new Inspection__c ( Id = p.Id, Issue_Type__c = 'Needs Parts' );
        sgInspectionController.post(i, 'Pickups', new List<string>{'Issue_Type__c'});
        
        Inspection__c inspection = [select Id, Issue_Type__c, Mark_Issue__c from Inspection__c where Id = :i.Id limit 1];
        
        system.assertEquals(true, inspection.Mark_Issue__c);
    }
    
    static testMethod void put() {
        Inspection__c i = new Inspection__c();
        
        insert i;
        
        sgInspectionController.put(i.Id);
        
        Inspection__c inspection = [select Id, Inspection_Start_Time__c from Inspection__c where Id = :i.Id limit 1];
        
        system.assert(inspection.Inspection_Start_Time__c != null);
    }
    
    static testMethod void patch1() {
        Inspection__c i = new Inspection__c();
        
        insert i;
        
        sgInspectionController.patch(i.Id, 'Approve');
        
        Inspection__c inspection = [select Id, Request_Approval__c from Inspection__c where Id = :i.Id];
        
        system.assertEquals(inspection.Request_Approval__c, true);
    }
    
    static testMethod void patch2() {
        Inspection__c i = new Inspection__c();
        
        insert i;
        
        sgInspectionController.patch(i.Id, 'Approved');
        
        Inspection__c inspection = [select Id, Sales_Approved__c from Inspection__c where Id = :i.Id];
        
        system.assertEquals(inspection.Sales_Approved__c, true);
    }
   
}