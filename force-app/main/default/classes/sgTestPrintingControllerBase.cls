@isTest
public class sgTestPrintingControllerBase {

    static testMethod void construct() {
        //arrange
        //
        Map<String,String> parameters = ApexPages.currentPage().getParameters();
        parameters.put('templateType', 'test');
        parameters.put('organization', 'Govberg');
        createOrg('Govberg', 'url');
        //act
        //
        sgPrintingControllerBase ctl = new sgPrintingControllerBase();
        String logo = ctl.getLogo();
        String alt = ctl.getLogoAlt();
        
        //assert
        //
        System.assertEquals('Govberg', ctl.Org.Name);
        System.assertEquals('url', logo);
        System.assertEquals('Govberg Logo', alt);
    }
    
    //*****************************************************************
    // helper methods 
    
    static void createOrg(String name, String url) {
        Organization__c org = new Organization__c(
        	Name = name,
            Logo_Url__c = url
        );
        insert org;
    }
    
}