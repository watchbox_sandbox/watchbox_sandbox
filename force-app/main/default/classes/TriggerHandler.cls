public abstract class TriggerHandler {

    public virtual void execute() {
        if(Trigger.isBefore) {
            if(Trigger.isInsert)            
                beforeInsert();            
            else if(Trigger.isUpdate)            
                beforeUpdate();            
            else if(Trigger.isDelete)            
                beforeDelete();            
        }
        else if(Trigger.isAfter) {
            if(Trigger.isInsert)            
                afterInsert();            
            else if(Trigger.isUpdate)            
                afterUpdate();            
            else if(Trigger.isDelete)            
                afterDelete();            
            else if(Trigger.isUndelete)            
                afterUndelete();            
        }
    }
    
    public virtual void beforeInsert(){}
    public virtual void afterInsert(){}
    
    public virtual void beforeUpdate(){}
    public virtual void afterUpdate(){}
    
    public virtual void beforeDelete(){}
    public virtual void afterDelete(){}
    
    public virtual void afterUndelete(){}
}