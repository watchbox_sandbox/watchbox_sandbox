@isTest
public class NoteBatchTest {
    
    static testmethod void execute_note_attached_test() {
        Account acc=TestFactory.createAccount();
       	Note note =TestFactory.createNote(acc.id);
                
        Test.startTest();
        NoteBatch batch = new NoteBatch();
        database.executeBatch(batch);
  		Test.stopTest();
        
        Account account = [SELECT Id, IsNoteAttached__c FROM Account WHERE Id =:acc.Id];
        
        System.assertEquals(account.IsNoteAttached__c,'Y');        
    }
    
    static testmethod void execute_note_notattached_test() {
        Account acc=TestFactory.createAccount();
        Test.startTest();
        NoteBatch batch = new NoteBatch();
        database.executeBatch(batch);
  		Test.stopTest();
        
        Account account = [SELECT Id, IsNoteAttached__c FROM Account WHERE Id =:acc.Id];
        
        System.assertEquals(account.IsNoteAttached__c,'N');        
    }

}