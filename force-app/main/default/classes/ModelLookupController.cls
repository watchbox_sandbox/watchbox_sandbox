public class ModelLookupController {
    public List<Model__c> Models {get; set;}
    public string SearchTerm { get; set;} 
    
    public PageReference searchModels() {
        Models = new List<Model__c>();
        
        try {
			string brandId = ApexPages.currentPage().getParameters().get('brandId');
            
            string sosl = 'FIND :SearchTerm IN ALL FIELDS RETURNING Model__c(Id, Name, Reference__c WHERE model_brand__c=:brandId ORDER BY NAME) LIMIT 100';
         	System.debug('Executing ModelLookupController.searchProducts with SearchTerm - ' + SearchTerm);
            
            Search.SearchResults searchResults = Search.find(sosl);
            List<Search.SearchResult> modelList = searchResults.get('Model__c');
            
            for (Search.SearchResult searchResult : modelList) { 
                Model__c model = (Model__c)searchResult.getSObject(); 
                Models.add(model);
            } 
        } catch (Exception e) {
            System.debug('Error searching models in ModelLookupController.searchProducts - ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);     
        }       

        return null;
    }
}