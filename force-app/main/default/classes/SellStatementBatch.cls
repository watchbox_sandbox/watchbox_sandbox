global class SellStatementBatch implements Database.Batchable<sObject> {
	global Database.QueryLocator start(Database.BatchableContext BC){
    	return Database.getQueryLocator([SELECT Id, Sell_Statement__c, Originating_LeadGenLog__r.Full_Name__c, Originating_LeadGenLog__r.Sell_Brand__c,
                                         Originating_LeadGenLog__r.Sell_Model__c,Originating_LeadGenLog__r.Sell_Watch_Serial__c,
                                         Originating_LeadGenLog__r.Watch_Reference__c,Originating_LeadGenLog__r.Name_Your_Price_ASK__c,
                                         Originating_LeadGenLog__r.Type_of_Metal__c,Originating_LeadGenLog__r.Dial_Description__c,
                                         Originating_LeadGenLog__r.Boxes_and_Papers__c, Originating_LeadGenLog__r.Last_Service_Date__c,
                                         Originating_LeadGenLog__r.Images__c                                         
                                         FROM Opportunity WHERE Originating_LeadGenLog__c != null]);
	}

	global void execute(Database.BatchableContext BC, List<Opportunity> opps){
        System.debug('Entering SellStatementBatch.execute');
        
        for (Opportunity opp : opps) {
            string images = '';
        
            if (!string.isBlank(opp.Originating_LeadGenLog__r.Images__c)) {
                images = opp.Originating_LeadGenLog__r.Images__c.replace(',', '\n');
            }
            
            //Requested by client as is...
            opp.Sell_Statement__c = opp.Originating_LeadGenLog__r.Full_Name__c + ' is looking to sell his/her ' + opp.Originating_LeadGenLog__r.Sell_Brand__c + ' ' + opp.Originating_LeadGenLog__r.Sell_Model__c;
            opp.Sell_Statement__c += ' '+ opp.Originating_LeadGenLog__r.Sell_Model__c + ' with serial ' + opp.Originating_LeadGenLog__r.Sell_Watch_Serial__c;
            opp.Sell_Statement__c += ' and reference '+ opp.Originating_LeadGenLog__r.Watch_Reference__c + ' for ' + opp.Originating_LeadGenLog__r.Name_Your_Price_ASK__c;
            opp.Sell_Statement__c += ' The watch has ' + opp.Originating_LeadGenLog__r.Type_of_Metal__c + ' material.  It\'s dial is ' +  opp.Originating_LeadGenLog__r.Dial_Description__c;
            opp.Sell_Statement__c += '.  The watch comes with ' + opp.Originating_LeadGenLog__r.Boxes_and_Papers__c+ ' and was last serviced ' + opp.Originating_LeadGenLog__r.Last_Service_Date__c;
            opp.Sell_Statement__c += '.  Here are the watch images: \n\n'+ images;
        }    
        
        update opps;
	}

	global void finish(Database.BatchableContext BC){
	
    }
}