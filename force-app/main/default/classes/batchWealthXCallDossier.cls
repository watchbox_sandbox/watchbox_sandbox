global class batchWealthXCallDossier implements Database.Batchable<Integer>,Database.AllowsCallouts,Database.stateful{
    public Integer startIndex = 0;
    public Integer endIndex = 0;
    public List<CronTrigger> cronTriggerList;
    global batchWealthXCallDossier(Integer startIndex,Integer endIndex){
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }
    
    global System.Iterable<Integer> start(Database.BatchableContext BC) {
        List<Integer> intLst = new List<Integer>();
        intLst.add(1);
        return intLst;
    }
    
    global void execute(Database.BatchableContext BC, List<Integer> scope) {
        WealthXCallDossier.getAllDossier(startIndex,endIndex); 
    }
    global void finish(Database.BatchableContext BC) {
       
    }
}