public class createIntakes {
    
    public ApexPages.StandardController stdController;
    public id oppId;
    //public createInspections insp = new createInspections();
    //public createShippingLabels ship = new createShippingLabels();
    public createIntakes(){
        //standard no argument constructor
    }
    
    public createIntakes(ApexPages.StandardController controller){
        //constructor - get opportunity incase createIntakeRecords() fails
        stdController = controller;
        //get opportunityID
        oppId = stdController.getId();
        
        //system.debug(oppId);
    }
    public PageReference createIntakeRecords(){
        //get correct record type
        String recordType = 'Intake Origination';
        RecordType purchaseIntake = [SELECT id, name FROM RecordType WHERE name = :recordType];
        system.debug(purchaseIntake);
        system.debug('entered createIntakeRecords method');
        //declare and initialize variables
        Integer recordsToCreate=0;
        List<Intake__c> newIntakes = new List<Intake__c>();
        Set<Id> preexistingWpIds = new Set<Id>();
        List<Watch_Purchase__c> watchPurchases = new List<Watch_Purchase__c>();
        List<Intake__c> existingIntakes = [SELECT id, watch_purchase__c, return__c FROM Intake__c WHERE deal__c = :oppid AND return__c = null];
        Opportunity [] countPurchases; 
        
        for(intake__c intk : existingIntakes){
            preexistingWpIds.add(intk.watch_purchase__c);
            system.debug('Record: '+ intk);
            
        }
        //show visual force page in the event no ID is present
        if(oppId == null){
            system.debug('oppId is null :(');
            return null;
        }
        //get watchPurchases from current Opp
        //so we can count them and push them into an array to use the queried fields
        //to populate fields on the intake records we are creating
        countPurchases = [SELECT id, (SELECT id, Brand__r.name, Model__r.name, Description__c, Serial__c FROM Origination_Records__r WHERE id NOT IN :preexistingWpIds) FROM Opportunity WHERE id = :oppId];
        system.debug('countPurchases='+countPurchases);
        //iterate over query results
        for(Opportunity currentOpp : countPurchases){
            //iterate through subquery results, capturing each watch purchase and counting records
            for(Watch_Purchase__c newPurchase : currentOpp.Origination_Records__r){
                //add watch purchases to list
                watchPurchases.add(newPurchase);
                recordsToCreate++;
                system.debug(newPurchase);
                //system.debug('recordsToCreate=' +recordsToCreate);
            }
        }
        //subtract number of existing intakes from number of watch purchases to maintain a 1:1 ratio beween theses records
        
        //only run the following code if the deal has at least one watch purchase record related
        if(recordsToCreate>0){
           for(Watch_Purchase__c wp: watchPurchases){
                //create a new intake for each iteration and tie it to the current deal, 
                //then add to array for insert at end
               if(!preexistingWpIds.contains(wp.id)){
                   system.debug('Watch Purchase Id: '+wp.id+'PreExistingWpIds:' +preexistingWpIds);
                  Intake__c newIntake = new Intake__c(deal__c = oppId, intake_status__c = 'New',
                                                    Watch_Purchase__c = wp.id,
                                                    Description__c = wp.Description__c, 
                                                    Model__c = wp.Model__r.name, 
                                                    Serial__c = wp.Serial__c,
                                                    Brand__c = wp.Brand__r.name,
                                                    RecordType = purchaseIntake);
                newIntakes.add(newIntake);
               system.debug(newIntake); 
               }
                
            }
            //insert all new intakes in one push
            insert newIntakes;
        }
              
        //and now we return to the deal record, making the whole transaction look like a page refresh
        
        PageReference refreshOpp = new PageReference('/'+oppId);
        refreshOpp.setRedirect(true);
        return refreshOpp;
        
    }
    
    
    /*overloaded method for use outside of visualforce page
    public PageReference createIntakeRecords(Id getId){
        oppId = getId;
        //system.debug('in createIntakeRecords');
        //declare and initialize variables
        
        Integer recordsToCreate=0;
        List<Intake__c> newIntakes = new List<Intake__c>();
        List<Watch_Purchase__c> watchPurchases = new List<Watch_Purchase__c>();
        Opportunity [] countPurchases; 
        
        //show visual force page in the event no ID is present
        if(oppId == null){
            //system.debug('oppId is null :(');
            return null;
        }
        //get watchPurchases from current Opp
        //so we can count them and push them into an array to use the queried fields
        //to populate fields on the intake records we are creating
        countPurchases = [SELECT id, (SELECT id, Brand__r.name, Model__r.name, Description__c, Serial__c FROM Origination_Records__r) FROM Opportunity WHERE id = :oppId];
        //iterate over query results
        for(Opportunity currentOpp : countPurchases){
            //iterate through subquery results, capturing each watch purchase and counting records
            for(Watch_Purchase__c newPurchase : currentOpp.Origination_Records__r){
                //add watch purchases to list
                watchPurchases.add(newPurchase);
                recordsToCreate++;
                //system.debug('recordsToCreate=' +recordsToCreate);
            }
        }
        //only run the following code if the deal has at least one watch purchase record related
        if(recordsToCreate>0){
           for(integer i=0; i<recordsToCreate; i++){
                //create a new intake for each iteration and tie it to the current deal, 
                //then add to array for insert at end
                Intake__c newIntake = new Intake__c(deal__c = oppId, intake_status__c = 'New', 
                                                    Description__c = watchPurchases[i].Description__c, 
                                                    Model__c = watchPurchases[i].Model__r.name, 
                                                    Serial__c = watchPurchases[i].Serial__c,
                                                    Brand__c = watchPurchases[i].Brand__r.name);
                newIntakes.add(newIntake);
            }
            //insert all new intakes in one push
            insert newIntakes;
        }
        //now that we have intakes created it's time to call the inspection automation
        //insp.createInspectionRecords(oppId);
        //lastly we generate a shipping label
        //ship.createShipment(oppId);
        return null;
        
    }*/
}