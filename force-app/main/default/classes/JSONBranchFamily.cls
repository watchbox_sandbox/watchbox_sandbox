public class JSONBranchFamily {

    public String id {get;set;} 
    public String creator {get;set;} 
    public Long creationInstant {get;set;} 
    public String modifier {get;set;} 
    public Long modifiedInstant {get;set;} 
    public String name {get;set;} 
    public String brandId {get;set;} 

    public JSONBranchFamily(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'id') {
                        id = parser.getText();
                    } else if (text == 'creator') {
                        creator = parser.getText();
                    } else if (text == 'creationInstant') {
                        creationInstant = parser.getLongValue();
                    } else if (text == 'modifier') {
                        modifier = parser.getText();
                    } else if (text == 'modifiedInstant') {
                        modifiedInstant = parser.getLongValue();
                    } else if (text == 'name') {
                        name = parser.getText();
                    } else if (text == 'brandId') {
                        brandId = parser.getText();
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSONBranchFamily consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    
    public static List<JSONBranchFamily> parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return arrayOfJSONBranchFamily(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    


    private static List<JSONBranchFamily> arrayOfJSONBranchFamily(System.JSONParser p) {
        List<JSONBranchFamily> res = new List<JSONBranchFamily>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new JSONBranchFamily(p));
        }
        return res;
    }



}