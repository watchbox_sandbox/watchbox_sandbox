@IsTest
public class JSONBranchResultTest {
    
    static testMethod void testParse() {
        String json = '['+
        '    {'+
        '        \"id\": \"e71f2fa5-a4ce-4bcb-9a29-f2685049ee11\",'+
        '        \"creator\": \"watchbox-p-user\",'+
        '        \"creationInstant\": 1560968822983,'+
        '        \"modifier\": \"watchbox-p-user\",'+
        '        \"modifiedInstant\": 1560968822983,'+
        '        \"name\": \"Bell & Ross\"'+
        '    }'+
        ']';
        List<JSONBranchResult> r = JSONBranchResult.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        JSONBranchResult objJSONBranchResult = new JSONBranchResult(System.JSON.createParser(json));
        System.assert(objJSONBranchResult != null);
        System.assert(objJSONBranchResult.id == null);
        System.assert(objJSONBranchResult.creator == null);
        System.assert(objJSONBranchResult.creationInstant == null);
        System.assert(objJSONBranchResult.modifier == null);
        System.assert(objJSONBranchResult.modifiedInstant == null);
        System.assert(objJSONBranchResult.name == null);
    }
}