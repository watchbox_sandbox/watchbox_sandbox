@isTest
public class sgTestPostingProductsController {

    static testMethod void get_allProducts() {
        //arrange
        //
        addProducts(10, 2, 2);
        setupRequestContext(null, null, null);
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();

        //assert
        //
        System.assertEquals(10, postingProducts.size());
    }
    
    static testMethod void get_Search() {
        //arrange
        //
        String inventoryId = '123456';
        List<Product2> products = addProducts(10, 2, 2);
        //update the first product with the inventory id
        products[0].WUW_Inventory_ID__c = inventoryId;
        update products[0];
        //add a request context with the inventory id as a param
        setupRequestContext(inventoryId, null, null);
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(1, postingProducts.size());
    }
    
    static testMethod void get_Location() {
        //arrange
        //
        //create a mock location
        Location__c loc = addLocation('test location');
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        //create a location for 2 of the products
        products[0].Location__c = loc.Id;
        products[1].Location__c = loc.Id;
        update products;
        
        //add a request context with the inventory id as a param
        setupRequestContext(null, loc.Id, null);
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(2, postingProducts.size());
    }
    
    static testMethod void get_Published() {
        //arrange
        //
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        //update 1 channel to have a posted date
        ChannelPost__c post = [SELECT Id FROM ChannelPost__c LIMIT 1];
        post.Post_Date__c = Date.valueOf('2016-12-24');
        update post;
        
        //add a request context with the inventory id as a param
        setupRequestContext(null, null, 'true');
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(1, postingProducts.size());
    }
    
    static testMethod void get_Published_False() {
        //arrange
        //
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        //update 1 channel to have a posted date
        ChannelPost__c post = [SELECT Id FROM ChannelPost__c LIMIT 1];
        post.Post_Date__c = Date.valueOf('2016-12-24');
        update post;
        
        //add a request context with the inventory id as a param
        setupRequestContext(null, null, 'false');
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(9, postingProducts.size());
    }
    
    static testMethod void get_Published_Location() {
        //arrange
        //
        //create a mock location
        Location__c loc = addLocation('test location');
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        //update 1 channel to have a posted date
        ChannelPost__c post = [SELECT Id FROM ChannelPost__c WHERE Product__c = :products[0].Id LIMIT 1];
        post.Post_Date__c = Date.valueOf('2016-12-24');
        update post;
        //create a location for 2 of the products
        products[0].Location__c = loc.Id;
        products[1].Location__c = loc.Id;
        update products;
        
        //add a request context with the inventory id as a param
        setupRequestContext(null, loc.Id, 'true');
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(1, postingProducts.size());
    }
    
    static testMethod void get_Published_False_Location() {
        //arrange
        //
        //create a mock location
        Location__c loc = addLocation('test location');
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        //update 1 channel to have a posted date
        ChannelPost__c post = [SELECT Id FROM ChannelPost__c WHERE Product__c = :products[1].Id LIMIT 1];
        post.Post_Date__c = Date.valueOf('2016-12-24');
        update post;
        //create a location for 2 of the products
        products[0].Location__c = loc.Id;
        products[1].Location__c = loc.Id;
        update products;
        
        //add a request context with the inventory id as a param
        setupRequestContext(null, loc.Id, 'false');
        
        //act
        //
        List<sgPostingProductsDTO> postingProducts = sgPostingProductsController.get();
        
        //assert
        //
        System.assertEquals(1, postingProducts.size());
    }
    
    static testMethod void post_ProductsNull() {
        //arrange
        //
        List<sgChannelInfoDTO> channelInfo = new List<sgChannelInfoDTO>();
        channelInfo.add(new sgChannelInfoDTO());
        List<sgPostingProductsDTO> products = new List<sgPostingProductsDTO>();
        
        //act
        //
        Boolean result1 = sgPostingProductsController.post(null, channelInfo);
        Boolean result2 = sgPostingProductsController.post(products, channelInfo);
        
        //assert
        //
        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
    }
    
    static testMethod void post_ChannelInfoNull() {
        //arrange
        //
        List<sgChannelInfoDTO> channelInfo = new List<sgChannelInfoDTO>();
        List<sgPostingProductsDTO> products = new List<sgPostingProductsDTO>();
        products.add(new sgPostingProductsDTO());
        
        //act
        //
        Boolean result1 = sgPostingProductsController.post(products, null);
        Boolean result2 = sgPostingProductsController.post(products, channelInfo);
        
        //assert
        //
        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
    }
    
    static testMethod void post_UpdateProducts() {
        //arrange
        //
        //create some products
        List<Product2> products = addProducts(10, 2, 2);
        
        //create mock channelInfo list
        List<sgChannelInfoDTO> channelInfo = new List<sgChannelInfoDTO>();
        sgChannelInfoDTO chan1 = new sgChannelInfoDTO();
        chan1.Name = 'Google Shopping';
        chan1.Visible = true;
        chan1.Post = false;
        channelInfo.add(chan1);
        
        sgChannelInfoDTO chan2 = new sgChannelInfoDTO();
        chan2.Name = 'Instagram';
        chan2.Visible = true;
        chan2.Post = true;
        chan2.PostDate = Date.valueOf('2016-12-24');
        channelInfo.add(chan2);
        
        sgChannelInfoDTO chan3 = new sgChannelInfoDTO();
        chan3.Name = 'Ebay';
        chan3.Visible = false;
        chan3.Post = false;
        channelInfo.add(chan3);
        
        //create mock postingProducts list
        List<sgPostingProductsDTO> postingProducts = new List<sgPostingProductsDTO>();
        sgPostingProductsDTO prod1 = new sgPostingProductsDTO();
        prod1.Id = products[0].Id;
        postingProducts.add(prod1);
        
        sgPostingProductsDTO prod2 = new sgPostingProductsDTO();
        prod2.Id = products[8].Id;
        postingProducts.add(prod2);
        
        insert new Organization__c(Name = 'Watchbox', Organization_Code__c = 'Watchbox');
        
        //act
        //
        Boolean result = sgPostingProductsController.post(postingProducts, channelInfo);
        
        //assert
        //
        System.assertEquals(true, result);
    }    
    
    static testMethod void post_BadProductId() {
        //arrange
        //

        //create mock channelInfo list
        List<sgChannelInfoDTO> channelInfo = new List<sgChannelInfoDTO>();
        sgChannelInfoDTO chan1 = new sgChannelInfoDTO();
        chan1.Name = 'Google Shopping';
        chan1.Visible = true;
        chan1.Post = false;
        channelInfo.add(chan1);
        
        //create mock postingProducts list
        List<sgPostingProductsDTO> postingProducts = new List<sgPostingProductsDTO>();
        sgPostingProductsDTO prod1 = new sgPostingProductsDTO();
        prod1.Id = '12345';
        postingProducts.add(prod1);
        
        Exception testEx;
        
        //act
        //
        try {
            sgPostingProductsController.post(postingProducts, channelInfo);
        } 
        catch(Exception ex) {
            testEx = ex;
        }
        
        //assert
        //
        System.assert(testEx != null);
    }
    //****************************************************************************
    // support methods
    // 
    
    static void setupRequestContext(String InventoryId, String LocationId, String IsPublished) {
        RestContext.request = new RestRequest();
        if (InventoryId != null) {
            RestContext.request.params.put('InventoryId', InventoryId);
        }
        If (LocationId != null) {
            RestContext.request.params.put('LocationId', LocationId);
        }
        If (IsPublished != null) {
            RestContext.request.params.put('IsPublished', IsPublished);
        }
    }
	
	//these are required fields for a product to be eligible for posting
	static boolean ran = false;
	static Brand_object__c brand = null;
    static Model__c model = null;
    static Series__c series = null;
    
    static List<Product2> addProducts(Integer count, Integer withWuw, Integer withGov) {
        if (!ran) {
            brand = new Brand_object__c();
            insert brand;
            
            series = new Series__c(
                Watch_brand__c = brand.Id
            );
            insert series;
            
            model = new Model__c(
            	Model_Brand__c = brand.Id,
                Model_Series__c = series.Id
            );
            insert model;
        }
        
        List<Product2> products = new List<Product2>();
        //loop `count` times
        for(Integer i = 0; i < count; i++) {
            //create the product
            Product2 product = addProduct();
            //add the Wuw
            if (withWuw > 0) {
                withWuw--;
                addChannelInfo(product, 'Watch U Want', 'Watchbox', true, false);
            }
            //add te Govberg
            if (withGov > 0) { 
                withGov--;
                addChannelInfo(product, 'Govberg', 'Govberg', false, true);
            }
            products.add(product);
        }
        return products;
    }
    
    static integer inventoryId = 100;
    static Product2 addProduct() {
        Product2 product = new Product2(
            Name = 'name',
            Photographed__c = Datetime.valueOf('2016-12-24 12:00:00'),
            Model__c = model.Id,
            WUW_Inventory_ID__c = '' + inventoryId++,
            ASK__c = 1000,
            IsActive = true
        );
        insert product;
        return product;
    }
    
    static void addChannelInfo(Product2 product, String name, String channel, Boolean visible, Boolean post) {
        ChannelPost__c chpost = new ChannelPost__c(
            Name = name,
            Product__c = product.Id,
            Channel__c = channel,
            Post__c = post,
            Visible__c = visible
        );
        insert chpost;
    }
    
    static Location__c addLocation(String locationName) {
        Location__c loc = new Location__c(
            Name = locationName
        );
        insert loc;
        return loc;
    }
}