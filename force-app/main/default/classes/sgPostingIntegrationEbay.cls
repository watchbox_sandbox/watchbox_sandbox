public class sgPostingIntegrationEbay extends sgPostingIntegration {
    public class InvalidServiceResponseException extends Exception{}
    public class ErrorServiceResponseException extends Exception{}
    public class MissingPhotographException extends Exception{}
    public class MissingLayoutException extends Exception{}

    static Map<String,String> ConditionMap = new Map<String,String> {
        'Pre-Owned' => 'USED_EXCELLENT',
        'BNIB' => 'NEW'
    };
    
    Map<String,Object> ebayLocation;
    Map<String,Object> ebayProduct;
    Map<String,Object> ebayOffer;
    
    //********************************************************
    // Integration methods
    
    public override void SetPost() {
        //see if product exists
        // if not create it
        // if yes then 
        //   see if an offer exists
        System.debug('Start Post');
        ensureOffer();
        System.debug('End Post');
    }
    
    public override void SetVisible() {
        //see if the product exists
        // if not create it
        // if so update
        System.debug('Start Visible');
        ensureOffer();
        System.debug(ebayOffer);
        //see if the offer is already published
        if (ebayOffer.get('status') == 'UNPUBLISHED' || ebayOffer.get('status') == null) {
            publishOffer();
        }
        System.debug('End Visible');
    }

    public override void RemoveVisible() {
        //see if the product exists
        //if so then update
        //if not then done
        if (hasProduct()) {
            if (hasOffer()) {
            	//delete the offer
            	deleteOffer();
        	}
        }
    }
    
    public override void RemovePost() {
        super.RemovePost(); //not implemented
    }
    
    public override void DoUpdate() {
        //if we have a posted date then proceed
        System.debug('Start Update');
        if (ChannelPost.Posted_Date__c != null) {
            createProduct(); //the same endpoint is used to create and update
            if (ChannelPost.External_Id__c != null) {
                ebayOffer = updateOffer();
            }
            else {
                ebayOffer = createOffer();
            }
            
        }
        System.debug('End Update');
    }
     
    //********************************************************
    // Support methods
    
    void ensureOffer() {
        if (!hasProduct()) {
            createProduct();
        }
        if (!hasOffer()) {
            ebayOffer = createOffer();
        }
    }
    
    Boolean hasProduct() {
        if (ebayProduct == null) {
            ebayProduct = getProduct();
        }
        if (ebayProduct != null) {
            return true;
        }
        return false;
    }
    
    Map<String,Object> getProduct() {
        System.debug('Get Product');
        
        Blob resultBlob = this.ExecuteRequest('Get Product', null);
        Map<String,Object> result = handleResponse('Get Product', resultBlob, '25710');
        if (result != null) {
            System.debug('Get Product Done: Found');
            return result;
        }
        System.debug('Get Product Done: Not found');
        return null;
    }

    void createProduct() {
        System.debug('Create Product');
        
        //this call will not have any content
        // if there isn't an exception thrown then we're good
        Blob requestBlob = createProductPayload();
        Blob resultBlob = this.ExecuteRequest('Create Product', requestBlob);
        handleResponse('Create Product', resultBlob, 'empty');
        AddActivity('Created Product', '', 'Completed');
        
        System.debug('Create Product Done');
    }
    
    Blob createProductPayload() {
        return Blob.valueOf(
            '{' +
            '	"availability": {' +
            '		"shipToLocationAvailability": {' +
            '	    	"quantity": 1' +
            '	    }' +
            '	},' +
            '	"condition": "' + ConditionMap.get(ChannelPost.Product__r.Condition__c) + '",' +
            '	"product": {' +
            '	    "brand": "{!Product__r.Model__r.model_brand__c}",' +
            '	    "description": "' + getLayoutHtml() + '",' +
            '	    "imageUrls": [' +
            '			"' + getImageUrl() + '"' +
            '	    ],' +
            '	    "mpn": "{!Product__r.Model__r.Reference__c}",' +
            '	    "subtitle": "{!Product__r.Model__r.Name}",' +
            '	    "title": "{!Product__r.Model__r.Name}"' +
            '	},' +
            '	"sku": "{!Product__r.Inventory_ID__c}",' +
            '   "upc":["{!Product__r.Watch_Reference__c}"]' +
            '}'
        );
    }
    
    String getImageUrl() {
        Organization__c org = [SELECT Organization_Code__c FROM Organization__c WHERE Id =:ChannelPost.Organization__c LIMIT 1];
        List<Photograph__c> photos = [SELECT Url__c FROM Photograph__c WHERE Related_Product__c = :ChannelPost.Product__c AND Organization__c = :org.Id];
        if (photos.size() == 0) {
            throw new MissingPhotographException('No image for the "' + org.Organization_Code__c + '" Organization');
        }
        return photos[0].Url__c;
    }
    
    Boolean hasOffer() {
        if (ebayOffer == null) {
            ebayOffer = getOffer();
        }
        if (ebayOffer != null) {
            return true;
        }
        return false;
    }
    
    Map<String, Object> getOffer() {
        System.debug('Get Offer');
        
        Blob resultBlob = this.ExecuteRequest('Get Offer', null);
        Map<String,Object> result = handleResponse('Get Offer', resultBlob, '25713,25710');
        if (result == null) {
            System.debug('Get Offer Done: Not Found');
            return null;
        }
        System.debug(result);
        //extract the first offer record
        Map<String,Object> offer = (Map<String,Object>)((List<Object>)result.get('offers'))[0];
        this.ChannelPost.External_Id__c = (String)offer.get('offerId'); //update the ChannelPost so it can get saved in the controller
        this.Endpoint.UpdateContext('External_Id__c',this.ChannelPost.External_Id__c);
        
        System.debug('Get Offer Done: Found');
        return offer;
    }
    
    Map<String,Object> createOffer() {
        System.debug('Create Offer');
        
        Blob resultBlob = this.ExecuteRequest('Create Offer', createOfferPayload());
        Map<String,Object> offer = handleResponse('Create Offer', resultBlob, null);
        this.ChannelPost.External_Id__c = (String)offer.get('offerId'); //update the ChannelPost so it can get saved in the controller
        this.ChannelPost.External_Id_Name__c = 'offerId';
        this.Endpoint.UpdateContext('External_Id__c', this.ChannelPost.External_Id__c);
        this.Endpoint.UpdateContext('External_Id_Name__c', 'offerId');
        AddActivity('Created Offer', resultBlob.toString(), 'Completed');
        
        System.debug('Create Offer Done');
        return offer;
    }
    
    Map<String,Object> updateOffer() {
        System.debug('Update Offer');
        
        Blob resultBlob = this.ExecuteRequest('Update Offer', createOfferPayload());
        Map<String,Object> offer = handleResponse('Update Offer', resultBlob, 'empty');
        
        AddActivity('Updated Offer', '', 'Completed');
        
        System.debug('Update Offer Done');
        return offer;
    }

    Blob createOfferPayload() {
        return Blob.valueOf(
            '{' +
            '	"sku": "{!Product__r.Inventory_ID__c}",' +
            '   "merchantLocationKey": "' + getLocationKey() + '",' +
            '	"marketplaceId": "EBAY_US",' +
            '	"format": "FIXED_PRICE",' +
            '	"availableQuantity": 1,' +
            '	"categoryId": "{!Organization__r.Ebay_Category_Id__c}",' +
            '	"listingDescription": "{!Product__r.Model__c.Description__c}",' +
            '	"listingPolicies": {' +
            '		  "fulfillmentPolicyId": "{!Organization__r.Ebay_Fulfillment_Policy_Id__c}",' +
            '		  "paymentPolicyId": "{!Organization__r.Ebay_Payment_Policy_Id__c}",' +
            '		  "returnPolicyId": "{!Organization__r.Ebay_Return_Policy_Id__c}"' +
            '	},' +
            '	"pricingSummary": {' +
            '		  "price": {' +
            '		      "currency": "USD",' +
            '		      "value": "{!Product__r.MSP__c}"' +
            '		  }' +
            '	},' +
            '	"quantityLimitPerBuyer": 1' +
            '}'
        );
    }
    
    String getLocationKey() {
        if (ebayLocation == null) {
            ebayLocation = getLocation();
        }
        if (ebayLocation == null) {
            createLocation();
        }
        return this.ChannelPost.Organization__r.Id;
    }
    
    Map<String,Object> getLocation() {
        System.debug('Get Location');
        
        Blob resultBlob = this.ExecuteRequest('Get Location', null);
        Map<String,Object> result = handleResponse('Get Location', resultBlob, '25804');
        
        System.debug('Get Location Done');
        return result;
    }
    
    void createLocation() {
        System.debug('Create Location');
        
        Blob resultBlob = this.ExecuteRequest('Create Location', createLocationPayload());
        handleResponse('Create Location', resultBlob, 'empty');
        AddActivity('Created Location', '', 'Completed');
        
        System.debug('Create Location Done');
    }
    
    Blob createLocationPayload() {
        return Blob.valueOf(
            '{' +
            '    "location": {' +
            '        "address": {' +
            '            "addressLine1": "{!Organization__r.Address_Line_1__c}",' +
            '            "addressLine2": "{!Organization__r.Address_Line_2__c}",' +
            '            "city": "{!Organization__r.City__c}",' +
            '            "stateOrProvince": "{!Organization__r.State_or_Province__c}",' +
            '            "postalCode": "{!Organization__r.Postal_Code__c}",' +
            '            "country": "{!Organization__r.Country_Code__c}"' +
            '        }' +
            '    },' +
            '    "locationInstructions": "Items ship from here.",' +
            '    "name": "{!Organization__r.Id}",' +
            '    "merchantLocationStatus": "ENABLED",' +
            '    "locationTypes": [' +
            '        "WAREHOUSE"' +
            '    ]' +
            '}'
        );
    }
    
    void publishOffer() {
        System.debug('Publish Offer');
        
        Blob resultBlob = this.ExecuteRequest('Publish Offer', null);
        Map<String,Object> result = handleResponse('Publish Offer', resultBlob, null);
        
        this.ChannelPost.External_Id_2__c = (String)result.get('listingId'); //update the ChannelPost so it can get saved in the controller
        this.ChannelPost.External_Id_2_Name__c = 'listingId';
        this.Endpoint.UpdateContext('External_Id_2_Name__c', 'listingId');
        this.Endpoint.UpdateContext('External_Id_2__c', this.ChannelPost.External_Id_2__c);
        AddActivity('Published', resultBlob.toString(), 'Completed');
        
        System.debug('Publish Offer Done');
    }
    
    void deleteOffer() {
        System.debug('Delete Offer');
        
        Blob resultBlob = this.ExecuteRequest('Delete Offer', null);
        handleResponse('Delete Offer', resultBlob, 'empty');
        this.ChannelPost.External_Id__c = null; //update the ChannelPost so it can get saved in the controller
        this.ChannelPost.External_Id_2__c = null;
		this.ChannelPost.External_Id_Name__c = null;
        this.ChannelPost.External_Id_2_Name__c = null;
        
        AddActivity('Offer Deleted', '', 'Completed');
        
        System.debug('Delete Offer Done');
    }
    
    String getLayoutHtml() {
        PageReference pRef = getLayoutPage();
        Map<String,String> parameters = pRef.getParameters();
        parameters.put('id', ChannelPost.Id);
        
        Blob output = pRef.getContent();
        String outputStr = output.toString();
        outputStr = outputStr.replaceAll('\n|\r|\t', '');
        outputStr = outputStr.replaceAll('["]', '\'');
        outputStr = outputStr.replace('<!DOCTYPE HTML>', '');
        outputStr = outputStr.replaceAll('[<]meta[^>]+[\\/][>]', '');
		outputStr = outputStr.replaceAll('[<]script[^<]+[<][\\/]script[>]', '');
        
        return outputStr;
    }
    
    PageReference getLayoutPage() {
        if (ChannelPost.Organization__r.Organization_Code__c == 'Watchbox') {
            return Page.sgPostingIntegrationEbayWuwLayout;
        }
        else if (ChannelPost.Organization__r.Organization_Code__c == 'Goveberg') {
            return Page.sgPostingIntegrationEbayGovLayout;
        }
        throw new MissingLayoutException('There isn\'t a HTML layout for ' + ChannelPost.Organization__r.Organization_Code__c);
    }
    
    Map<String,Object> handleResponse(String serviceName, Blob resultBlob, String allowedErrors) {
        //
        if (resultBlob == null) {
            if (allowedErrors != null && allowedErrors.contains('empty')) {
                return null;
            }
            throw new InvalidServiceResponseException('Empty response for service ' + serviceName);
        }
        String resultStr = resultBlob.toString();
        if (resultStr == '') {
            return null;
        }
        //extract the data
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(resultStr);
        //check for and handle error response
        if (result.get('errors') != null) {
            System.debug('Errors Found');
            List<Object> errors = (List<Object>)result.get('errors');
            if (allowedErrors != null) {
                System.debug('Errors Errors Allowed: ' + allowedErrors);
                Map<String,Object> error = (Map<String,Object>)errors[0];
                System.debug(error.get('errorId'));
                if (allowedErrors.contains(String.valueOf(error.get('errorId')))) {
                    System.debug('Error OK');
                    return null;
                }
            }
            System.debug(errors);
            handleError(serviceName, result);
        }
        return result;
    }
    
    void handleError(String serviceName, Map<String, Object> result) {
        //log the error
        Map<String,Object> error = (Map<String,Object>)((List<Object>)result.get('errors'))[0];
        String msg = (String)error.get('message');
        if (error.containsKey('longMessage')) {
            msg += ': ' + (String)error.get('longMessage');
        }
        throw new ErrorServiceResponseException('Error Calling the "' + serviceName + '" Service: ' + msg);
    }
    
}