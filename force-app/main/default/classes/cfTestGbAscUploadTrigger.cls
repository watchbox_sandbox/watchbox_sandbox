@isTest
public class cfTestGbAscUploadTrigger {
    @isTest static void TestInsertGbAscUpload() {
        // Setup
		List<GBASCUPLOAD__c> sampleUploads = GetGbAscUploadSamples(); // Pending Batch Test
        
        System.debug(sampleUploads);
		
        List<Database.SaveResult> results = new List<Database.SaveResult>();
        
        // Perform
        Test.startTest();
        for (GBASCUPLOAD__c u :sampleUploads) {
        	results.add(Database.insert(u, true));
        }
        Test.stopTest();
        
        System.debug(results);
        
        // Verify Insertion
        System.assert(results[0].isSuccess());
        
        List<Id> resultIds = new List<Id>();
        
        for (Database.SaveResult sr :results) {
            resultIds.add(sr.getId());
        }
        
        Map<String, GBASCUPLOAD__c> uploadsMap = new Map<String, GBASCUPLOAD__c>();
            
        List<GBASCUPLOAD__c> uploads = [
            SELECT 
                Id, ASCInventoryID__c, Name, ASK__c, Cost__c, DateInventoried__c, Description__c, 
                IsActive__c, ItemNum__c, Location__c, LocationID__c, Product_Type__c, 
                ReferenceNum__c, VendorName__c, VendorNum__c, Watch_Brand__c, Model__c, Model__r.Reference__c 
            FROM GBASCUPLOAD__c
            WHERE Id IN :resultIds LIMIT 10
        ];
        
        for (GBASCUPLOAD__c upload :uploads) {
            uploadsMap.put(upload.ASCInventoryID__c, upload);
        }
        
        System.debug(uploadsMap.values());
        
        GBASCUPLOAD__c mainSample = uploadsMap.get('3296845'); // for general conditions
        GBASCUPLOAD__c preOwnedSample = uploadsMap.get('3279692'); // for Pre-Owned type
        GBASCUPLOAD__c closeoutSample = uploadsMap.get('2896728'); // for Closeout type
        
        // References and corresponding values should be resolved after insert
        System.assert(mainSample.Product_Type__c != null, 'Product Type assignment');
        System.assert(mainSample.Watch_Brand__c != null, 'Watch Brand assignment');
        System.assert(mainSample.Location__c != null, 'Location assignment');
        System.assert(mainSample.Model__c != null, 'Model assignment');
        // ---
        System.assertEquals(mainSample.Product_Type__c, 'Regular Goods', 'Product Type expected as [Regular Goods]');
        System.assertEquals(mainSample.Model__r.Reference__c, 'A10350', 'Model correct value');
    	
        System.assertEquals(preOwnedSample.Product_Type__c, 'Pre-Owned', 'Product Type on second sample expected as [Pre-Owned]');
        System.assertEquals(closeoutSample.Product_Type__c, 'Closeout', 'Product Type on third sample expected as [Closeout]');
        
		// ----------
        
        List<Product2> products = [
            select 
                Id, GB_Inventory_ID__c, GB_ASC_Group_Code__c, Name, Watch_Brand__r.Name, 
                Watch_Reference__c, Product_Type__c, Cost__c, ASK__c, GB_Date_Inventoried__c, 
                ISGBInventory__c, GB_Vendor_Name__c, IsActive, Model__c, Model__r.Reference__c 
            from Product2];
        
        System.debug(products);
        
        for (Product2 product :products) {
            
            if (uploadsMap.containsKey(product.GB_Inventory_ID__c)) {
            	GBASCUPLOAD__c upload = uploadsMap.get(product.GB_Inventory_ID__c);
                
                if (product.ISGBInventory__c) {
                    
                	System.assertEquals(true, product.IsActive, 'GB Inventory in upload should be set to active');
                    
                    // Govberg Inventories with ISGBInventory attribute should be updated (or inserted)
                    // Individual Check
                    if (product.GB_Inventory_ID__c == '3296845') {
                        //gbActiveProduct
                        System.assertNotEquals('DMS_API_TEST-3296845', product.Name, 'Name on GB-Active-Product should be updated');
                		System.assert(product.Model__c != null, 'Model should be linked for gbActiveProduct 3296845');
                    }
                    else if (product.GB_Inventory_ID__c == '3279692') {
                        //gbInactiveProduct
                        System.assertNotEquals('DMS_API_TEST-3279692', product.Name, 'Name on GB-Inactive-Product should be updated');
                    }
                    else if (product.GB_Inventory_ID__c == '2896728') {
                        //gbInactiveProduct
                        System.assertNotEquals('DMS_API_TEST-2896728', product.Name, 'Name on Non-GB-Inactive-Product that is uploaded should be updated and marked as GB-Product');
                    }
                }
                //else {
                //    if (product.GB_Inventory_ID__c == '2896728') {
                //        //nonGbActiveProduct
                //        System.assertEquals('DMS_API_TEST-2896728', product.Name, 'Name on Non-GB-Active-Product should NOT be updated');
                //    }
                //}
            }
            else {
                if (product.ISGBInventory__c) {
                    System.assertEquals(false, product.IsActive, 'GB Inventory not in upload should be set to inactive');
                }
                else {
                    // check all none gb inventory are still active
                    System.assertEquals(true, product.IsActive, 'Non-GB Inventory should remain active (untouched)');
                }
            }
        }
    }
    
    @testSetup static void Init() {
        // Setup test data
        insert GetSampleBrands();
        insert GetSampleLocations();
        insert GetSampleModels();
        insert GetSampleAscGroupCodes();
        GetSampleProducts();
        
        System.debug([
            SELECT 
            	Id, Name, ASC_Man_ID__c, Brand_Description__c, WUW_Brand_Description__c 
            FROM Brand_object__c WHERE ASC_Man_ID__c != NULL]);
        System.debug([
            SELECT 
            	Id, Name, LocNum__c, Segment__c, Telephone_Number__c, Manager_Name__c
            FROM Location__c LIMIT 10]);
        System.debug([
            SELECT 
            	Id, Name, Case_Size__c, Reference__c, model_brand__c, model_brand__r.ASC_Man_ID__c //, Case_Material__c
            FROM Model__c LIMIT 10]);
        System.debug([
            SELECT 
            	Id, ItemNum__c, Model__r.Name, Watch_Brand__r.ASC_Man_ID__c, 
                    Reference__c, Description__c, SHWatchID__c, Active_Inventory__c, VendorID__c
            FROM ASC_Group_Codes__c LIMIT 10]);
        System.debug([
            SELECT 
            	Id, GB_Inventory_ID__c, GB_ASC_Group_Code__c, Name, 
            	Watch_Brand__r.Name, Watch_Reference__c, Product_Type__c, 
            	Cost__c, ASK__c, GB_Date_Inventoried__c, ISGBInventory__c, 
            	GB_Vendor_Name__c, IsActive
            FROM Product2 LIMIT 10]);
    }
    
    static List<Brand_object__c> GetSampleBrands() {
        return new List<Brand_object__c> {
          new Brand_object__c (
              Name = 'Test Brand', ASC_Man_ID__c = 'BREI', Brand_Description__c = 'Testing brand'
          )
        };
    }
    static List<Location__c> GetSampleLocations() {
        return new List<Location__c> {
          new Location__c (
              Name = 'Govberg Office', LocNum__c = 11, Segment__c = 'Admin', 
              Telephone_Number__c = '(267) 443-2125', Manager_Name__c = 'Lauren Bodoff'
          )  
        };
    }
    static List<Model__c> GetSampleModels() {
        Brand_object__c brand = [SELECT Id FROM Brand_object__c WHERE ASC_Man_ID__c = 'BREI' LIMIT 1];
        return new List<Model__c>{
            new Model__c (
             	Name = 'Windrider Wings Automatic', //Case_Material__c = 'Stainless Steel', 
                Case_Size__c = 38, Reference__c = 'A10350', model_brand__c = brand.Id
            ),
            new Model__c (
             	Name = 'Zeitwerk 18K Yellow Gold 42mm', //Case_Material__c = 'Yellow Gold', 
                Case_Size__c = 42, Reference__c = '140.021'
            )
        };
    }
    static List<ASC_Group_Codes__c> GetSampleAscGroupCodes() {
        Model__c model = [SELECT Id, model_brand__c FROM Model__c WHERE model_brand__r.ASC_Man_ID__c = 'BREI' LIMIT 1];
        return new List<ASC_Group_Codes__c>{
            new ASC_Group_Codes__c(
                ItemNum__c = 'BREI107192', Model__c = model.Id, Watch_Brand__c = model.model_brand__c, 
            	Reference__c = 'AB01146B/M524', Description__c = 'SS CHRM44-GOVBERGblk/GRPH', 
            	SHWatchID__c = '46922', Active_Inventory__c = true, VendorID__c = '101085'
            )
        };
    }
    static List<Product2> GetSampleProducts() {
        List<Product2> items = new List<Product2>{
            new Product2 (
                GB_Inventory_ID__c = '3296845', Name = 'DMS_API_TEST-3296845', 
                ISGBInventory__c = true, IsActive = true),
            new Product2 (
                GB_Inventory_ID__c = '3279692', Name = 'DMS_API_TEST-3279692', 
                ISGBInventory__c = true, IsActive = false),
            new Product2 (
                GB_Inventory_ID__c = '2896728', Name = 'DMS_API_TEST-2896728', 
                ISGBInventory__c = false, IsActive = true),
            new Product2 (
                GB_Inventory_ID__c = '50000', Name = 'DMS_API_TEST-50000', 
                ISGBInventory__c = false, IsActive = true),
            new Product2 (
                GB_Inventory_ID__c = '50001', Name = 'DMS_API_TEST-50001', 
                ISGBInventory__c = true, IsActive = true)
        };
        Database.insert(items, false);
		
        /*
        List<Product2> lst = new List<Product2>();
        for (integer j = 0; j < 20; j++) {
            for (integer i = 0; i < 200; i++) {
                string v = string.valueOf(i);
                lst.add(new Product2 (
                    GB_Inventory_ID__c = v, Name = 'DMS_API_TEST-' + v, 
                    ISGBInventory__c = true, IsActive = true));
            }
            //system.debug('lst count: ' + lst.size());
            Database.insert(lst, false);
            lst.clear();
        }*/

        return items;
    }
    
    static List<GBASCUPLOAD__c> GetGbAscUploadSamples() {
        return new List<GBASCUPLOAD__c> {
            new GBASCUPLOAD__c (
                LocationID__c = 11, VendorNum__c = '101085', VendorName__c = 'BREITLING',
                ReferenceNum__c = 'A1332016/G698', ItemNum__c = 'BREI107192', ASCInventoryID__c = '3296845',
                Description__c = 'SS SO HERIT CHR-SLV/BLUE', Cost__c = 3093, DateInventoried__c = Date.valueOf('2016-05-25'),
                ASK__c = 5155),
            new GBASCUPLOAD__c (
                LocationID__c = 11, VendorNum__c = '101085', VendorName__c = 'BREITLING',
                ReferenceNum__c = 'A45320B9/C902', ItemNum__c = 'BREI309859', ASCInventoryID__c = '3279692',
                Description__c = 'SS GALACTIC44-METALLICBLU', Cost__c = 2820, DateInventoried__c = Date.valueOf('2015-12-17'),
                ASK__c = 4700),
            new GBASCUPLOAD__c(
                LocationID__c = 11, VendorNum__c = '102125', VendorName__c = 'BREITLING',
                ReferenceNum__c = 'AB01146B/M524', ItemNum__c = 'BREI107192', ASCInventoryID__c = '2896728',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68, DateInventoried__c = Date.valueOf('2013-11-08'),
                ASK__c = 8190),
            new GBASCUPLOAD__c(
                LocationID__c = 11, VendorNum__c = '101085', VendorName__c = 'BREITLING',
                ReferenceNum__c = 'AB01146B/M524', ItemNum__c = 'PANA307192', ASCInventoryID__c = '2896736',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68, DateInventoried__c = Date.valueOf('2013-11-08'),
                ASK__c = 8190),
            new GBASCUPLOAD__c(
                LocationID__c = 11, VendorNum__c = '101085', VendorName__c = 'BREITLING',
                ReferenceNum__c = 'AB01146B/M524', ItemNum__c = 'BREI107192', ASCInventoryID__c = '2900264',
                Description__c = 'SS CHRM44-GOVBERGblk/GRPH', Cost__c = 4309.68, DateInventoried__c = Date.valueOf('2013-11-08'),
                ASK__c = 8190)
        };
    }
}