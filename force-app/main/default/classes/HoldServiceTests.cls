@isTest
public class HoldServiceTests {
    static testmethod void extendHold_success_test() {
        DateTime start = Datetime.now();
        Holds__c hold = TestFactory.createHold(null, null);
        
        System.assertEquals(null, hold.Hold_Expiration__c);
        
        HoldService.extendHold(hold.Id, 1);
        
        Holds__c result = [SELECT Id, Hold_Expiration__c FROM Holds__c WHERE Id=:hold.Id];
        
        System.assert(result.Hold_Expiration__c > start);
    }
}