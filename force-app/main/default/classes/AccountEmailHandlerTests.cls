@isTest
public class AccountEmailHandlerTests {
    @testSetup
    static void setup() {
        TestFactory.createAccountEmailServiceSettings();
    }
    
    static testmethod void handleInboundEmail_success_test() {
        Account account = TestFactory.buildAccount();
        account.Email__c = 'test@test.com';
        insert account;
        
        Messaging.InboundEmail email = TestFactory.buildInboundEmail();
        Messaging.InboundEnvelope envelope = TestFactory.buildInboundEnvelope();
        
        Test.startTest();
        AccountEmailHandler handler = new AccountEmailHandler();
        handler.handleInboundEmail(email, envelope);        
        Test.stopTest();
        
        EmailMessage message = [SELECT Id, RelatedToId, HasAttachment FROM EmailMessage WHERE RelatedToId =: account.Id];
        
        System.assert(message.HasAttachment, 'Attachments not created');
    }
    
    static testmethod void handleInboundEmail_exception_test() {
     	Exception ex = null;        
        
        try {
            Account account = TestFactory.createAccount();
            
        	Messaging.InboundEmail email = TestFactory.buildInboundEmail();
        	Messaging.InboundEnvelope envelope = TestFactory.buildInboundEnvelope();
           
            Test.startTest();
            email.toAddresses = null;
            AccountEmailHandler handler = new AccountEmailHandler();
            handler.handleInboundEmail(email, envelope);        
            Test.stopTest();
        } catch (Exception e) {
            ex = e;
        }
        
        System.assertNotEquals(null, ex, 'Expected exception not thrown');
    }
    
    static testmethod void handleInboundEmail_newAccount_test() {       
        Messaging.InboundEmail email = TestFactory.buildInboundEmail();
        Messaging.InboundEnvelope envelope = TestFactory.buildInboundEnvelope();
        
        Test.startTest();        
        AccountEmailHandler handler = new AccountEmailHandler();
        handler.handleInboundEmail(email, envelope);        
        Test.stopTest();
        
        EmailMessage message = [SELECT Id, RelatedToId, HasAttachment FROM EmailMessage LIMIT 1];
        
        System.assert(message.HasAttachment, 'Attachments not created');
        System.assertNotEquals(null, message.RelatedToId, 'New Account was not created');
    }
    
    static testmethod void handleInboundEmail_getUserByEmail_test() {
        TestFactory.createSalesUser();
        Messaging.InboundEmail email = TestFactory.buildInboundEmail();
        Messaging.InboundEnvelope envelope = TestFactory.buildInboundEnvelope();
        
        Test.startTest();        
        AccountEmailHandler handler = new AccountEmailHandler();
        handler.handleInboundEmail(email, envelope);        
        Test.stopTest();
        
        Account account = [SELECT Id, OwnerId FROM Account];
        
        System.assertNotEquals(null, account.OwnerId, 'New Account was not assigned');
    }
}