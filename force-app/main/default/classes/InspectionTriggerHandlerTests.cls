@isTest
public class InspectionTriggerHandlerTests {
    static testmethod void beforeInsert_success_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Intake__c intake = TestFactory.createIntake(opp, wp);
        
        Product2 product = [SELECT Id, Inspection_Date__c FROM Product2 LIMIT 1];        
        
        System.assertEquals(null, product.Inspection_Date__c);        
        
        Inspection__c inspection = TestFactory.createInspection(opp, intake, wp);
        
        product = [SELECT Id, Inspection_Date__c FROM Product2 LIMIT 1];
        
        System.assertEquals(inspection.TimeInspected__c, product.Inspection_Date__c, 'Inspection date should be set');   
    }
    
    static testmethod void beforeInsert_existingInspectionDate_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Watch_Purchase__c wp = TestFactory.createWatchPurchase(opp);
        Intake__c intake = TestFactory.createIntake(opp, wp);
        
        Product2 product = [SELECT Id, Inspection_Date__c FROM Product2 LIMIT 1];        
        product.Inspection_Date__c = Datetime.now().addDays(-1);
        
        Inspection__c inspection = TestFactory.createInspection(opp, intake, wp);        
        product = [SELECT Id, Inspection_Date__c FROM Product2 LIMIT 1];
        
        System.assert(inspection.TimeInspected__c > product.Inspection_Date__c, 'Inspection date should not have been updated');  
    }
}