@RestResource(urlMapping='/BatchScan/*')
global class sgBatchScanController {
    global class Scans {
        public Batch_Scan__c CurrentScan {get; set;}
        public List<Batch_Scan__c> PreviousScans {get; set;}
        
    }
    
    private static string userLocation {
        get {
            return sgAuth.user.Location__c;
        }
    }
	
    private static Batch_Scan__c getBatch(string id) {
        List<string> productFields = new List<String>{'Id',
            'Inventory_ID__c',
            'Model__r.Name', 
            'Model__r.AWS_Image_Array__c', 
            'Ask__c', 
            'Cost__c',
            'MSP__c',
            'RFID__c',
            'Origination_Type__c', 
            'WUW_Owner__c',
            'Watch_Reference__c',
            'Model__r.model_brand__r.Name',
            'Deal__r.Salesperson__r.Name__c',
            'Last_Price_Change__c',
            'Posted_On_WUW__c',
            'Status__c',
            'Date_Created__c'
        };
            
        string productFieldsString = '';
        for (string s : productFields) {
            productFieldsString += 'InventoryItem__r.' + s + ', ';
        }
            
        string query = 
            'select Id, Name, Scan_Start__c, Location_Scan__c, Location_Scan__r.Name, Team_Member__r.Name__c,' +
            '(select ' + productFieldsString + 'Scanned__c, InventoryItem__c, Scan_Type__c ' +
            'from Batch_Details__r)' +
            'from Batch_Scan__c where';
        
        if (id != null) {
            query += ' Id = :id';
        } else {
            string location = userLocation;
            query += ' Location_Scan__c = :location and Scan_Completed__c = null';
        }
        
        query += ' limit 1';
        
        return Database.query(query);
    }
    
	@HttpGet
    global static Scans get() {
        sgAuth.check();
		
        Scans s = new Scans();
        
        try {
        	s.CurrentScan = getBatch(null);
        } catch (Exception e) {
            
        }
        
        s.PreviousScans = [
            select Id, Name, CreatedDate, LastModifiedDate, Scan_Start__c, 
            	Scan_Completed__c, Team_Member__r.Name__c, Location_Scan__r.Name
            from Batch_Scan__c 
            where Location_Scan__c = :userLocation and Scan_Completed__c != null limit 10
        ];
        
        return s;
    }
    
    @HttpPost
    global static Batch_Scan__c post() {
        sgAuth.check();
        
        Batch_Scan__c batch = new Batch_Scan__c (
        	Location_Scan__c = userLocation,
            Team_Member__c = sgAuth.user.Id,
            Scan_Start__c = system.now()
        );
        insert batch;
        
		List<Product2> products = [select Id from Product2 where Location__c = :userLocation and IsActive = true limit 2000];
        List<Batch_Detail__c> batchDetails = new List<Batch_Detail__c>();
        for (Product2 p : products) {
            batchDetails.add(new Batch_Detail__c(
            	Batch_Scan__c = batch.Id,
                InventoryItem__c = p.Id,
                Scan_Location__c = userLocation
            ));
        }        
        
        insert batchDetails;
        
		return getBatch(batch.Id);
    }
    
    @HttpPut
    global static void put(List<string> batchDetailIds, string scanType) {
        sgAuth.check();
        
        string query = 'select Id, Batch_Scan__c, Scanned__c, Scanned_By__c, Scan_Type__c from Batch_Detail__c where Id in :batchDetailIds';
        List<Batch_Detail__c> details = Database.query(query);
        
        for (Batch_Detail__c d : details) {
            d.Scanned__c = true;
            d.Scanned_By__c = sgAuth.user.Id;
            d.Scan_Type__c = scanType;
        }
        
        update details;
        
        //check if all details are now scanned
        List<Batch_Detail__c> unscanned = Database.query('select Id, Batch_Scan__c, Scanned__c from Batch_Detail__c where Scanned__c != true');
        
        if (unscanned.size() == 0) {
            //finishBatch(details.get(0).Batch_Scan__c);
        }
    }
    
    @HttpPatch
    global static boolean patch(string id) {
        sgAuth.check();
        
        finishBatch(id);
        
        return true;
    }
    
    private static void finishBatch(string batchId) {
        Batch_Scan__c batch = [select Id, Scan_Completed__c from Batch_Scan__c where Id = :batchId limit 1];

        batch.Scan_Completed__c = system.now();
        
        update batch;
    }
}