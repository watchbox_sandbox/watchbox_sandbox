public class OpportunityNewPageSelector {
    
    public static List<Watch_Sale__c> getWatchSaleList(Id oppId) {
         System.debug('inside  getWatchSaleList');
        List<Watch_Sale__c> watchSaleList = [SELECT   id
                                                    , Sale_Brand__c
                                                    , Inventory_ID__c
                                                    , Client_s_Offer__c
                                                    , Notes__c
                                                    , MSP__c
                                                    , Sale_Model__c
                                                    , Watch_Sale_Delta__c
                                                    , Opportunity__c
                                            FROM Watch_Sale__c
                                            WHERE Opportunity__c = : oppId];
        return watchSaleList;
    }
     public static List<Watch_Purchase__c> getOriginationList(Id oppId) {
         System.debug('inside  getOriginationList');
        List<Watch_Purchase__c> watchSaleList = [SELECT   id
                                                    , Origination_Model__c
                                                    , Client_s_Desired_Value__c
                                                    , Origination__c
                                                    , Origination_Value_Offered__c
                                                    , Attributes_differ_from_original_watch__c
                                                    , Notes__c
                                                    , Is_ref_currently_in_stock__c
                                                    , Watch_Origination_Delta__c
                                                    , New_watch_purchased_from_Govberg_WB__c
                                                    , Origination_Opportunity__c
                                            FROM Watch_Purchase__c
                                            WHERE Origination_Opportunity__c = : oppId];
        return watchSaleList;
    }
    public static id getDelayedManagerId(id userId) {
         System.debug('inside  getDelayedManagerId');
        List<User> recordTypeList = [SELECT id 
                                                , DelegatedApproverId  
                                           FROM   user
                                          WHERE id =:userId];
        if(recordTypeList != null && recordTypeList.size() > 0) {
            return recordTypeList[0].DelegatedApproverId;
            
        }else {
            return null;
        }
    }
    
    public static Set<id> getDelayedManagerIds(Set<id> userIds) {
         System.debug('inside  getDelayedManagerId');
        List<User> UserList = [SELECT id 
                                                , DelegatedApproverId  
                                           FROM   user
                                          WHERE id IN: userIds];
        Set<id> ManagerSet=new Set<id>();
        
        if(UserList != null && UserList.size() > 0) {
            for(User user:UserList){
               ManagerSet.add(user.DelegatedApproverId);
            }
              return ManagerSet;         
        }
        else{
            return null;
        }
    }
    
    public static String getManagerName(Id userId) {
         System.debug('inside  getManagerName');
        List<User> userList = [SELECT id 
                                                , Name  
                                           FROM   user
                                          WHERE id = :userId];
        if(userList != null && userList.size() > 0) {
            return userList[0].Name;
            
        }else {
            return null;
        }
    }
    
     public static Map<Id,String> getManagerNamesMap(Set<Id> userIds) {
         System.debug('inside  getManagerName');
        List<User> userList = [SELECT id,DelegatedApproverId 
                                                , Name  
                                           FROM   user
                                          WHERE id IN :userIds];
        Map<Id,String> UserMapToReturn =new Map<Id,String>();
        
        if(userList != null && userList.size() > 0) {
            for(user user:userList){
              
           UserMapToReturn.put(user.id,user.Name);
            
        }
            system.debug(':::::'+UserMapToReturn);
            return UserMapToReturn;
        }
         else{
            return null;
         }
    }
   
    public static id getOpp(id oppId) {
         System.debug('inside  getOpp');
        List<Opportunity> opportunityList = [SELECT id 
                                                , Test__c
                                           FROM   Opportunity
                                          WHERE id =:oppId];
        if(opportunityList != null && opportunityList.size() > 0) {
            return opportunityList[0].Test__c;
            
        }else {
            return null;
        }
    }

}