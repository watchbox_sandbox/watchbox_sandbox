public class cfGbAscUploadTrigger {
    List<Product2> productBatch;
    List<GBASCUPLOAD__c> uploadBatch;
    Map<Double, Id> locations;
    Map<String, Id> brands;
    Map<String, Id> groupCodes;
    List<GBASCUPLOAD__c> gbAscUploads;
    Set<String> inventoryIds;
    Set<String> vendorNumCloseoutDelimiters;
    Map<String, Product2> gbInventoriesToUpdate;
    final Integer DML_UPDATE_LIMIT = 10000;
    List<Product2> products;
    final Integer STATUSES_THRESHOLD = 15; // limit in seconds
    static boolean isLogPatchEnabled = false;
    
    // Retain the DateTime since Gb Inventories IsActive was clean between trigger on same batch
    static DateTime lastCleanUp;
    
    public cfGbAscUploadTrigger(List<GBASCUPLOAD__c> gbAscs) {
        Init(gbAscs);
        
        if (!StatusesUpdatedRecently()) {
        	CleanGbInventoryStatuses();
        }
        
        // Retrive used inventories not previously selected
        FillUnselectedGbInventories();
        
        RunProductLogPatch();
        UpdateGbAscItemsAndInventories();
    }
    
    private void Init(List<GBASCUPLOAD__c> gbAscs) {
        // Initialize
        System.debug('UserInfo: ' + UserInfo.getUserEmail());
        System.debug('Initializing cfGbAscUploadTrigger');
        gbAscUploads = gbAscs;
        productBatch = new List<Product2>();
        uploadBatch = new List<GBASCUPLOAD__c>();
        locations = new Map<Double, Id>();
        brands = new Map<String, Id>();
        groupCodes = new Map<String, Id>();
        inventoryIds = new Set<String>();
        gbInventoriesToUpdate = new Map<String, Product2>();
        
        // To be set on an SFObject for ease of changeability 
        vendorNumCloseoutDelimiters = new Set<String> {
            '102155', '102161', '102499', '102115', '102459', '102405', '102189', '102275', '102244', '102119', 
            '102197', '102156', '102254', '102130', '102120', '102519', '102520', '102293', '102533', '102067', 
            '102021', '102251', '102142', '102521', '102063', '102129', '102123', '102127', '102062', '102464', 
            '102125', '102335', '102108', '102538' }; 
            
        // Fill used Locations, Brands, ASC Group Codes and Inventories
        FillLists();
        
        System.debug('Locations: ' + JSON.serialize(locations));
    }
    
    private void RunProductLogPatch() {
        
        if (!Test.isRunningTest() && !isLogPatchEnabled) { // non-RESTful transaction
            System.debug('Running Product Log patch for current transaction...');
            isLogPatchEnabled = true;
            
            Team_Member__c loaderUser = null;
            
            try	{
                loaderUser = [SELECT Id, Token__c, Token_Expiration__c FROM Team_Member__c WHERE Work_Email__c = :UserInfo.getUserEmail() OR (First_Name__c =: UserInfo.getFirstName() AND Last_Name__c =: UserInfo.getLastName()) LIMIT 1];
            } catch (Exception e) {
                throw new sgException('Non-matching team member for current session user found!');
            }
            
            if (loaderUser.Token_Expiration__c == null || System.now().addSeconds(STATUSES_THRESHOLD) > loaderUser.Token_Expiration__c) {
                System.debug('Refreshing Data Loader Token...');
                loaderUser.Token__c = sgUtility.generateRandomString(128);
                loaderUser.Token_Expiration__c = System.now().addMinutes(5);
            }
            
            System.debug('Loader User: ' + loaderUser);
            update loaderUser;
            
            // Set sgAuth.user with linked Team_Member__c by Work_Email__c so sgLog won't crash on log creation when evaluating sgAuth.user.Id on sgLog line 39.
            sgAuth.check(loaderUser.Token__c);
        }
    }
    
    private void FillLists() {
        
        System.debug('Filling lists');
        
        Set<Decimal> locationIds = new Set<Decimal>();
        Set<String> manIds = new Set<String>();
        Set<String> itemNos = new Set<String>();
        
        for(GBASCUPLOAD__c im: gbAscUploads){

            if (im.ASCInventoryID__c != null) {
                inventoryIds.add(im.ASCInventoryID__c);
            }
            if (im.LocationID__c != null) {
                locationIds.add(im.LocationID__c);
            }
            if (im.ItemNum__c != null) {
                itemNos.add(im.ItemNum__c);
                manIds.add(im.ItemNum__c.left(4));
            }
        }
        
        List<Location__c> locs = [SELECT Id, LocNum__c FROM Location__c WHERE LocNum__c IN :locationIds];
        List<Brand_object__c> brds = [SELECT Id, ASC_Man_ID__c FROM Brand_object__c WHERE ASC_Man_ID__c IN :manIds];
        List<ASC_Group_Codes__c> grps = [SELECT ItemNum__c, Model__c FROM ASC_Group_Codes__c WHERE Model__c != null AND ItemNum__c IN :itemNos];
        
        for (Location__c loc :locs) {
            locations.put(loc.LocNum__c, loc.Id);
        }    
        for (Brand_object__c bnd :brds) {
            brands.put(bnd.ASC_Man_ID__c, bnd.Id);
        }
        for (ASC_Group_Codes__c gpcd :grps) {
            groupCodes.put(gpcd.ItemNum__c, gpcd.Model__c);
        }
    }
    
    private boolean StatusesUpdatedRecently() {
        // Check and return whether inventory status cleaned recently
        // Note: This is an alternative to not clean status while inserting batch of GbAscUploads (only on initial trigger)
        
        System.debug('Checking statuses');
        
        DateTime currentTime = System.now();        
        System.debug('Current Time: ' + currentTime);
        
        if (lastCleanUp == null) {
            List<Product2> lastCleanUpItems = [
                SELECT GB_Time_Status_Last_Modified__c 
                FROM Product2 
                WHERE ISGBInventory__c = TRUE AND GB_Time_Status_Last_Modified__c != null
                ORDER BY GB_Time_Status_Last_Modified__c DESC
                LIMIT 1
            ];
            
            if (lastCleanUpItems.size() > 0) {
            	lastCleanUp = lastCleanUpItems[0].GB_Time_Status_Last_Modified__c;
            }
        }
        
        if (lastCleanUp == null || currentTime > lastCleanUp.addSeconds(STATUSES_THRESHOLD)) {
            
            lastCleanUp = currentTime;
			
            System.debug('Gb Inventory statuses have NOT been updated recently');
            return false;
        }
        
        System.debug('Gb Inventory statuses updated RECENTLY');
        return true;
    }
    
    private void CleanGbInventoryStatuses() {
        // CF: On Product2, select all watches where "ISGBInventory = TRUE" and make "Active = FALSE"
        
        System.debug('Cleaning Gb Inventory Statuses');
        DateTime currentTime = System.now(); 
        
        products = [
            SELECT 
                Id, GB_Inventory_ID__c, GB_ASC_Group_Code__c, Name, Watch_Brand__r.Name, 
                Watch_Reference__c, Product_Type__c, Cost__c, ASK__c, GB_Date_Inventoried__c, 
                ISGBInventory__c, GB_Vendor_Name__c, IsActive, Model__c, Model__r.Reference__c, GB_Time_Status_Last_Modified__c
            FROM Product2 WHERE ISGBInventory__c = TRUE AND IsActive = TRUE];
        
        system.debug('Products Size: ' + products.size());
        
        for(Product2 p :products) {
            productBatch.add(p);
            
            p.IsActive = FALSE;
            p.GB_Time_Status_Last_Modified__c = currentTime;
            
            if(p.GB_Inventory_ID__c != null) {
                gbInventoriesToUpdate.put(p.GB_Inventory_ID__c, p);
            }
            if (productBatch.size() > DML_UPDATE_LIMIT) {
                //system.debug('updating products batch');
                Database.update(productBatch, false);
                productBatch.clear();
            }
        }
        if (productBatch.size() > 0) {
            //system.debug('updating remaining products batch');
            Database.update(productBatch, false);
            productBatch.clear();
        }
    }
    
    private void FillUnselectedGbInventories() {
        
        System.debug('Filling unselected Gb Inventories');
        
        products = [
            SELECT 
                Id, GB_Inventory_ID__c, GB_ASC_Group_Code__c, Name, Watch_Brand__r.Name, 
                Watch_Reference__c, Product_Type__c, Cost__c, ASK__c, GB_Date_Inventoried__c, 
                ISGBInventory__c, GB_Vendor_Name__c, IsActive, Model__c, Model__r.Reference__c 
            FROM Product2 WHERE GB_Inventory_ID__c IN :inventoryIds AND GB_Inventory_ID__c NOT IN :gbInventoriesToUpdate.keySet()];
        
        for(Product2 p :products) {
            gbInventoriesToUpdate.put(p.GB_Inventory_ID__c, p);
        }
        
        System.debug('gbInventoriesToUpdate: ' + gbInventoriesToUpdate);
    }
    
    private void UpdateGbAscItemsAndInventories() {
        // CF: On GBASCUPLOADS to figure out location compare LocationID to object 'Location' and then fill in with the value (this we will transfer to Products2)
        
        System.debug('Updating Gb Asc Items and Inventories');
        DateTime currentTime = System.now(); 
        
        for(GBASCUPLOAD__c im: gbAscUploads){
            uploadBatch.add(im);
            
            if (im.LocationID__c != null) {
                im.Location__c = locations.get(im.LocationID__c);
            }
            if (im.ItemNum__c != null) {
                String itemNo = im.ItemNum__c;
                String manId = itemNo.left(4);
                String inventoryId = im.ASCInventoryID__c;
                
                im.Watch_Brand__c = brands.get(manId);
                im.Model__c = groupCodes.get(itemNo);
                
                // CF: If the FIFTH character in ItemNum = '3' then the watch is a 'Pre-Owned'
                if (itemNo.left(5).endsWith('3')) {
                    im.Product_Type__c = 'Pre-Owned';
                }
                // CF: If VendorNum equals any of these (vendorNumCloseoutDelimiters) then the watch is a 'Closeout'
                else if (vendorNumCloseoutDelimiters.contains(im.VendorNum__c)) {
                    im.Product_Type__c = 'Closeout';
                }
                // CF: Any other watches are 'Regular Goods'
                else {
                    im.Product_Type__c = 'Regular Goods';
                }
                
                // Update Product
                Product2 gbProduct = null;
                
                if (gbInventoriesToUpdate.containsKey(inventoryId)) {
                    //System.debug('gbInventoriesToUpdate contains key(' + inventoryId + ')');
                    gbProduct = gbInventoriesToUpdate.get(inventoryId);
                    //System.debug('Product (Before): ' + gbProduct);
                }
                else {
                    //System.debug('gbInventoriesToUpdate does not contains key(' + inventoryId + ')');
                    gbProduct = new Product2();
                    gbProduct.GB_Inventory_ID__c = im.ASCInventoryID__c;
                    gbInventoriesToUpdate.put(inventoryId, gbProduct);
                }
                productBatch.add(gbProduct);
                                
                gbProduct.GB_ASC_Group_Code__c = itemNo;
                gbProduct.Name = im.Description__c;
                gbProduct.Watch_Brand__c = im.Watch_Brand__c;
                gbProduct.Watch_Reference__c = im.ReferenceNum__c;
                gbProduct.Product_Type__c = im.Product_Type__c;
                gbProduct.Location__c = im.Location__c;
                if (im.Cost__c != null) gbProduct.Cost__c = im.Cost__c; // [Using Patch] Null Exception thrown on update in sgProductLog -> sgLog -> log, Line 39
                if (im.ASK__c != null) gbProduct.ASK__c = im.ASK__c;
                gbProduct.GB_Date_Inventoried__c = im.DateInventoried__c;
                gbProduct.ISGBInventory__c = true;
                gbProduct.GB_Vendor_Name__c = im.VendorName__c;
                gbProduct.Model__c = im.Model__c;
                gbProduct.IsActive = true;
            	gbProduct.GB_Time_Status_Last_Modified__c = currentTime;
                
                //System.debug('Product (After): ' + gbProduct);
            }
            if (productBatch.size() > DML_UPDATE_LIMIT) {
                system.debug('upserting products batch');
                Database.upsert(productBatch, false);
                productBatch.clear();
            }
            if (uploadBatch.size() > DML_UPDATE_LIMIT) {
                system.debug('updating gbascuploads batch');
                Database.update(uploadBatch, false);
                uploadBatch.clear();
            }
        }
        if (productBatch.size() > 0) {
            system.debug('upserting remaining products batch');
            Database.upsert(productBatch, false);
            productBatch.clear();
        }
        if (uploadBatch.size() > 0) {
            system.debug('updating remaining gbascuploads batch');
            Database.update(uploadBatch, false);
            uploadBatch.clear();
        }
    }
}