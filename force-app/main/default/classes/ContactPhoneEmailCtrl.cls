public class ContactPhoneEmailCtrl{
    public Contact objCont {get;set;}
    public String strPhone {get;set;}
    public String strEmail {get;set;}
    public String strSecondaryEmail {get;set;}
    public String strMobile{get;set;}
    public Boolean userflag {get;set;}
    public Boolean onView {get;set;}
    public Boolean onEditCancel {get;set;}
    PUBLIC Boolean onEdit {get;set;}
    public ContactPhoneEmailCtrl(ApexPages.StandardController controller){
        strPhone = '';
        strEmail = '';
        onView = false;
        userflag = false;
        onEditCancel = false;
        onEdit = false;
        objCont = (Contact) controller.getRecord();
        List<Contact> lstCont = new List<Contact>([select id,phone,Email,Secondary_Email__c,
                                                    MobilePhone,OwnerId
                                                From Contact where id =: objCont.Id]);
        objCont = lstCont[0];
        if(objCont.ownerId != UserInfo.getUserId()){
            onEdit = true;
            
            if(objCont.Email != null){
                strEmail = '***********'+objCont.Email.right(4);
            }
            if(objCont.Secondary_Email__c != null){
                strSecondaryEmail = '***********'+objCont.Secondary_Email__c.right(4);
            }
           /* if(objCont.Phone != null){
                strPhone = '******'+objCont.Phone.right(4);
            }
            if(objCont.MobilePhone != null){
                strMobile= '******'+objCont.MobilePhone.right(4);
            }*/
        }else{
            userflag = true;            
            if(objCont.Email != null){
                strEmail = objCont.Email;
            }
            if(objCont.Secondary_Email__c != null){
                strSecondaryEmail = objCont.Secondary_Email__c;
            }
           /* 
           if(objCont.Phone != null){
                strPhone = objCont.Phone;
            }
           if(objCont.Phone != null){
                strPhone = objCont.Phone;
            }
            if(objCont.MobilePhone != null){
                strMobile= objCont.MobilePhone;
            }*/
        }
        system.debug('@@@onView '+onView );
    }
    
    
    public void EditPhone(){
        onView = true;
        onEditCancel = true;
        onEdit = true;
    }
    public void Save(){
        update objCont;
        
        if(objCont.Email != null){
            strEmail = objCont.Email;
        }
        if(objCont.Secondary_Email__c != null){
            strSecondaryEmail = objCont.Secondary_Email__c;
        }
       /* if(objCont.MobilePhone != null){
            strMobile= objCont.MobilePhone;
        }*/
        onView = false;
        userflag = true;
        onEditCancel = false;
        onEdit = false;  
    }
    public Void Cancel(){
        onView = false;
        userflag = false;
        onEditCancel = false;
        onEdit = false;      
    }
}