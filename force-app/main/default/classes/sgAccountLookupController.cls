@RestResource(urlMapping='/AccountLookup/*')
global class sgAccountLookupController {
	@HttpGet
    global static Map<string, string> get() {
        Map<string, string> results = new Map<string, string>();
        
        string name = '%' + string.escapeSingleQuotes(RestContext.request.params.get('name')) + '%';
        
        List<Account> accounts = [select Id, Name from Account where WUW_NC_Vendor_ID__c != null and Name like :name limit 25];
        
        for (Account a : accounts) {
            results.put(a.Id, a.Name);
        }
        
        return results;
    }
}