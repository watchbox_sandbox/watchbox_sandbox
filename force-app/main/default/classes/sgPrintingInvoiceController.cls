public class sgPrintingInvoiceController extends sgPrintingControllerBase {

	final String DEFAULT_TEMPLATE_TYPE = 'external';
    
    public Opportunity Deal {get;set;}
    public List<Watch_Sale__c> Watches {get;set;}
    public List<Watch_Purchase__c> Trades{get;set;}
    public List<Incoming_Payment__c> IncomingPayments{get;set;}
        
    public sgPrintingInvoiceController() {
        super();
        
        if (templateType == null) {
            templateType = DEFAULT_TEMPLATE_TYPE;
        }
        
        Deal = [
            SELECT Id,
            DealID__c,
            CloseDate,
            Owner.Name,
            Account.Name,
            ChargentSFA__Billing_Address__c,
            ChargentSFA__Billing_Address_Line_2__c,
            ChargentSFA__Billing_City__c,
            ChargentSFA__Billing_State__c,
            ChargentSFA__Billing_Zip__c,
            ChargentSFA__Billing_Country__c,
            ChargentSFA__Billing_Phone__c,
            ChargentSFA__Billing_Email__c,
            Customer_Type__c,
            Sale_Amount__c,
            Total_of_Trades__c,
            Sub_Total__c,
            Other_Fees__c,
            ChargentSFA__Shipping_Amount__c,
            Accessories__c,
            ChargentSFA__Tax__c,
            Surtax__c,
            Total__c,
            ChargentSFA__Transaction_Total__c,
            Amount_Due__c
            FROM Opportunity 
            WHERE Id = :parameters.get('id')
        ];
        Watches = [
            SELECT Name,
            Inventory_Item__r.Name,
            Retail__c,
            MSP__c,
            Cost__c,
            ASK__c,
            Sale_Amount__c
            FROM Watch_Sale__c
            WHERE Opportunity__c = :parameters.get('id')
        ];
        Trades = [
            SELECT Name,
            Description__c,
            Cost__c,
            MSP__c,
            Asking__c
            FROM Watch_Purchase__c
            WHERE Origination_Opportunity__c = :parameters.get('id')
        ];
        IncomingPayments = [
          SELECT Transaction_Number__c,
          Deal_Type__c,
          Payment_Type__c,
          Payment_Date__c,
          Payment_Amount__c,
          CreatedBy.Name
          FROM Incoming_Payment__c
          WHERE Opportunity__c = :parameters.get('id')
        ];
    }
    
    public Boolean getIsInternal() {
        return templateType == 'internal';
    }

}