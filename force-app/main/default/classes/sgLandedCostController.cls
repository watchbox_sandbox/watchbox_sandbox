@RestResource(urlMapping='/LandedCosts/*')
global class sgLandedCostController {
	@HttpGet
    global static List<Map<string, string>> get() {
        sgAuth.check();
        
        return sgSerializer.serializeFromQuery(
            'select Id, Filename__c, InventoryID__r.Inventory_ID__c, Expense_Type__c, Cost_Type__c, Expense_Description__c, Expense_Cost__c , Original_Cost__c, CreatedDate from Landed_Cost__c',
            RestContext.request.params
        );
    }
    
    @HttpPost
    global static string post(Landed_Cost__c lc) {
        sgAuth.requireRole('Admin,Operations,Watchmaker');
        
        try {
        	Product2 p = [select Id from Product2 where Inventory_ID__c = :lc.Inventory_ID__c limit 1];
            lc.Inventory_ID__c = null;
            lc.InventoryID__c = p.Id;
            
            lc.Team_Member__c = sgAuth.user.Id;
            
            insert lc;
            
            return lc.Id;
        } catch (Exception e) {
            return 'Could not find a product with inventory ID ' + lc.Inventory_ID__c;
        }
    }
}