public class AccountPhoneEmailCtrl{
    public Account objAcc {get;set;}
    public String strPhone {get;set;}
    public String strEmail {get;set;}
    public String strHomePhone {get;set;}
    public String strSecondaryEmail {get;set;}
    public String strMobile{get;set;}
    public Boolean userflag {get;set;}
    public Boolean onView {get;set;}
    public Boolean onEditCancel {get;set;}
    PUBLIC Boolean onEdit {get;set;}
    public AccountPhoneEmailCtrl(ApexPages.StandardController controller){
        strPhone = '';
        strEmail = '';
        onView = false;
        userflag = false;
        onEditCancel = false;
        onEdit = false;
        objAcc = (Account) controller.getRecord();
        List<Account> lstAcc = new List<Account>([select id,phone,Email__c,
                                                    Home_Phone__c,Secondary_Email__c ,Mobile__c,OwnerId
                                                From Account where id =: objAcc.Id]);
        objAcc = lstAcc[0];
        if(objAcc.ownerId != UserInfo.getUserId()){
            onEdit = true;
            if(objAcc.Phone != null){
                strPhone = '******'+objAcc.Phone.right(4);
            }
            if(objAcc.Email__c != null){
                strEmail = '***********'+objAcc.Email__c.right(4);
            }
            if(objAcc.Home_Phone__c != null){
                strHomePhone = '******'+objAcc.Home_Phone__c.right(4);
            }
            if(objAcc.Secondary_Email__c != null){
                strSecondaryEmail = '***********'+objAcc.Secondary_Email__c.right(4);
            }
            if(objAcc.Mobile__c != null){
                strMobile= '******'+objAcc.Mobile__c.right(4);
            }
        }else{
            userflag = true;
            if(objAcc.Phone != null){
                strPhone = objAcc.Phone;
            }
            if(objAcc.Email__c != null){
                strEmail = objAcc.Email__c;
            }
            if(objAcc.Home_Phone__c != null){
                strHomePhone = objAcc.Home_Phone__c;
            }
            if(objAcc.Secondary_Email__c != null){
                strSecondaryEmail = objAcc.Secondary_Email__c;
            }
            if(objAcc.Mobile__c != null){
                strMobile= objAcc.Mobile__c;
            }
        }
        system.debug('@@@onView '+onView );
    }
    
    
    public void EditPhone(){
        onView = true;
        onEditCancel = true;
        onEdit = true;
    }
    public void Save(){
        update objAcc;
        if(objAcc.Phone != null){
            strPhone = objAcc.Phone;
        }
        if(objAcc.Email__c != null){
            strEmail = objAcc.Email__c;
        }
        if(objAcc.Home_Phone__c != null){
            strHomePhone = objAcc.Home_Phone__c;
        }
        if(objAcc.Secondary_Email__c != null){
            strSecondaryEmail = objAcc.Secondary_Email__c;
        }
        if(objAcc.Mobile__c != null){
            strMobile= objAcc.Mobile__c;
        }
        onView = false;
        userflag = true;
        onEditCancel = false;
        onEdit = false;  
    }
    public Void Cancel(){
        onView = false;
        userflag = false;
        onEditCancel = false;
        onEdit = false;      
    }
}