@isTest
public class sgTestNoteController {
    static testMethod void post() {
        Product2 p = new Product2(
            Name = 'Test Product',
            Status__c = 'Available'
        );
        
        insert p;

        string body = 'test';        
        sgNoteController.post(p.Id, body);
        
        Note n = [select Id, Body from Note limit 1];
        
        system.assertEquals(body, n.Body);
    }
}