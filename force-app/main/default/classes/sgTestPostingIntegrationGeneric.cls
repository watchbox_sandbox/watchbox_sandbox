@isTest
public class sgTestPostingIntegrationGeneric {

    static testMethod void allMethods() {
        //arrange
        //
        sgPostingIntegrationGeneric generic = new sgPostingIntegrationGeneric();
        generic.ChannelPost = new ChannelPost__c (
        	Channel__c = 'Criteo'
        );
        insert generic.ChannelPost;
        
        //act
        //
        generic.SetPost();
        generic.SetVisible();
        generic.RemoveVisible();
        generic.RemovePost();
        generic.DoUpdate();
        
        //assert
        //
        System.assertEquals(5, generic.ActivityLog.size());
    }
    
}