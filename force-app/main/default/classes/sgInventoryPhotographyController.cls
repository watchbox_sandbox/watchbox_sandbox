@RestResource(urlMapping='/InventoryPhotography/*')
global class sgInventoryPhotographyController {

    global class InventoryPhotography {
        public List<Map<string, string>> products {get; set;}
        public List<PhotographyDto> photographs {get; set;}
		public List<OrganizationDto> organizations {get; set;}       
    }
    
	global class PhotographyDto {
        global String id {get;set;}
        global Decimal displayOrder {get;set;}
        global String productId {get;set;}
        global String url {get;set;}
        global String organization {get;set;}
    }
    
    global class OrganizationDto {
        global String id {get;set;}
        global String name {get;set;}
        global String watermarkUrl {get;set;}        
    }
    
    global class PhotographyPostResponse{
        string id;
        string url;
        string fileName;
    }
    
    @HttpGet
    global static InventoryPhotography get() {
        
        InventoryPhotography inventoryPhotography = new InventoryPhotography();
        
        Map<string, string> params = new Map<string, string> {
        	'Product__r.Status__c' => 'Photography' 
        };
        inventoryPhotography.products = sgIntakeData.AsProducts(params);
		
		List<string> productIds = new List<string>();
        
        for(Map<string, string> product : inventoryPhotography.products){
            productIds.add(product.get('Id'));
        }
        
        inventoryPhotography.photographs = new List<PhotographyDto>();
            
        PhotographyDto photographs = new PhotographyDto();
        
		List<Photograph__c> photos = [select Id,
                                      Url__c,
                                      Display_Order__c,
                                      Related_Product__c,
                                      Organization__r.Organization_Code__c
                                   from Photograph__c
                                     order by Display_Order__c];
                                    //where Related_Product__c in (:string.join(productIds, '", "'))];
                                    //where Related_Product__c in ('01t500000049ePIAAY')];

        for(Photograph__c photo : photos){
            PhotographyDto photoDto = new PhotographyDto();
            photoDto.id = photo.Id;
            photoDto.displayOrder = photo.Display_Order__c;
            photoDto.productId = photo.Related_Product__c;            
            photoDto.url = photo.Url__c;  
            photoDto.organization = photo.Organization__r.Organization_Code__c;
            inventoryPhotography.photographs.add(photoDto);
        }

        inventoryPhotography.organizations = new List<OrganizationDto>();
        
        List<Organization__c> organizations = [select Id,
                                      WatermarkUrl__c,
                                      Name
                                      from Organization__c];

        for(Organization__c organization : organizations){
            OrganizationDto orgDto = new OrganizationDto();
            orgDto.id = organization.Id;
            orgDto.watermarkUrl = organization.WatermarkUrl__c;
            orgDto.name = organization.Name;            
            inventoryPhotography.organizations.add(orgDto);
        }
		
        return inventoryPhotography;
    
    }
    
    @HttpPost
    global static PhotographyPostResponse post(string file, string productId, string fileName, string mimeType, string organizationId, string photoId) {
        
        Product2 product = [select Id, Name, AWS_Web_Images__c from Product2 where Id = :productId limit 1];
        
        sgs3 s3 = new sgs3();
        
        s3.upload(product, fileName, file, mimeType);
        
        string s3Url = sgs3.s3Url(product, fileName);
        
        Photograph__c photo = new Photograph__c();
        
        if (fileName == 'watermark.png') {
            try {
                Photograph__c photoToDelete = [select Id from Photograph__c where Related_Product__c = :productId and Organization__c = :organizationId];
                delete photoToDelete;
            } catch (Exception e) {}
        } 
        
        photo.Url__c = s3Url;
        photo.Related_Product__c = productId;
        photo.Display_Order__c = 0;
        photo.Organization__c = organizationId;


        // kludge: the next few lines are kind of a hack AWS_Web_Images__c needs to be kept
		// in sync with the Photograph__c object. AWS_Web_Images__c needs to go away  
		// but it is to expensive to remove it from the application at the moment
		// Photograph__c allows us to support raw and watermarked images in a clean way
        product.AWS_Web_Images__c = product.AWS_Web_Images__c == null ? s3Url : product.AWS_Web_Images__c + ',' + s3Url;
        
        if (photoId != null){
		    photo.Id = photoId;
        }
        upsert photo;

        update product;
        
        PhotographyPostResponse response =  new PhotographyPostResponse();
        response.url = s3Url;
        response.id = photo.id;
        response.fileName = fileName;
        return response;
    }
    
    @HttpDelete
    global static void remove(){
        
    	string productId = RestContext.request.params.get('productId');
        
        string photographId = RestContext.request.params.get('photographId');

		string url = RestContext.request.params.get('url');
        
        Photograph__c photo = [select Id from Photograph__c where id = :photographId limit 1];

        Product2 product = [select Id, Name, AWS_Web_Images__c from Product2 where Id = :productId limit 1];

        integer startIndex = product.AWS_Web_Images__c.indexOf(url);
        
        if (startIndex > 0){
            url = ',' + url;
        }else{
            url = url + ',';
        }

        product.AWS_Web_Images__c = product.AWS_Web_Images__c.replace(url, '');

        update product;
        
        delete photo;
    }
    
    @HttpPatch
    global static void patch(List<Photograph__c> photos, string urlCsv, string productId) {
        Product2 product = [select Id from Product2 where Id = :productId limit 1];
        
        update photos;
        
        product.AWS_Web_Images__c = urlCsv;
        
        update product;
    }
}