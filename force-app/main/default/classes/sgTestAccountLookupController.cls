@isTest
public class sgTestAccountLookupController {
    static testMethod void get() {
        Account a = new Account(
        	Name = 'Test Account',
            Last_Name__c = 'Test',
            WUW_NC_Vendor_ID__c = '1'
        );
        insert a;
        
        RestRequest req = new RestRequest();
        req.addParameter('name', 'test');
        RestContext.request = req;
        
        Map<string, string> results = sgAccountLookupController.get();
        
        system.assertEquals(1, results.keySet().size());
    }
}