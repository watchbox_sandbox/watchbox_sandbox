public with sharing class MagentoBrandSerializer {
    public static MagentoBrandDTO brandToDTO(Brand_object__c providedBrand) {
        // Re-fetch to get all of our required properties.
        Brand_object__c brand = providedBrand; 
        /*
        [
            SELECT
                Model__r.Model__Series__r.Brand_object__r.Id,
                Name,
                Brand_Description__c,
                URL_Slug__c,
                WUW_Website_Active__c,
                Brand_Hex__c
            FROM Brand_object__c
            WHERE Id = :providedBrand.Id
        ];
		*/
        
        // Create an empty DTO instance.
        MagentoBrandDTO bdto = new MagentoBrandDTO();

        // Fill er up.
        bdto.sfId = brand.Id;
        bdto.name = brand.Name;
        bdto.description = brand.Brand_Description__c;
        bdto.slug = brand.URL_Slug__c;
        bdto.active = true; //if there's a product, it's active //brand.WUW_Website_Active__c;
        bdto.hex = brand.Brand_Hex__c;

        return bdto;
    }

    public static String brandsToDTOJson(List<Brand_object__c> brands) {
        List<MagentoBrandDTO> bdtos = new List<MagentoBrandDTO>();

        for (Brand_object__c b: brands) {
            MagentoBrandDTO bdto = MagentoBrandSerializer.brandToDTO(b);
            bdtos.add(bdto);
        }

        Map<String, List<MagentoBrandDTO>> batchData = new Map<String, List<MagentoBrandDTO>>{
        	'brands' => bdtos
        };

        String dataString = JSON.serialize(batchData);

        return dataString;
    }
}