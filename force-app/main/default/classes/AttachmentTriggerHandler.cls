public class AttachmentTriggerHandler extends TriggerHandler {
    public override void afterInsert(){
        Set<Id>parentIds = new Set<Id>();
        List<Attachment> attachments = new List<Attachment>();
        for(Id attachmentId : trigger.newMap.keyset()){
            Attachment a = (Attachment)trigger.newMap.get(attachmentId);
            parentIds.add(a.ParentId);
            attachments.add(a);
        }
        
        //copyShipmentAttachments(parentIds, attachments);
    }
    
    public override void beforeInsert() {
        Map<Id, Account> accountUpdateMap = new Map<Id, Account>();
        Map<Id, Opportunity> opportunityUpdateMap = new Map<Id, Opportunity>();        
        Set<Id> objectIds = new Set<Id>();
        
        for (Attachment a : (List<Attachment>)Trigger.new) {            
            if (a.ParentId != null)
                objectIds.add(a.ParentId);
        }
        
        if (!objectIds.isEmpty()) {
            Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Last_Activity_Date__c FROM Account WHERE Id IN :objectIds]);                        
            
            if (!accountMap.isEmpty()) {
                for (Attachment a : (List<Attachment>)Trigger.new) {
                    if (a.ParentId != null) {
                        Account account = null;
                        //First check in existing list
                        account = accountUpdateMap.get(a.ParentId);
                        
                        //If not already incremented
                        if (account == null) {
                            account = accountMap.get(a.ParentId);
                                                        
                            if (account != null && account.Last_Activity_Date__c != System.today()) {
                                account.Last_Activity_Date__c = System.today(); 
                                accountUpdateMap.put(account.Id, account);
                            }                                 
                        }
                    }                    
                }
                
                //Update accounts
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
            
            Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([SELECT Id, Last_Activity_Date__c, Account.Id, Account.Last_Activity_Date__c 
                                                                            FROM Opportunity WHERE Id IN :objectIds]);
            accountUpdateMap = new Map<Id, Account>();
            
            if (!opportunityMap.isEmpty()) {
                for (Attachment a : (List<Attachment>)Trigger.new) {
                    if (a.ParentId != null) {
                        Opportunity opp = null;
                        //First check in existing list
                        opp = opportunityUpdateMap.get(a.ParentId);
                        
                        //If already in updates
                        if (opp == null) {
                            opp = opportunityMap.get(a.ParentId);
                            
                            if (opp != null && opp.Last_Activity_Date__c != System.today()) {
                                opp.Last_Activity_Date__c = System.today();                              
                                opportunityUpdateMap.put(opp.Id, opp);
                                
                                if (opp.Account != null && opp.Account.Last_Activity_Date__c != System.today()) {
                                    Account account = new Account
                                    (
                                        Id=opp.Account.Id, 
                                        Last_Activity_Date__c= System.today()
                                    );
                                    
                                    accountUpdateMap.put(account.Id, account);
                                }
                            }                                 
                        }
                    }                    
                }
                
                //Update opportunities
                if (!opportunityUpdateMap.isEmpty())
                    update opportunityUpdateMap.values();
                
                //Update accounts for opportunity touches
                if (!accountUpdateMap.isEmpty())
                    update accountUpdateMap.values();
            }
        }
    }

    //Method queries all shipments related to the attachment records
    //caught in the trigger.  Then makes a duplicate and attaches it
    //to the shipments related Deal record
    /*public static void copyShipmentAttachments(Set<Id> shipmentIds, List<Attachment> oldAttachments){
        List<zkfedex__shipment__c> shipments = [SELECT id, fedex_shipments_one_click__c, return__c, zkfedex__recipientOpportunity__c 
                                                FROM zkfedex__shipment__c 
                                                WHERE id IN :shipmentIds];        
        
        List<Attachment> newAttachments = new List<Attachment>();
        for(Attachment a : oldAttachments){
            for(zkfedex__shipment__c shipment : shipments){
                id parId;
                if(shipment.Return__c != null){
                    parId = shipment.Return__c;
                }else if(shipment.fedex_Shipments_One_Click__c != null){
                    parId = shipment.Fedex_Shipments_One_Click__c;
                }else if(shipment.zkfedex__RecipientOpportunity__c !=null){
                    parId = shipment.zkfedex__RecipientOpportunity__c;
                }
                if(a.ParentId == shipment.Id && (shipment.fedex_shipments_one_click__c != Null || shipment.zkfedex__recipientOpportunity__c != Null)){
                    Attachment copiedAttachment = new Attachment(name=a.name, body=a.body, ParentId = parId);
                    newAttachments.add(copiedAttachment);
                }
            }            
        }
        
        if(!newAttachments.isEmpty()){
            system.debug(newAttachments);
            insert newAttachments;
        }        
    }*/
}