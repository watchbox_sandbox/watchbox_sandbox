@isTest
public class OverrideWatchSaleExtensionTests {
    static testmethod void populateInventory_success_test() {
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.populateInventory();
        
        System.assertNotEquals(null, extension.DisplayProduct);
        System.assertEquals(1, extension.ImageURLs.size());
    }
    
    static testmethod void populateInventory_exception_test() {
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        ws.Inventory_Item__c = null;
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.populateInventory();
        
        System.assert(ApexPages.hasMessages()); 
        System.assertEquals(0, extension.ImageURLs.size());        
    }
    
    static testmethod void addToDeal_success_test() {
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.populateInventory();
        extension.addToDeal();
        
        ws = (Watch_Sale__c)sController.getRecord();
                        
        //Assert values were copied
        System.assertEquals(TestFactory.TEST_AMOUNT, ws.ASK__c);
        System.assertEquals(TestFactory.TEST_AMOUNT, ws.MSP__c);
        System.assertEquals(TestFactory.TEST_AMOUNT, ws.Cost__c);        
    }
    
    static testmethod void addToDeal_exception_test() {
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.populateInventory();
        
        //Create exception scenario
        extension.DisplayProduct = null;        
        extension.addToDeal();
        
        System.assert(ApexPages.hasMessages());  
    }
    
    static testmethod void createHold_success_test() {
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.populateInventory();
        Product2 product = [SELECT Id, AWS_Web_Images__c, Watch_Reference__c, ASK__c, Description, MSP__c, Name, Cost__c, Calculated_Status__c, Inventory_ID__c
                                FROM Product2 
                                WHERE Id =:ws.Inventory_Item__c FOR UPDATE];
        
        Holds__c hold = extension.createHold(ws, product);
        
        System.assertNotEquals(null, hold);
    }
    
    static testmethod void updateInventory_success_test() {
        Product2 product = TestFactory.createProduct();
        
        Watch_Sale__c ws = TestFactory.createWatchSale(null, null);
        
        ApexPages.StandardController sController = new ApexPages.StandardController(ws);
        OverrideWatchSaleExtension extension = new OverrideWatchSaleExtension(sController); 
        
        extension.updateInventory(product);
        
        Product2 result = [SELECT Id, Calculated_Status__c FROM Product2 WHERE Id =:product.Id];
        
        System.assertEquals('On Hold', result.Calculated_Status__c);                
    }
}