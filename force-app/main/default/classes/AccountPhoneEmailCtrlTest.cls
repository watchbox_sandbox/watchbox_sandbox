@isTest
public class AccountPhoneEmailCtrlTest {
    
     public static testmethod void TestAccountPhoneEmailCtrl() {
        Account Account = new Account();
        Account.Name = 'Test';
        Account.Email__c = 'test@test.com';
        Account.Secondary_Email__c = 'test123@test.com';
        Account.Phone = '12365412563';
        Account.Mobile__c = '12365412563';
        Account.Home_Phone__c = '12365412563';
        insert Account;
        ApexPages.StandardController controller = new ApexPages.StandardController(Account);
        AccountPhoneEmailCtrl cds = new AccountPhoneEmailCtrl(controller);
        cds.EditPhone();
        cds.Save();
        cds.Cancel();
     }    
}