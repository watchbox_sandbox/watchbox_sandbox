public with sharing class OpportunityWrapperr {
    Public String   FirstName { get; set; }
    Public String   FullName { get; set; } 
    Public String   LastName { get; set; } 
    Public String   Phone { get; set; } 
    Public String   Email { get; set; } 
    Public String   GAgclid { get; set; } 
    Public String   TransactionType { get; set; } 
    Public String   WhatBringsYouInToday { get; set; } 
    Public String   Opportunity { get; set; } 
    Public String   NumberofWatchInCollection { get; set; } 
    Public String   MatchedAccount { get; set; } 
    Public String   SignupWinAWatch { get; set; } 
    Public String   WatchImages { get; set; } 
    Public String   interestedinBrand { get; set; } 
    Public String   interestedinModel { get; set; } 
    Public String   InterestedinProduct { get; set; } 
    Public String   InterestedinProductListprice { get; set; } 
    Public String   InterestedinProductUrl { get; set; } 
    Public String   interestedinReferenceNumber { get; set; } 
    Public String   Boxes { get; set; } 
    Public String   Papers { get; set; } 
    Public String   GCLID { get; set; } 
    Public String   appcampaignid { get; set; } 
    Public String   EbayUserName { get; set; } 
    Public String   ircid { get; set; } 
    Public String   irtrackid { get; set; } 
    Public String   irpid { get; set; } 
    Public String   irclickid { get; set; } 
    Public String   IPCountry { get; set; } 
    Public String   PageID { get; set; } 
    Public String   IPAddress { get; set; } 
    Public String   PageURL { get; set; } 
    Public String   ReceiveUpdates { get; set; } 
    Public String   SubmissionDate { get; set; } 
    Public String   Comments { get; set; } 
    Public String   LeadSource { get; set; } 
    Public String   InitialDealDetails { get; set; } 
    Public String   AdditionalInfo { get; set; } 
    Public String   SellBrand { get; set; } 
    Public String   NameYourPriceASK { get; set; } 
    Public String   SellModel { get; set; } 
    Public String   LastServiceDate { get; set; } 
    Public String   NewWatchModel { get; set; } 
    Public String   BoxesandPapers { get; set; } 
    Public String   BoxOnly { get; set; }
    Public String   ModelNumber { get; set; } 
    Public String   TypeofMetal { get; set; } 
    Public String   SellWatchSerial { get; set; } 
    Public String   DialDescription { get; set; } 
    Public String   WatchReference { get; set; } 
    Public String   Images { get; set; } 
    Public String   WatchBrand { get; set; } 
    Public String   InventoryID { get; set; } 
    Public String   WatchModel { get; set; } 
    Public String   SKU { get; set; } 
    Public String   SalePrice { get; set; } 
    Public String   GACampaign { get; set; } 
    Public String   GASegment { get; set; } 
    Public String   GAContent { get; set; } 
    Public String   GATerm { get; set; } 
    Public String   GAMedium { get; set; } 
    Public String   GAVisits { get; set; } 
    Public String   ReferringURL { get; set; } 
    Public String   GaSource { get; set; }
    public String   CloseDate { get; set; }
    public String   StageName { get; set; }
    public String   Quantity { get; set; }
    public String   UnitPrice { get; set; }
    Public String   Accountid { get; set; }
    Public String   Contactid { get; set; }
    Public String   opportunityid { get; set; }
    Public Decimal  amount {get; set;}
    Public String typeOfTransaction {get; set;}
    
    
    //default values 
    public String MarketingBrand { get; set; }
    public String WhatWouldLike { get; set; }
    public String LandingPage { get; set; }
    Public String TypeofProduct { get; set; }
    public OpportunityWrapperr() {
    }
}