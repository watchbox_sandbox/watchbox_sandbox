@isTest
public class sgTestLandedCostController {
    static testMethod void get() {
        RestContext.request = new RestRequest();
        
        Landed_Cost__c lc = new Landed_Cost__c (
        );
        
        insert lc;
        
        List<Map<string, string>> costs = sgLandedCostController.get();
        
        system.assertEquals(1, costs.size());
    }
    
    static testMethod void post1() {
        Product2 p = sgDataSeeding.ProductsWithBrands().get(0);
        p.WUW_Inventory_ID__c = '1';
        insert p;
        
        Landed_Cost__c lc = new Landed_Cost__c (
            Inventory_ID__c = p.WUW_Inventory_ID__c
        );
        
        sgLandedCostController.post(lc);
        
        system.assertEquals(1, [select Id from Landed_Cost__c].size());
    }
    
    static testMethod void post2() {
        
        Landed_Cost__c lc = new Landed_Cost__c (
            Inventory_ID__c = '1'
        );
        
        string response = sgLandedCostController.post(lc);
            
        system.assert(response.contains('Could not find'));
    }
    
    static testmethod void upload() {
        sgUploadController.test();
        
        sgS3.test();
    }
}