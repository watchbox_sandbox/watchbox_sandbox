@RestResource(urlMapping='/Me')
global class sgMeController {
	@HttpGet
    global static Map<string, string> get() {
        sgAuth.check();
        
        Map<string, string> params = new Map<string, string> {
            'Token__c' => sgAuth.token
        };
            
        string query = 'select Id, First_Name__c, Last_Name__c, Work_Email__c, Profile_Picture__c, Group__r.Name, Role__r.Name, Organization__c, Location__c from Team_Member__c';
        
        List<Map<string, string>> tm = sgSerializer.serializeFromQuery(query, params);
        
        if (tm.size() == 1) {
        	return tm.get(0);
        }
        
        return null;
    }
}