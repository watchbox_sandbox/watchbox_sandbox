@isTest
private class MagentoTestModelSerializer {
    @testSetup
    static void init() {
        Model__c model1 = new Model__c(
            Name = 'Model One',
            URL_Slug__c = 'model-one'
        );

        insert model1;
    }

    @isTest
    public static void testModelToDTO() {
        Model__c modelOne = [
            SELECT
            	 Id,
                 Name,
                 URL_Slug__c,
                 Verified__c,
                 AWS_Image_Array__c,
                 Also_Known_As__c,
                 Also_Known_As_2__c,
                 Bezel__c,
                 Bracelet_Material__c,
                 Caliber_Details__c,
                 Case_Color__c,
                 Case_Material__c,
                 Case_Size__c,
                 Caseback__c,
                 Complications__c,
                 Description__c,
                 Dial_Color__c,
                 Dial_Type__c,
                 Gender__c,
                 Model_Write_Up__c,
                 Movement_Type__c,
                 Tonneau_Case_Size__c,
                 Watch_Case_Shape__c,
                 Watch_Display_Type__c,
                 Water_Resistance_Rating__c
            FROM Model__c WHERE Name = 'Model One'
        ];

        MagentoModelDTO mdto = MagentoModelSerializer.modelToDTO(modelOne);

        System.assertEquals(modelOne.Id, mdto.sfId);
        System.assertEquals('Model One', mdto.name);
        System.assertEquals('model-one', mdto.slug);
    }

    @isTest
    public static void testModelToDTOJson() {
        List<Model__c> model = [
            SELECT
            	 Id,
                 Name,
                 URL_Slug__c,
                 Verified__c,
                 AWS_Image_Array__c,
                 Also_Known_As__c,
                 Also_Known_As_2__c,
                 Bezel__c,
                 Bracelet_Material__c,
                 Caliber_Details__c,
                 Case_Color__c,
                 Case_Material__c,
                 Case_Size__c,
                 Caseback__c,
                 Complications__c,
                 Description__c,
                 Dial_Color__c,
                 Dial_Type__c,
                 Gender__c,
                 Model_Write_Up__c,
                 Movement_Type__c,
                 Tonneau_Case_Size__c,
                 Watch_Case_Shape__c,
                 Watch_Display_Type__c,
                 Water_Resistance_Rating__c
            FROM Model__c
        ];

        String jsonblob = MagentoModelSerializer.modelToDTOJson(model);

        MagentoModelDTOs mdtos = (MagentoModelDTOs)JSON.deserialize(jsonblob, MagentoModelDTOs.class);

        System.assertEquals('Model One', mdtos.models[0].name);
    }
}