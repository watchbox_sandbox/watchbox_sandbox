public class OriginationController {
    public Watch_Purchase__c origination {get;set;}
     public String queryString {get;set;}
    public String oppId {get;set;}
    private ApexPages.StandardController myStdController;
    
    public OriginationController(ApexPages.StandardController controller){
        
        origination=new Watch_Purchase__c();
		origination = (Watch_Purchase__c) controller.getRecord();
         oppId = apexpages.currentpage().getparameters().get('oppid');

        myStdController=controller;
        system.debug('oppid-----'+oppId);
        if(oppId!=null )
        origination.Origination_Opportunity__c=oppId.trim();
         PageReference thisPage = ApexPages.currentPage();
        if(thisPage <> NULL && thisPage.getUrl() <> NULL){
            List<String> url = thisPage.getUrl().split('\\?');
            if(url <> NULL && url.size()>2){
                queryString = url[1];
            }
        }

    }
    
    public pagereference save(){
        pagereference oppPage;
		system.debug('origination record::'+origination);
    
        try {
            Opportunity opp=[Select Id,Deal_Status__c,Loss_Reason__c from opportunity where id = :oppId Limit 1];
            System.debug(opp.Deal_Status__c);
            System.debug(opp.Loss_Reason__c);

            if (opp.Deal_Status__c=='Deal Lost' && opp.Loss_Reason__c == 'Couldn\'t Agree on Price' && (origination.WB_Trade_Offer__c == null|| origination.WB_Trade_Offer__c ==0.00)){
                system.debug('inside');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Trade offer is mandatory when Loss Reason is \'Couldn\'t Agree on Price\''));
                opppage=null;
            }else if(opp.Deal_Status__c=='Deal Lost' && opp.Loss_Reason__c == 'Couldn\'t Agree on Price' && (origination.Origination_Value_Offered__c == null||origination.Origination_Value_Offered__c ==0)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'WB Direct Offer is mandatory when Loss Reason is \'Couldn\'t Agree on Price\''));
                opppage=null;                
            }
            else{
                insert origination;
                oppPage= new pagereference('/'+oppId);
                oppPage.setRedirect(true);
            }
                
        }
        catch(System.DMLException e) {
            Integer numErrors = e.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDmlMessage(i)));
            }
        }
       
        return oppPage;
    }
    public pagereference cancelButton(){
        pagereference oppPage= new pagereference('/'+oppId);
        System.debug('oppPage----------'+oppPage);
        //oppPage = new ApexPages.StandardController(origination).view();
                oppPage.setRedirect(true);
        return oppPage;
    }
    
     @AuraEnabled
    public static List<String> originationBrandValues(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Watch_Purchase__c.Origination__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
      @AuraEnabled
    public static void addOriginationSale(Watch_Purchase__c originationObj,Id oppid){
         system.debug('inside save');
             originationObj.Origination_Opportunity__c=oppid;
                insert originationObj;
       
        
    }
     @AuraEnabled
    public static Opportunity getOpportunityName(Id oppId){
        Opportunity opp= [Select name from Opportunity where id=:oppId];
        return opp;
    }
     @AuraEnabled
    public static Opportunity getDealStatusLossReason(Id oppid){
        Opportunity opp= [Select Deal_Status__c,Loss_Reason__c from Opportunity where id=:oppid];
        return opp;
    }
}