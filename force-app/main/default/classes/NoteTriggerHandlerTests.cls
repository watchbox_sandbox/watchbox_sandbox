@isTest
public class NoteTriggerHandlerTests {
    static testmethod void afterInsert_accountSuccess_test() {
        Account account = TestFactory.createAccount();
        Note note = TestFactory.createNote(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =: account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');  
    }
    
    static testmethod void afterInsert_accountNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Note note = TestFactory.buildNote(contact.Id);
        insert note;
        
        System.assertNotEquals(null, note.Id);
    }
    
    static testmethod void afterInsert_multipleNotesSameAccount_test() {
        List<Note> notes = new List<Note>();
        Account account = TestFactory.createAccount();
        notes.add(TestFactory.buildNote(account.Id));
        notes.add(TestFactory.buildNote(account.Id));
        notes.add(TestFactory.buildNote(account.Id));
        
        insert notes;
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');           
    }
    
    static testmethod void afterInsert_existingAccountToggle_test() {
        Account account = TestFactory.buildAccount();
        insert account;
        
        Note note = TestFactory.createNote(account.Id);
        
        Account result = [SELECT Id, Last_Activity_Date__c FROM Account WHERE Id =:account.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');  
    }
    
    static testmethod void afterInsert_opportunitySuccess_test() {
        Opportunity opp = TestFactory.createOpportunity();
        Note note = TestFactory.createNote(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');  
    }
    
    static testmethod void afterInsert_opportunityNoResults_test() {
        Contact contact = TestFactory.createContact(null);
        Note note = TestFactory.buildNote(null);
        note.ParentId = contact.Id;
        insert note;
        
        System.assertNotEquals(null, note.Id);
    }
    
    static testmethod void afterInsert_multipleNotesSameOpportunity_test() {
        List<Note> notes = new List<Note>();
        Opportunity opp = TestFactory.createOpportunity();
        notes.add(TestFactory.buildNote(opp.Id));
        notes.add(TestFactory.buildNote(opp.Id));
        notes.add(TestFactory.buildNote(opp.Id));
        
        insert notes;
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');               
    }
    
    static testmethod void afterInsert_existingOpportunityToggle_test() {
        Opportunity opp = TestFactory.buildOpportunity();
        insert opp;
        
        Note note = TestFactory.createNote(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c FROM Opportunity WHERE Id =:opp.Id];
        
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today');  
    }
    
    static testmethod void afterInsert_opportunityAccount_test() {
        Account account = TestFactory.createAccount();
        Opportunity opp = TestFactory.createOpportunity(account);
        Note note = TestFactory.createNote(opp.Id);
        
        Opportunity result = [SELECT Id, Last_Activity_Date__c, Account.Last_Activity_Date__c FROM Opportunity WHERE Id =: opp.Id];
                
        System.assertEquals(System.today(), result.Last_Activity_Date__c, 'Last Activity Date should be today'); 
        System.assertEquals(System.today(), result.Account.Last_Activity_Date__c, 'Last Activity Date should be today'); 
    }
}