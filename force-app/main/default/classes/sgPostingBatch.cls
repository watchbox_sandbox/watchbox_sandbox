global class sgPostingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    public static Integer RunCount{get;set;} //for testing
    
    Map<ID, String> Executions = new Map<ID, String>(); //stores the Id of the executing object and the error message if there is one
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id FROM ChannelPost__c WHERE Is_Dirty__c = true AND Is_Processing__c = false');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //loop through the list of objects
        // mark each one as processing and the
        // execute the controller method
        for(sObject sObj : scope) {
            String err = null;
            try {
                ChannelPost__c channelPost = (ChannelPost__c)sObj;
                channelPost.Is_Processing__c = true; //mark as processing so we don't run multiple processes on the same channel
                channelPost.Is_Dirty__c = false; //unmark dirty so we don't process it again
                channelPost.Update__c = false; //remove the update flag so the next time we update this process will kick off
                update channelPost;
                //run the controller
                sgPostingIntegrationController.Execute(channelPost.Id);
            }
            catch(Exception ex) {
                err = ex.getMessage();
            }
            //add this entry to the executions map with 
            Executions.put(sObj.Id, err);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //TODO: Think about logging that the batch ran

        RunCount = Executions.size(); //set the static count for testing
    }
    
}