public class OppAccPhoneEmailCtrl{
    public Opportunity objOpp {get;set;}
    public String strPhone {get;set;}
    public String strEmail {get;set;}
    public Boolean userflag {get;set;}
    
    public OppAccPhoneEmailCtrl(ApexPages.StandardController controller){
        strPhone = '';
        strEmail = '';
        userflag = true;
        objOpp = (Opportunity) controller.getRecord();
        List<Opportunity> lstOpp = new List<Opportunity>([select id,Account_Email4templates__c,AccountPhone4Templates__c,Account.OwnerId
                                                From Opportunity where id =: objOpp.Id]);
        if(lstOpp.size() > 0){
            objOpp = lstOpp[0];
            if(objOpp.Account.ownerId != UserInfo.getUserId()){
                userflag = false;
                if(objOpp.AccountPhone4Templates__c != null){
                    strPhone = '******'+objOpp.AccountPhone4Templates__c.right(4);
                }
                if(objOpp.Account_Email4templates__c != null){
                    strEmail = '***********'+objOpp.Account_Email4templates__c.right(4);
                }
            }else{
                userflag = true;
                if(objOpp.AccountPhone4Templates__c != null){
                    strPhone = objOpp.AccountPhone4Templates__c;
                }
                if(objOpp.Account_Email4templates__c != null){
                    strEmail = objOpp.Account_Email4templates__c;
                }
            }
        }
        
    }
}