@isTest
private class MagentoTestSeriesSerializer {
    @testSetup
    static void init() {
        Brand_object__c brand = new Brand_object__c(
            Name = 'Test Brand'
        );

        insert brand;

        // Series One is active.
        Series__c series1 = new Series__c(
            Series_Description__c = 'Series One description.',
            Series_Quote__c = 'Series One quote.',
            URL_Slug__c = 'series-one',
            WUW_Website_Active__c = true,
            Watch_Brand__c = brand.Id,
            Name = 'Series One'
        );

        insert series1;

        // Series Two is inactive.
        Series__c series2 = new Series__c(
            Series_Description__c = 'Series Two description.',
            Series_Quote__c = 'Series Two quote.',
            URL_Slug__c = 'series-two',
            WUW_Website_Active__c = false,
            Watch_Brand__c = brand.Id,
            Name = 'Series Two'
        );

        insert series2;
    }
    
    static Series__c getSeries(string name){
        return [
            SELECT
            Id,
            Series_Description__c,
            Series_Quote__c,
            URL_Slug__c,
            WUW_Website_Active__c,
            Watch_Brand__c,
            Name,
            
            Watch_Brand__r.Id,
            Watch_Brand__r.Name,
            Watch_Brand__r.Brand_Description__c,
            Watch_Brand__r.URL_Slug__c,
            Watch_Brand__r.WUW_Website_Active__c,
            Watch_Brand__r.Brand_Hex__c
            FROM Series__c 
            WHERE Name = :name
        ];
    }

    @isTest
    public static void testSeriesToDTO() {
        Brand_object__c brand = [SELECT Id FROM Brand_object__c WHERE Name = 'Test Brand'];

        Series__c seriesOne = getSeries('Series One');
        
        MagentoSeriesDTO sdto = MagentoSeriesSerializer.seriesToDTO(seriesOne);

        System.assertEquals(seriesOne.Id, sdto.sfId);
        System.assertEquals('Series One', sdto.name);
        System.assertEquals('Series One description.', sdto.description);
        System.assertEquals('series-one', sdto.slug);
        System.assertEquals(true, sdto.active);
        System.assertEquals(brand.Id, sdto.brandId);
    }

    @isTest
    public static void testSeriesToDTOJson() {
        List<Series__c> series = new List<Series__c> {
            getSeries('Series One'),
            getSeries('Series Two')
        };

        String jsonblob = MagentoSeriesSerializer.seriesToDTOJson(series);

        MagentoSeriesDTOs sdtos = (MagentoSeriesDTOs)JSON.deserialize(jsonblob, MagentoSeriesDTOs.class);

        System.assertEquals('Series One', sdtos.series[0].name);
        System.assertEquals('Series Two', sdtos.series[1].name);
    }
}