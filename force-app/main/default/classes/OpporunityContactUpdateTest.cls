@isTest
public class OpporunityContactUpdateTest {
    

    public static testMethod void  opportunityContact(){
        Account account = TestFactory.buildAccount();
        account.First_Name__c = 'Test';
        account.Last_Name__c = 'User';
        account.Email__c = 'test@unittest.com';
        insert account;
        Contact contact=TestFactory.createContact(account);
        Opportunity opp=new  Opportunity ();
        opp.AccountId=account.Id;
        opp.Name = 'Test';
		opp.StageName = 'Test';
        opp.CloseDate=System.today().addMonths(1); 
       	insert opp;
        Opportunity opportunity = [SELECT Id, OwnerId,Contact_Record__c FROM Opportunity where id=: opp.id ];
        System.assertEquals(opportunity.Contact_Record__c,contact.id);
       
        
    }
    
    
    

}