public class LeadGenLogDecorator {
    public Account Account {get;set;}
    public Lead_Gen_Log__c Lead {get;set;}
    public Opportunity Opportunity {get;set;}
    public Lead LeadRecord {get;set;}
}