public class InspectionTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        updateGeneratedInventory();
    }
    
    private void updateGeneratedInventory() {
        Set<Id> intakeIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        List<Inspection__c> inspections = (List<Inspection__c>)Trigger.new;
        List<Product2> productsToUpdate = new List<Product2>();
        
        try {
            //Build inspection set
            for (Inspection__c insp : inspections) {
                if (insp.Intake__c != null) {
                    intakeIds.add(insp.Intake__c);
                }
            }
        
            if (intakeIds.isEmpty())
				return;
            
        	Map<Id, Intake__c> intakeMap = new Map<Id, Intake__c>([SELECT Id, Product__c FROM Intake__c WHERE Id IN :intakeIds]);
            
            //Build Product set
            for (Intake__c intake : intakeMap.values()) {
                if (intake.Product__c != null) {
                    productIds.add(intake.Product__c);
                }
            }
            
            if (productIds.isEmpty())
                return;
            
            Map<Id, Product2> productMap = new Map<Id, Product2>([SELECT Id, Inspection_Date__c FROM Product2 WHERE Id IN :productIds]);
            
            //Update products
            for (Inspection__c inspection : inspections) {
                if (inspection.Intake__c != null) {
                    Intake__c intake = intakeMap.get(inspection.Intake__c);
                    if (intake != null && intake.Product__c != null) {
                        Product2 product = productMap.get(intake.Product__c);
                        
                        //Only update if no value exists
                        if (product.Inspection_Date__c == null) {
                            product.Inspection_Date__c = inspection.TimeInspected__c;
                        	productsToUpdate.add(product);
                        }                        
                    }
                }                                
            }
            
            if (!productsToUpdate.isEmpty()) {
                Database.update(productsToUpdate, false);
            }            
        } catch (Exception e) {
            //Silent failure to allow operation to continue
            System.debug('Error updating generated inventory time inspected.  Inspection Id - ');
        }
    }
}