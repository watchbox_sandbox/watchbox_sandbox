public class sgCurrencyChange {
    public static void setDate(List<Product2> newProducts, Map<id, Product2> old) {
        List<string> currencyFields = new List<string>{
            'ASK__c',
            'Cost__c',
            'MSP__c',
            'Retail__c'
        };
            
        for (Product2 p : newProducts) {
            for (string field : currencyFields) {
                if (p.get(field) != old.get(p.Id).get(field)) {
                    p.Last_Currency_Change__c = Date.today();
                }
            }
        }
    }
}