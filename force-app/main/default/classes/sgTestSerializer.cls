@isTest
public class sgTestSerializer {
    static testMethod void serialize1() {
        Team_Member__c tmObj = sgDataSeeding.TeamMembers(1).get(0);
        insert tmObj;
        
        string query = 'select First_Name__c, Last_Name__c, Work_Email__c from Team_Member__c';
        
        List<string> fields = new List<string> {
            'First_Name__c',
			'Last_Name__c',
            'Work_Email__c'
        };
        
        List<Map<string,string>> serialized = sgSerializer.serializeFromQuery(query);
		
        Map<string,string> tm = new Map<string,string>();
        
        for (string f : fields) {
            tm.put(f, (string) tmObj.get(f));
        }
        
        for (string f : fields) {
            system.assertEquals(tm.get(f), serialized.get(0).get(f));
        }
    }
}