public class CreateWatchSaleAndWatchDetailBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    public  final string query ;
    public String log =''; 
    public  Map<id,String> successSaleDealIds=new Map<id,String>();
    public  Map<id,String> failSaleDealIds=new Map<id,String>();
    public  Map<id,String> successOriginationDealIds=new Map<id,String>();
    public  Map<id,String> failOriginationDealIds=new Map<id,String>();
    //Set<id> setIds = new Set<Id>();
    public CreateWatchSaleAndWatchDetailBatch() {
        
       /*setIds.add('0065000000iCa9ZAAS');
        setIds.add('0065000000iCY2CAAW');*/
        System.debug(query);
        /*query = 'SELECT id,Brand__c,Sell_Brand__c,Lead_Source__c,Interested_in_Product_Url__c,Model__c,interested_in_Brand__c,Interested_in_Product__c,Sell_Model__c,Initial_Deal_Details__c,Boxes_and_Papers__c,Name_Your_Price_ASK__c,SKU__c,Inventory_ID_Scratchpad__c,Sale_Price_Discussed__c FROM Opportunity '+ 
            ' WHERE (Brand__c != null OR Sell_Brand__c != null) AND (Id IN : setIds) LIMIT 2';*/
        query = 'SELECT id,Brand__c,Sell_Brand__c,Lead_Source__c,Interested_in_Product_Url__c,Model__c,interested_in_Brand__c,Interested_in_Product__c,Sell_Model__c,Initial_Deal_Details__c,Boxes_and_Papers__c,Name_Your_Price_ASK__c,SKU__c,Inventory_ID_Scratchpad__c,Sale_Price_Discussed__c FROM Opportunity '+ 
            ' WHERE Brand__c != null OR Sell_Brand__c != null';
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Opportunity> oppList){
        System.debug(oppList);
        List<Watch_Sale__c> watchSaleList = new List<Watch_Sale__c>();
        List<Watch_Purchase__c> watchOriginateList = new List<Watch_Purchase__c>();
        for(Opportunity opp : oppList){
            try{
            Watch_Sale__c ws ;
            Watch_Purchase__c wp;
            
            if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
               (opp.Lead_Source__c.equalsIgnoreCase('Govberg eBay') ||  opp.Lead_Source__c.equalsIgnoreCase('Watch Bang eBay')) ) {
				ws = new Watch_Sale__c();
                ws.Inventory_ID__c = opp.Inventory_ID_Scratchpad__c;
                ws.Sale_Brand__c = opp.Brand__c;
                ws.Client_s_Offer__c = opp.Sale_Price_Discussed__c;
           	} else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
			  (opp.Lead_Source__c.equalsIgnoreCase('Thewatchbox.com - Make An Offer'))) {
				ws = new Watch_Sale__c();
                ws.Sale_Brand__c = opp.Brand__c;
                ws.Sale_Model__c = opp.Model__c;
				if(opp.Initial_Deal_Details__c != null ) {
					List<String> lst = opp.Initial_Deal_Details__c.split('\\n');
					system.debug('list ----'+lst);
					List<String> values = new List<String>();
					for(String s :lst) {
                        if(s.containsIgnoreCase('Offering')) {
                            system.debug('s contains offering ');
                            if(s.contains(':-')) {
                                values = s.split(':-'); 
                                system.debug('values :-'+values);
                            }else {
                                values = s.split(':');
                                system.debug('values : '+values);
                            }
                            String var=values[1];
                            if(values.size() ==2) {
                                if(values[1].contains(',')) {
                                   var = values[1].remove(',');
                                }
                                if (var.contains('.')) {
                                   var= var.substringBefore('.');      
                                }
                                if(var.contains('$')){
                                     var= var.substringAfter('$');
                                }
                                  ws.Client_s_Offer__c =double.valueOf(var); 
                            }
                        } else if(s.containsIgnoreCase('Shipping to')) {
                            ws.Notes__c += ' '+s!=null?s:'';
                            system.debug(' final ws.Notes__c   : '+  ws.Notes__c  );
                        }else if(s.containsIgnoreCase('Product URL')) {
                            
                            ws.Notes__c =' '+ s!=null?s:'';
                            system.debug('  ws.Notes__c   : '+  ws.Notes__c  );
                        } else if(s.containsIgnoreCase('Payment Method')) {
                            ws.Notes__c += ' '+s!=null?s:'';
                            system.debug(' final ws.Notes__c   : '+  ws.Notes__c  );
                        }
                    }
                }       
                        //ws.Client_s_Offer__c =  opp.Initial_Deal_Details__c;        
            } else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null 
				&& (opp.Lead_Source__c.equalsIgnoreCase('Thewatchbox.com - Sell Your Watch'))){
                    wp = new Watch_Purchase__c();
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c = opp.Boxes_and_Papers__c!=null?'Box and Papers : '+opp.Boxes_and_Papers__c:'';
                    wp.Notes__c += ' '+opp.Initial_Deal_Details__c!=null?'  Initial Deal Details : '+opp.Initial_Deal_Details__c:'';
                    wp.Client_s_Desired_Value__c = opp.Name_Your_Price_ASK__c;
			} else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('TheWatchBox.com - Click&Buy'))) {
                    ws = new Watch_Sale__c();
                    ws.Inventory_ID__c = opp.SKU__c;
                    ws.Sale_Brand__c = opp.Brand__c;
                    ws.Sale_Model__c = opp.Model__c;
                    ws.Client_s_Offer__c = opp.Sale_Price_Discussed__c;
                    if(opp.Initial_Deal_Details__c != null ) {
                        List<String> lst = opp.Initial_Deal_Details__c.split('\\n');
                        List<String> values = new List<String>();
                        for(String s :lst){
                            if(s.containsIgnoreCase('Order Product Reference Number')) {
                                if(s.contains(':-')) {
                                    values = s.split(':-'); 
                                }else {
                                    values = s.split(':');
                                }
                                if(values.size() == 2) {
                                    ws.Sale_Model__c = values[1];
                                }
                            } 
                        }
                    }   
                } else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('Govbergwatches.com - TradeForm'))) {
                    ws = new Watch_Sale__c();
                    wp = new Watch_Purchase__c();
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c = opp.Boxes_and_Papers__c;
                    wp.Client_s_Desired_Value__c = opp.Name_Your_Price_ASK__c;
                    ws.Sale_Brand__c = opp.Brand__c;
                    ws.Sale_Model__c = opp.Model__c;
             } else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('Govbergwatches.com - Sell your watch'))) {
                    wp = new Watch_Purchase__c();
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c =  opp.Boxes_and_Papers__c!=null?'Box and Papers : '+opp.Boxes_and_Papers__c:'';
                    wp.Client_s_Desired_Value__c = opp.Name_Your_Price_ASK__c;
                    wp.Notes__c +=' '+opp.Initial_Deal_Details__c!=null?'   Initial Deal Details : '+opp.Initial_Deal_Details__c:'';
			} else if( opp.Lead_Source__c != null && opp.Lead_Source__c.equalsIgnoreCase('Govbergwatches.com - Price Request')) {
                system.debug('inside price request');
                ws = new Watch_Sale__c();
                ws.Sale_Brand__c = opp.interested_in_Brand__c;
                ws.Sale_Model__c = opp.Interested_in_Product__c;
                ws.Notes__c=opp.Interested_in_Product_Url__c;
            } else if(opp.Lead_Source__c != null && opp.Lead_Source__c.equalsIgnoreCase('Thewatchbox.com')) {
                ws = new Watch_Sale__c();
                ws.Inventory_ID__c = opp.SKU__c;
            }else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('eBay - luxwatchbuyer.com'))) {
                    wp = new Watch_Purchase__c();
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c = opp.Boxes_and_Papers__c!=null?'Box and Papers : '+opp.Boxes_and_Papers__c:'';
                    wp.Notes__c +=' '+opp.Initial_Deal_Details__c!=null?'  Initial Deal Details : '+opp.Initial_Deal_Details__c:'';
			}else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('Watchbox App - Sell Your Watch')
				||opp.Lead_Source__c.equalsIgnoreCase('Thewatchbox.com - Unbounce Page'))) {
                    wp = new Watch_Purchase__c();
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c = opp.Boxes_and_Papers__c;
                    wp.Client_s_Desired_Value__c = opp.Name_Your_Price_ASK__c;
			} else if((opp.Brand__c !=null || opp.Sell_Brand__c != null) && opp.Lead_Source__c != null &&
				(opp.Lead_Source__c.equalsIgnoreCase('Thewatchbox.com - Trade Your Watch'))) {
                    ws = new Watch_Sale__c();
                    wp = new Watch_Purchase__c();
                    ws.Sale_Brand__c = opp.Brand__c;
                    ws.Sale_Model__c = opp.Model__c;
                    wp.Origination__c = opp.Sell_Brand__c;
                    wp.Origination_Model__c = opp.Sell_Model__c;
                    wp.Notes__c = opp.Boxes_and_Papers__c;
                    if(opp.Initial_Deal_Details__c != null ) {
                        List<String> lst = opp.Initial_Deal_Details__c.split('\\n');
                        List<String> values = new List<String>();
                        for(String s :lst){
                            if(s.containsIgnoreCase('Watch They want to sell')) {
                                if(s.contains(':-')) {
                                    values = s.split(':-'); 
                                }else {
                                    values = s.split(':');
                                }
                                if(values.size() == 2) {
                                    ws.Sale_Model__c = values[1]; 
                                }
                                
                            } else {
                                ws.Notes__c +='  '+ s!=null?s:'';
                            }
                        }
                    }   
                }else {
                    if(opp.Brand__c !=null) {
                        ws = new Watch_Sale__c();
                        ws.Sale_Brand__c = opp.Brand__c;
                        //ws.Opportunity__c = opp.id;
                        //watchSaleList.add(ws);
                    }
                    if(opp.Sell_Brand__c != null) {
                        wp = new Watch_Purchase__c();
                        wp.Origination__c = opp.Sell_Brand__c;
                        
                    } 
                }
            if(ws != null) {
                ws.Opportunity__c = opp.id;
                watchSaleList.add(ws);
                
            }
            if(wp != null) {
                wp.Origination_Opportunity__c = opp.id;
                watchOriginateList.add(wp); 
            }
            }
            catch(Exception e){
                system.debug('Error occured: '+e.getMessage()+'  '+e.getLineNumber());
                
        }
         
    }
        
        
        if(watchSaleList.size() > 0) {
            Database.SaveResult[] saleList =Database.insert(watchSaleList,false) ;
            for (Integer i = 0; i < saleList.size(); i++){
                if (!saleList[i].isSuccess()){
					failSaleDealIds.put(watchSaleList[i].Opportunity__c,saleList[i].getErrors()[0].getMessage());
                    this.log +='Sale fail deal ids:::'+ failSaleDealIds+'\n Error in sale list: ' + watchSaleList[i].name +'sale id ::'+watchSaleList[i].Opportunity__c+ '. Error msg='+
                        saleList[i].getErrors()[0].getMessage();
                }else{
                    successSaleDealIds.put(watchSaleList[i].Opportunity__c,'Success[Sale]') ;
                }
            }
            System.debug('failSaleDealIds ==>'+failSaleDealIds);
            System.debug('successSaleDealIds ==>'+successSaleDealIds);
        }
        if(watchOriginateList.size() > 0) {
            Database.SaveResult[] originationList =Database.insert(watchOriginateList,false);
             for (Integer i = 0; i < originationList.size(); i++){
                if (!originationList[i].isSuccess()){
                    failOriginationDealIds.put(watchOriginateList[i].Origination_Opportunity__c,originationList[i].getErrors()[0].getMessage()) ;
                    this.log += failOriginationDealIds+'\n Error in Origination list: ' + watchOriginateList[i].name + 'Deal id ::'+watchOriginateList[i].Origination_Opportunity__c+'. Error msg='+
                        originationList[i].getErrors()[0].getMessage();
                }
                 else{
                      successOriginationDealIds.put(watchOriginateList[i].Origination_Opportunity__c,'Success[Origination]') ;
                 }
            }
            System.debug('failOriginationDealIds ==>'+failOriginationDealIds);
            System.debug('successOriginationDealIds ==>'+successOriginationDealIds);
        }
         
    }
    public void finish(Database.BatchableContext BC){
        String failsell='';
        String failOrig='';
        for (String str: failSaleDealIds.keySet()){
            failsell+=str+failSaleDealIds.get(str);
        }
        for (String str: failOriginationDealIds.keySet()){
            failOrig+=str+failOriginationDealIds.get(str);
        }
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { 'moktika.sharma@maantic.com','kirti.rathod@thinqloud.com' };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Watch sale AND Watch Detail Result';

        
        message.plainTextBody = 'failsell ==>'+failsell +' '+'failOrig ==>'+failOrig+ ''+'successSaleDealIds ==>'+successSaleDealIds +' '+'successOriginationDealIds ==>'+successOriginationDealIds+ '';
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
            
    }
    
    
    
}