public class RankingResult {
    public integer Rank {get;set;}
    public string Name {get;set;}
    public decimal Score {get;set;}
    public string DisplayScore {
        get {
        	string grade;
        
            if (this.Score == null || this.Score <= 0.20) {
                grade = 'F';
            } else if (this.Score >= 0.70) {
                grade = 'A';
            } else if (this.Score >= 0.50) {
                grade = 'B';
            } else if (this.Score >= 0.30) {
                grade = 'C';
            } else if (this.Score >= 0.20) {
                grade = 'D';
            }   
            
            string sc = (this.Score != null) ? string.valueOf(this.Score) : '0.00';
            
            return grade + ' - (' + sc + ')';
        } 
            set;}
}