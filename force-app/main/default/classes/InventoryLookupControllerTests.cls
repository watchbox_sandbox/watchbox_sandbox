@isTest
public class InventoryLookupControllerTests {
    static testmethod void searchProducts_success_test() {
        Product2 product = TestFactory.createProduct();

        Product2 result = [SELECT Id FROM Product2 WHERE Name=:product.Name];
        
        System.assertNotEquals(null, result);
        
        Id[] fixedSearchResults = new Id[]{result.Id};
        Test.setFixedSearchResults(fixedSearchResults);
        
        InventoryLookupController ctrl = new InventoryLookupController();
        ctrl.SearchTerm = product.Name;
        ctrl.searchProducts();
        
        System.assertEquals(1, ctrl.Products.size());
    }

    static testmethod void searchProducts_exception_test() {
        PageReference lookupPage = Page.InventoryLookup;
		Test.setCurrentPage(lookupPage);
        
        //Creating exception scenario
        InventoryLookupController ctrl = new InventoryLookupController();
        ctrl.SearchTerm = null;
        ctrl.searchProducts();
        
        //Assert exception was thrown
        System.assert(ApexPages.hasMessages());
    }
}