global class sgPostingBatchScheduled implements Schedulable {

    global void execute(SchedulableContext sc) {
        sgPostingBatch batch = new sgPostingBatch();
        database.executebatch(batch, 1);
    }
    
}