global class NoteBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id FROM Account where IsNoteAttached__c!='Y' limit 50000000  ]);
       
    }
    
    global void execute(Database.BatchableContext BC, List<Account> listAccount){
         system.debug('---list of Account----'+listAccount);
      
        Map<Id, Account> accountUpdateMap = new Map<Id, Account>();
        Set<Id> objectIds = new Set<Id>();
        try{
            for (Account acc : listAccount ) {            
                    objectIds.add(acc.Id);
                	accountUpdateMap.put(acc.id,acc);
            }
            
            if (!objectIds.isEmpty()) {
                Map<Id, Note> noteMap = new Map<Id, Note>();                        
                for(Note noteObject: [SELECT id,parentId FROM Note WHERE parentId IN :objectIds]){
                    noteMap.put(noteObject.ParentId,noteObject);
                }
                
                 system.debug('---noteMap----'+noteMap);
                    for (Account acc : listAccount) {
                    		Account account = null;
                            if(noteMap!=null && noteMap.get(acc.id)!=null ){
                                Note note=noteMap.get(acc.id);
                                account=accountUpdateMap.get(note.ParentId);
                                   if (account != null){
                                       account.IsNoteAttached__c='Y';
                                        accountUpdateMap.put(account.Id, account);
                                   }
                            }
                            else{
								account=accountUpdateMap.get(acc.id);
                                  account.IsNoteAttached__c='N';
                                   accountUpdateMap.put(account.Id, account);
                            }
                               
                     }             
          }
                    
                    //Update accounts
                    if (!accountUpdateMap.isEmpty())
                        update accountUpdateMap.values();
         
            
        }catch (Exception e) {
            System.debug('Error during NoteBatch - ' + e.getMessage() + ' - ' + e.getStackTraceString()+ ' - '+e.getLineNumber());
            throw e;
        }   
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}