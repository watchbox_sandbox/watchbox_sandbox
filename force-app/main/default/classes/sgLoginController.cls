@RestResource(urlMapping='/Login')
global class sgLoginController {
    @HttpPost
    global static Map<string,string> login(string email, string password) {
        try {
            Team_Member__c u = [select Id, Work_Email__c, Salt__c, Password__c, Token__c, Token_Expiration__c from Team_Member__c where Work_Email__c = :email];
            
            string hashed = sgAuth.hashPassword(password, u.Salt__c);
            
            if (hashed != u.Password__c) {
                throw new sgException('Incorrect password');
            }
            
            u.Token__c = sgUtility.generateRandomString(128);
            u.Token_Expiration__c = System.now().addDays(14);
            
            update u;

            return new Map<string, string>{
                'Token' => u.Token__c,
                'Expiration' => String.valueOf(u.Token_Expiration__c.getTime())
            };
        } catch(Exception e) {
            throw new sgException('Your email address or password is incorrect');
        }
        
        return null;
    }
}