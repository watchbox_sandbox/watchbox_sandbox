@isTest
public class sgTestSearchConroller {
    static void setup(string param) {
        insert sgDataSeeding.Brands();
        insert sgDataSeeding.Models();
        
        
        Team_Member__c tm = sgDataSeeding.TeamMembers(1).get(0);
        tm.Token__c = 'token';
        tm.Token_Expiration__c = system.now().addDays(1);
        insert tm;
            
        RestRequest req = new RestRequest();
        req.params.put('sgtoken', 'token');
        req.params.put('query', param);
        RestContext.request = req;
        
        
    }
    
    static testMethod void search1() {
        setup('1');
		system.debug([select name, verified__c from model__c]);        
        sgSearchResult results = sgSearchController.get();
        
        system.assertEquals(1, results.Brands.size());
        system.assertEquals(5, results.Models.size());
    }
    
    static testMethod void search2() {
        setup('Test');
	        
        sgSearchResult results = sgSearchController.get();
		
        system.assertEquals(1, results.Users.size());
    }
}