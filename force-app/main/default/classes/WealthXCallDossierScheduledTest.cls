@isTest
public class WealthXCallDossierScheduledTest {
    @testSetup
    static void setup() {
        WealthXIndexSettings__c wxIndex = new WealthXIndexSettings__c();
        wxIndex.Name = 'WealthXIndex';
        wxIndex.Start_Index__c = '1';
        wxIndex.End_Index__c = '20';
        wxIndex.No_of_Records__c = '20';
        wxIndex.Max_Batch_Size__c = '1';
        wxIndex.Max_Index_Size__c  = '20';
        insert wxIndex;
    }
    public static testMethod void callBatchWealthXCallDossier(){
        
        system.assertequals(1,[SELECT count() FROM WealthXIndexSettings__c]);
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            WealthXCallDossierScheduled objWealthXCallDossierScheduled = new WealthXCallDossierScheduled(); 
            objWealthXCallDossierScheduled.execute(null);
        Test.stopTest();
    }
}