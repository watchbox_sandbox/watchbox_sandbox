@isTest
public class sgTestPostingIntegrationController {

    static testmethod void construct() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', false, false, null, null, null, false);
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        
        //act
        //
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //assert
        //
        System.assert(controller.Worker != null);
        System.assert(controller.ChannelPost != null);
    }
    
    static testmethod void execute_withPost_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', true, false, null, null, null, false);
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: post', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_withVisible_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', true, true, Date.today(), Date.today(), null, false);
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: visible', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_withUpdate_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', true, true, Date.today(), Date.today(), Date.today(), true);
        
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: update', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_withRemoveVisible_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', true, false, Date.today(), Date.today(), Date.today(), false);
        
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: unvisible', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_withRemovePost_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', false, false, Date.today(), Date.today(), null, false);
        
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: unpost', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_withRemovePostVisible_success() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', false, false, Date.today(), Date.today(), Date.today(), false);
        
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals('Performed the following action: unpost,unvisible', task.Description);
        System.assertEquals(false, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_onlyVisible_failure() {
        //arrange
        //
        createServiceSettings('Facebook');
        sgClassResolver resolver = (sgClassResolver)Test.createStub(sgClassResolver.class, new MockClassResolver());
        ChannelPost__c channelPost = createChannelPost('Facebook', false, true, null, null, null, false);
        sgEndpoint endpoint = (sgEndpoint)Test.createStub(sgEndpoint.class, new MockEndpoint());
        sgPostingIntegrationController controller = new sgPostingIntegrationController(channelPost, resolver, endpoint);
        
        //act
        //
        controller.Execute();
        
        //assert
        //
        Task task = [SELECT Subject, Description FROM Task WHERE WhatId = :channelPost.Id];
        System.assertEquals(true, task.Description.contains('Failed to perform the action: invalid'));
        System.assertEquals(true, channelPost.Is_Processing__c);
    }
    
    static testmethod void execute_static() {
        //arrange
        //
        createServiceSettings('Ebay');
        ChannelPost__c channelPost = createChannelPost('Ebay', true, true, Date.today(), null, null, false);
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(400, null, null));
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('Ebay', env, 'token');
        
        //act
        //
        Test.startTest();
        sgPostingIntegrationController.Execute(channelPost.Id);
        Test.stopTest();
        
        //assert
        //
        //TODO: figure out what to test
    }
    
    static testmethod void execute_future() {
        //arrange
        //
        createServiceSettings('Ebay');
        ChannelPost__c channelPost = createChannelPost('Ebay', false, false, null, Date.today(), Date.today(), false);
        Test.setMock(HttpCalloutMock.class, new sgMockMultipurposeHttpResponse(400, null, null));
        String env = sgUtility.isSandbox() ? 'sandbox' : 'production';
        createFakeSetting('Ebay', env, 'token');
        
        //act
        //
        Test.startTest();
        sgPostingIntegrationController.ExecuteFuture(channelPost.Id);
        Test.stopTest();
        
        //assert
        //
        //TODO: figure out what to test
    }
    
    //**********************************************************
    // Support methods
    // 

    public class MockClassResolver implements System.StubProvider {
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (stubbedMethodName == 'resolveClass') {
                return Type.forName('MockPostingIntegration');
            }
            else if (stubbedMethodName == 'classExists') {
                return true;
            }
            else {
                return new sgPostingIntegrationTest();
            }
        }
    }
    
    public class MockEndpoint implements System.StubProvider {
        
        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            
            return null;
        }

    }
    
    static ChannelPost__c createChannelPost(String name, Boolean post, boolean visible, Date postDate, Date postedDate, Date visibleDate, Boolean priceUpdate) {
        ChannelPost__c cp = new ChannelPost__c(
            Name = name,
            Channel__c = name,
            Post__c = post,
            Post_Date__c = postDate,
            Posted_Date__c = postedDate,
            Visible__c = visible,
            Visible_Date__c = visibleDate,
            Update__c = priceUpdate,
            Is_Processing__c = true
        );
        insert cp;
        return cp;
    }

    static Surge_Endpoint_Settings__c createFakeSetting(String name, String env, String authMode) {
        Surge_Endpoint_Settings__c setting = new Surge_Endpoint_Settings__c();
        setting.Environment__c = env;
        setting.Name = setting.Endpoint_Name__c = name;
        setting.Auth_Mode__c = authMode;
        setting.Auth_Url__c = 'https://test.authendpoint/?clientId={clientId}/';
        setting.Client_ID__c = '12345';
        setting.Client_Secret__c = '67891';
        setting.Password__c = 'password';
        setting.User_Name__c = 'username';
        setting.Auth_Response_Key__c = 'access_token';
        setting.Endpoint_Url__c = 'https://test.endpoint/';
        insert setting;
        return setting;
    }
    
    static void createServiceSettings(String name) {
        Surge_Integration_Settings__c setting = new Surge_Integration_Settings__c(
            Name = name,
            Integration__c = name,
            Organization__c = 'Watchbox',
            Service__c = 'Watchbox',
            Path__c = '/test',
            Parameters__c = 'test=test'
        );
        
        insert setting;
    }
}