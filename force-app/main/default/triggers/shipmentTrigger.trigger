trigger shipmentTrigger on zkfedex__Shipment__c (before insert) {
	system.debug('Shipment Trigger Fired');
    if(trigger.isBefore && trigger.isInsert){
        for(zkfedex__Shipment__c shipment : trigger.new){
          shipment.Fedex_Shipments_One_Click__c	=  shipment.zkfedex__RecipientOpportunity__c;
        }
    }
}