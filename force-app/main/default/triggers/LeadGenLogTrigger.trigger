trigger LeadGenLogTrigger on Lead_Gen_Log__c (after insert, before insert) {
	private static boolean firstRun = true;
    LeadGenLogTriggerHandler handler = new LeadGenLogTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during LeadGenLogTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Lead_Gen_Log__c record : (List<Lead_Gen_Log__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance' + e.getMessage() + ' - ' + e.getStackTraceString());
        }   
    }
}