trigger WatchOriginationTrigger on Watch_Purchase__c (after insert,after update,after delete, before delete) {

    if(WatchOriginationTriggerHandler.isFirstTime){
    	if(trigger.isAfter && trigger.isInsert || trigger.isAfter && trigger.isUpdate){
            WatchOriginationTriggerHandler.isFirstTime = false;
            WatchOriginationTriggerHandler.countRLonOpportunityUpdate(trigger.new);
        }
    }
    if(WatchOriginationTriggerHandler.isFirstTimeDelete){
        if(trigger.isafter && trigger.isDelete){
            WatchOriginationTriggerHandler.isFirstTimeDelete = false;
            WatchOriginationTriggerHandler.countRLonOpportunityUpdate(trigger.old);
        }
    }
}