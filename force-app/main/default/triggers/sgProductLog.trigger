trigger sgProductLog on Product2 (after update) {
	sgLog.product(Trigger.new, Trigger.oldMap);
    sgServicing.statusChange(Trigger.new, Trigger.oldMap);
}