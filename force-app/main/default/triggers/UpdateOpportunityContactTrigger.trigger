trigger UpdateOpportunityContactTrigger on Opportunity (before insert) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            OpporunityContactUpdate obj=new OpporunityContactUpdate();
            obj.updateContacts(Trigger.new);
        }
    }
    
}