trigger UserTrigger on User (before update) {
    private static boolean firstRun = true;
    
    UserTriggerHandler handler = new UserTriggerHandler();
    
    
    if (firstRun) {
        System.debug('firstRun**');
        firstRun = false;
        handler.execute();
    } 
}