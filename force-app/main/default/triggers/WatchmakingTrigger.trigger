trigger WatchmakingTrigger on Watchmaking__c (after insert, after update) {
    private static boolean firstRun = true;
    WatchmakingTriggerHandler handler = new WatchmakingTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
    	}
    } catch (Exception e) {
        System.debug('Error during WatchmakingTrigger - ' + e.getMessage());
        for (Watchmaking__c record : (List<Watchmaking__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}