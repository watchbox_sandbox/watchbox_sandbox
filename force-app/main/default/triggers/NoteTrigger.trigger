trigger NoteTrigger on Note (after insert) {
	private static boolean firstRun = true;
    NoteTriggerHandler handler = new NoteTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
    	}
    } catch (Exception e) {
        System.debug('Error during NoteTrigger - ' + e.getMessage());
        for (Note record : (List<Note>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}