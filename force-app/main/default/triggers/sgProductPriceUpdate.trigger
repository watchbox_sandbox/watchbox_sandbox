trigger sgProductPriceUpdate on Product2 (before update) {
	//loop through the products
	
    List<string> productIds = new List<string>();
    
    for(Product2 product : Trigger.new) {
    	Product2 oldProduct = (Product2)Trigger.oldMap.get(product.Id);
        //see if the price changed
        if (product.Trigger_Suppress__c != 'sgProductPriceUpdate' && (product.ASK__c != oldProduct.ASK__c || product.MSP__c != oldProduct.MSP__c)) {
        	productIds.add(product.Id);
        }
        
    }
    
    List<ChannelPost__c> channels =  [SELECT Id FROM ChannelPost__c WHERE Product__c in :productIds];
    for(ChannelPost__c channel : channels) {
        channel.Update__c = true;
    }
    update channels;

}