trigger AccountTrigger on Account (before insert, before update) {
	private static boolean firstRun = true;
    AccountTriggerHandler handler = new AccountTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during AccountTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Account record : (List<Account>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}