trigger cfGbWuwSalesTrigger on GBWUW_Sales__c (after insert) {
    
    List<GBWUW_Sales__c> uploadBatch = new List<GBWUW_Sales__c>();
    // Constant Values
    final integer DML_LIMIT = 100-1;
    final string PRE_OWNED = 'Pre-Owned';
    final string CLOSEOUT = 'Closeout';
    final string REGULAR = 'Regular Goods';
    final string GB_OWNER = 'Govberg Jewelers';
    // Item Maps
    Map<String, Id> locations = new Map<String, Id>();
    Map<String, Id> gbBrands = new Map<String, Id>();
    Map<String, Id> wuwBrands = new Map<String, Id>();
    Map<String, Id> groupCodes = new Map<String, Id>();
    Map<String, Id> gbUsers = new Map<String, Id>();
    Map<String, Id> wuwUsers = new Map<String, Id>();
    Map<String, Id> gbCustomers = new Map<String, Id>();
    Map<String, Id> wuwCustomers = new Map<String, Id>();
    
    // Retrieve GbWuwSales Inserted
    List<GBWUW_Sales__c> gbWuwSalesUploads = [
        SELECT 
             Id, Name, VendorNum__c, Reference__c, ItemNum__c, Description__c, Loc__c, ASCInventoryID__c, 
        	Trans_Date__c, SlspID__c, Customer_Number__c, Cost__c, Extension_Retail__c, Actual_Sale_Price__c, 
        	DaysToSell__c, Date_Inventoried__c, Product_Type__c, Brand__c, Team_Member__c, Location__c, Model__c, 
        	Transaction_Number__c, DocType__c, GBCustomer__c, SalesUserID__c, NCCustomerID__c, WUWInventoryID__c, 
        	WUWManID__c, WUWItemCust8__c, WUWIsGBVirtual__c, Account__c
        FROM GBWUW_Sales__c
        WHERE Id IN :Trigger.newMap.keySet()];
    
    // Map Keys
    Set<integer> locKeys = new Set<integer>();
    Set<string> gbUserKeys = new Set<string>();
    Set<integer> wuwUserKeys = new Set<integer>();
    Set<string> gbCustKeys = new Set<string>();
    Set<string> wuwCustKeys = new Set<string>();
    
    // Set Used Keys
    for (GBWUW_Sales__c gbws :gbWuwSalesUploads) {
        if (gbws.Loc__c != null) {
            try {
                string lc = gbws.Loc__c.remove(',').trim();
                locKeys.add(integer.valueOf(lc));
            } catch (Exception e) {}
        }
        if (gbws.ItemNum__c != null) {
            if (gbws.SlspID__c != null) {
                gbUserKeys.add(gbws.SlspID__c);
            }
            if (gbws.Customer_Number__c != null) {
                gbCustKeys.add(gbws.Customer_Number__c);
            }
        }
        else {
            if (gbws.SalesUserID__c != null) {
                try {
            		string su = gbws.SalesUserID__c.remove(',').trim();
                	wuwUserKeys.add(integer.valueOf(gbws.SalesUserID__c));
                } catch(Exception e) {}
            }
            if (gbws.NCCustomerID__c != null) {
                wuwCustKeys.add(gbws.NCCustomerID__c);
            }
        }
    }
    
    // Set Maps for Used Keys
    for (Location__c loc :[select Id, LocNum__c from Location__c where LocNum__c in: locKeys]) {
        locations.put(string.valueOf(loc.LocNum__c), loc.Id);
    }
    for (Brand_object__c bnd :[select Id, ASC_Man_ID__c, NC_Man_ID__c from Brand_object__c]) {
        if (bnd.ASC_Man_ID__c != null) gbBrands.put(bnd.ASC_Man_ID__c, bnd.Id);
        if (bnd.NC_Man_ID__c != null) wuwBrands.put(bnd.NC_Man_ID__c, bnd.Id);
    }
    for (ASC_Group_Codes__c gpcd :[select ItemNum__c, Model__c from ASC_Group_Codes__c where Model__c != null]) {
        groupCodes.put(gpcd.ItemNum__c, gpcd.Model__c);
    }
    for (Team_Member__c tm :[select Id, GB_ASC_Sale_ID__c, NCUserID__c from Team_Member__c where GB_ASC_Sale_ID__c in: gbUserKeys or NCUserID__c in: wuwUserKeys]) {
        if (tm.GB_ASC_Sale_ID__c != null) gbUsers.put(tm.GB_ASC_Sale_ID__c, tm.Id);
        if (tm.NCUserID__c != null) wuwUsers.put(string.valueOf(tm.NCUserID__c), tm.Id);
    }
    for (GBCustomer__c cm :[select Id, CustomerNumber__c from GBCustomer__c where CustomerNumber__c in: gbCustKeys]) {
        gbCustomers.put(cm.CustomerNumber__c, cm.Id);
    }
    for (Account ac :[select Id, WUW_NC_Customer_ID__c from Account where WUW_NC_Customer_ID__c in: wuwCustKeys]) {
        wuwCustomers.put(ac.WUW_NC_Customer_ID__c, ac.Id);
    }
    
    // To be set on an SFObject for ease of changeability 
    Set<String> vendorNumCloseoutDelimiters = new Set<String> {
        '102155', '102161', '102499', '102115', '102459', '102405', '102189', '102275', '102244', '102119', 
        '102197', '102156', '102254', '102130', '102120', '102519', '102520', '102293', '102533', '102067', 
        '102021', '102251', '102142', '102521', '102063', '102129', '102123', '102127', '102062', '102464', 
        '102125', '102335', '102108' };
    
    // CF: On GBWUW_Sales to figure out location compare LocationID to object 'Location' and then fill in with the value (this we will transfer to Products2)
    integer index = 0;
    integer uploadSize = gbWuwSalesUploads.size();
    for(GBWUW_Sales__c im: gbWuwSalesUploads){
        index++;
        uploadBatch.add(im);
        
        if (im.Trans_Date__c != null && im.DaysToSell__c != null) {
            string dts = im.DaysToSell__c.remove(',').trim();
            try {
                integer daysToSell = integer.valueOf(dts);
                im.Date_Inventoried__c = im.Trans_Date__c.addDays(-1 * daysToSell);
            } catch (Exception e) {}
        }
        if (im.Loc__c != null) {
            im.Location__c = locations.get(im.Loc__c);
        }
        im.Cost__c = im.Cost__c != null 
            ? im.Cost__c.intValue() 
            : 0;
        im.Extension_Retail__c = im.Extension_Retail__c != null 
            ? im.Extension_Retail__c.intValue()
            : 0;
        im.Actual_Sale_Price__c = im.Actual_Sale_Price__c != null 
            ? im.Actual_Sale_Price__c.intValue()
            : 0;
        
        // Govberg Sale
        if (im.ItemNum__c != null) {
            String itemNo = im.ItemNum__c;
            String manId = itemNo.left(4);
            String inventoryId = im.ASCInventoryID__c;
            
            // Set Model
            im.Model__c = groupCodes.get(itemNo);
            // Set Brand
            im.Brand__c = gbBrands.get(manId);
            // Set Customer
            if (im.Customer_Number__c != null) {
                im.GBCustomer__c = gbCustomers.get(im.Customer_Number__c);
            }
            // Set Sales Person
            if (im.SlspID__c != null) {
                im.Team_Member__c = gbUsers.get(im.SlspID__c);
            }
            // Set Product Type
            // CF: If the FIFTH character in ItemNum = '3' then the watch is a 'Pre-Owned'
            if (itemNo.left(5).endsWith('3')) {
                im.Product_Type__c = PRE_OWNED;
            }
            // CF: If VendorNum equals any of these (vendorNumCloseoutDelimiters) then the watch is a 'Closeout'
            else if (vendorNumCloseoutDelimiters.contains(im.VendorNum__c)) {
                im.Product_Type__c = CLOSEOUT;
            }
            // CF: Any other watches are 'Regular Goods'
            else {
                im.Product_Type__c = REGULAR;
            }
        }
        // Watchuwant Sale
        else {
            // Set Brand
            if (im.WUWManID__c != null) {
                im.Brand__c = wuwBrands.get(im.WUWManID__c);
            }
            // Set Customer
            if (im.NCCustomerID__c != null) {
                im.Account__c = wuwCustomers.get(im.NCCustomerID__c);
            }
            // Set Sales Person
            if (im.SalesUserID__c != null) {
                im.Team_Member__c = wuwUsers.get(im.SalesUserID__c);
            }
            // Set Product Type
            im.Product_Type__c = PRE_OWNED;
            if (im.WUWItemCust8__c != null && im.WUWIsGBVirtual__c != null) {
                if (im.WUWItemCust8__c == GB_OWNER && im.WUWIsGBVirtual__c == false) {
                    im.Product_Type__c = CLOSEOUT;               
                }
            }
        }
        // Batch Update
        if (uploadBatch.size() > DML_LIMIT || index == uploadSize) {
            system.debug('updating gbascuploads batch');
            Database.update(uploadBatch, false);
            uploadBatch.clear();
        }
    }
}