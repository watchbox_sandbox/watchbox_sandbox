trigger sgChannelPostChange on ChannelPost__c (before insert, before update) {
    Map<ID, SObject> old = Trigger.oldMap;
    //loop through all channels
    for(ChannelPost__c channelPost : Trigger.new) {
        ChannelPost__c channelPostOld = (Trigger.isUpdate) ? (ChannelPost__c)old.get(channelPost.Id) : null;

        //Watchbox and Govburg are internal and should not use this integration setup
        if (channelPost.Channel__c != 'Watchbox' && channelPost.Channel__c != 'Govburg') {
            //dont run if this was a change in either the dirty or processing values
            if (sgPostingIntegrationController.IsDirty(channelPostOld, channelPost)) {
                //see if we are already processing
                if (channelPost.Is_Processing__c == false) {
                    channelPost.Is_Processing__c = true; //mark processing so we aren't processing it multiple times at once
                    channelPost.Is_Dirty__c = false; //remove dirty so the batch doesn't pick it up again after this is finished
                    channelPost.Update__c = false; //remove the update flag so the next time we update this process will kick off
                    //start a new job that will execute the controller
                    // so the record has time to be saved
                    if(!Test.isRunningTest()) {
                    	channelPost.Job_Id__c = System.enqueueJob(new sgPostingQueueable(channelPost.Product__c, channelPost.Channel__c));
                    }
                }
                else {
                    channelPost.Is_Dirty__c = true; //mark as dirty so the batch can pick it up later
                }
            }
        }
        else {
            //update the products First_Posted_Date__c if it hasn't been already
            // this only happens for the internal channels
        	if (Trigger.isUpdate && channelPost.Post__c == true) {
                try {
                    Product2 product = [SELECT First_Posted_Date__c FROM Product2 WHERE Id = :channelPost.Product__c];
                    if (product.First_Posted_Date__c == null) {
                        product.First_Posted_Date__c = Date.today();
                        update product;
                    }
                }
                catch(Exception ex) {
                    //TODO: log the exception
                }
            }
            
            //something for the Magento site??
            //
        }
    }
}