trigger cfGbAscUpload on GBASCUPLOAD__c (after insert) {
    
    List<GBASCUPLOAD__c> gbAscUploads = [
        SELECT 
        	Id, Name, ASCInventoryID__c, ASK__c, Cost__c, DateInventoried__c, Description__c, 
        	IsActive__c, ItemNum__c, Location__c, LocationID__c, Product_Type__c, 
        	ReferenceNum__c, VendorName__c, VendorNum__c, Watch_Brand__c 
        FROM GBASCUPLOAD__c
        WHERE Id IN :Trigger.newMap.keySet()];
    
    new cfGbAscUploadTrigger(gbAscUploads);
}