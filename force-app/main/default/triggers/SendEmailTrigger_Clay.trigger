trigger SendEmailTrigger_Clay on Account (after update) {
    
     if(Trigger.isAfter){
        if(Trigger.isUpdate){
            List<AccountOwnerChange__mdt> metaObj=[Select OwnerChangeNotification__c from AccountOwnerChange__mdt];
            if(metaObj[0].OwnerChangeNotification__c==true){
                SendEmailTrigger_ClayHandler obj=new SendEmailTrigger_ClayHandler();
                obj.sendEmailOnAccountOwnerChange(Trigger.New,Trigger.oldMap);
            }
          
        }
    }
    

}