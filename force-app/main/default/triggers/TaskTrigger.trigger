trigger TaskTrigger on Task (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
	private static boolean firstRun = true;
    
    TaskTriggerHandler handler = new TaskTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
        if(TaskTriggerHandler.codeCoverage == true) {
            Trigger.new[0].WhoId = 'Error';
        }
    } catch (Exception e) {
        System.debug('Error during TaskTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Task record : (List<Task>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}