trigger SendEmailTrigger on Account (after update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            List<AccountOwnerChange__mdt> metaObj=[Select OwnerChangeNotification__c from AccountOwnerChange__mdt];
            if(metaObj[0].OwnerChangeNotification__c==true){
                SendEmailTriggerHandler obj=new SendEmailTriggerHandler();
                obj.sendEmailOnAccountOwnerChange(Trigger.New,Trigger.oldMap);
            }
        }
    }    
}