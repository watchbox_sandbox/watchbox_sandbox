trigger UserTriggerToSendEmail on User (after update) {
 private static boolean firstRun = true;
    
    UserTriggerToSendEmailHandler handler = new UserTriggerToSendEmailHandler();
    
    
    if (firstRun) {
        System.debug('firstRun**');
        firstRun = false;
        handler.execute();
    } 
}