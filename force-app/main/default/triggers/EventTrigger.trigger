trigger EventTrigger on Event (before insert) {
	private static boolean firstRun = true;
    EventTriggerHandler handler = new EventTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during EventTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Event record : (List<Event>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}