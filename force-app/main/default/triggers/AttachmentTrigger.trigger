trigger AttachmentTrigger on Attachment (before insert, after insert) {
private static boolean firstRun = true;
    AttachmentTriggerHandler helper = new AttachmentTriggerHandler();
    try{
        if(firstRun){
            firstRun=false;
            helper.execute();
        }        
    } catch(Exception e){
        system.debug('Error during attachmentTrigger - '+e.getMessage());
        for(Attachment a : trigger.New){
            a.addError('Unable to process transaction. Please contact your administrator for assistance');
        }
    }
}