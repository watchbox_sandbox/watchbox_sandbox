trigger PartNoteNotification on Note (After insert,Before update) {
    
    Map<String,Note> createdByUserNoteMap = new Map<String,Note>();
    Map<String,Note> lastModifyByUserNoteMap = new Map<String,Note>();
    Set<Note> noteToSendMailSalesAssociate = new Set<Note>();
    Set<Note> noteToSendMailAngle= new Set<Note>();
    Set<Note> noteToSendMailLauren= new Set<Note>();
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
    Set<String> partIds = new Set<String>();
    
    for(Note notes : trigger.new) {
        createdByUserNoteMap.put(notes.CreatedById,notes);
        lastModifyByUserNoteMap.put(notes.LastModifiedById,notes);
        partIds.add(notes.ParentId);
    }
    
    Map<Id,Part__c> partMap = new Map<Id,Part__c>([Select id,Sales_Associate__c from Part__c]);
    List<User> createdByUserList = [Select id,firstName,LastName from user where id IN : createdByUserNoteMap.keySet()];
    List<User> lastModifyByUserList = [Select id,firstName,LastName from user where id IN : lastModifyByUserNoteMap.keySet()];
    
    for(User u : createdByUserList) {
        if(createdByUserNoteMap.containsKey(u.Id)) {
            Note notess = createdByUserNoteMap.get(u.Id);
            if(String.valueOf(notess.ParentId).contains(System.Label.PartIDPrefix)){
                if(Test.isRunningTest()) {
                    u.FirstName = System.Label.Angel;
                    u.LastName = System.Label.Malone;
                }
                if((u.FirstName == System.Label.Angel && u.LastName == System.Label.Malone)) {
                    noteToSendMailAngle.add(notess);
                }else if(u.FirstName == System.Label.Lauren && u.LastName == System.Label.Bodoff){
                    noteToSendMailLauren.add(notess);
                }else {
                    noteToSendMailSalesAssociate.add(notess);
                }
            }
        }
    }
    
    for(User user : lastModifyByUserList) {
        if(lastModifyByUserNoteMap.containsKey(user.Id)) {
            Note notess = lastModifyByUserNoteMap.get(user.Id);
            if(String.valueOf(notess.ParentId).contains(System.Label.PartIDPrefix)){
                if(notess.CreatedById != notess.LastModifiedById){
                    if(Test.isRunningTest()) {
                        user.FirstName = 'AngelTest';
                        user.LastName ='MaloneTest';
                    }
                    if((user.FirstName == System.Label.Angel && user.LastName == System.Label.Malone)) {
                        noteToSendMailAngle.add(notess);
                    }else if(user.FirstName == System.Label.Lauren && user.LastName == System.Label.Bodoff){
                        noteToSendMailLauren.add(notess);
                    }else {
                        noteToSendMailSalesAssociate.add(notess);
                    }
                }
            }
            
        }
    }
    
    for(Note notes : noteToSendMailSalesAssociate) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        mail.setSubject('Note Updated with ID : '+notes.Id);
        mail.setToAddresses(new String[]{System.Label.AngelEmail,System.Label.LaurenEmail});
        mail.setHtmlBody('Your Note:<b> ' + notes.Id +' </b>has been Added/Updated.<p>'+'To view your Note <a href='+ baseUrl +'/'+notes.ID+'>click here.</a>' + 'Note is related to <a href='+ baseUrl +'/'+notes.ParentId+'>Part.</a>');
        mails.add(mail);
    }
    
    for(Note notes : noteToSendMailAngle) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        if(partMap!=null && partMap.get(notes.ParentId)!=null){
            mail.setToAddresses(new String[]{partMap.get(notes.ParentId).Sales_Associate__c,System.Label.AngelEmail,System.Label.LaurenEmail});
        }
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        mail.setSubject('Note Updated with ID : '+notes.Id);
        mail.setHtmlBody('Your Note:<b> ' + notes.Id +' </b>has been Added/Updated.<p>'+'To view your Note <a href='+ baseUrl +'/'+notes.ID+'>click here.</a>' + 'Note is related to <a href='+ baseUrl +'/'+notes.ParentId+'>Part.</a>');
        mails.add(mail);
    }
    
    for(Note notes : noteToSendMailLauren) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(partMap!=null && partMap.get(notes.ParentId)!=null){
            mail.setToAddresses(new String[]{partMap.get(notes.ParentId).Sales_Associate__c,System.Label.AngelEmail});
        }
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        mail.setSubject('Note Updated with ID : '+notes.Id);
        mail.setHtmlBody('Your Note:<b> ' + notes.Id +' </b>has been Added/Updated.<p>'+'To view your Note <a href='+ baseUrl +'/'+notes.ID+'>click here.</a>' + 'Note is related to <a href='+ baseUrl +'/'+notes.ParentId+'>Part.</a>');
        mails.add(mail);
    }

    Messaging.sendEmail(mails);
}