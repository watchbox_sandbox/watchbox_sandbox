trigger OpportunityTrigger on Opportunity (before update, after update, before delete,after Insert) {
    private static boolean firstRun = true;
 
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
        if (firstRun) {
            System.debug('firstRun**');
            firstRun = false;
            handler.execute();
         }
    
    Set<Id> setOppIds = new Set<Id>();
    List<Opportunity>  lstOpportunity = new List<Opportunity>();
    if(Trigger.isAfter && Trigger.isInsert){
        for(Opportunity opp:Trigger.New){
           setOppIds.add(opp.id);
        }
        UpdateOppEventDateField.updateEventDateField(setOppIds);
    }
    
}