trigger sgAccountCreation on Team_Member__c (after insert) {
    for (Team_Member__c tm : Trigger.new) {
        sgAccountCreationTrigger.finalizeAccount(tm.Id);
    }
}