trigger InspectionTrigger on Inspection__c (before insert) {
	private static boolean firstRun = true;
    InspectionTriggerHandler handler = new InspectionTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during InspectionTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Inspection__c record : (List<Inspection__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}