trigger PicklistTrigger on Picklist__c (before insert, after insert, before update) {    
    private static boolean firstRun = true;
    PicklistTriggerHandler handler = new PicklistTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during PicklistTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Picklist__c record : (List<Picklist__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}