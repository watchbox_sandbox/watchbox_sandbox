trigger WatchSaleTrigger on Watch_Sale__c (after insert,after update, after delete, before insert, before update,before delete) {
    private static boolean firstRun = true;
    WatchSaleTriggerHandler handler = new WatchSaleTriggerHandler();
    insertBidOfferTriggerHandler insertbidofferhandler = new insertBidOfferTriggerHandler();
    if(WatchSaleTriggerHandler.isFirstTime){
        
        if(trigger.isAfter && trigger.isInsert || trigger.isAfter && trigger.isUpdate){
            WatchSaleTriggerHandler.isFirstTime = false;
            WatchSaleTriggerHandler.countRLonOpportunityUpdate(trigger.new);
           
        }        
        if(trigger.isafter && trigger.isDelete){
            WatchSaleTriggerHandler.isFirstTime = false;
            WatchSaleTriggerHandler.countRLonOpportunityUpdate(trigger.old);
        }
    }
   
    
        
    
    try {
        
       if (insertBidOfferTriggerHandler.isFirstTime && ((trigger.isAfter && trigger.isInsert) || (trigger.isbefore && trigger.isUpdate))) {
            system.debug('inside if of trigger insertbidofferhandler');
            insertbidofferhandler.execute();
            insertBidOfferTriggerHandler.isFirstTime = false;
        }
        
        if (firstRun) {
            firstRun = false;
            handler.execute();
            system.debug('inside if of trigger');
        }
       
    } catch (Exception e) {
        System.debug('Error during WatchSaleTrigger - ' + e.getMessage());
        for (Watch_Sale__c record : (List<Watch_Sale__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance');
        }   
    }
}