trigger IntakeTrigger on Intake__c (after insert, before update) {
	private static boolean firstRun = true;
    IntakeTriggerHandler handler = new IntakeTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during IntakeTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Intake__c record : (List<Intake__c>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance'  + e.getMessage() + ' - ' + e.getStackTraceString());
        }   
    }
}