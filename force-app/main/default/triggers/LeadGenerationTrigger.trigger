trigger LeadGenerationTrigger on Lead (after insert, before insert) {
    private static boolean firstRun = true;
    LeadTriggerHandler handler = new LeadTriggerHandler();
    
    try {
        if (firstRun) {
            firstRun = false;
            handler.execute();
        }
    } catch (Exception e) {
        System.debug('Error during LeadGenLogTrigger - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        for (Lead record : (List<Lead>)Trigger.new) {
            record.addError('Unable to process transaction, please try again or contact your administrator if you need assistance' + e.getMessage() + ' - ' + e.getStackTraceString());
        }   
    }
    
}